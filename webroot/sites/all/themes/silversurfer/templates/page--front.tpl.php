	<div id="header">

  		<?php print render($page['header_first']); ?>
  		<?php print render($page['header_second']); ?>
  		<?php print render($page['header_third']); ?>
		<div id="header-banner">
  			<?php print render($page['header_banner']); ?>
			<div id="header-banner-page-title">
				<?php print render($page['header_page_title']); ?>
			</div>
		</div>
  		  		  
	</div>

	
	<div id="content" class="content front-content" style="opacity: 0;">

	    <div class="separator"></div>
	  
	  	<div id="content-top" class="content-row front-content front-content-top">
	  		
	  		<div id="content-top-first" class="content-panel content-panel-first">
				<?php print render($page['content_top_first']); ?>
			</div>  	
	
			<div id="content-top-second" class="content-panel content-panel-second">
				<?php print render($page['content_top_second']); ?>
			</div>  	
	
		</div>
	    
	  	<div id="content-bottom" class="content-row front-content front-content-bottom">
	  	
	  		<div id="content-bottom-first" class="content-panel content-panel-first">
				<?php print render($page['content_bottom_first']); ?>
			</div>  	
	  	
	  		<div id="content-bottom-second" class="content-panel content-panel-second">
				<?php print render($page['content_bottom_second']); ?>
			</div>  	
	
		</div>
		
	    <div class="separator"></div>
				
	</div>

  
	<div id="footer">
  	
  		<?php print render($page['footer']); ?>
		
	</div>
  
  
	
		
  