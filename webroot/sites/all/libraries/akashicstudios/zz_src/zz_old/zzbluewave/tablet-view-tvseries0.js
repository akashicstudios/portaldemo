

var tabletViewTvSeries0Controller = new ItvView('tabletViewTvSeries0');

tabletViewTvSeries0Controller.viewItemDefaults.tagName = 'LI';
tabletViewTvSeries0Controller.viewItemDefaults.navIdPrefix = tabletViewTvSeries0Controller.cssClass + 'Item';
tabletViewTvSeries0Controller.viewItemDefaults.inFocusCss = tabletViewTvSeries0Controller.cssClass + '-list-item-focus';

tabletViewTvSeries0Controller.init();
//tabletViewTvSeries0Controller.hide();

window.gNav.views['tabletViewTvSeries0'] = tabletViewTvSeries0Controller;
window.gNav.viewsZeroBased.push(tabletViewTvSeries0Controller);

for (var i=0; i< tabletViewTvSeries0Controller.viewItemsZeroBased.length; i++) {
	
	var nid = tabletViewTvSeries0Controller.viewItemsZeroBased[i].nav.viewItemNid;
	tabletViewTvSeries0Controller.viewItemsZeroBased[i].domNode.dataset.viewitemnid = nid;
	tabletViewTvSeries0Controller.viewItemsZeroBased[i].domNode.dataset.cssclass = tabletViewTvSeries0Controller.viewItemsZeroBased[i].cssClass;
	tabletViewTvSeries0Controller.viewItemsZeroBased[i].domNode.addEventListener('click', function() {
		
		window.gNav.views['tabletViewTvSeries0'].blurViewItems();
		
		if(	 ! this.classList.contains(window.gNav.views['tabletViewTvSeries0'].viewItemDefaults.inFocusCss)  	) {		 
			this.classList.add(window.gNav.views['tabletViewTvSeries0'].viewItemDefaults.inFocusCss);		
		}
		
		
		
	},false);
	
}

jQuery(".tabletViewTvSeries0FancyBox").fancybox({
	maxWidth	: 900,
	maxHeight	: 700,
	width		: 900,
	height		: 700,
	fitToView	: false,
	autoSize	: true,
	autoCenter	: true,
	closeClick	: false,
	openEffect	: 'none',
	closeEffect	: 'none',
	iframe		: {scrolling : 'no'},
	padding		: 10,
	margin		: 0
});
