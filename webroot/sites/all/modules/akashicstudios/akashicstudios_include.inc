<?php 

define('RSS_DOC', '<rss version="2.0" xmlns:jwplayer="http://rss.jwpcdn.com/"><channel></channel></rss>');

/*
 * <item>
 * <title></title>
 * <description></description>
 * <jwplayer:image></jwplayer:image>
 * <jwplayer:source file="" />
 * </item>
 * 
 * */

include 'akashicstudios_xml.php';

/*
 * IMPORTANT !!!! 
 * 
 * 1) redirects the favi stick to the itv site
 * 2) future: will redirect video signage displays based on IP address
 * 
 * */
// function akashicstudios_boot() {

// 	if($_SERVER['HTTP_USER_AGENT'] == 'ss-4gb') {
		
// 		$uri = $_SERVER['REQUEST_URI'];
		
// 		if($uri != '/?q=itv') {
// 			drupal_goto('itv');			
// 		}
// 	}
	
// }

function akashicstudios_language_select($lang,$isItv) {
	
	$_SESSION['lang'] = $lang;
    
	// echo 'session: '.$_SESSION['lang'];
    // echo 'isItv: '.$isItv;
    
    // $servers = akashicstudios_default_servers();
    // $server = 'http://'.$servers['PORTAL'];
    
    //header('Location: /'.$isItv);   //$server.'
    // exit;
    
    if($isItv=='itv') {
	   drupal_goto('itv');
    }
    else {
       drupal_goto('/');
    }
	
}




// function akashicstudios_get_mtngps() {
    
//     $sql = "SELECT ship.title AS shipname, mtngps.title, FROM_UNIXTIME(mtngps.created,'%Y-%m-%d %H:%i:%s') as postdate, "; 
//     $sql .= "latitude.field_lat_value as latitude, longitude.field_long_value AS longitude ,"; 
//     $sql .= "shipcode.field_ct_ship_code_value as shipcode, mapicon.field_icon_image_fid as fid ";
//     $sql .= "FROM node as mtngps ";
//     $sql .= "INNER JOIN field_data_field_lat as latitude on latitude.entity_id = mtngps.nid ";
//     $sql .= "INNER JOIN field_data_field_long as longitude on longitude.entity_id = mtngps.nid ";
//     $sql .= "INNER JOIN field_data_field_ship as mtngps_ship on mtngps_ship.entity_id = mtngps.nid ";
//     $sql .= "INNER JOIN node as ship on mtngps_ship.field_ship_nid = ship.nid ";
//     $sql .= "INNER JOIN field_data_field_ct_ship_code as shipcode on shipcode.entity_id = ship.nid ";
//     $sql .= "INNER JOIN field_data_field_icon_image as mapicon on mapicon.entity_id = ship.nid ";
//     $sql .= "WHERE (( (mtngps.type IN  ('mtn_gps')) )) ";
//     $sql .= "ORDER BY ship.status DESC, mtngps.title ASC";

    
//     $result = db_query($sql);
    
//     $retval = array();
//     if($result) {
//         while($record = $result->fetchAssoc() ) {
//              $row = $record;

//              $file = file_load($row['fid']);             
//              if($file){
//                 $row['mapicon'] = file_create_url($file->uri);    
//              }                                   
                
//             $retval[] = $row;
//         }
//     }
    
//     return $retval;
    
// }
    

function akashicstudios_sql_json($proc) {
	
	return drupal_json_output(akashicstudios_sql_rows($proc));
	
}

function akashicstudios_sql_rows($proc) {

	//echo 'proc='.$proc;

	$conn = Database::getConnection();
	$saved_class = $conn->getAttribute(PDO::ATTR_STATEMENT_CLASS);
	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('PDOStatement'));

	// Prepare the statement and bind params
	$callStr = "Call ".$proc;
	
	$statement = $conn->prepare("Call ".$proc);

	// Execute the statement and reset the connection's statement class to the original.
	$exec_result = $statement->execute();
	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, $saved_class);

	// Get your data
	$rows = $statement->fetchAll();
	$statement->closeCursor();
	
	return $rows;
}


function akashicstudios_json_vod_satellitetv(){

	$conn = Database::getConnection();
	$saved_class = $conn->getAttribute(PDO::ATTR_STATEMENT_CLASS);
	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('PDOStatement'));

	// Prepare the statement and bind params
	$statement = $conn->prepare("Call vod_satellitetv()");

	// Execute the statement and reset the connection's statement class to the original.
	$exec_result = $statement->execute();
	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, $saved_class);

	// Get your data
	$rows = $statement->fetchAll();
	$statement->closeCursor();

	return drupal_json_output($rows);

}


function akashicstudios_json_vod_tvseries() {

	$retval = null;
	
	// Get the Drupal database connection and change the statement class to PDOStatement.
	// Save the current class for cleanup later.
	$conn = Database::getConnection();
	$saved_class = $conn->getAttribute(PDO::ATTR_STATEMENT_CLASS);
	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('PDOStatement'));

	// Prepare the statement and bind params
	$statement = $conn->prepare("Call vod_tvseries()");

	// Execute the statement and reset the connection's statement class to the original.
	$exec_result = $statement->execute();
	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, $saved_class);

	// Get your data
	$rows = $statement->fetchAll();
	$statement->closeCursor();

	
	if($rows) {
		
		$retval = array();
	
		foreach($rows as $row) {
			
			$retval[] = array(
					'id'=>$row['id'],
					'title'=>$row['title'],
					'poster_image_src'=>$row['poster_image_src'],
					'actors'=>$row['actors'],
					'story'=>$row['story'],
					'episodes'=> akashicstudios_sql_rows('vod_tvseries_episodes('.$row['id'].')')
			);
		}
	
	}
	
	return drupal_json_output($retval);
	
}

function akashicstudios_json_vodfilms() {

	$retval = null;

	// Get the Drupal database connection and change the statement class to PDOStatement.
	// Save the current class for cleanup later.
	$conn = Database::getConnection();
	$saved_class = $conn->getAttribute(PDO::ATTR_STATEMENT_CLASS);
	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('PDOStatement'));

	// Prepare the statement and bind params
	$statement = $conn->prepare("Call vod_films()");

	// Execute the statement and reset the connection's statement class to the original.
	$exec_result = $statement->execute();
	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, $saved_class);

	// Get your data
	$rows = $statement->fetchAll();
	$statement->closeCursor();

	
	if($rows) {
		
		$retval = array();
	
		foreach($rows as $row) {
			
			
			//$child = akashicstudios_sql_ship_venues($row['tid']);
			$retval[] = array(
					'id'=>$row['id'],
					'title'=>$row['title'],
					'runtime'=>$row['runtime'],
					'year'=>$row['year'],
					'categories'=>$row['categories'],
					'rating_image_src'=>$row['rating_image_src'],
					'rating_code'=>$row['rating_code'],
					'actors'=>$row['actors'],
					'directors'=>$row['directors'],
					'meta_data'=> akashicstudios_sql_rows('vod_film_meta_data('.$row['id'].')'),
					'media_items'=> akashicstudios_sql_rows('vod_film_media_items('.$row['id'].')')
					
			);
		}
	
	}
	
	return drupal_json_output($retval);
}

function akashicstudios_json_mtngps() {
            
//    return drupal_json_output(akashicstudios_get_mtngps());
    
    // Get the Drupal database connection and change the statement class to PDOStatement.
    // Save the current class for cleanup later.
    $conn = Database::getConnection();
    $saved_class = $conn->getAttribute(PDO::ATTR_STATEMENT_CLASS);
    $conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('PDOStatement'));
    
    // Prepare the statement and bind params
    $statement = $conn->prepare("Call mtngps()");
    
    // Execute the statement and reset the connection's statement class to the original.
    $exec_result = $statement->execute();
    $conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, $saved_class);
    
    // Get your data
    $rows = $statement->fetchAll();
    $statement->closeCursor();
    
    return drupal_json_output($rows);
}


function akashicstudios_json_datetime() {
	
	$date = new DateTime();
// 	$date->setTimeZone(new DateTimeZone('Pacific/Galapagos'));	//date_default_timezone_get
	$date->setTimeZone(new DateTimeZone(ini_get('date.timezone')));	//
	
	$retval = array(
			'd' => $date->format('d'),
			'D' => $date->format('D'),
			'j' => $date->format('j'),
			'l' => $date->format('l'),
			'w' => $date->format('w'),

			'F' => $date->format('F'),
			'm' => $date->format('m'),
			'M' => $date->format('M'),
			'n' => $date->format('n'),

			'Y' => $date->format('Y'),
			'y' => $date->format('y'),

			'g' => $date->format('g'),
			'G' => $date->format('G'),
			'h' => $date->format('h'),
			'H' => $date->format('H'),

			'i' => $date->format('i'),

			's' => $date->format('s'),

			'a' => $date->format('a'),
			'A' => $date->format('A'),
				
			'e' => $date->format('e'),
			'T' => $date->format('T'),
			'Z' => $date->format('Z')
	);
	
	return drupal_json_output($retval);
	
}

// function akashicstudios_json_ship_venues() {

// 	$out = array('ship_venues'=>akashicstudios_sql_ship_venues());

// 	echo json_encode($out);

// }

// function akashicstudios_sql_ship_venues(){

// 	$retval = null;
	
// 	// Get the Drupal database connection and change the statement class to PDOStatement.
// 	// Save the current class for cleanup later.
// 	$conn = Database::getConnection();
// 	$saved_class = $conn->getAttribute(PDO::ATTR_STATEMENT_CLASS);
// 	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('PDOStatement'));

// 	// Prepare the statement and bind params
// 	$statement = $conn->prepare("Call ship_venues()");

// 	//$op_status = $statement->bindParam(1, $parentid, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT);

// 	// Execute the statement and reset the connection's statement class to the original.
// 	$exec_result = $statement->execute();
// 	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, $saved_class);

// 	// Get your data
// 	$rows = $statement->fetchAll();
// 	$statement->closeCursor();

// 	if($rows) {
// 		$retval = array();

// 		foreach($rows as $row) {
// 			//$child = akashicstudios_sql_ship_venues($row['tid']);
// 			$retval[] = array(
// 					'name'=>$row['name'],
// 					'image'=>$row['image'],
// 					'image_hover'=>$row['image_hover'],
// 					'description'=>$row['description'],
// 					'long_description'=>$row['long_description'],
// 					'tid'=>$row['tid'],
// 					'parent'=>$row['parent'],
// 					'telephone'=>$row['telephone'],
// 					'from_time'=>$row['from_time'],
// 					'to_time'=>$row['to_time'],
// 					'location_type'=>$row['location_type'],
// 					'weight'=>$row['weight']);
// 		}

// 	}
	
// 	return $retval;

// }


function akashicstudios_json_system_defaults() {

	$out = array('system_defaults'=>akashicstudios_sql_system_defaults('0'));

	echo json_encode($out);

}


function akashicstudios_sql_system_defaults($parentid){

	
	// Get the Drupal database connection and change the statement class to PDOStatement.
	// Save the current class for cleanup later.
	$conn = Database::getConnection();
	$saved_class = $conn->getAttribute(PDO::ATTR_STATEMENT_CLASS);
	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('PDOStatement'));
	
	// Prepare the statement and bind params
	$statement = $conn->prepare("Call system_defaults(?)");
	
	$op_status = $statement->bindParam(1, $parentid, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT);
	
	// Execute the statement and reset the connection's statement class to the original.
	$exec_result = $statement->execute();
	$conn->setAttribute(PDO::ATTR_STATEMENT_CLASS, $saved_class);	
	
	// Get your data
	$rows = $statement->fetchAll();
	$statement->closeCursor();
	
	$retval = array();
	
	foreach($rows as $row) {
		if($row['value'] == null) {
			$retval[$row['name']] = akashicstudios_sql_system_defaults($row['tid']);
		}
		else {
			$retval[$row['name']] = array('value'=>$row['value'],'image'=>$row['image'], 'long_text'=>$row['long_text']);
		}
	}
	
	return $retval;
	
}


function akashicstudios_json_newslink(){
	
	$retval = null;
	
	$folderPath = '/sites/default/files/feeds/newslink';
	
	$saveRoot = $_SERVER['DOCUMENT_ROOT'] . $folderPath;
	$pattern = $saveRoot.'/*.pdf';
	
	$files = glob($pattern);
	
	// if the files are more than one then create the rssdoc, loop through the files & create the items
	if($files) {
		
		$newslink = array();
		
		foreach ($files as $file) {
			
			$pathInfos = pathinfo($file);
// 			$strTitle = $pathInfos['filename'];
			
// 			$strTitle = str_replace('_', ' ', $strTitle);
// 			$strTitle = str_replace('-', '', $strTitle);
// 			$strTitle = str_replace('(8)', '', $strTitle);
// 			$strTitle = str_replace('(4)', '', $strTitle);
			 
			$url = $folderPath.'/'.$pathInfos['basename'];

			$newslink[] = array('filename'=>$pathInfos['filename'], 'url'=>$url);
		}

		$retval = json_encode($newslink);
		
	}
	
	echo $retval;
	
	
}

function akashicstudios_default_servers() {
	
	$portalServer = $_SERVER['SERVER_NAME'];
    $portalServerPort = $_SERVER['SERVER_PORT'];
	
    // echo $portalServer . ':'.$portalServerPort;
    
    if($portalServerPort) {
        $portalServer.=':'.$portalServerPort;
    }
    
	//**************
	//DEBUG ONLY
   // if(strstr($portalServer,'labportal')) {
   	// $portalServer='labportal.silversea.com:9002';
   // }
	//***************
    
    $system_defaults = akashicstudios_sql_system_defaults(0);
    
//     echo print_r($system_defaults);
    
//	$result = db_query($query);
	
    $vod = $system_defaults['modules']['vod']['vod_dns']['value'];
    $music = $system_defaults['modules']['music']['music_dns']['value']. ':'.$system_defaults['modules']['music']['music_port']['value'];
    $npd = $system_defaults['modules']['pressreader']['pressreader_server_dns']['value'];
    
	$retval = array(
			'PORTAL' => $portalServer,
			'VOD' => $vod,
			'MUSIC' => $music,
			'NPD' => $npd
	);
			
// 	if($result) {
// 		while($record = $result->fetchAllKeyed(0,1)){
// 			$retval = $record;
// 		}
// 	}
	
	return $retval;

}

function akashicstudios_music_feed() {

	echo 'here';
	$libraryRoot = '/library';
	$sectionRoot = '/sections/';
	$sectionNumber = 1;

	$folderRoot = '/folder';
	$saveRoot = '/sites/default/files/feeds/music';
	
	$servers = @akashicstudios_default_servers();
	
	
echo print_r($servers);

    $foldersXml = false;
    
    if($servers) {

        if(strlen($servers['MUSIC'])>0) {
            $sxml = new AkashicXml();
        
            $server = 'http://'.$servers['MUSIC'];
            $sectionList = $sxml->loadXmlFile($server.$libraryRoot.$sectionRoot);
            $domXPath = new DOMXPath($sectionList);
            $sectionList_directories = $domXPath->query('//MediaContainer/Directory');
            
            foreach ($sectionList_directories as $directory) {
                $sectionNumber = $directory->attributes->getNamedItem('key')->nodeValue;
            }
            
            //load the list of folders from the plex server
            $foldersXml = $sxml->loadXmlFile($server.$libraryRoot.$sectionRoot.$sectionNumber.$folderRoot);
        }
        
    }
    else {
        echo print_r($servers);
    }

	
	$maxItemCount = 300;
	
	
	if($foldersXml) {
		
		$domXPath = new DOMXPath($foldersXml);
		$mediaContainer_directories = $domXPath->query('//MediaContainer/Directory');
		
		if($mediaContainer_directories) {
			
			foreach ($mediaContainer_directories as $directory) {
				$folderUrl = '';
				$folderTitle = '';			
					
				$folderUrl = $directory->attributes->getNamedItem('key')->nodeValue;
				$folderTitle = $directory->attributes->getNamedItem('title')->nodeValue;

				$rssDoc = new DOMDocument();
				$rssDoc->loadXML('<rss version="2.0" xmlns:jwplayer="http://rss.jwpcdn.com/"><channel></channel></rss>');
				
				$rssChannel = $rssDoc->firstChild->firstChild;
					
				$folderItemList = '';
				if($folderTitle != 'Uncategorized') {
					$folderItemList = $sxml->loadXmlFile($server.$folderUrl);
				}

				if($folderItemList!='') {
					
					$domXPath = new DOMXPath($folderItemList);
					$mediaContainer_tracks = $domXPath->query('//MediaContainer/Track');
					
					if($mediaContainer_tracks->length > 0) {
					    
                        if($mediaContainer_tracks->length < $maxItemCount) {
                            $maxItemCount = $mediaContainer_tracks->length;
                        }
						
						$maxItemCounter = 0;
						
						while ($maxItemCounter < $maxItemCount) {
							
                            if($mediaContainer_tracks->length > $maxItemCount) {
    							$index = rand(0,$mediaContainer_tracks->length-1);
                            }
                            else {
                                $index = $maxItemCounter;
                            }
							
							$track = $mediaContainer_tracks->item($index);
								
							$loadString = $server.$track->attributes->getNamedItem('key')->nodeValue;
																
							$metadataXml = $sxml->loadXmlFile($loadString);
									
							if($metadataXml) {
								
								$maxItemCounter++;
								
								$itemNode = $rssDoc->createElement('item');
								$titleNode = $rssDoc->createElement('title');
								$descNode = $rssDoc->createElement('description');
								$imageNode = $rssDoc->createElement('jwplayer:image');
								$sourceNode = $rssDoc->createElement('jwplayer:source');
								$fileAttr = $rssDoc->createAttribute('file');
								
								$itemNumAttr = $rssDoc->createAttribute('item_no');
								$itemNumAttr->nodeValue = $maxItemCounter;
								$itemNode->appendChild($itemNumAttr);
								
								$trackNode = $metadataXml->firstChild->childNodes->item(1);
								$trackAttributes = $trackNode->attributes;
								$mediaNode = $trackNode->childNodes->item(1);
								$partNode = $mediaNode->childNodes->item(1);
								
								$val = $trackAttributes->getNamedItem('title')->nodeValue;
								$val != 'undefined' ? $titleNode->nodeValue = $val : $titleNode->nodeValue = ''; 
								
								$val = $trackAttributes->getNamedItem('grandparentTitle')->nodeValue;
								$val != 'undefined' ? $descNode->nodeValue = $val : $descNode->nodeValue = '';
								
								$descNode->nodeValue .= '|';

								$val = $trackAttributes->getNamedItem('parentTitle')->nodeValue;
								$val != 'undefined' ? $descNode->nodeValue .= $val : $descNode->nodeValue .= '';
								
								$val = $partNode->attributes->getNamedItem('key')->nodeValue;
								$val != 'undefined' ? $fileAttr->nodeValue = $server. $val : $fileAttr->nodeValue = '';
								
								$sourceNode->appendChild($fileAttr);

								$itemNode->appendChild($titleNode);
								$itemNode->appendChild($descNode);
								$itemNode->appendChild($imageNode);
								$itemNode->appendChild($sourceNode);
								
								$rssChannel->appendChild($itemNode);
									
							}
									
						}
													
					}

				}
				

				$savePath = $saveRoot.'/'.strtolower(str_replace(' ', '-', $folderTitle)). '.xml';
				$rssDoc->save($_SERVER['DOCUMENT_ROOT'].$savePath);
						
				/*echo '<p><a href="'.$savePath.'" target="_blank">'.$folderTitle.'</a></p><br>';*/
					
				unset($folderItemList);
				unset($rssDoc);
				unset($rssChannel);
														
				
			}
		}
		
		
	}
	
	
	
	
	//for each folder, load its item-list
	
	// if the list has items
	// load the rss doc template and access the channels node
	
	
	//for each item in the list
	//get its metadata key value
	// load up the meta data file
	// setup the new rss item node
	// append the new rss item to the channels nodelist
	
	// save the rss file with the category name & move to the next folder-list
	
	
	/*
	
	
	$retval = array(
			array('artist' => 'SSS Fleetwood Mac',
				'song' => 'Go Your Own Way',
				'album' => 'Greatest Hits [Warner Bros]',
				'recordlabel' => 'Island Records',
				'image2' => 'http://labmusic.silversea.com:32400/library/metadata/428/art/1400007007',
// 				'sources' => array(
// 					array(
							'file'=> 'http://labmusic.silversea.com:32400/library/parts/2456/file.mp3'
// 				)
// 				)
			),
			array(
				'artist' => 'SSS Led Zeppelin',
				'song' => 'Houses of the Holy',
				'album' => 'Latter Days=> The Best of Led Zeppelin, Vol. 2',
				'recordlabel' => 'Island Records',
				'image2' => 'http://labmusic.silversea.com:32400/library/metadata/205/art/1400007427',
// 				'sources' => array(
// 					array(
							'file'=> 'http://labmusic.silversea.com:32400/library/parts/2473/file.mp3'
// 				)
// 				)
			),
			array(
				'artist' => 'SSS ABBA',
				'song' => 'I Do, I Do, I Do, I Do, I Do',
				'album' => '20th Century Masters - The Millennium Collection=> The Best of ABBA',
				'recordlabel' => 'Island Records',
				'image2' => 'http://labmusic.silversea.com:32400/library/metadata/612/art/1400006579',
// 				'sources' => array(
// 					array(
							'file'=> 'http://labmusic.silversea.com:32400/library/parts/2479/file.mp3'
// 				)
// 				)
			)
					
	
	);


	return drupal_json_output($retval);
	
	
	
	
	echo '<rss version="2.0" xmlns:jwplayer="http://rss.jwpcdn.com/"><channel>' 
		. '<title>'. $category .'</title>'
		. '<item>' 
		.	'<title>Go Your Own Way</title>' 
		. 	'<description>Fleetwood Mac|Greatest Hits [Warner Bros]</description>' 
// 		. 	'<jwplayer:image>http://labmusic.silversea.com:32400/library/metadata/428/art/1400007007</jwplayer:image>' 
		. 	'<jwplayer:source file="http://labmusic.silversea.com:32400/library/parts/2456/file.mp3" />' 
		. '</item>'
		. '<item>' 
		.	'<title>Houses of the Holy</title>' 
		. 	'<description>Led Zeppelin|Latter Days: The Best of Led Zeppelin, Vol. 2</description>' 
// 		. 	'<jwplayer:image>http://labmusic.silversea.com:32400/library/metadata/205/art/1400007427</jwplayer:image>' 
		. 	'<jwplayer:source file="http://labmusic.silversea.com:32400/library/parts/2473/file.mp3" />' 
		. '</item>'
		. '<item>' 
		.	'<title>I Do, I Do, I Do, I Do, I Do</title>' 
		. 	'<description>ABBA|20th Century Masters - The Millennium Collection: The Best of ABBA</description>' 
// 		. 	'<jwplayer:image>http://labmusic.silversea.com:32400/library/metadata/612/art/1400006579</jwplayer:image>' 
		. 	'<jwplayer:source file="http://labmusic.silversea.com:32400/library/parts/2479/file.mp3" />' 
		. '</item>'		
		. '</channel></rss>';
		
		*/
		
}

function akashicstudios_newslink_feed() {
    
    $folderPath = '/sites/default/files/feeds/newslink';
//     $fileName = 'kvhnewslink.xml';
  
    $saveRoot = $_SERVER['DOCUMENT_ROOT'] . $folderPath;
    $pattern = $saveRoot.'/*.pdf';
    
    $servers = akashicstudios_default_servers();
//     $sxml = new AkashicXml();
    $server = 'http://'.$servers['PORTAL'];
    
    // execute the wget script to download new files into content folder
    exec($_SERVER['DOCUMENT_ROOT'] .'/sites/all/modules/akashicstudios/kvhftp.sh '.$_SERVER['DOCUMENT_ROOT']);
    
    // read contents of content folder and generate the feed xml
    // glob the results into an array
//     $files = glob($pattern);
        
//     // if the files are more than one then create the rssdoc, loop through the files & create the items
//     if($files) {
        
//         $rssDoc = new DOMDocument();
//         $rssDoc->loadXML('<items></items>');
        
//         foreach ($files as $file) {
//             // item title
//             // item post-date
//             // item file url
            
//             $itemNode = $rssDoc->createElement('item');
//             $titleNode = $rssDoc->createElement('title');
//             $dateNode = $rssDoc->createElement('postdate');
//             $urlNode = $rssDoc->createElement('url');
            
//             $pathInfos = pathinfo($file);
//             $strTitle = $pathInfos['filename'];
//             $strTitle = str_replace('_', ' ', $strTitle);
//             $strTitle = str_replace('-', '', $strTitle);
//             $strTitle = str_replace('(8)', '', $strTitle);
//             $strTitle = str_replace('(4)', '', $strTitle);
       
//             $postDate = date("Y-m-d H:i:s", filemtime($file));

//             $titleNode->nodeValue = $strTitle;
//             $dateNode->nodeValue = $postDate;
//             $urlNode->nodeValue = $folderPath.'/'.$pathInfos['basename'];
            
//             $itemNode->appendChild($titleNode);
//             $itemNode->appendChild($dateNode);
//             $itemNode->appendChild($urlNode);
            
//             $rssDoc->firstChild->appendChild($itemNode);
            
//         }

//         // save the doc
//         $rssDoc->save($saveRoot.'/'.$fileName);
    
//     }
    
}

function akashicstudios_mtngps_feed() {
    
    // execute the wget script to download new files into content folder
   $cmd = $_SERVER['DOCUMENT_ROOT'] .'/sites/all/modules/akashicstudios/mtnftp.sh '. $_SERVER['DOCUMENT_ROOT'];
   exec($cmd);

}

function purgeDBImageData() {

	$result = db_query('SELECT fm.fid, fm.uri FROM {file_managed} fm LEFT JOIN {file_usage} fu ON fm.fid = fu.fid WHERE fu.fid IS NULL ORDER BY fm.fid');

	$num_files_deleted =0;$num_records_deleted =0;

	$time_before_start = time();

	foreach($result as $record){
		$file = file_load($record -> fid);
		if($file != FALSE){
			if(file_delete($file)){
				print(drupal_realpath($record -> uri)." has been successfully deleted.\n");
				$num_files_deleted++;
			}
		}
		db_delete('file_managed')-> condition('fid', $record -> fid)-> execute();
		$num_records_deleted++;
	}
	$time_after_finish = time();
	$total_time =($time_after_finish - $time_before_start);
	$total_time_h = $total_time /3600%24;
	$total_time_m = $total_time /60%60;
	$total_time_s = $total_time %60;
	if($num_records_deleted || $num_files_deleted >=1){
		print("\n\n");
		print($num_files_deleted ." files purged and ". $num_records_deleted ." in ". $total_time_h .":". $total_time_m .":". $total_time_s .".\n");
	}
	else {
// 		print("Nothing to purge.\n");
	}
}


function deleteUnusedFiles() {
	//db_query to find all files not attached to a node:

	$strQuery = "SELECT fm.* FROM file_managed AS fm LEFT OUTER JOIN file_usage AS fu ON ( fm.fid = fu.fid ) LEFT OUTER JOIN node AS n ON ( fu.id = n.nid ) WHERE (fu.type = 'node' OR fu.type IS NULL) AND n.nid IS NULL";
	$result = db_query($strQuery);
    
    //$result = db_query("SELECT fid FROM file_managed WHERE NOT EXISTS (SELECT * FROM file_usage WHERE file_managed.fid = file_usage.fid) ");
    //echo "<p>deleteUnusedFiles() result: ".$result->rowCount()."</p>";
	
	//Delete file & database entry

	for ($i = 1; $i <= $result->rowCount(); $i++) {
		$record = $result->fetchObject();

		$file = file_load($record->fid);

		if ($file != NULL) {

        echo "<p>deleted: $file</p>";
			file_delete($file);

		}
	}
}
