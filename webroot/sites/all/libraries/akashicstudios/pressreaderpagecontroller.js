function PressreaderPageController(){

	this._domContentTop = document.getElementById('content-top');
	this._domContentBottom = document.getElementById('content-bottom');
	this._domDescription = this._domContentTop.getElementsByClassName('description').item(0);

	this._links = {
		'android' : this._domContentBottom.getElementsByClassName('link-android').item(0),
		'apple' : this._domContentBottom.getElementsByClassName('link-apple').item(0),
		'activate' : this._domContentBottom.getElementsByClassName('link-activate').item(0)
	};
	
	
	function init(){
		this._domDescription.innerHTML = window.AkashicStudios._system_defaults._defaults.modules.pressreader.pressreader_page_description.long_text;		
		this._links.android.href = window.AkashicStudios._system_defaults._defaults.modules.pressreader.urls.pressreader_download_url_android.value;		
		this._links.apple.href = window.AkashicStudios._system_defaults._defaults.modules.pressreader.urls.pressreader_download_url_apple.value;		
		this._links.activate.href = window.AkashicStudios._system_defaults._defaults.modules.pressreader.urls.pressreader_activation_service_url.value;		
	}
	this.init = init;
	
	
}


	


