﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PlayHLSVideo.aspx.cs" Inherits="WiFiVideo.PlayHLSVideo" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
    html, body {
        height: 100%;
        overflow: auto;
    }
    body {
        padding: 0;
        margin: 0;
    }
    
    #player {
        height: 100%;
        text-align:center;
        vertical-align:middle;
    }
    
    </style>
          
</head>
<body style="width:100%; height:100%; background-color:Black; vertical-align:middle" >
    <form id="form1" runat="server" style="background-color:Black; width:100%; height:100%">
    <div id ="infoDiv" runat = "server" ></div>  
   <div style="width: 100%; height: 100%;" id="player"></div>
<script type="text/javascript" src="jwplayer.js"></script>


<script type="text/javascript">

    jwplayer("player").setup({
        width: '100%', height: '100%',
        skin: "newtubedark.zip",
        plugins: { 'qualitymonitor.swf': {} },
        modes: [
{ type: 'flash', src: 'player.swf', config: { provider: 'HLSProvider5.swf', file: '<%=GetSrc%>'} },
{ type: 'html5', config: { file: '<%=GetSrc%>'} }
]
    });
    jwplayer("player").play();
</script>
    </form>
     
</body>
</html>
