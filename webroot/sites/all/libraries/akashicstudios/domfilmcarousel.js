
function DomFilmCarousel() {
	
	this._cssClass = '';
	this._cssClassLink = '';
	
	this._prevButton = {};
	this._nextButton = {};
	
	this._domNode = {};
	this._links = new Array();
	this._ul = {};
	this._ul_default = {};
	this._liWidth = {};
	
	
	function init() {
		this.loadDomNode();
		this.loadItems();
		
		this.jcarousel_init();
		this.jcarousel_setwidth();

	}
	this.init = init;
	
	
	function loadDomNode(){
		
		var dn = document.getElementsByClassName(this._cssClass);
		if(dn != null) {
			this._domNode = dn.item(0);
		}		
		
		this._ul = this._domNode.firstElementChild;
		
		
		var p = document.createElement('A');
		p.className='jcarousel-prev';
		p.href='#';
		var n = document.createElement('A');
		n.className='jcarousel-next';
		n.href='#';
		this._domNode.appendChild(p);
		this._domNode.appendChild(n);
		
		this._domNode.addEventListener("touchmove", function(event){
			event.preventDefault();
			
		}, false);
		
	}
	this.loadDomNode = loadDomNode;
	

	function loadItems(){
		var me = this;
		
		var vf = window.AkashicStudios._pageController._controller._movies_json._vodFilms;
		var dn = document.getElementById('jcarousel-entertainment-movies-item');
		
		vf.forEach(function(s, i){
			
			var li = dn.cloneNode(true);
			var d = li.firstElementChild;
			
			li.removeAttribute('id');
			li.style.removeProperty('display');
			
			d.dataset.nid = s.nid();
			d.dataset.categories = s.categories();
			d.dataset.languages = s.getMediaItemLanguagesCSV();
			
			d.children[0].src = s.getMetaData('en').poster();
			
			d.onclick = function(){
				window.AkashicStudios._pageController._controller._dom_unique_movie.lookup(this.dataset.nid);				
			};
			
			dn.parentElement.appendChild(li);
			me._links.push(li.firstElementChild);
			
		});
		
		this._ul = dn.parentElement;
		this._ul_default = this._ul.cloneNode(true);
		this._ul_default.removeChild(this._ul_default.children[0]);
		
		dn.parentElement.removeChild(dn);
		
	}
	this.loadItems = loadItems;
	
	
	
	function jcarousel_setwidth(){
		this._liWidth = 0;
		var ch = this._ul.children;
		
		for(var i=0; i<ch.length; i++){
			if(ch[i].scrollWidth>0) {
				this._liWidth += ch[i].scrollWidth;
			}
		}
		
		this._ul.style.width = this._liWidth + 'px';
	}
	this.jcarousel_setwidth = jcarousel_setwidth;
	
	
	function jcarousel_init(){
		
		var jc = jQuery('.jcarousel-entertainment-movies').jcarousel({
			wrap: 'circular'
		});
		jQuery('.jcarousel-entertainment-movies .jcarousel-prev').jcarouselControl({
	        target: '-=6',
	        carousel: jc
	    });

		jQuery('.jcarousel-entertainment-movies .jcarousel-next').jcarouselControl({
	        target: '+=6',
	        carousel: jc
	    });
		
		jQuery('.jcarousel-entertainment-movies').on('jcarousel:fullyvisiblein', 'li', function(event, carousel) {
			//this.firstElementChild.onclick();
		});

	}
	this.jcarousel_init = jcarousel_init;


	function jcarousel_destroy(){
		jQuery('.jcarousel-entertainment-movies').jcarousel('destroy');
	}
	this.jcarousel_destroy = jcarousel_destroy;
	
	
	function jcarousel_moveFirst(){
		jQuery('.jcarousel-entertainment-movies').jcarousel('scroll',0);
		if(this._ul.children[0] != undefined){
			this._ul.children[0].children[0].onclick();
		}
	}
	this.jcarousel_moveFirst = jcarousel_moveFirst;
	

	function setFilters() {
		
		var cat = window.AkashicStudios._pageController._controller._dom_movie_filters.selectedCategory();
		var lang = window.AkashicStudios._pageController._controller._dom_movie_filters.selectedLanguage();
		
		var myself = this;

		this.jcarousel_destroy();
		this._ul.innerHTML = '';
		
		
		for(var i = 0; i<this._ul_default.children.length; i++) {
			
			var li = this._ul_default.children[i];
			
			var bAppend = true;
			
			var cats = li.children[0].dataset.categories.split(',');
			var langs = li.children[0].dataset.languages.split(',');
		
			if(cats.indexOf(cat)==-1) {
				bAppend = false;
			}
			if(langs.indexOf(lang)==-1) {
				bAppend = false;
			}
			
			if(bAppend){
				var newli = myself._ul.appendChild(li.cloneNode(true));
				newli.firstElementChild.onclick = function(){
					window.AkashicStudios._pageController._controller._dom_unique_movie.lookup(this.dataset.nid);
				};
			}
		}
		
		this.jcarousel_init();
		this.jcarousel_setwidth();
		this.jcarousel_moveFirst();

	}
	this.setFilters = setFilters;


}
