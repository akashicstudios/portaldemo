	<div id="header">
	
  		<?php print render($page['header_first']); ?>
  		<?php print render($page['header_second']); ?>
  		<?php print render($page['header_third']); ?>
		<div id="header-banner">
  			<?php print render($page['header_banner']); ?>
			<div id="header-banner-page-title">
				<?php print render($page['header_page_title']); ?>
			</div>
		</div>
  		  		  
	</div>	
  
	<div id="content">

	    <div class="separator"></div>
	  
	  	<div id="content-top" class="content-row entertainment-television-content-top">
	  	
		  	 <div class="region region-content-top entertainment-television" style="">
			  	<div id="entertainment-television-content-tvseries" class="entertainment-television-content-tvseries vod-tvseries">
					<div class="field poster">
						<img src="" width="220" height="330">
					</div>  
					<div class="field title"></div>  
					<div class="field actors"></div>  
					<div class="field story">    
						<span>Story: </span>    
						<div></div>
					</div>
					<div class="field episodes"></div>
			  	</div>
	
	  		  	<div id="entertainment-television-content-satellitetv" class="entertainment-television-content-satellitetv vod-satellitetv">
					<div class="field poster">
						<div class="play-button"></div>
						<img src="" width="220" height="330">
					</div>  
					<div class="field title"></div>  
					<div class="field story"></div>
			  	</div>
			
			</div>  	
		</div>
		
	    <div class="separator"></div>
				
		<div id="content-bottom" class="content-row entertainment-television-content-bottom">

			<div class="jcarousel-entertainment jcarousel-entertainment-tvseries" data-jcarousel="true">    
				<ul class="jcarousel-list">          
					<li id="jcarousel-entertainment-tvseries-item" style="display: none;">  
						<div>
							<img src="" width="120" height="180">
						</div>  
					</li>
				</ul>
			</div>
			
			<div class="jcarousel-entertainment jcarousel-entertainment-satellitetv" data-jcarousel="true">    
				<ul class="jcarousel-list">          
					<li id="jcarousel-entertainment-satellitetv-item" style="display: none;">  
						<div>
							<img src="" width="120" height="180">
						</div>  
					</li>
				</ul>
			</div>
			
		</div>
		
	    <div class="separator"></div>
				
	</div>

  
  
	<div id="footer">
  	
  		<?php print render($page['footer']); ?>
  	
	</div>
  
	<div id="content" style="background-color: #000; display: none;">
		<?php print render($page['content']); ?>
	</div>

	
  	<div id="content-messages">
      
      <?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      <?php endif; ?>

      <?php print render($page['content_messages']); ?>
      
    </div>
	
		
	