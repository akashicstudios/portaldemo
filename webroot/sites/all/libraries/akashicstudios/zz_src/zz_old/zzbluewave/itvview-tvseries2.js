

var itvViewTvSeries2Controller = new ItvView('itvViewTvSeries2');

itvViewTvSeries2Controller.viewItemDefaults.tagName = 'LI';
itvViewTvSeries2Controller.viewItemDefaults.navIdPrefix = itvViewTvSeries2Controller.cssClass + 'Item';
itvViewTvSeries2Controller.viewItemDefaults.inFocusCss = itvViewTvSeries2Controller.cssClass + '-list-item-focus';
itvViewTvSeries2Controller.html.scroll.direction = 'vertical-scroll';

itvViewTvSeries2Controller.nav.dpad.back = 'itvViewTvSeries1';





itvViewTvSeries2Controller.html.position.maxWidth = 540;
itvViewTvSeries2Controller.html.isOverlay = true;

itvViewTvSeries2Controller.init();
itvViewTvSeries2Controller.hide();

window.gNav.views['itvViewTvSeries2'] = itvViewTvSeries2Controller;
window.gNav.viewsZeroBased.push(itvViewTvSeries2Controller);


