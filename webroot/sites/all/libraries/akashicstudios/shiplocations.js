
function ShipLocations() {
	
	this._json = {};
	
	this._locations = '';
	
	function init() {
		
		var me = this;
		
		jQuery.getJSON( '/sql/json/ship_locations()' )
		.done(function(data){
			
			me._json = data;			
			
			me._locations = new Array();
			
			me._json.forEach(function(elem){
				var sv = new ShipLocation();
				sv._json = elem;
				me._locations.push(sv);
				
			});
			
			window.AkashicStudios.onShipLocationsLoaded();
			

		})
		.fail(function( jqXHR, textStatus, errorThrown){
			console.log(textStatus + '\n' + errorThrown);
		});
	
		

	}
	this.init = init;
	
	
	
	this.getShipLocation = function(tid) {
		
		var retval = null;
		
		this._locations.forEach(function(elem){
			if(tid == elem.id()) {
				retval = elem;
			}
		});
		
		return retval;
	}

	
	this.getLocationsByType = function(lt,tid_deck){
		
		var me = this;
		
		var deck = typeof tid_deck != undefined ? this.getShipLocation(tid_deck) : null;
		
		var retval = new Array();
		
		this._locations.forEach(function(elem){
			if(lt == elem.location_type()) {
				if(deck != null) {
					var elem_deck = me.getParent(elem.id(),'d');
					var dktid = elem_deck != null ? elem_deck.id() : null;
					if(deck.id() == dktid){
						retval.push(elem);
					}
				}
				else {
					retval.push(elem);
				}
			}
		});
		
		return retval;
		
	}

	
	this.getParent = function(tid,lt){
		
		var v = this.getShipLocation(tid);
		var vp = this.getShipLocation(v.parent());
		while(vp.location_type() != lt) {
			vp = this.getShipLocation(vp.parent());
		}
		return vp;		
	}	
	
	
	
	this.getChildren = function(tid){
		var retval = new Array();
		
		this._locations.forEach(function(elem){
			if(tid == elem.parentId()) {
				retval.push(elem);
			}
		});
		
		return retval;
		
	}
	
	
	
	

}




function ShipLocation(){
	
	this._json = {};

	
	this.id = function(){
		return this._json.id;
	}

	this.parentId = function(){
		return this._json.parentId;
	}
	
	this.name = function(){
		return this._json.name;
	}

	this.description = function(){
		return this._json.description;
	}

	this.location_type = function(){
		return this._json.location_type;
	}

	this.weight = function(){
		return this._json.weight;
	}
	
	
	
	
	
	
}
