
	function breakPoint(element) {
		console.log('here');
	}

	/*
	 * Not fully implemented but will be tested in future so that a running error log can be kept on the server.
	 * future try..catch blocks can be outputted here for posting to the server log.
	 * 
	 * */


	function debugMsg(msg) {
		
		var bOnScreen = false;

		if(bOnScreen) {
			try {
				$('#spanDebug').html(msg);
			}
			catch (err) {
				console.log(err.message);
			}			
		}
		
		console.log(msg);
		
		//Using the core $.ajax method
		$.ajax({
		 
			  // the URL for the request
			  url : "/index.php/favi_debug",
			 
			  // the data to send
			  // (will be converted to a query string)
			  data : {msg: msg},
			 
			  // whether this is a POST or GET request
			  type : "GET",
			 
			  // the type of data we expect back
			  dataType : "text"
			 
/*			  
			  // code to run if the request succeeds;
			  // the response is passed to the function
			  success : function ( text ) {
				  console.log('success: '+text);
			  },
			  
			  // code to run if the request fails;
			  // the raw request and status codes are
			  // passed to the function
			  error : function( xhr, status ) {
			    alert("Sorry, there was a problem!");
			  },
			 
			  // code to run regardless of success or failure
			  complete : function( xhr, status ) {
			    alert("The request is complete!");
			  }
*/
		});		 
	}
	

	/*
	 * checks all elements for a matching "data-navid"
	 * */
	function getElementByNavId(dpadnavid) {

		var retval=null;
		var allElements = document.getElementsByTagName('*');

		for (var i = 0; i < allElements.length; i++) {

			if (allElements[i].dataset.dpadnavid == dpadnavid) {
				// Element exists with attribute. Add to array.
				
				//console.log(allElements[i]);
				retval = allElements[i];
			}
		}

//		console.log('getElementByNavId retval:');
//		console.log(retval);
		return retval;

	}



	

	/*
	 * inspects all elements in a container, and updates their direction left/right or up/down limits, so they don't go past their own boundaries.
	 * call this method, and then call the setNavOnElement() for the end elements if they should move off to a different group of nav elements
	 * */
	function resetNavLimits(container, tagName, dpadnavid, direction) {

		try {

			if(container!=null) {
				elements = container.getElementsByTagName(tagName);
				if(elements!=null) {
					
					var elemPrev = 0;
					var elemNext = 0;
					
					for (var i=0; i<elements.length; i++) {
						
						elemPrev = i-1;
						if(i==0) elemPrev++;	
						elemNext = i+1;
						if(i==elements.length-1) elemNext--;
						
						if(direction=='vertical') {	
							//vertical
							elements[i].dataset.dpadnavidup = dpadnavid+elemPrev;
							elements[i].dataset.dpadnaviddown = dpadnavid+elemNext;

						}
						else {	
							//horizontal
							elements[i].dataset.dpadnavidleft = dpadnavid+elemPrev;
							elements[i].dataset.dpadnavidright = dpadnavid+elemNext;
						}
					}
				}
			}
		}
		catch(err){
			console.log("resetNavLimits()  ERROR: " + err.message);
		}	

	}
	
	/*
	 * updates the navleft / navup/navright / navdown attribute, setting the specified direction to the new elementID
	 * */
	function setNavOnElement(dpadnavid,direction,dpadnavidNew) {
		
		try {

			var elem = getElementByNavId(dpadnavid);
			
			if(elem!=null) {
				
				switch (direction) {
				case 'up':
					elem.dataset.dpadnavidup = dpadnavidNew;
					break;
				case 'left':
					elem.dataset.dpadnavidleft = dpadnavidNew;
					break;
				case 'right':
					elem.dataset.dpadnavidright = dpadnavidNew;
					break;
				case 'down':
					elem.dataset.dpadnaviddown = dpadnavidNew;
					break;
				}
				
			}
			
		}
		catch (err) {
			console.log("setNavOnElement()  ERROR: " + err.message);
		}

	}
	
	/*
	 * will update the title attribute of all the elements in a container, setting one 'side' to that element's own ID, effectively making the dpad not move when the user clicks that direction.
	 * (moves to itself only)
	 * */
	function setNavEdgeOnContainer(container,direction,tagName) {

		try {

			tagName = typeof tagName !== 'undefined' ? tagName : 'CANVAS';
			
			if(container!=null) {
				
				var navItems = container.getElementsByTagName(tagName);
				for(var i=0; i<navItems.length; i++) {
					setNavOnElement(navItems[i].dataset.dpadnavid, direction, navItems[i].dataset.dpadnavid);
				
				}
			}
		}
		catch(err) {
			console.log("setNavEdgeOnContainer()  ERROR: " + err.message);
		}

	}
	


	/*
	 * initializes / resets the data-navid for all elements in a the container
	 * */
	function resetNavIdOnElement(elem, dpadnavid) {
		elem.dataset.dpadnavid = dpadnavid;
	}


	/*
	 * initializes / resets the data-navid for all elements in a the container
	 * */
	function resetNavIdOnContainer(container, tagName, dpadnavid) {

		try {

			if(container!=null) {
				elements = container.getElementsByTagName(tagName);

				if(elements!=null) {
					
					for (var i=0; i<elements.length; i++) {
						elements[i].dataset.dpadnavid = dpadnavid+i;
						elements[i].removeAttribute('title');
					}
				}
			}
		}
		catch(err){
			console.log("resetNavLimits()  ERROR: " + err.message);
		}	

	}




	
	/*
	 * sets the direction on an element at the edge of the container
	 * */
	function setNavOnContainerFirstLastElement(container,position,direction,dpadnavidNew,tagName) {

		try {	

			tagName = typeof tagName !== 'undefined' ? tagName : 'CANVAS';

			
			if(container!=null) {
				
				var navItems = container.getElementsByTagName(tagName);

				if(position=='first') {
					setNavOnElement(navItems[0], direction, newElementNavId);
				}
				if(position=='last') {
					setNavOnElement(navItems[navItems.length-1], direction, dpadnavidNew);
				}
				
			}
		}
		catch(err){
			console.log("setNavOnContainerFirstLastElement()  ERROR: " + err.message);
		}	

	}
	
	/*
	 * returns the id of an element at the edge of the container
	 * */
	function getContainerFirstLastElement(container,position,tagName) {

		try {

			tagName = typeof tagName !== 'undefined' ? tagName : 'CANVAS';

			if(container!=null) {
				
				var navItems = container.getElementsByTagName(tagName);

				if(position=='first') {
					return navItems[0].dataset.dpadnavid;
				}
				if(position=='last') {
					return navItems[navItems.length-1].dataset.dpadnavid;
				}
				
			}
		}
		catch(err){
			console.log("setNavOnContainerFirstLastElement()  ERROR: " + err.message);
		}	

	}
	
	
	
	/*
	 * will update the title attribute for one 'side' of all the elements in a container to the specified element ID
	 * */
	function changeNavOnContainer(container,direction,dpadnavidNew,tagName) {

		try {
			//console.log(newElementNavId);
			tagName = typeof tagName !== 'undefined' ? tagName : 'CANVAS';

			if(container!=null) {
				
				var navItems = container.getElementsByTagName(tagName);
				for(var i=0; i<navItems.length; i++) {
					setNavOnElement(navItems[i].dataset.dpadnavid, direction, dpadnavidNew);

				}
			}
		}
		catch(err){
			console.log("changeNavOnContainer()  ERROR: " + err.message);
		}	

	}
	




	/*
	 * Checks for the physical location of one element inside the viewable area of it's parent.
	 * meant for scrolling <li> elements inside a parent, to see which <li> (row) is in view
	 * */
	function isElementInView(containerSelector,elementSelector) {

			try {
				var containerViewTop = $(containerSelector).offset().top;						 
				var containerViewBottom = containerViewTop + $(containerSelector).height();		 
				
			    var elemTop = $(elementSelector).offset().top;									 
			    var elemBottom = elemTop + $(elementSelector).height();							 
			
				var retval = ((elemBottom <= containerViewBottom) && (elemTop >= containerViewTop));
				
				return retval;
			}
			
			catch(err){
				console.log("isElementInView()  ERROR: " + err.message);
			}

	}







		



























