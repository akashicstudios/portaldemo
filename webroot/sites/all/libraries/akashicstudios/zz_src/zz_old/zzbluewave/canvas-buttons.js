
	function inFocusOverlay (elem) {

		var elemId = elem.id;
		var elemRect = elem.getBoundingClientRect();
		
		var newDiv = document.createElement('div');
		newDiv.id = elemId+'_overlay';
		newDiv.style.backgroundColor='#8FEBF7';
		newDiv.style.position = 'absolute';
		newDiv.style.top = elemRect.top+'px';
		newDiv.style.left = elemRect.left+'px';
		newDiv.style.width = elemRect.width+'px';
		newDiv.style.height = elemRect.height+'px';
		newDiv.style.zIndex='10';
		newDiv.style.visibility="hidden";
		newDiv.style.display="block";
		
		elem.parentNode.appendChild(newDiv);
	}
	
	
	function inFocusBorder (elem,bShow) {
		if(bShow==1) {
			elem.style.border='4px solid silver';
		}
		else if(bShow==0) {
			elem.style.border='4px solid transparent';
		}
		else {
			if(elem.style.border=='4px solid transparent') {
				elem.style.border='4px solid silver';
			}
			else {
				elem.style.border='4px solid transparent';
			}
		}
		elem.style.borderRadius='4px';
	}
	
	

	function inFocusOverlayVisible (elem, bVisible) {
		var ovr = document.getElementById(elem.id+"_overlay");
		if(bVisible=='1') {
			ovr.style.visibility = 'visible';
		}
		else {
			ovr.style.visibility = 'hidden';
		}	
	}
	


	
	function setCanvasButtonBlack(canvas, strButtonText, bHover, strImgSrc, isSelected, imgAlign, fontPixels) {
		
		var ctx=canvas.getContext("2d");

		// draw rounded button
		var x=5;
		var y=5;
		var width=canvas.width-8;
		var height=canvas.height-8;
		var arcsize=5;
		
		var color1 = "#222";
		if(bHover) {
			color1 = "#fff";
		}
		

		// Create gradient
		var gradient=ctx.createLinearGradient(0,0,0,canvas.height);
		gradient.addColorStop(0.0,'rgba(0,0,0,1.0)');
		gradient.addColorStop(1.0,'rgba(0,0,0,1.0)');			
		ctx.fillStyle=gradient;
				
		ctx.lineWidth=4;
		ctx.strokeStyle=color1;
		
		ctx.beginPath();
		ctx.moveTo(x+arcsize, y);
		ctx.strokeStyle=color1;
		ctx.lineTo(x+width-arcsize, y);
		ctx.strokeStyle=color1;
		ctx.arcTo(x+width, y, x+width, y+arcsize, arcsize);
		ctx.strokeStyle=color1;
		ctx.lineTo(x+width,y+height-arcsize);
		ctx.strokeStyle=color1;
		ctx.arcTo(x+width, y+height, x+width-arcsize, y+height, arcsize);
		ctx.strokeStyle=color1;
		ctx.lineTo(x+arcsize, y+height);
		ctx.strokeStyle=color1;
		ctx.arcTo(x, y+height, x, y-arcsize, arcsize);
		ctx.strokeStyle=color1;
		ctx.lineTo(x, y+arcsize);
		ctx.strokeStyle=color1;
		ctx.arcTo(x, y, x+arcsize, y, arcsize);
		ctx.strokeStyle=color1;
		ctx.lineTo(x+arcsize, y);
		ctx.stroke();

		ctx.shadowBlur=3;
		ctx.shadowOffsetX=1.5;
		ctx.shadowOffsetY=1.5;
		ctx.shadowColor="#000";
		
		ctx.fill();
		
		
		
		grad=ctx.createLinearGradient(0,0,0,canvas.height);
		grad.addColorStop(0.0,'rgba(200,200,200,0.0)');
		grad.addColorStop(0.4,'rgba(200,200,200,0.1)');
		grad.addColorStop(0.7,'rgba(200,200,200,0.1)');
		grad.addColorStop(1.0,'rgba(200,200,200,0.0)');

		x=5;
		y=5;
		width=canvas.width-8;
		height=canvas.height*0.45;
		arcsize=5;

		ctx.fillStyle=grad;
		
		ctx.shadowBlur=3;
		ctx.shadowOffsetX=1.5;
		ctx.shadowOffsetY=1.5;
		ctx.shadowColor="#000";
				
		ctx.beginPath();
		ctx.moveTo(x+arcsize, y);
		ctx.lineTo(x+width-arcsize, y);
		ctx.arcTo(x+width, y, x+width, y+arcsize, arcsize);
		ctx.lineTo(x+width,y+height-arcsize);
		ctx.arcTo(x+width, y+height, x+width-arcsize, y+height, arcsize);
		//ctx.arcTo(x+width-arcsize, y+height, x+arcsize, y+height, 9);
		ctx.lineTo(x+arcsize, y+height);
		ctx.arcTo(x, y+height, x, y-arcsize, arcsize);
		ctx.lineTo(x, y+arcsize);
		ctx.arcTo(x, y, x+arcsize, y, arcsize);
		ctx.lineTo(x+arcsize, y);
		
		ctx.fill();
		

		var fontSize = 14;
		if(fontPixels!=null) {
			fontSize = fontPixels;
		}
		var font = "'Droid Sans', sans-serif";		//"'Droid Sans', Lucida Sans Unicode, Lucida Grande, Helvetica";
		var fontWeight = 'normal';
		var fontColor = 'silver';

		if(isSelected!=null) {
			if(isSelected=='1') {
				fontColor='white';
				//fontWeight='bold';
			}
		}

		ctx.fillStyle=fontColor;

		if(strButtonText.length>20) fontSize = 14;
		if(strButtonText.length>26) fontSize = 12;
		if(strButtonText.length>32) fontSize = 10;
		ctx.font=fontWeight + ' ' + fontSize + 'px ' + font;
		

		ctx.textAlign="center";
		ctx.textBaseline="middle";
		ctx.shadowBlur=0;
		ctx.shadowOffsetX=0;
		ctx.shadowOffsetY=0;
		ctx.shadowColor="black";
		
		
		var rectImg = new Array(0,0,canvas.width,canvas.height);
		var rectText = new Array(0,0,canvas.width,canvas.height);
		var rectImgLeftAlign = new Array(0,0,canvas.width*.45,canvas.height);
		var rectTextLeftAlign = new Array(
				(canvas.width-(canvas.width*.7)),
				0,
				canvas.width*.55,
				canvas.height
				);
		
		if(imgAlign==null) {
			ctx.fillText(strButtonText,(canvas.width/2)-2,(canvas.height/2)-2);
		}
		
		if(imgAlign!=null) {
			if(imgAlign=='left') {
				ctx.fillText(strButtonText,(rectImgLeftAlign[2]+20),(canvas.height/2));
			}
			else {
				ctx.fillText(strButtonText,(canvas.width/2)-2,(canvas.height/2)-2);
			}
		}


		if(strImgSrc!=null) {
			if(strImgSrc.length>0) {
				var img = new Image();
				img.onload = function(){
					ctx.globalAlpha = 1.0;
					if(isSelected!=null) {
						if(isSelected=='0') {
							ctx.globalAlpha = 0.25;
						}
						else {
							ctx.globalAlpha = 1.0;
						}
					}

					if(imgAlign!=null) {
						if(imgAlign=='left') {
							ctx.drawImage(img,(rectImgLeftAlign[2]*0.5-img.width*0.5),(canvas.height*0.5-img.height*0.5));
						}
					}
					else {
						ctx.drawImage(img,(canvas.width*0.5-img.width*0.5),(canvas.height*0.5-img.height*0.5));
					}
					

				};
				
				img.src = strImgSrc;
				
			}
		}	
		
		
		if(bHover==1) {
			var hoverGrad=ctx.createLinearGradient(0,0,0,canvas.height);
			hoverGrad.addColorStop(0.0,'rgba(51,153,255,0.3)');
			ctx.fillStyle=hoverGrad;
			ctx.fill();
		}

			
				
	return canvas;
	}
	






	function setCanvasArrowButtonBlack(canvas, bFaceDown, bHover) {

		var ctx=canvas.getContext("2d");
		ctx.clearRect(0,0,canvas.width, canvas.height);
		
		var color1 = "rgba(192,192,192,1.0)";
		if(bHover==1) {
			color1 = "rgba(51,153,255,0.3)";
		}
		var canvasPadding = 5;
		
		ctx.lineWidth=3;
		ctx.strokeStyle=color1;
		ctx.lineJoin = "round";

		ctx.beginPath();
		
		if(bFaceDown==1) {
			ctx.moveTo(canvas.width-canvasPadding, canvasPadding);
			ctx.lineTo((canvas.width)/2, canvas.height-canvasPadding);
			ctx.lineTo(canvasPadding,canvasPadding);		
		}
		else {
			ctx.moveTo(canvasPadding,canvas.height-canvasPadding);
			ctx.lineTo(canvas.width/2,canvasPadding);
			ctx.lineTo(canvas.width-canvasPadding, canvas.height-canvasPadding);
		}
		
		//ctx.closePath();
		ctx.stroke();
		
		/*
		// Create gradient for hover
		if(bHover==1) {
			var gradient=ctx.createLinearGradient(0,0,0,canvas.height);
			gradient.addColorStop(0.0,'rgba(192,192,192,0.0)');
			gradient.addColorStop(0.5,'rgba(192,192,192,1.0)');
			gradient.addColorStop(0.9,'rgba(192,192,192,0.5)');
			gradient.addColorStop(1.0,'rgba(192,192,192,1.0)');
			ctx.fillStyle=gradient;
			ctx.fill();
		}
		*/
		
		
	return canvas;
	}