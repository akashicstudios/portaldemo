

var eps = document.getElementsByClassName('tabletViewTvSeriesEpisodes-list-item');
for (var i=0; i< eps.length; i++) {
	
	var nav = eps[i].getElementsByClassName('itvNavBlockItem-navData').item(0);
	var nid = nav.dataset.viewitemnid || '';
	var url = nav.dataset.dpadenterurl || '';
	
	eps[i].dataset.viewitemnid = nid;
	eps[i].dataset.dpadenterurl = url;
	

	eps[i].addEventListener('click', function() {
		
		if(this.dataset.dpadenterurl !='') {
			window.open(this.dataset.dpadenterurl);
		}
		
	},false);
	
}
