<?php


// $to_email       = "jason.anthony.cohn@gmail.com dfbuckley@gmail.com";
$to_email       = "donb@silversea.com jasonc@silversea.com jason.anthony.cohn@gmail.com dfbuckley@gmail.com tstoeterau@gmail.com";
$subject		= 'Silversea Shipboard Portal Guest Comment Submission';
$message_text        = filter_var($_POST["message_text"], FILTER_SANITIZE_STRING);

$message_body = 'The following submission was received:'.$message_text;

$headers = 'From: labportal@silversea.com\r\n' .
		'Reply-To: noreply@silversea.com\r\n' .
		'X-Mailer: PHP/' . phpversion();

$send_mail = mail($to_email, $subject, $message_body, $headers);

$output = json_encode(array('type'=>'message', 'text' => ' Thank you for your submission. ('.$send_mail.')'));
die($output);

?>