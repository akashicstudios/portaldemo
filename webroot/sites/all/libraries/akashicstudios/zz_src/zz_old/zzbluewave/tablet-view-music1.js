window.gNav = new ItvNavigationState();	


var tabletViewMusic1Controller = new ItvView('tabletViewMusic1');

tabletViewMusic1Controller.viewItemDefaults.tagName = 'LI';
tabletViewMusic1Controller.viewItemDefaults.navIdPrefix = tabletViewMusic1Controller.cssClass + 'Item';
tabletViewMusic1Controller.viewItemDefaults.inFocusCss = tabletViewMusic1Controller.cssClass + '-list-item-focus';

//tabletViewMusic1Controller.html.position.maxWidth = 820;
tabletViewMusic1Controller.html.isJWPlayer = true;

//tabletViewMusic1Controller.init();
//tabletViewMusic1Controller.hide();

window.gNav.views['tabletViewMusic1'] = tabletViewMusic1Controller;
window.gNav.viewsZeroBased.push(tabletViewMusic1Controller);



/************************************************************************************************************************************************************/
// JWPlayer setup & init
/************************************************************************************************************************************************************/



tabletViewMusic1Controller.jwPlayer = function(cat) {

	window.tabletViewMusic1JWPlayer0 = new ItvJWPlayer('tabletViewMusic1JWPlayer0');
	window.tabletViewMusic1JWPlayer0.category = cat;
	window.tabletViewMusic1JWPlayer0.skin = '/sites/all/themes/red_cell_blue_wave/jwskin/bluewave/bluewave-tablet.xml';

	var strCat = new String(cat);
	
	var url = window.location.origin + Drupal.settings.redCell.musicFeedPath + '/' + strCat.replace(' ', '-').toLowerCase() + '.xml';
	
	window.tabletViewMusic1JWPlayer0.playlistUrl = url;
	window.tabletViewMusic1JWPlayer0.primary = 'html5';

	window.gNav.views['tabletViewMusic1'].domNode.style.backgroundImage = "url('/" + Drupal.settings.redCell.theme + '/img/musicCategory/bg-' + strCat.toLowerCase().replace(' ', '-') + ".jpg')";
	
	// window.tabletViewMusic1JWPlayer0.audioBackground = window.location.origin + '/' + Drupal.settings.redCell.theme + '/img/musicCategory/bg-' + strCat.replace(' ', '-').toLowerCase() + '.jpg';

	window.tabletViewMusic1JWPlayer0.setupRandomRepeat();
	
	// jwplayer('tabletViewMusic1JWPlayer0').load(window.tabletViewMusic1JWPlayer0.playlistUrl);
	
	if(isApprovedDevice()){
		jQuery('#tabletViewMusic1JWPlayer0ClickMsg').show();
	} else
	{
		jQuery('#tabletViewMusic1JWPlayer0ClickMsg').hide();
	}
	
		
};


tabletViewMusic1Controller.init = function(category) {

	var cat = category.toLowerCase();
	
	// window.gNav.views['tabletViewMusic1'].domNode.style.backgroundImage = "url('/" + Drupal.settings.redCell.theme + '/img/musicCategory/bg-' + cat.replace(' ', '-') + ".jpg')";
	
	window.gNav.views['tabletViewMusic1'].jwPlayer(cat);
	
	// jwplayer('tabletViewMusic1JWPlayer0').load(window.tabletViewMusic1JWPlayer0.playlistUrl);
	
	// jQuery('#tabletViewMusic1JWPlayer0ClickMsg').show();
	
};


jQuery('#tabletViewMusic1JWPlayer0Container').click(function(){
	
	$('#tabletViewMusic1JWPlayer0ClickMsg').hide('slow');
	
});
