function DomMovieFilters(){

	this._catSelect = {};
	this._langSelect = {};

	
	
	function selectedCategory(){
		var s = this._catSelect.firstElementChild;
		return s.options[s.selectedIndex].value;
	}
	this.selectedCategory = selectedCategory;
	
	function selectedLanguage(){
		var s = this._langSelect.firstElementChild;
		return s.options[s.selectedIndex].value;
	}
	this.selectedLanguage = selectedLanguage;
	

	function init() {
		
		this.getMoviesCategorySelect();
		
	}
	this.init = init;
	

	
	this.getMoviesCategorySelect = function(){
		
		var me = this;
		
		jQuery.getJSON( '/sql/json/moviecategoryview()' )
		.done(function(data){

			var options = new Array();
			
			data.forEach(function(elem){
				options.push({
					'value' : elem.id,
					'innerHTML' : elem.display_name
				});
				
			});
		
			var params = {
					'id' : 'entertainment-movies-select-category',
					'className' : 'entertainment-movies-select-category',
					'selectText' : '',
					'options' : options,
					'onchange' : function(){
						window.AkashicStudios._pageController._controller._dom_film_carousel.setFilters();
					}					
				};
			
			me._catSelect = window.AkashicStudios.getSelect(params);
			
			me.getMoviesLanguageSelect();

		});	
	}

	
	
	this.getMoviesLanguageSelect = function(){

		var me = this;
		var options = new Array();
		
		var langs = window.AkashicStudios.languages;
		for(var i = langs.length-1; i>=0; i--) {
			var elem = langs[i];
			options.push({
				'value' : elem.code,
				'innerHTML' : elem.local_language
			});
		}
			
		var params = {
				'id' : 'entertainment-movies-select-language',
				'className' : 'entertainment-movies-select-language',
				'selectText' : '',
				'options' : options,
				'onchange' : function(){
					window.AkashicStudios._pageController._controller._dom_film_carousel.setFilters();
				}					
			};
			
		me._langSelect = window.AkashicStudios.getSelect(params);
			
			
		var params = {
				'type' : 'filter',
				'selects' : [me._catSelect,me._langSelect]
		};
			
		window.AkashicStudios.getHeaderBannerFilter(params);

		
	}		
		
		

	
	
}


	
	
