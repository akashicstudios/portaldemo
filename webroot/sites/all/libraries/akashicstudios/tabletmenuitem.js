

/*
 */
function TabletMenuItem(){
	
	this._cssClass = '';
	this._parentMenu = '';
	
	this._domNode = '';
	
	this._linkDom = '';
	this._imageDom = '';
	this._titleDom = '';
	this._bodyTextDom = '';
	this._dataDom = '';
	
	this._navid = '';
	this._parentNavid = '';
	this._contentRegion = '';
	this._linkType = '';
	
	this._isVisible = true;


	function init(){
		
		//parses domNode and assigns all class properties
		this._linkDom = this._domNode.children[0].firstElementChild.firstElementChild;
		if(this._linkDom != null) {
			this._imageDom = this._linkDom.firstElementChild;
		}
		this._titleDom = this._domNode.children[1].firstElementChild;
		this._bodyTextDom = this._domNode.children[2].firstElementChild;
		this._navLinkDom = this._domNode.children[3].firstElementChild;
		this._dataDom = this._domNode.children[4].firstElementChild.firstElementChild;
	
		this._domNode._linkDom = this._linkDom;
		this._domNode._dataDom = this._dataDom;
	
		// properties
		// IDs
		this._navid = this._dataDom.dataset.navid;
		this._parentNavid = this._dataDom.dataset.parentnavid;
		
		this._cssClass += '.' + this._navid;


		// linkType & click Handler
		this.setClickHandler();
		
		this.showHide(false);
	
	}
	this.init = init;	
	
	
	
	function showHide(isVisible) {
		
		if(isVisible) {
			this._domNode.classList.remove("flip");
		}
		else {
			this._domNode.classList.add("flip");
		}
		this._isVisible = isVisible;
		
	}
	this.showHide = showHide;
	
	
	
	
	function setClickHandler() {
		
		var me = this;
		
		if(this._linkDom.hash == '#child') {
			this._linkType = 'child';
			this._linkDom.onclick = function(event){event.preventDefault();};
			
			this._domNode.onclick = function(){
				if(typeof window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent] != 'undefined'){
					window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent].showHideMenuItems(false);
					window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent].showChildren(me._dataDom.dataset.navid);
				}
				else {
					window.location.href = '/?panels='+me._dataDom.dataset.navid;
				}
			};
			
			this._navLinkDom.onclick = function(){
				if(typeof window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent] != 'undefined'){
					window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent].showHideMenuItems(false);
					window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent].showChildren(this.parentElement.parentElement.dataDom.dataset.navid);
				}
			}
			
		}
		
		else if(this._linkDom.hash == '#parent') {
			this._linkType = 'parent';
			this._linkDom.onclick = function(event){event.preventDefault();};
			
			this._domNode.onclick = function(){
				if(typeof window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent] != 'undefined'){
					window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent].showHideMenuItems(false);
					window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent].showSiblings(me._dataDom.dataset.parentnavid);
				}
			};
			
			this._navLinkDom.onclick = function(){
				if(typeof window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent] != 'undefined'){
					window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent].showHideMenuItems(false);
					window.AkashicStudios._pageController._controller[me._parentMenu._objNameMainContent].showSiblings(this.parentElement.parentElement.dataDom.dataset.parentnavid);
				}
			}
			
		}
//		else if(this._linkDom.hash == '#front') {
//			
//		}
		
		else {
			//this.linkDom.target = '_blank';
			this._linkType = 'url';
			this._domNode.onclick = function(){
				this._linkDom.click();
			};
			
		}
		
		
	}
	this.setClickHandler = setClickHandler;
	
	
}



















