
function DomMusic() {

	
	this._domNode = document.getElementById('entertainment-music-content');	

	this._domHeader = this._domNode.getElementsByClassName('header').item(0);
	this._domTitle = this._domNode.getElementsByClassName('title').item(0);
	this._domDescription = this._domNode.getElementsByClassName('description').item(0);

	this._domContainer = this._domNode.getElementsByClassName('jwplayer-container').item(0);

	this._domLoadImg = this._domNode.getElementsByClassName('loadimg').item(0);
	this._domLoadImg.addEventListener('click', function(){
		window.AkashicStudios._pageController._controller._dom_music._musicPlayer.togglePlay();
	});
	
	this._musicPlayer = new MusicPlayer();
	this._musicPlayer._domMusic = this;
	
	window.AkashicStudios.musicPlayer = this._musicPlayer;
	window.AkashicStudios.musicPlayer.domHeader = this._domHeader;
	window.AkashicStudios.musicPlayer.domTitle = this._domTitle;
	window.AkashicStudios.musicPlayer.domDescription = this._domDescription;
	window.AkashicStudios.musicPlayer.domLoadImg = this._domLoadImg.children[0];
	
	

	function lookup(tid){
		
		var mc = window.AkashicStudios._pageController._controller._music_json.getMusicCategory(tid);
		
		this._domHeader.innerHTML = mc.category();
		this._domNode.style.backgroundImage = 'url("'+mc.background()+'")';
		
		this._domTitle.innerHTML = 'Title';
		this._domDescription.innerHTML = 'Description';
		
		this._musicPlayer.category = mc.category();
		this._musicPlayer.playlistUrl = mc.playlist();
		this._musicPlayer.setupRandomRepeat();
		
	}
	this.lookup = lookup;
	

}	


