
function FleetMap() {
	
	this.map;	
	this.rotationIntervalMS = 10000;	
	
	this.ships = Drupal.settings.redCell.mtngps;
	this.zoomLevel = [1,7,10];
	this.zoomControl = false;
	
	this.mapDiv = 'shipPositionsMap';
	
	this.bIsItv = false;
	
	this.rotation = {
		shipIndex: 0,
		zoomLevel: 0
	};
	
	var init = function (bIsItv) {

		this.bIsItv = bIsItv;		

		// this.newMap(8.95, -79.55);
		this.newMap(0.00,25.00);
		
		// add an OpenStreetMap tile layer
		// L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
		    // attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		// }).addTo(this.map);

		L.tileLayer('http://'+document.domain + ':9000/{z}/{x}/{y}.png', {
		    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(this.map);

		
		this.mapAddShipMarkers();
		
		this.rotation.zoomLevel = 0;

	};
	this.init = init;
	
	
	
	var newMap = function(lat,lon) {

		if(!this.bIsItv) {
			this.mapDiv = 'shipPositionsMapTablet';
			this.zoomLevel = [2,7,10];
			//this.zoomControl = true;
		}

		this.map = L.map(this.mapDiv,{
			zoomControl: this.zoomControl,
			minZoom : 2,
			maxZoom : 10,
			maxBounds : L.latLngBounds(L.latLng(-80.00, -179.99), L.latLng(80.00, 190.00))
		}).setView([lat,lon], this.zoomLevel[this.rotation.zoomLevel]);

		// ,maxZoom : 12
		
	};
	this.newMap = newMap;
	
	
	//loops through the array of ships and puts the marker on the map
	var mapAddShipMarkers = function() {

        for(var i=0; i < this.ships.length; i++) {
	        var ship = this.ships[i];
	        
	        var mapicon = L.icon({
	        	iconUrl : ship.mapicon
	        	,iconSize: [114, 82]
			    ,iconAnchor: [57, 82]
			    // ,popupAnchor: [90, -20]
	        });
	        
			ship.marker = L.marker([ship.latitude, ship.longitude], { icon : mapicon } )
		    // .bindPopup(ship.title + '<br/>' + convertDMS(ship.latitude, ship.longitude),{
		    	// offset: [90, -20], 
		    	// closeButton: false
	    	// })
	    	.addTo(this.map);
		    // .openPopup();
		    
		    // console.log('marker: ' + ship.title + ' - ' + ship.latitude +' / '+ ship.longitude);
		}

	};
	this.mapAddShipMarkers =  mapAddShipMarkers;





	var mapRemoveShipMarkers = function() {
		
        for(var i=0; i < this.ships.length; i++) {
			this.map.removeLayer(this.ships[i].marker);        
		}

	};
	this.mapRemoveShipMarkers =  mapRemoveShipMarkers;
	
	



	var mapResetToGlobalView = function(bIsItv) {
		
		//this.map.fitWorld();
		// this.map.setView([0.00,0.00],this.zoomLevel[this.rotation.zoomLevel]);
		this.map.panTo([0,0]);
		this.map.setZoom(1);
		
	};
	this.mapResetToGlobalView = mapResetToGlobalView;
	
	
	var currentCoords = function() {
		return [this.ships[this.rotation.shipIndex].latitude, this.ships[this.rotation.shipIndex].longitude];
	};
	this.currentCoords = currentCoords;
	
	
	var currentZoomLevel = function () {
		return this.zoomLevel[this.rotation.zoomLevel];
	};
	this.currentZoomLevel = currentZoomLevel;

	
	var mapSetZoomLevel = function(zoomLevel) {
		this.map.setZoom(zoomLevel);
	};
	this.mapSetZoomLevel = mapSetZoomLevel;
	
	
	var scrollIntoView = function() {
		var md = document.getElementById(this.mapDiv);
		md.scrollIntoView();
	}
	this.scrollIntoView = scrollIntoView;
	
	
	
	
	var mapRotate = function () {
		
		// if the zoom level is less than length-1 (not at the end), increment the zoom level
		this.rotation.zoomLevel++;

		if(this.rotation.zoomLevel == this.zoomLevel.length) {
			this.rotation.zoomLevel = 0;
		}
		

		// if the zoom level is zero
		// advance the shipIndex / reset the shipIndex to zero if nec.
		// reset the map to global-all-ships

		if(this.rotation.zoomLevel == 0) {
			this.ships[this.rotation.shipIndex].marker.closePopup();
			this.rotation.shipIndex++;
			if(this.rotation.shipIndex == this.ships.length) { this.rotation.shipIndex = 0;}
			
			this.mapResetToGlobalView();
		}
		
		else if(this.rotation.zoomLevel == 1) {

				this.map.setView([this.ships[this.rotation.shipIndex].latitude, this.ships[this.rotation.shipIndex].longitude], this.zoomLevel[this.rotation.zoomLevel]);
				this.ships[this.rotation.shipIndex].marker.openPopup();

		}
			
		// set the zoom level if nonzero
		else {
			
			this.map.setZoom(this.zoomLevel[this.rotation.zoomLevel]);

		}
		
		// console.log('zoomLevel = ' + this.zoomLevel[this.rotation.zoomLevel]);
		
	};
	this.mapRotate = mapRotate;
	
	
}
 
