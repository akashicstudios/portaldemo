function DomSatelliteTv() {
	
	this._domPlayButton = {};
	this._domPoster = {};	
	this._domTitle= {};
	this._domStory= {};
	
	
	function init() {
		
		var s = document.getElementsByClassName('vod-satellitetv').item(0);

		this._domPlayButton = s.getElementsByClassName('play-button').item(0);
		this._domPoster = s.getElementsByClassName('poster').item(0).children[1];
		this._domTitle = s.getElementsByClassName('title').item(0);
		this._domStory = s.getElementsByClassName('story').item(0);

	}
	this.init = init;
	
	
	
	function lookup(nid) {
		
		var s = window.AkashicStudios._pageController._controller._satellitetv_json.getSatelliteTv(nid);
			
		this._domPoster.src = s.poster();
		this._domTitle.innerHTML = s.title();
		this._domStory.innerHTML = s.story();
		
		
		this._domPlayButton.dataset.mediaid = s.media_id();
		this._domPlayButton.onclick = function(event) {
			event.preventDefault();
			window.open(window.AkashicStudios._system_defaults._defaults.modules.vod.vod_play_url.value+this.dataset.mediaid);
		};

		
	}
	this.lookup = lookup;

		
	
	
}

