





var itvViewDebarkInfo0Controller = new ItvView('itvViewDebarkInfo0');

itvViewDebarkInfo0Controller.viewItemDefaults.tagName = 'LI';
itvViewDebarkInfo0Controller.viewItemDefaults.navIdPrefix = itvViewDebarkInfo0Controller.cssClass + 'Item';
itvViewDebarkInfo0Controller.viewItemDefaults.inFocusCss = itvViewDebarkInfo0Controller.cssClass + '-list-item-focus';
itvViewDebarkInfo0Controller.html.scroll.direction = 'omni';
itvViewDebarkInfo0Controller.html.scroll.viewableItems = 3;

itvViewDebarkInfo0Controller.nav.dpad.back = 'itvMainMenu';
itvViewDebarkInfo0Controller.nav.dpad.left = 'itvViewMainMenuItem5';


itvViewDebarkInfo0Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewDebarkInfo0-nav-breadcrumb-settings', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewDebarkInfo0Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbSettings
});

itvViewDebarkInfo0Controller.init();
itvViewDebarkInfo0Controller.hide();

window.gNav.views['itvViewDebarkInfo0'] = itvViewDebarkInfo0Controller;
window.gNav.viewsZeroBased.push(itvViewDebarkInfo0Controller);



