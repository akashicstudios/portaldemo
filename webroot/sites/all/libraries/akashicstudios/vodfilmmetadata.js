function VodFilmMetaData() {
	
	this._json = null;

	function language(){
		return this._json.language;
	}
	this.language = language;

	function poster(){
		return this._json.poster_image_src;
	}
	this.poster = poster;

	function display_title(){
		return this._json.display_title;
	}
	this.display_title = display_title;

	function story(){
		return this._json.story;
	}
	this.story = story;

}
