

var itvViewLiveTv0Controller = new ItvView('itvViewLiveTv0');

itvViewLiveTv0Controller.viewItemDefaults.tagName = 'LI';
itvViewLiveTv0Controller.viewItemDefaults.navIdPrefix = itvViewLiveTv0Controller.cssClass + 'Item';
itvViewLiveTv0Controller.viewItemDefaults.inFocusCss = itvViewLiveTv0Controller.cssClass + '-list-item-focus';
itvViewLiveTv0Controller.html.scroll.direction = 'omni';

itvViewLiveTv0Controller.nav.dpad.back = 'itvMainMenu';
itvViewLiveTv0Controller.nav.dpad.left = 'itvViewMainMenuItem2';

itvViewLiveTv0Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewTvSeries1-nav-breadcrumb-livetv', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewLiveTv0Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbLiveTv
});


itvViewLiveTv0Controller.init();
itvViewLiveTv0Controller.hide();

window.gNav.views['itvViewLiveTv0'] = itvViewLiveTv0Controller;
window.gNav.viewsZeroBased.push(itvViewLiveTv0Controller);


