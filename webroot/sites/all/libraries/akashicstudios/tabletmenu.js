/*
 
 * 
 * */
function TabletMenu(){
	
	this._cssClass = '';
	this._cssClassItems = '';
	this._objNameMainContent = ''
	this._containerId = '';
	this._menuItemsDom = '';
	this._menuItemsZeroBased = new Array();
	
	this._isVisible = false;

	
	function init(){
		this.loadMenuItemsZeroBased();	
	}
	this.init = init;
	
	
	function hideMenu() {
		this._isVisible = true;
		this.toggleMenu();
	}
	this.hideMenu = hideMenu;
	
	
	
	function showMenu() {
		this._isVisible = false;
		this.toggleMenu();
	}
	this.showMenu = showMenu;
	
	
	
	function toggleMenu() {
		this._isVisible = !this._isVisible;
		
		if(this._isVisible) {
			document.getElementById(this._containerId).classList.add('hover');
			jQuery('.'+this._cssClass).show();
		}
		else {
			jQuery('.'+this._cssClass).hide();
			document.getElementById(this._containerId).classList.remove('hover');
		}

	}
	this.toggleMenu = toggleMenu;
	
	
	
	function loadMenuItemsZeroBased() {
		
		// loop through document and find items that have 'AkMenuitem' class marker, call init on each one
		this._menuItemsDom = document.getElementsByClassName(this._cssClassItems);
		if(typeof this._menuItemsDom != 'undefined') {
			
			for(var i=0; i< this._menuItemsDom.length; i++) {
				
				var mi = new TabletMenuItem();
				mi._domNode = this._menuItemsDom.item(i);
				mi._cssClass = this._cssClassItems;
				mi._parentMenu = this;
				mi.init();
				
				this._menuItemsZeroBased[i] = mi;
			}
		}	
	}
	this.loadMenuItemsZeroBased = loadMenuItemsZeroBased;

	
	
	
	function showHideMenuItems(isVisible) {
		this._menuItemsZeroBased.forEach(function(mi) {
			mi.showHide(isVisible);
		});
	}
	this.showHideMenuItems = showHideMenuItems;
	
	
	
	function showChildren(parentNavid) {
		this._menuItemsZeroBased.forEach(function(mi) {
			if(mi._parentNavid == parentNavid) {
				mi.showHide(true);
			}
		});
	}
	this.showChildren = showChildren;
	
	
	
	function showSiblings(navid) {
		var p = this.getParentNavid(navid);
		this.showChildren(p);
	}
	this.showSiblings = showSiblings;



	function getParentNavid(navid) {
		var retval = 0;
		this._menuItemsZeroBased.forEach(function(mi) {
			if(mi._navid == navid){
				retval = mi._parentNavid;
			}
		});
		return retval;
	}
	this.getParentNavid = getParentNavid;

	
	
}
