
function VodTvSeries() {
	
	this._json = '';
	
	this._episodes = '';
	
	
	function hasSeasons(){
		var retval = true;
		
		this._episodes.forEach(function(ep){
			if(isNaN(ep.season())) {
				retval = false;
			}
			else {
				if(ep.season()==0) {
					retval = false;
				}
			}
		});
	}
	this.hasSeasons = hasSeasons;
	
	
	function getSeasons(){
		var retval = null;
		
		var s = '';
		var a = new Array();
		this._episodes.forEach(function(ep){
			if(!isNaN(ep.season())){
				
				if((ep.season()!='0') && (ep.season() != s)) {
					s = ep.season();
					a.push(ep.season());
				}
			}
		});
		
		if(a.length>0){ retval = a; }
		
		return retval;
	}
	this.getSeasons = getSeasons;
	
	
	function nid(){
		return this._json.id;
	}
	this.nid = nid;
	
	
	function title(){
		return this._json.title;
	}
	this.title = title;
	
	
	function poster(){
		return this._json.poster_image_src;
	}
	this.poster = poster;
	
	
	function actors(){
		return this._json.actors;
	}
	this.actors = actors;

	
	function story(){
		return this._json.story;
	}
	this.story = story;
	
}
