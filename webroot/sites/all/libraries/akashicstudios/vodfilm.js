
function VodFilm() {
	
	this._json = '';
	
	this._metaData = new Array();
	this._mediaItems = new Array();
	
	this.getMediaItemLanguagesCSV = function(){
		var retval = '';
		this._mediaItems.forEach(function(m){
			if(retval.length>0){ retval +=',';}
			retval += m.language();
		});	
		return retval;		
	}
	
	function getMediaItem(language) {
		var retval = null;
		this._mediaItems.forEach(function(m){
			if(m.language().toLowerCase() == language.toLowerCase()) {
				retval = m;
			}
		});	
		return retval;		
	}
	this.getMediaItem = getMediaItem;
	

	function getMetaData(language) {
		var retval = null;
		this._metaData.forEach(function(m){
			if(m.language().toLowerCase() == language.toLowerCase()) {
				retval = m;
			}
		});
		if(retval == null) {
			if(this._metaData.length>0) {
				retval = this._metaData[0];
			}
		}
		return retval;		
	}
	this.getMetaData = getMetaData;

	
	function languages(){
		return this.getMediaItemLanguagesCSV();
	}
	this.languages = languages;
	
	function categories(){
		return this._json.categories;
	}
	this.categories = categories;
	
	function id(){
		return this._json.id;
	}
	this.id = id;

	function nid(){
		return this._json.id;
	}
	this.nid = nid;

	function year(){
		return this._json.year;
	}
	this.year = year;
	
	function directors(){
		return this._json.directors;		
	}
	this.directors = directors;

	function actors(){
		return this._json.actors;		
	}
	this.actors = actors;
	
	function rating(){
		return this._json.rating_image_src;		
	}
	this.rating = rating;

	function rating_code(){
		return this._json.rating_code;		
	}
	this.rating_code = rating_code;

	function runtime(){
		var retval = '';
		var rt = new Number(this._json.runtime);
		if(rt>0){
			rt = rt/60;
			retval = Math.floor(rt/60) + ':';
			var s = rt%60;
			if(s<10){retval+='0';}
			retval += s; 
		}
		return retval;		
	}
	this.runtime = runtime;
	
}
