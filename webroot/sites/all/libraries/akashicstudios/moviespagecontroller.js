function MoviesPageController(){

	this._movies_json = new MoviesJson();
	this._movies_json.init();

	this._dom_unique_movie = new DomUniqueMovie();
	this._dom_unique_movie.init();

	this._dom_film_carousel = new DomFilmCarousel();
	this._dom_film_carousel._cssClass = 'jcarousel-entertainment-movies';
	this._dom_film_carousel._cssClassLink = 'link-jcarousel-vodfilm';
	
	this._dom_movie_filters = new DomMovieFilters();
	this._dom_movie_filters.init();
		
	function init(){}
	this.init = init;
	
	
	this.onJsonLoaded = function(){
		
		this._dom_film_carousel.init();

		this._dom_film_carousel.jcarousel_moveFirst();

	}
}


	


