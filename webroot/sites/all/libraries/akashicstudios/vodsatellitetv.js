
function VodSatelliteTv() {
	
	this._json = '';
	
	
	function nid(){
		return this._json.id;
	}
	this.nid = nid;
	
	
	function title(){
		return this._json.title;
	}
	this.title = title;
	
	
	function poster(){
		return this._json.poster_image_src;
	}
	this.poster = poster;

	
	function story(){
		return this._json.story;
	}
	this.story = story;
	
	
	function media_id(){
		return this._json.media_id;
	}
	this.media_id = media_id;
}
