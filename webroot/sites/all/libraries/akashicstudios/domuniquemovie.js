function DomUniqueMovie() {
	
	this._domPlayButton = '';
	this._domPoster = '';	
	this._domYear= '';	
	this._domTitle= '';
	this._domDirectors= '';	
	this._domActors= '';	
	this._domStory= '';
	this._domRating= '';	
	this._domRuntime= '';
	this._domLanguageFlags = {};
	
	function init() {
		
		var vf = document.getElementsByClassName('vod-film').item(0);
		var vfmd = document.getElementsByClassName('vod-film-meta-data').item(0);
		var vfmi = document.getElementsByClassName('vod-film-media-item').item(0);

		this._domPlayButton = document.getElementById('movie-play-button');
		this._domPoster = document.getElementById('movie-poster-img');
		this._domYear = document.getElementById('movie-field-year');
		this._domTitle = document.getElementById('movie-field-title');
		this._domDirectors = document.getElementById('movie-field-directors');
		this._domActors = document.getElementById('movie-field-actors');
		this._domStory = document.getElementById('movie-field-story').children[1];
		this._domRating = document.getElementById('movie-field-rating-img');
		this._domRuntime = document.getElementById('movie-field-runtime');
		
		this._domLanguageFlags = document.getElementById('row-flags-inner');
		this._languageFlags = window.AkashicStudios.languages;
		
		var lf = document.getElementById('language-flag-def');
		this._languageFlags.forEach(function(l){
			var d = lf.cloneNode(true);
			d.id = 'language-flag-' + l.code;
			d.className = l.code;
			d.children[0].id = 'language-flag-img-' + l.code;
			d.children[0].src = l.flag_img_src;
			d.style.display = 'none';
			lf.parentElement.appendChild(d);
		});
		this._domLanguageFlags.removeChild(this._domLanguageFlags.children[0]);		
		
		
	}
	this.init = init;
	
	
	function lookup(nid) {
		
		var vf = window.AkashicStudios._pageController._controller._movies_json.getVodFilm(nid);
			
		this._domPoster.src = vf.getMetaData('en').poster();
		
		this._domTitle.innerHTML = vf.getMetaData('en').display_title();

		window.AkashicStudios.setField(this._domYear,vf.year(),'');
		window.AkashicStudios.setField(this._domRuntime,vf.runtime(),'');
		
		window.AkashicStudios.setField(this._domDirectors,vf.directors(),'Directed By: ');
		window.AkashicStudios.truncateField(this._domDirectors,'1');
		
		window.AkashicStudios.setField(this._domActors,vf.actors(),'Actors: ');
		window.AkashicStudios.truncateField(this._domActors,'1');
		
		this._domStory.innerHTML = '';
		this._domStory.innerHTML = vf.getMetaData('en').story();
		window.AkashicStudios.truncateField(this._domStory,'3');

		this._domRating.src = vf.rating();

		
		this._domPlayButton.dataset.nid = nid;
		this._domPlayButton.onclick = function(event) {
			event.preventDefault();
			event.stopPropagation();
			window.AkashicStudios._pageController._controller._dom_unique_movie.playMovieSelect(this.dataset.nid);
		};

		this.showHideLanguageFlags(vf);
		
	}
	this.lookup = lookup;

	
	function showHideLanguageFlags(vf) {
		var lf = this._domLanguageFlags.children;
		
		for(var i=0; i<lf.length; i++){
			var lang = lf[i].className;
			if(vf.getMediaItem(lang)!=null) {
				if(lf[i].style.display != undefined) {
					lf[i].style.removeProperty('display');
				}
			}
			else {
				lf[i].style.display = 'none';
			}
		}
		
	}
	this.showHideLanguageFlags = showHideLanguageFlags;
	
	
	
	function playMovieSelect(nid){
		
		
		var d = document.getElementById('vod-unique-movie-play-button-select-audio');
		if(d==null){
			d = document.createElement('DIV');
			d.id = 'vod-unique-movie-play-button-select-audio';
			d.style.display = 'none';
		}
		d.innerHTML = '';
		
		var sd = document.createElement('DIV');
		sd.className = 'item header';
		sd.innerHTML = 'Select Audio:';
		d.appendChild(sd);

		
		
		var vf = window.AkashicStudios._pageController._controller._movies_json.getVodFilm(nid);
		
		vf._mediaItems.forEach(function(mi,idx){
			var sd = document.createElement('DIV');
			sd.className = 'item';
			sd.dataset.mediaid = mi.media_id();
			
			if(idx%2==0){
				sd.classList.add('even');
			}

			sd.onclick = function(){
				event.preventDefault();
				event.stopPropagation();
				this.parentElement.style.display = 'none';
				window.open(window.AkashicStudios._system_defaults._defaults.modules.vod.vod_play_url.value+this.dataset.mediaid);
			};
			
			sd.innerHTML = window.AkashicStudios.local_languages[mi.language()];
			d.appendChild(sd);
			
		});
		
		this._domPoster.parentElement.insertBefore(d, this._domPoster.parentElement.firstChild);
		d.style.display = 'block';
		
		
	}
	this.playMovieSelect = playMovieSelect;
	
	
}

