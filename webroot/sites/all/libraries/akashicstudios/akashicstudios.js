
function AkashicStudios(){

	window['AkashicStudios'] = this;

	this.themePath = '/sites/all/themes/silversurfer';
	
	this._debug = new Debug();

	this._system_defaults = new SystemDefaults();

	this._ship_locations = new ShipLocations();

	this._ship_services = new ShipServices();

	
	this.onSystemDefaultsLoaded = function(){
		this._ship_locations.init();
	};
	
	this.onShipLocationsLoaded = function(){
		this._ship_services.init();
	};

	this.onShipServicesLoaded = function(){
		this.init();
	};
	
	
	this.init = function(){
		this._pageController = new PageController();
		this._pageController.init();
		
		if(this._pageController._controller.init != undefined) {
			this._pageController._controller.init();
		}
		
	};

	
	this._weekday = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
	
	this._themeColor = 'aqua';
	
	this.isMobile = function(){
		var retval = false;
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			 retval = true;
		}
		return retval;
	};

	this.languages = [
      {'code':'ja',
          'local_language' : '日本語',
        'flag_img_src' : '/sites/all/themes/silversurfer/img/languages/lang_flag_ja.png'},
        {'code':'fr',
            'local_language' : 'Français',
	        'flag_img_src' : '/sites/all/themes/silversurfer/img/languages/lang_flag_fr.png'},
        {'code':'es',
            'local_language' : 'Español',
	        'flag_img_src' : '/sites/all/themes/silversurfer/img/languages/lang_flag_es.png'},
        {'code':'de',
            'local_language' : 'Deutsch',
	        'flag_img_src' : '/sites/all/themes/silversurfer/img/languages/lang_flag_de.png'},
        {'code':'it',
            'local_language' : 'Italiano',
          'flag_img_src' : '/sites/all/themes/silversurfer/img/languages/lang_flag_it.png'},
        {'code':'en',
	        'local_language' : 'English',
	        'flag_img_src' : '/sites/all/themes/silversurfer/img/languages/lang_flag_en.png'}
      ];
	
	this.local_languages = {'en':'English',
	                        'de':'Deutsch',
	                        'es':'Español',
	                        'fr':'Français',
	                        'it':'Italiano',
	                        'ja':'日本語'};
	
	this.getWeekdaysToCurrent = function(){
		
		var dtm = new Date();
		var weekday = dtm.getDay();
		
		var retval = new Array();
		
		for(var i=weekday+1; i<=6; i++){
			retval.push(this._weekday[i]);
		}
		
		for(var i=0; i<=weekday; i++){
			retval.push(this._weekday[i]);
		}
		
		return retval;
		
	}
	
	this.getRuntime = function(runtime){
		var retval = '';
		var rt = new Number(runtime);
		if(rt>0){
			rt = rt/60;
			if(rt>=60){
				retval = Math.floor(rt/60) + ':';
				var s = rt%60;
				if(s<10){retval+='0';}
				retval += s;
			}
			else {
				var s = rt%60;
				retval = s + ' min';
			}
		}
		return retval;		

	};
	
	
	/**
	 * params {
	 * 
	 * 	id: // element id
	 * 	className 		// element classname
	 *  selectText		// text for the default select option or ''
	 *  options			// array of {'value':'','innerHTML':''} for the options
	 *  onchange		// onchange function object
	 *  
	 * }
	 */
	this.getSelect = function(params) {
		
		var div = document.createElement('DIV');
		div.id = params.id;
		div.className = params.className + ' select-' + this._themeColor;

		var s = document.createElement('SELECT');
		s.id = params.id + '-select';
		s.className = 'select-' + this._themeColor;

		if(params.selectText != '') {
			var op_0 = document.createElement('OPTION');
			op_0.value = '0';
			op_0.innerHTML = params.selectText;
			s.appendChild(op_0);
		}
		
		params.options.forEach(function(opt){
			var o = document.createElement('OPTION');
			o.value = opt.value;
			o.innerHTML = opt.innerHTML;
			s.appendChild(o);
		});

		s.onchange = params.onchange;
		
		div.appendChild(s);
		
		return div;
	}
	
	
	
	
	/**
	 * vars {
	 * 	type : 'filter' | 'select',			// specifies the 'Filter By:' or 'Select' text
	 * 	selects : [{select}, {select}]		// array of select elements from getSelect
	 * }
	 */
	this.getHeaderBannerFilter = function(vars){
		
		var me = this;
		
		var filterDiv = document.createElement('DIV');
		filterDiv.id = 'header-banner-filters';
		(vars.selects.length>1) ? filterDiv.className = 'double-filter' : filterDiv.className = 'single-filter';
		
		var textDiv = document.createElement('DIV');
		textDiv.className = 'text';
		(vars.type == 'filter') ? textDiv.innerHTML = 'Filter By:' : textDiv.innerHTML = 'Select:';
		
		filterDiv.appendChild(textDiv);
		
		for(var i=0; i<vars.selects.length; i++){
			sd = document.createElement('DIV');
			sd.className = 'select'+ i +' select-' + me._themeColor;
			sd.appendChild(vars.selects[i]);
			filterDiv.appendChild(sd);
		}
		
		document.getElementById('header-banner').appendChild(filterDiv);
		
	}
	
	

	
	this.truncateField = function(obj,lines){
		(function($,elem){
			$clamp(elem, {clamp: lines});
		}(jQuery,obj));
	}
	
	this.setField = function(field,value,label){
		var s = new String(value);
		if(s.length>0) {
			field.innerHTML = label + s;
			if(field.style.display != undefined){
				field.style.removeProperty('display');
			}
		}
		else {
			field.style.display = 'none';
		}		
	}

	
	this.currentDateAndTimeDivUpdate = function(){
		
		var currentDateAndTimeDiv = document.getElementById('currentDateAndTime');
		
		jQuery.getJSON('/json/datetime')
				.done( function( data ) {
					var da = data;
					currentDateAndTimeDiv.innerHTML = da['l'] + ' ' + da['M'] + ' ' + da['j'] + ', ' + da['Y'] + '<br/>' + da['g'] + ':' + da['i'] + ' ' + da['A'];
				})
				.fail(function( jqxhr, textStatus, error ) {
				    var err = textStatus + ", " + error;
				    console.log( "Request Failed: " + err );
			});
			
//		var t = setInterval(function(){
//		  window.AkashicStudios.currentDateAndTimeDivUpdate();
//		},300000);
		
	};
	
	

	this.convertDMS = function( lat, lng ) {
		
		var retval = Math.floor(Math.abs(lat)) + "' " + 
		    
		    // take only decimal, Multiply by 60, take only whole integer of answer for minutes
		    (Math.floor((Math.abs(lat)-Math.floor(Math.abs(lat)))*60)) + "\"" + 
		    
		    // if lat is +, North. if is -, South.
		    ((lat > 0) ? " N" : " S") + ", " +
		    
		    // do longitude
		    Math.floor(Math.abs(lng)) + "' " +
		    
		    // take only decimal, Multiply by 60, take only whole integer of answer for minutes
		    (Math.floor((Math.abs(lng)-Math.floor(Math.abs(lng)))*60)) + "\"" +
		    
		    // if lng is +, East. if is -, West.
		    ((lng > 0) ? " E" : " W");
		    
		   return retval;
	}
	
	
	
	
	
	
	this.jsonImagePreload = function(url){
		
		//vod_film_meta_data_posters
		
		jQuery.getJSON( url )
		.done(function(data){
			
			data.forEach(function(elem){
				var i = new Image();
				i.src = elem.src;
			});
			
		});
		
		
	}


	this.isIOSDevice = function() {
		
		var i = 0;
		var retval = false;
	    var iDevice = ['iPad', 'iPhone', 'iPod'];

		for ( ; i < iDevice.length ; i++ ) {
		    if( navigator.platform === iDevice[i] ){ retval = true; break; }
		}

		return retval;
		
	}

	
	this.isAndroidDevice = function() {
		
		/*
		// this is a possible test on user agent too, but might be concerned more with installability of .apk
	 	var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
		*/

		var i = 0;
		var retval = false;
	    var iDevice = ['Android'];

		for ( ; i < iDevice.length ; i++ ) {
		    if( navigator.platform === iDevice[i] ){ retval = true; break; }
		}

		return retval;

	}

	
	this.isApprovedDevice = function( aDevices ) {
		
		if(aDevices==null) {
			aDevices = ['iPad','iPad Simulator','iPhone','iPod','Android','Linux armv7l'];
		}
		var i = 0;
		var retval = false;

		for ( ; i < aDevices.length ; i++ ) {
		    if( navigator.platform === aDevices[i] ){ retval = true; break; }
		    //console.log (navigator.platform);
		}

		return retval;
	}


	
	this.getJson = function(jsonUrl, object){
			
		jQuery.getJSON( jsonUrl )
		.done(function(data){
			
			object.onJsonLoaded(data);
			
		})
		.fail(function( jqXHR, textStatus, errorThrown){
			console.log(textStatus + '\n' + errorThrown);
		});
		
	};
	
	
	
	
/**
 * BOOTSTRAP BEGINS HERE --------------------------------
 */	
	this._system_defaults.init();

/**
 * ------------------------------------
 */

}









/*
 GLOBALS
 * 
 * */

jQuery('document').ready(function(){
	var a = new AkashicStudios();
});




/*
GLOBALS
* 
* */


