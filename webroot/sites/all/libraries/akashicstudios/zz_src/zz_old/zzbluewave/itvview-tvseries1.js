

var itvViewTvSeries1Controller = new ItvView('itvViewTvSeries1');

itvViewTvSeries1Controller.viewItemDefaults.tagName = 'LI';
itvViewTvSeries1Controller.viewItemDefaults.navIdPrefix = itvViewTvSeries1Controller.cssClass + 'Item';
itvViewTvSeries1Controller.viewItemDefaults.inFocusCss = itvViewTvSeries1Controller.cssClass + '-list-item-focus';
itvViewTvSeries1Controller.html.scroll.direction = 'horizontal';

itvViewTvSeries1Controller.nav.dpad.back = 'itvViewTvSeries0';


itvViewTvSeries1Controller.icons.arrows.push(new ItvNavIcon({
	name: 'back', 
	cssClass : 'itvViewTvSeries1-nav-arrow-back', 
	top: 317, 
	left: 295, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewTvSeries1Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.back
}));

itvViewTvSeries1Controller.icons.arrows.push(new ItvNavIcon({
	name: 'forward', 
	cssClass : 'itvViewTvSeries1-nav-arrow-forward', 
	top: 317, 
	left: 1119, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewTvSeries1Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.forward
}));


itvViewTvSeries1Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewTvSeries1-nav-breadcrumb-tvseries', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewTvSeries1Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbTvSeries
});


itvViewTvSeries1Controller.html.position.maxWidth = 810;


itvViewTvSeries1Controller.init();
itvViewTvSeries1Controller.hide();

window.gNav.views['itvViewTvSeries1'] = itvViewTvSeries1Controller;
window.gNav.viewsZeroBased.push(itvViewTvSeries1Controller);


