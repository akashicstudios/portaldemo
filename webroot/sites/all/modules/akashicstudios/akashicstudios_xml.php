<?php
/**
 * (extremely) simple class to convert a DOMNode to an array.
 * @todo Add support for attributes, namespaces and other goodies...
 */



class AkashicXml
{
	

	public function loadXmlFile($strPath) {
	
		$retval = '';
	
		libxml_use_internal_errors(true);
		$doc = new DOMDocument();
	
		libxml_clear_errors();
		$doc->load($strPath);
	
		foreach(libxml_get_errors() as $error) {
			echo '<!-- '. $strPath.'-'.$error->message .' -->';
		}
	
		if($doc->hasChildNodes()) {
			$retval = $doc;
		}
	
		return $retval;
	}
	
	
	/**
	 * Checks if node has any children other than just text
	 *
	 * @param DOMNode
	 * @return boolean
	 */
	public static function nodeHasChild( $node )
	{
		if ( $node->hasChildNodes() )
		{
			foreach ( $node->childNodes as $child )
			{
				if ( $child->nodeType == XML_ELEMENT_NODE )
				{
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Takes a DOMNode (or a DOMNodeList) and returns it as an array
	 *
	 * @param DOMNode|DOMNodeList $item
	 * @return array
	 */
	public static function process( $xml )
	{
		if ( $xml instanceOf DOMNodeList )
		{
			$items = array();
			foreach ( $xml as $item )
			{
				$items[] = self::process( $item );
			}

			return $items;
		}

		$itemData = array();
		foreach ( $xml->childNodes as $node )
		{
			if ( self::nodeHasChild( $node ) )
			{
				$itemData[$node->nodeName] = self::process( $node );
			}
			else
			{
				$itemData[$node->nodeName] = $node->nodeValue;
			}
		}

		return $itemData;
	}
	
	
	
	
	public function getXmlValueByXpath($xmlFilePath,$xPath) {
		$retval = '';
			
		$doc = $this->loadXmlFile($_SERVER['DOCUMENT_ROOT'].'/'.$xmlFilePath);
		
		
		if($doc) {
			if($doc->hasChildNodes()) {
				
				$xQuery = '//'.$xPath;
					
				$domXPath = new DOMXPath($doc);
				$nodes = $domXPath->query($xQuery);
				if($nodes) {
					
					$node = $nodes->item(0);
					if($node) {
						
						$retval = $node->nodeValue;
						
					}
				}
			}
		}
					
		return $retval;
	}
	
	
	public Function updateXmlValueByXpath($xmlFilePath,$xPath,$newValue) {
		
		$strPath = $_SERVER['DOCUMENT_ROOT'].'/'.$xmlFilePath;
		
		$doc = $this->loadXmlFile($strPath);
		if($doc) {
			$xpath = new DOMXPath($doc);
			$nodes = $xpath->query('//'.$xPath);
			$node = $nodes->item(0);
			if($node) {
				$node->nodeValue = $newValue;
			}
			
			$doc->save($strPath);
			
		}
			
	}
	
	
	
}