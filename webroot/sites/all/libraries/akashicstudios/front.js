
function FrontPageController(){
	this._front_content_menu=new TabletMenu();
	this._front_content_menu._objName='_front_content_menu';
	this._front_content_menu._cssClass='front-content-menu';
	this._front_content_menu._cssClassItems='front-content-menu-item';
	this._front_content_menu._objNameMainContent='_front_content_menu';
	this._front_content_menu._containerId='';
	this._front_content_menu.init();
	
	function init(){
		var panels=this.getParameterByName('panels');
		if(panels!=''){
			this._front_content_menu.showHideMenuItems(false);
			this._front_content_menu.showChildren(panels);
		}
		else{
			this._front_content_menu.showChildren('0');
			window.setTimeout(function(){
				var ct = document.getElementById('content');
				if(ct!=null) {
					if(ct.style!=undefined) {
						ct.removeAttribute('style');
					}
				}				
			}, 600);
		}
		
		this.setIcsLoginUrls();
				
	}
	this.init=init;
	
	function getParameterByName(name){
		name=name.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
		var regex=new RegExp("[\\?&]"+name+"=([^&#]*)"),results=regex.exec(location.search);
		return results===null?"":decodeURIComponent(results[1].replace(/\+/g," "));
	}
	this.getParameterByName=getParameterByName;

	
	this.setIcsLoginUrls = function(){
		
		var links = document.getElementsByTagName('A');
		var icsLoginUrl = window.AkashicStudios._system_defaults._defaults.modules.MTN.icsloginurl.value;
		var icsFaqUrl = window.AkashicStudios._system_defaults._defaults.modules.MTN.icsfaqurl.value;
		
		for(var i=0; i<links.length; i++) {
			
			var l = links[i];
			var h = l.href;
			var newUrl = '';
			
			if(h == icsLoginUrl){
				
				var strurl = l.href;
				
				strurl += '?UI=' + Drupal.settings.silversurfer.MTN.UI 
				+ '&UIP=' + Drupal.settings.silversurfer.MTN.UIP 
				+ '&NI=' + Drupal.settings.silversurfer.MTN.NI 
				+ '&MA=' + Drupal.settings.silversurfer.MTN.MA
				+ '&SIP=' + Drupal.settings.silversurfer.MTN.SIP
				+ '&OS=' + Drupal.settings.silversurfer.MTN.OS;
				
				l.href = strurl;
				newUrl = strurl;
				
				l.target = "_blank";
				
			}
			
			if(h == icsFaqUrl){
				l.target = "_blank";
				newUrl = icsFaqUrl;
			}
			
			if(newUrl!='') {
				
				var d = l.parentElement.parentElement.parentElement;
				d.dataset.url = newUrl;
				d.onclick = function(event){
					event.preventDefault();
					window.open(this.dataset.url);
				};

			}
			
		}
		
	};
	
//	this.preloadMovieImages = function(){
//		
//		window.AkashicStudios.jsonImagePreload('/sql/json/vod_film_meta_data_posters()');
//		window.AkashicStudios.jsonImagePreload('/sql/json/vod_film_rating_img_src()');
//		
//	};
	

}