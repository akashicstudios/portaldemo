function ServicesPageController(){
	
	this._dom_services_carousel = new DomServicesCarousel();
	this._dom_services_carousel._cssClass = 'jcarousel-onboard-services';
	this._dom_services_carousel._cssClassLink = 'venue-image';

	this._dom_services = new DomServices();
	this._dom_services._parent = this;
	
	this._dom_services_filters = new DomServicesFilters();
	
	
	
	function init(){
		this._dom_services_carousel.init();
		this._dom_services_carousel.jcarousel_moveFirst();
		this._dom_services_filters.init();
	}
	this.init = init;
	
	
	

}