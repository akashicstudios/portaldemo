	<div id="header">

  		<?php print render($page['header_first']); ?>
  		<?php print render($page['header_second']); ?>
  		<?php print render($page['header_third']); ?>
		<div id="header-banner">
  			<?php print render($page['header_banner']); ?>
			<div id="header-banner-page-title">
				<?php print render($page['header_page_title']); ?>
			</div>
		</div>
  		  		  
	</div>
  
  	<script src="/sites/all/libraries/leaflet/leaflet.js"></script>
	<div id="content" class="onboard-fleettracker-content">

	    <div class="separator"></div>
	  
	  	<div id="content-top" class="content-row onboard-fleettracker-content-top">
	  	
		  	 <div class="region region-content-top onboard-fleettracker" style="">
	
	  		  	<div id="onboard-fleettracker-content-leaflet" class="onboard-fleettracker-content-leaflet">
			  	</div>
			
			</div>  	
		</div>
		
	    <div class="separator"></div>
				
	</div>

  
  
	<div id="footer">
  	
  		<?php print render($page['footer']); ?>
  	
	</div>
  
  
	<div id="content" style="background-color: #000; display: none;">
		<?php print render($page['content']); ?>
    	<?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
    	<?php endif; ?>
		<?php print render($page['content_messages']); ?>
	</div>

	
		
	