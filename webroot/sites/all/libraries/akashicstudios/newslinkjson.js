
function NewslinkJson() {
	
	this._json_titles = {};
	this._json_files = {};
	
	this._newslinkItems = new Array();
	
	
	this._weekdays = {};
	
	function init() {
		
		var me = this;
		
		jQuery.getJSON( '/sql/json/newslink_titles' )
		.done(function(data){
			
			me._json_titles = data;			

			me._json_titles.forEach(function(elem){
				var s = new Newslink();
				s._json = elem;
				me._newslinkItems.push(s);
			});

			
			jQuery.getJSON( '/json/newslink' )
			.done(function(data){
			
				me._json_files = data;
				
				me._json_files.forEach(function(elem){
					var nf = new NewslinkFile();
					nf._json = elem;
					
					me.appendNewslinkFiletoTitle(nf);
				});
				
				window.AkashicStudios._pageController._controller.newslinkjson_onload();
				
			})
			.fail(function( jqXHR, textStatus, errorThrown){
				console.log(textStatus + '\n' + errorThrown);
			});
	
		})
		.fail(function( jqXHR, textStatus, errorThrown){
			console.log(textStatus + '\n' + errorThrown);
		});

	}
	this.init = init;
	
	
	
	
	
	this.appendNewslinkFiletoTitle = function(nf){
		
		this._newslinkItems.forEach(function(nt){
			
			var fn = nf.filename();
			var fnm = nt.filename_mask();
			
			if(fn.indexOf(fnm)!=-1) {
				nt._newslinkFiles.push(nf);
				return;
			}
			
		});
		
	}
	
	
	this.getNewslinkItem = function(nid){
		
		var retval = null;
		this._newslinkItems.forEach(function(ni){
			if(nid == ni.nid()) {
				retval = ni;
			}
		});
		return retval;
		
	}
	
	
	
	function getTitlesByWeekday(weekday) {
		
		var retval = null;
		var count = 0;
		
		if(typeof this._weekdays[weekday] != 'undefined') {
			retval = this._weekdays[weekday];
		}
		else {
		
			this._newslinkItems.forEach(function(elem){
				if(elem.title().indexOf(weekday)>0) {
					if(count==0){
						retval = new Array();
					}
					count++;
					retval.push(elem);
				}
			});
		
		}
		
		return retval;
	}
	this.getTitlesByWeekday = getTitlesByWeekday;
	
	
	

}
