

var tabletViewMovies1Controller = new ItvView('tabletViewMovies1');

tabletViewMovies1Controller.viewItemDefaults.tagName = 'LI';
tabletViewMovies1Controller.viewItemDefaults.navIdPrefix = tabletViewMovies1Controller.cssClass + 'Item';
tabletViewMovies1Controller.viewItemDefaults.inFocusCss = tabletViewMovies1Controller.cssClass + '-list-item-focus';


tabletViewMovies1Controller.init();
//tabletViewMovies1Controller.hide();

tabletViewMovies1Controller.refreshNavArrowLimits();

window.gNav.views['tabletViewMovies1'] = tabletViewMovies1Controller;
window.gNav.viewsZeroBased.push(tabletViewMovies1Controller);

for (var i=0; i< tabletViewMovies1Controller.viewItemsZeroBased.length; i++) {
	
	var nid = tabletViewMovies1Controller.viewItemsZeroBased[i].nav.viewItemNid;
	tabletViewMovies1Controller.viewItemsZeroBased[i].domNode.dataset.viewitemnid = nid;
	tabletViewMovies1Controller.viewItemsZeroBased[i].domNode.dataset.cssclass = tabletViewMovies1Controller.viewItemsZeroBased[i].cssClass;
	tabletViewMovies1Controller.viewItemsZeroBased[i].domNode.onclick = function(){

		window.gNav.views['tabletViewMovies1'].blurViewItems();
		if(	 ! this.classList.contains(window.gNav.views['tabletViewMovies1'].viewItemDefaults.inFocusCss)  	) {		 
			this.classList.add(window.gNav.views['tabletViewMovies1'].viewItemDefaults.inFocusCss);		
		}
		window.gNav.views['tabletViewMovies2'].setViewItemsNidFilter(this.dataset.viewitemnid);
		jQuery('div.tabletViewMovies2').show();
				
		window.gNav.views['tabletViewMovies0'].domNode.scrollIntoView();
		
	};
}

/*
jQuery(".tabletViewMovies1FancyBox").fancybox({
	maxWidth	: 900,
	maxHeight	: 700,
	width		: 900,
	height		: 700,
	fitToView	: false,
	autoSize	: true,
	autoCenter	: true,
	closeClick	: false,
	openEffect	: 'none',
	closeEffect	: 'none',
	iframe		: {scrolling : 'no'},
	padding		: 10,
	margin		: 0,
	scrollOutside : false,
	scrolling : 'no'
});
*/