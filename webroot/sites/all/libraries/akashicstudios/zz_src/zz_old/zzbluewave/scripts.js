/*


*/

var themePath = '/sites/all/themes/red_cell_blue_wave';

window.onerror = function (msg, url, line) {
   console.log("Message : " + msg );
   console.log("url : " + url );
   console.log("Line number : " + line );
};



function convertDMS( lat, lng ) {
	
	var retval = Math.floor(Math.abs(lat)) + "' " + 
	    
	    // take only decimal, Multiply by 60, take only whole integer of answer for minutes
	    (Math.floor((Math.abs(lat)-Math.floor(Math.abs(lat)))*60)) + "\"" + 
	    
	    // if lat is +, North. if is -, South.
	    ((lat > 0) ? " N" : " S") + ", " +
	    
	    // do longitude
	    Math.floor(Math.abs(lng)) + "' " +
	    
	    // take only decimal, Multiply by 60, take only whole integer of answer for minutes
	    (Math.floor((Math.abs(lng)-Math.floor(Math.abs(lng)))*60)) + "\"" +
	    
	    // if lng is +, East. if is -, West.
	    ((lng > 0) ? " E" : " W");
	    
	   return retval;
}


function movieMenuButtonFooter_show(strClass) {
	jQuery(function($){
		$('.'+strClass+' .view-footer').show();
	});
}


function menuButtonFooter_hide(strClass) {
	jQuery(function($){
		$('.'+strClass+' .view-footer').hide();
	});
}


function menuButtonFooter_toggle(strClass) {
	jQuery(function($){

		$('.'+strClass+' .view-footer').toggle('normal', function(){

			if (!$('.'+strClass+' .view-footer').is(':visible')) {
				
			}

		});

	});
}

function hideAllFooters(strExceptionClass) {

	jQuery(function($){

		if (strExceptionClass!='mainPageMenuButtonSilversea') {
			if(  $('.mainPageMenuButtonSilversea .view-footer').is(':visible')	) {
				menuButtonFooter_hide('mainPageMenuButtonSilversea');
			}
		}

		if (strExceptionClass!='mainPageMenuButtonInternet') {
			if(  $('.mainPageMenuButtonInternet .view-footer').is(':visible')	) {
				menuButtonFooter_hide('mainPageMenuButtonInternet');
			}
		}

		if (strExceptionClass!='mainPageMenuButtonEntertainment') {
			if(  $('.mainPageMenuButtonEntertainment .view-footer').is(':visible')	) {
				menuButtonFooter_hide('mainPageMenuButtonEntertainment');
			}
		}

		if (strExceptionClass!='mainPageMenuButtonNews') {
			if(  $('.mainPageMenuButtonNews .view-footer').is(':visible')	) {
				menuButtonFooter_hide('mainPageMenuButtonNews');
			}
		}


	});


}


function setClickHandler(strClass) {
	jQuery(function($){
		
		var scrollOffset = $('.mainPageMenuButton.'+strClass+' .menuButtonBanner').height();
		
		switch(strClass) {
		
			case 'mainPageMenuButtonSilversea':
//				scrollOffset = 20;
				break;
				
			case 'mainPageMenuButtonInternet':
				scrollOffset = 20;
				break;
				
			case 'mainPageMenuButtonEntertainment':
				scrollOffset = 0;
				break;
				
			case 'mainPageMenuButtonNews':
				scrollOffset = 0;
				break;

			default:
				break;
		
		}
		
		
		$('.mainPageMenuButton.'+strClass+' .menuButtonBanner').click(function(){

			hideAllFooters(strClass);

			$('html, body').animate({
		        scrollTop: $(this).offset().top - scrollOffset
		    }, 1000);

			menuButtonFooter_toggle(strClass);
		});

	});
}


/*
function setCarouselScrollWidth(container,childTag) {
	jQuery(function($){

		childTag = typeof childTag !== 'undefined' ? childTag : 'LI';
		
		var children = $(container).find(childTag);
		
		console.log(children);
		console.log(children.length);
		console.log(children.first().outerWidth(true));

		if(children.length) {
			var width = (children.first().outerWidth(true) * children.length);
			$(container).css('width', width + "px");
		}

	});
}
*/

Drupal.behaviors.mainPageMenuButtons = {
	  attach: function (context, settings) {

	try {

		// handlers

		setClickHandler('mainPageMenuButtonInternet');
		setClickHandler('mainPageMenuButtonSilversea');
		setClickHandler('mainPageMenuButtonEntertainment');
		setClickHandler('mainPageMenuButtonNews');

		hideAllFooters('');


		}
		catch (err) {
			console.log(err);
			//alert(err);
		}

	}
};




function isIOSDevice () {
	
	var i = 0;
	var retval = false;
    var iDevice = ['iPad', 'iPhone', 'iPod'];

	for ( ; i < iDevice.length ; i++ ) {
	    if( navigator.platform === iDevice[i] ){ retval = true; break; }
	}

	return retval;
	
}

function isAndroidDevice () {
	
	/*
	// this is a possible test on user agent too, but might be concerned more with installability of .apk
 	var ua = navigator.userAgent.toLowerCase();
	var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
	*/

	var i = 0;
	var retval = false;
    var iDevice = ['Android'];

	for ( ; i < iDevice.length ; i++ ) {
	    if( navigator.platform === iDevice[i] ){ retval = true; break; }
	}

	return retval;

}

function isApprovedDevice( aDevices ) {
	if(aDevices==null) {
		aDevices = ['iPad','iPad Simulator','iPhone','iPod','Android','Linux armv7l'];
	}
	var i = 0;
	var retval = false;

	for ( ; i < aDevices.length ; i++ ) {
	    if( navigator.platform === aDevices[i] ){ retval = true; break; }
	    //console.log (navigator.platform);
	}

	return retval;
}

