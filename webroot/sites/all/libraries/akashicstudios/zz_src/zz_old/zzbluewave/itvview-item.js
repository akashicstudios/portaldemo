



/************************************************************************************************************************************************************/
//		ITV View Item 
/************************************************************************************************************************************************************/
	


//dpadnavidenter: is set if enter moves to another item		
//dpad: 	{left,up,right,down}	
function ItvViewItem() {

	this.navState = '';
	this.cssClass = '';
	this.domNode = '';

	this.itvViewParent = '';

	this.nav = {
		'visible' : true,
		'viewItemCategory' : '',
		'viewItemNid' : '',
		'viewItemParentNid' : '',
		'id' : '',
		'misc' : '',
		dpad : {
			'up' : '', 
			'left' : '', 
			'right' : '', 
			'down' : '' , 
			'back' : '',
			'enter': {
				'dpadNavId' : '',
				'url' : '',
				'urlTarget' : '',
				'view' : '',
				'viewCategoryFilter' : '',
				'viewMusicFilter' : '',
				'viewMusicBkgdImg' : '',
				'viewItemFocus' : ''
			}
		}
	};
	
	
	
	
	
	
	
	/************************************************************************************************************************************************************/
	
	function scrollIntoFocus() {
		
		try {
				
			var viewContent = this.domNode.parentElement.parentElement;
			
			var ulMarginLeft = 0 + Math.floor(this.domNode.parentElement.style.marginLeft.replace('px',''));
			var ulMarginTop = 0 + Math.floor(this.domNode.parentElement.style.marginTop.replace('px',''));
			
			var scrollerDivLeft = viewContent.offsetLeft;
			var leftEdge = scrollerDivLeft + this.domNode.offsetLeft;
			var rightEdge = leftEdge + this.domNode.offsetWidth;
			var scrollerDivRight = scrollerDivLeft + this.itvViewParent.html.position.maxWidth;		//
			
/*			console.log('scrollerDivLeft: '+scrollerDivLeft);
			console.log('leftEdge: '+leftEdge);
			console.log('scrollerDivRight: '+scrollerDivRight);
			console.log('rightEdge: '+rightEdge);		*/
			
			if(this.itvViewParent.html.scroll.direction == 'vertical-scroll') {

				if(this.itvViewParent.cssClass == "tabletViewMovies2") {
					
					jQuery('.'+this.cssClass).scrollintoview(true);
					
				}
				else {

					viewContent.id = this.itvViewParent.cssClass+ '-view-content';
	
					var contentTop = jQuery('#'+viewContent.id).position().top;
					var contentBottom = contentTop + viewContent.clientHeight; 
					var liTop = jQuery('.'+this.cssClass).position().top;
					var liBottom = liTop + this.domNode.clientHeight + (this.domNode.clientTop*2);
					var diff = 0;
					
					if(liTop < contentTop) {
						diff = contentTop - liTop;
						ulMarginTop += diff;
						ulMarginTop += 10;
						this.domNode.parentElement.style.marginTop = ulMarginTop + 'px';
					}
	
					if(liBottom > contentBottom) {
						diff = liBottom - contentBottom;
						ulMarginTop -= diff;
						ulMarginTop -= 20;
						this.domNode.parentElement.style.marginTop = ulMarginTop + 'px';
					}
					
					this.itvViewParent.refreshNavArrowLimits();
					
				}
				
			}
			
			if(this.itvViewParent.html.scroll.direction == 'vertical') {
				
			}

			if(this.itvViewParent.html.scroll.direction == 'omni') {
				
				if(viewContent.clientWidth != 1) {
				
					if(this.domNode.offsetTop >= viewContent.clientHeight) {
						this.domNode.parentElement.style.marginTop = ulMarginTop - (this.domNode.offsetTop - viewContent.clientHeight) - this.domNode.clientHeight + 'px';
					}

	// 				if(this.domNode.offsetTop = viewContent.clientHeight) {

	// 				}

					if(this.domNode.offsetTop < 0) {

						this.domNode.parentElement.style.marginTop = (ulMarginTop - this.domNode.offsetTop) + 'px';
					}
					
				}
				
				this.itvViewParent.refreshNavArrowLimits();
				
			}
			
			if(this.itvViewParent.html.scroll.direction == 'horizontal') {

				if(viewContent.clientWidth != 1) {

					if(rightEdge > scrollerDivRight) {
						this.domNode.parentElement.style.marginLeft = (ulMarginLeft - (rightEdge - scrollerDivRight)) + 'px';

					}

					if(leftEdge < scrollerDivLeft) {

						this.domNode.parentElement.style.marginLeft = (ulMarginLeft + (scrollerDivLeft - leftEdge)) + 'px';
					}
					
				}
				
			}
			
		} catch (err) {
			console.log(this.cssClass + ": ItvViewItem.scrollIntoFocus()  ERROR: " + err.message);
		}
		
	}
	this.scrollIntoFocus = scrollIntoFocus;
	
	
	
	
	
	
	
	
	
	/************************************************************************************************************************************************************/

	function focus() {

		// try {

			// NEW DIV ELEMENT
			//	- check to see if the newItvViewItem has a itvParentDiv that needs to be lit. (main menu items won't have one)
			//		- hide the body.dataset.divactive; show this.itvViewParent.show()  
			//		- update the body.dataset.dpadnavmode and body.dataset.divactive appropriate for this parent DIV
			if ( this.itvViewParent ) {
				this.itvViewParent.show();
				this.itvViewParent.focus();
				
			}


			if(typeof window.gNav != 'undefined') {
					
				// blur the menu view if nec.
				if (this.itvViewParent.html.region == 'content') {
					if(typeof window.gNav.menuView == 'Object') {
						window.gNav.menuView.blur();
					}		
				}
	
	
				//	update navState active & previous items
				window.gNav.navState.previous.viewItem = window.gNav.navState.active.viewItem;
				window.gNav.navState.active.viewItem = this;
	
	
	
				//	set the active menuItem if nec.
				if (this.itvViewParent.html.region == 'menu') {
					window.gNav.navState.active.menuItem = this;
					
					if(window.gNav.navState.active.menuItem.nav.dpad.enter.dpadNavId != '') {
						window.gNav.viewItems[window.gNav.navState.active.menuItem.nav.dpad.enter.dpadNavId].itvViewParent.show();
					}
				}
	
				
				// blur out the previous item
				if(window.gNav.navState.previous.viewItem instanceof ItvViewItem ) {
					
					// new item is the content & old item is content
					// new item is menu (blur any old item, whether menu or content)
					if ( 	((this.itvViewParent.html.region == 'content') && (window.gNav.navState.previous.viewItem.itvViewParent.html.region == 'content')) ||
						(this.itvViewParent.html.region == 'menu')	) {
						
							window.gNav.navState.previous.viewItem.blur();
							
					}
					
				}		
				
			}	

			//		- add newItvViewItem.inFocusCssClass to the new element with newItvViewItem.focus();
			//		- call the DOM focus() on the newItvViewItem.dpadnavid for good measure to set the document.activeElement
			if(	 ! this.domNode.classList.contains(this.itvViewParent.viewItemDefaults.inFocusCss)  	) {		 
				this.domNode.classList.add(this.itvViewParent.viewItemDefaults.inFocusCss);		
			}


			// set the scroll.index of the view to sync with the scroll index of this item.
			this.itvViewParent.setScrollIndex(this.cssClass);
			
			
			// set the focus in the HTML DOM (for posterity)
			this.domNode.focus();
			
			
 			this.scrollIntoFocus();
 			
// 			this.itvViewParent.refreshNavArrowLimits();



	
		// } catch(err){
			// console.log(this.cssClass + ": ItvViewItem.focus()  ERROR: " + err.message);
		// }	
			
	}
	this.focus = focus;


	

	/************************************************************************************************************************************************************/

	function blur() {

		try {
			
			if(	 this.domNode.classList.contains(this.itvViewParent.viewItemDefaults.inFocusCss)  	) {		 
				this.domNode.classList.remove(this.itvViewParent.viewItemDefaults.inFocusCss);		
			}
			

		} catch(err) {
			console.log(this.cssClass + ": ItvViewItem.focus()  ERROR: " + err.message);
		}	


	}

	this.blur = blur; 






	/************************************************************************************************************************************************************
	 * updates nav.dpad attributes, setting the specified direction to the new elementID
	 * */
	function setNavOnElement(direction,dpadnavidNew) {
		
		try {

			this.nav.dpad[direction] = dpadnavidNew;
			
		}
		catch (err) {
			console.log("setNavOnElement()  ERROR: " + err.message);
		}

	}
	this.setNavOnElement = setNavOnElement;











	/************************************************************************************************************************************************************
	 * 
	 * */
	function setViewItemCategoryFilter(categoryFilter) {
		
		try {

			if(categoryFilter != '') {
				
				if(this.nav.viewItemCategory.indexOf(categoryFilter) == -1) {

					this.domNode.style.display = 'none';
					this.nav.visible = false;

				}
				
			}
			
			else {
				
				this.domNode.style.removeProperty('display');
				this.nav.visible = true;
				
			}
			
		}
		catch (err) {
			console.log("setViewItemCategoryFilter()  ERROR: " + err.message);
		}

	}
	this.setViewItemCategoryFilter = setViewItemCategoryFilter;
	




	/************************************************************************************************************************************************************
	 * 
	 * */
	function setViewItemNidFilter(nid) {
		
		try {

			if(nid != '') {
				
				if(this.nav.viewItemNid != nid) {

					this.domNode.style.display = 'none';
					this.nav.visible = false;

				}
				
				else {

					this.domNode.style.removeProperty('display');
					this.nav.visible = true;

				}
			
			}
			
		}
		catch (err) {
			console.log("setViewItemNidFilter()  ERROR: " + err.message);
		}

	}
	this.setViewItemNidFilter = setViewItemNidFilter;




	/************************************************************************************************************************************************************
	 * 
	 * */
	function setViewItemParentNidFilter(nid) {
		
		try {

			if(nid != '') {
				
				if(this.nav.viewItemParentNid != nid) {

					this.domNode.style.display = 'none';
					this.nav.visible = false;

				}
				
				else {

					this.domNode.style.removeProperty('display');
					this.nav.visible = true;

				}
			
			}
			
		}
		catch (err) {
			console.log("setViewItemVodParentItemFilter()  ERROR: " + err.message);
		}

	}
	this.setViewItemParentNidFilter = setViewItemParentNidFilter;
	
	
	
	
	
	/************************************************************************************************************************************************************
	 * 
	 * */
	function setViewItemLanguageFilter(languageFilter) {
		
		try {

			// get the languageFilter from the Drupal object
			var lf = 'English';
			
			// check & filter objects in 
			var fieldData = this.domNode.getElementsByClassName('itvVodFilmMetaData-fieldData') || '';
			if(fieldData != '') {
				
				for(var i=0; i< fieldData.length; i++) {
												
					if( fieldData[i].dataset.language != lf ) {

						fieldData[i].style.display = 'none';

					}

					else {

						fieldData[i].style.removeProperty('display');

					}
					
				}
				
			}
			
		}
		catch (err) {
			console.log("setViewItemCategoryFilter()  ERROR: " + err.message);
		}

	}
	this.setViewItemCategoryFilter = setViewItemCategoryFilter;


	
	
	
	
	
	
	
	/************************************************************************************************************************************************************/
	/*
	// ENTER keycode processing - 
	*/
	function processENTER() {
		
		try {
			
			event.preventDefault();
			

					
			//	- check the URL field, if present, check Target & call either window.open() || window.location.assign()
			if( this.nav.dpad.enter.url.length > 0 ) { 						
				
				if( this.nav.dpad.enter.urlTarget == '_blank') {
					window.open(this.nav.dpad.enter.url);
				}
				else {
//					window.setTimeout(function(){document.location.href = this.nav.dpad.enter.url;},1500);
					
//					window.gNav.menuView.viewItemsZeroBased[0].focus();
//					window.gNav.keyPress(66);

// 					console.log('window.location.href = '+ this.nav.dpad.enter.url);
					window.location.href = this.nav.dpad.enter.url;
					
//					document.location.href = '/?q=itv&playUrl='+this.nav.dpad.enter.url;
					
				}
				
			}


			//	- check the view field, setup the new view & load it
			else if( this.nav.dpad.enter.view.length > 0 ) { 						
				
				// set the category filter on the new view if nec.
				var cf = this.nav.dpad.enter.viewCategoryFilter || '';
				if(cf == '') {
					cf = this.itvViewParent.nav.categoryFilter;
				}
				
				if( cf != '' ) {
					
					window.gNav.views[this.nav.dpad.enter.view].clearViewItemsCategoryFilter();
					window.gNav.views[this.nav.dpad.enter.view].setViewItemsCategoryFilter(cf);
				}
				
				
				
				if ( (this.nav.dpad.enter.view == 'itvViewMovies3') || (this.nav.dpad.enter.view == 'itvViewTvSeries2') ) {

					// filter down itvMovies3
					window.gNav.views[this.nav.dpad.enter.view].setViewItemsParentNidFilter(this.nav.viewItemNid);

				}

				
				//setup jwPlayer with playlist & play
				if (this.nav.dpad.enter.view == 'itvViewMusic1') {
					
					window.gNav.views[this.nav.dpad.enter.view].domNode.style.backgroundImage = "url('" + Drupal.settings.redCell.theme + '/img/musicCategory/bg-' + this.nav.dpad.enter.viewMusicFilter.toLowerCase().replace(' ', '-') + ".jpg')";
					window.gNav.views[this.nav.dpad.enter.view].jwPlayer(this.nav.dpad.enter.viewMusicFilter);	//call the playlist & loadPlaylist() functions
					
				}

			
				// if itvViewMovies3 has only 1 movie language, play the movie, if not, load itvViewMovies3 with language selections:
				if (	(this.nav.dpad.enter.view == 'itvViewMovies3') 	&&	(window.gNav.views[this.nav.dpad.enter.view].viewItemsVisible.length == 1)	) {

					document.location.href = window.gNav.views[this.nav.dpad.enter.view].viewItemsZeroBased[window.gNav.views[this.nav.dpad.enter.view].viewItemsVisible[0]].nav.dpad.enter.url;
				}

				
				else {

					this.itvViewParent.dim();

					// if there is a viewItemFocus specified, focus on it & move there,
					if(this.nav.dpad.enter.viewItemFocus != '') {

						window.gNav.views[this.nav.dpad.enter.view].focusOnViewItemNodeId(this.nav.dpad.enter.viewItemFocus);

					}

					else {

						// or else move to the first viewItem
						window.gNav.views[this.nav.dpad.enter.view].moveFirst();

					}
					
				}

				
				
			}

			
			else {
				
				//	load the requested viewItem
				if(this.nav.dpad.enter.dpadNavId) {
					
// 					if(window.gNav.navState.activeRegion == 'content') {
// 						window.gNav.navStack.push(this.itvViewParent);
// 					}
// 					window.gNav.viewItems[this.nav.dpad.enter.dpadNavId].itvViewParent.clearViewItemsCategoryFilter();
					window.gNav.viewItems[this.nav.dpad.enter.dpadNavId].focus();
				}
		
				
			}
			
			
		} catch (err) {
			console.log(err.message);
		}

	}
	this.processENTER = processENTER;
	
	
	
}

