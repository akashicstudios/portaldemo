
function PageController() {

	this._bIsItv = false;
	this._bIsTablet = true;
	
	this._controller = {};

	this._header_dropmenu = {};
	this._header_dropmenu_phone = {};
	
	
	this.init = function(){
		
		this.loadHeaders();
		
		this.pageControllerInit();

		this.setDocumentClickHandlers();
		
		if(!this._bIsItv) {
			window.AkashicStudios._debug.getViewportStats();
		}
		
	}


	this.loadHeaders = function(){
		
		if(this._bIsTablet) {
			
			if(typeof TabletMenu != 'undefined') {
				this._header_dropmenu = new TabletMenu();
				this._header_dropmenu._cssClass = 'header-dropmenu';
				this._header_dropmenu._cssClassItems = 'header-dropmenu-item';
				this._header_dropmenu._objNameMainContent = '_front_content_menu';
				this._header_dropmenu._containerId = 'header-third';
				this._header_dropmenu.init();
			}
			
			
			this._header_dropmenu_phone = new DropMenu();
			this._header_dropmenu_phone._cssClass = 'header-second-dropmenu-telephone';
			this._header_dropmenu_phone._containerId = 'header-second-dropmenu-telephone';
			this._header_dropmenu_phone._buttonId = 'header-second-icon-telephone';
			this._header_dropmenu_phone._json_uri = '/sql/json/ship_services()';
			this._header_dropmenu_phone._menuType = 'telephone_list';
			this._header_dropmenu_phone.init();
		}
		
	}
	
	
	this.pageControllerInit = function(){
		//switch on page url and call the appropriate controller
		switch(window.location.pathname) {
		
		case '/':
			this._controller = new FrontPageController();
			
			window.setTimeout(function(){
				window.AkashicStudios.jsonImagePreload('/sql/json/vod_film_meta_data_posters()');
				window.AkashicStudios.jsonImagePreload('/sql/json/vod_film_rating_img_src()');				
			}, 5000);
			break;
			
//		case '/onboard/chronicle':
//			this._controller = new ShipguidePageController();
//			break;
			
		case '/onboard/services':
			this._controller = new ServicesPageController();
			break;

		case '/onboard/fleettracker':
			this._controller = new FleettrackerPageController();
			break;
			
		case '/entertainment/movies':
			this._controller = new MoviesPageController();
			break;
			
		case '/entertainment/television':
			this._controller = new TelevisionPageController();
			break;

		case '/entertainment/music':
			this._controller = new MusicPageController();
			break;

		case '/news/newslink':
			this._controller = new NewslinkPageController();
			break;
			
		case '/news/pressreader':
			this._controller = new PressreaderPageController();
			break;
			
		case '/itv':
			this._controller = new ItvPageController();
			this._bIsItv = true;
			this._bIsTablet = false;
			break;
			
			
		case '/user/login':
		case '/import':
			break;
			
		default:
			window.location.href = '/';
			
		}		
	}
	
	
	
	
	this.setDocumentClickHandlers = function(){
		
		if(this._bIsTablet) {
			
			document.getElementById(this._header_dropmenu._containerId).onclick = function(event){
				window.AkashicStudios._pageController._header_dropmenu.toggleMenu();
				event.stopPropagation();
				
				if(window.AkashicStudios._pageController._header_dropmenu_phone._isVisible) {  
					window.AkashicStudios._pageController._header_dropmenu_phone.toggleMenu();
				}
			};
	
			
			document.getElementById(this._header_dropmenu_phone._buttonId).onclick = function(event){
				window.AkashicStudios._pageController._header_dropmenu_phone.toggleMenu();
				event.stopPropagation();
				
				if(window.AkashicStudios._pageController._header_dropmenu._isVisible) {  
					window.AkashicStudios._pageController._header_dropmenu.toggleMenu();
				}
			};
		
			
			document.getElementById('header-second-icon-home').onclick = function(event){
				window.location.href = '/';
			};
				
			
			
			document.getElementsByTagName('HTML').item(0).onclick = function(){
				
				if(window.AkashicStudios._pageController._header_dropmenu._isVisible) {  
					window.AkashicStudios._pageController._header_dropmenu.toggleMenu();
				}
				
				if(window.AkashicStudios._pageController._header_dropmenu_phone._isVisible) {  
					window.AkashicStudios._pageController._header_dropmenu_phone.toggleMenu();
				}

				var d = document.getElementById('vod-unique-movie-play-button-select-audio');
				if(d!=null){
					d.style.display = 'none';
				}
			};
			
		}
		
		
	}

	
	
	this.preventHorizontalGestureScroll = function(obj) {
		obj.addEventListener('MozMousePixelScroll', function(evt){
		    if(evt.axis === evt.HORIZONTAL_AXIS){
		        evt.preventDefault();
		    }
		}, false);
	}


	

	
	
	
}






