function DomTvSeries() {
	
	this._domPoster = {};	
	this._domTitle= {};
	this._domActors= {};	
	this._domStory= {};
	this._domEpisodes= {};
	
	
	function init() {
		
		var ts = document.getElementById('entertainment-television-content-tvseries');

		this._domPoster = ts.getElementsByClassName('poster').item(0).children[0];
		this._domTitle = ts.getElementsByClassName('title').item(0);
		this._domActors = ts.getElementsByClassName('actors').item(0);
		this._domStory = ts.getElementsByClassName('story').item(0).children[1];
		this._domEpisodes = ts.getElementsByClassName('episodes').item(0);

	}
	this.init = init;
	
	
	
	function lookup(nid) {
		
		var vts = window.AkashicStudios._pageController._controller._tvseries_json.getTvSeries(nid);
			
		this._domPoster.src = vts.poster();
		this._domTitle.innerHTML = vts.title();
		
		window.AkashicStudios.setField(this._domActors,vts.actors(),'Actors: ');
		window.AkashicStudios.truncateField(this._domActors,'1');

		this._domStory.innerHTML = '';
		this._domStory.innerHTML = vts.story();
		window.AkashicStudios.truncateField(this._domStory,'3');

		this.seasonSelects(nid);

	}
	this.lookup = lookup;

		
	
	
	function seasonSelects(nid){
		
		var ts = window.AkashicStudios._pageController._controller._tvseries_json.getTvSeries(nid);
		
		var seasons = ts.getSeasons();
		
		if(seasons != null) {
			
			this._domEpisodes.innerHTML = '';
			
			for(var i=0; i<seasons.length; i++) {
				
				var s = document.createElement('SELECT');
				s.className = 'season-'+ seasons[i] + ' select-aqua';
				var d = document.createElement('DIV');
				d.className = 'episodes-select select-aqua';
				
				
				var op_0 = document.createElement('OPTION');
				op_0.value = '';
				op_0.innerHTML = 'Season ' + seasons[i];
				s.appendChild(op_0);
				
				ts._episodes.forEach(function(ep){
					if(ep.season() == seasons[i]) {
						var op = document.createElement('OPTION');
						op.value = ep.media_id();
						op.innerHTML = '<span class="numbering">'+ep.episode() + '.&nbsp;&nbsp;&nbsp;</span><span class="title">' + ep.title() + '</span>';
						s.appendChild(op);
					}
				});
	
				s.onchange = function(){
					event.preventDefault();
					event.stopPropagation();
					if(this.selectedIndex>0){
						window.open(window.AkashicStudios._system_defaults._defaults.modules.vod.vod_play_url.value+this.options[this.selectedIndex].value);
					}
					this.selectedIndex=0;
				};
				
				d.appendChild(s);
	
				this._domEpisodes.appendChild(d);
				
			}
				
			
		}
		
		
		else {
			var s = document.createElement('SELECT');
			s.className = 'select-aqua';
			var d = document.createElement('DIV');
			d.className = 'episodes-select select-aqua';
			
			var op = document.createElement('OPTION');
			op.value = '';
			op.innerHTML = 'Episodes';
			s.appendChild(op);
			
			ts._episodes.forEach(function(ep){
				var op = document.createElement('OPTION');
				op.value = ep.media_id();
				op.innerHTML = ep.title();
				s.appendChild(op);
			});

			s.onchange = function(){
				event.preventDefault();
				event.stopPropagation();
				if(this.selectedIndex>0){
					window.open(window.AkashicStudios._system_defaults._defaults.modules.vod.vod_play_url.value+this.options[this.selectedIndex].value);
				}
				this.selectedIndex=0;
			};
			
			d.appendChild(s);

			this._domEpisodes.innerHTML = '';
			this._domEpisodes.appendChild(d);	
		}
		
		
	}
	this.seasonSelects = seasonSelects;
	
	
}

