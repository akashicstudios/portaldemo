

var itvViewTvSeries0Controller = new ItvView('itvViewTvSeries0');

itvViewTvSeries0Controller.viewItemDefaults.tagName = 'LI';
itvViewTvSeries0Controller.viewItemDefaults.navIdPrefix = itvViewTvSeries0Controller.cssClass + 'Item';
itvViewTvSeries0Controller.viewItemDefaults.inFocusCss = itvViewTvSeries0Controller.cssClass + '-list-item-focus';
itvViewTvSeries0Controller.html.scroll.direction = 'omni';

itvViewTvSeries0Controller.nav.dpad.back = 'itvMainMenu';
itvViewTvSeries0Controller.nav.dpad.left = 'itvViewMainMenuItem1';


itvViewTvSeries0Controller.icons.arrows.push(new ItvNavIcon({
	name: 'up', 
	cssClass : 'itvViewTvSeries0-nav-arrow-up', 
	top: 65, 
	left: 715, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewTvSeries0Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.up
}));

itvViewTvSeries0Controller.icons.arrows.push(new ItvNavIcon({
	name: 'down', 
	cssClass : 'itvViewTvSeries0-nav-arrow-down', 
	top: 565, 
	left: 715, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewTvSeries0Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.down
}));


itvViewTvSeries0Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewTvSeries0-nav-breadcrumb-tvseries', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewTvSeries0Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbTvSeries
});

itvViewTvSeries0Controller.init();
itvViewTvSeries0Controller.hide();

window.gNav.views['itvViewTvSeries0'] = itvViewTvSeries0Controller;
window.gNav.viewsZeroBased.push(itvViewTvSeries0Controller);


