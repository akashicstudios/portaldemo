	<div id="header">
	
  		<?php print render($page['header_first']); ?>
  		<?php print render($page['header_second']); ?>
  		<?php print render($page['header_third']); ?>
		<div id="header-banner">
  			<?php print render($page['header_banner']); ?>
			<div id="header-banner-page-title">
				<?php print render($page['header_page_title']); ?>
			</div>
		</div>
  		  			  
	</div>	
  
	<div id="content" class="news-newslink-content">

	    <div class="separator"></div>
	  
	  	<div id="content-top" class="news-newslink-content-top">
	  		<div class="newslink-logo">
	  			<img src="/sites/all/themes/silversurfer/img/tablet/news/newslink-logo.jpg">
	  		</div>
	  		
  			<div id="jcarousel-news-newslink" class="jcarousel-news-newslink">
				<ul class="jcarousel-list">
					<li id="jcarousel-news-newslink-item" class="jcarousel-news-newslink-item" style="display: none;">
						<div class="item-text title"></div>
						<div class="item-text description"></div>
					</li>
				</ul>
			</div>
	  		
  		</div>
		
	    <div class="separator"></div>
				
		<div id="content-bottom" class="news-newslink-content-bottom">

			<div style="opacity: 0.15;"><img id="news-newslink-img0" class="newslink-day-img" src="" ></div>
			<div style="opacity: 0.25;"><img id="news-newslink-img1" class="newslink-day-img" src="" ></div>
			<div style="opacity: 0.40;"><img id="news-newslink-img2" class="newslink-day-img" src="" ></div>
			<div style="opacity: 0.55;"><img id="news-newslink-img3" class="newslink-day-img" src="" ></div>
			<div style="opacity: 0.70;"><img id="news-newslink-img4" class="newslink-day-img" src="" ></div>
			<div style="opacity: 0.85;"><img id="news-newslink-img5" class="newslink-day-img" src="" ></div>
			<div style="opacity: 1.00;"><img id="news-newslink-img6" class="newslink-day-img" src="" ></div>

		</div>
		
	    <div class="separator"></div>
				
	</div>

  
  
	<div id="footer">
  	
  		<?php print render($page['footer']); ?>
  	
	</div>
  
	<div id="content-hidden" style="background-color: #000; display: none;">
		<?php print render($page['content']); ?>
		<?php print render($page['content_top']); ?>
		<?php print render($page['content_bottom']); ?>
     	<?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      	<?php endif; ?>
    	<?php print render($page['content_messages']); ?>
	</div>

	
		
	