

var itvViewMusic1Controller = new ItvView('itvViewMusic1');

itvViewMusic1Controller.viewItemDefaults.tagName = 'LI';
itvViewMusic1Controller.viewItemDefaults.navIdPrefix = itvViewMusic1Controller.cssClass + 'Item';
itvViewMusic1Controller.viewItemDefaults.inFocusCss = itvViewMusic1Controller.cssClass + '-list-item-focus';

itvViewMusic1Controller.nav.dpad.back = 'itvViewMusic0';


itvViewMusic1Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewMusic1-nav-breadcrumb-music', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewMusic1Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbMusic
});


itvViewMusic1Controller.html.position.maxWidth = 820;
itvViewMusic1Controller.html.isJWPlayer = true;

itvViewMusic1Controller.init();
itvViewMusic1Controller.hide();

window.gNav.views['itvViewMusic1'] = itvViewMusic1Controller;
window.gNav.viewsZeroBased.push(itvViewMusic1Controller);



/************************************************************************************************************************************************************/
// JWPlayer setup & init
/************************************************************************************************************************************************************/

itvViewMusic1Controller.jwPlayer = function(cat) {

	window.itvViewMusic1JWPlayer0 = new ItvJWPlayer('itvViewMusic1JWPlayer0');
	window.itvViewMusic1JWPlayer0.category = cat;
	window.itvViewMusic1JWPlayer0.skin = '/sites/all/themes/red_cell_blue_wave/jwskin/bluewave/bluewave-itv.xml';

	var strCat = new String(cat);
	
	var url = Drupal.settings.redCell.musicFeedPath + '/' + strCat.replace(' ', '-').toLowerCase() + '.xml';

	window.itvViewMusic1JWPlayer0.playlistUrl = url;
	
	window.itvViewMusic1JWPlayer0.setupRandomRepeat();
	

};
