	<div id="header">

  		<?php print render($page['header_first']); ?>
  		<?php print render($page['header_second']); ?>
  		<?php print render($page['header_third']); ?>
		<div id="header-banner">
  			<?php print render($page['header_banner']); ?>
			<div id="header-banner-page-title">
				<?php print render($page['header_page_title']); ?>
			</div>
		</div>
  
	</div>

  
	<div id="content" class="onboard-services-content">

	    <div class="separator"></div>
	  
	  	<div id="content-top" class="onboard-services-content-top">
	
			<div id="jcarousel-onboard-services" class="jcarousel-onboard-services">
				<ul class="jcarousel-list">
					<li id="jcarousel-onboard-services-item" class="jcarousel-item" style="display: none;">
						<div class="venue-image">
							<img src="" height="330" width="880">
							<div class="location"></div>
							<div class="text">
								<div class="description"></div>
								<div class="long_description"></div>
							</div>
						</div>
					</li>
				</ul>
			</div>

  		</div>
		
	    <div class="separator"></div>
				
		<div id="content-bottom" class="onboard-services-content-bottom">

			<img id="onboard-services-content-bottom-deckplan-img" src="" width="880">
		
		</div>
		
	    <div class="separator"></div>
				
	</div>

  
  
	<div id="footer">
  	
  		<?php print render($page['footer']); ?>
  	
	</div>
  
  
	<div id="content" style="background-color: #000; display: none;">
		<?php print render($page['content']); ?>
    	<?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
    	<?php endif; ?>
		<?php print render($page['content_messages']); ?>
	</div>

	
		
	