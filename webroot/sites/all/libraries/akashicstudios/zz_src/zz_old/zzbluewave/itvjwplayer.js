/************************************************************************************************************************************************************/
//		ITV JW Player 		
/************************************************************************************************************************************************************/

function ItvJWPlayer(playerId) {
	

	this.playerId = playerId;
	this.category = '';
	this.playlistUrl = '';
	this.playList = '';

	this.domTitle = document.getElementById(this.playerId+'Title');
	this.domDescription = document.getElementById(this.playerId+'Description');
	this.domHeader = document.getElementById(this.playerId+'HeaderText');
	this.domContainer = document.getElementById(this.playerId+'Container');
	this.domWrapper = '';
	this.domImgLoading = document.getElementById(this.playerId+'ImageLoading');
	this.domClickMsg = document.getElementById(this.playerId+'ClickMsg');
	
	this.scrubLeft = 0;
	this.scrubRight = 0;
	
	this.primary = 'flash';
	this.autostart = true;
	this.audioBackground = '';
	
	this.volume = 100;
	
	
	this.path = '/sites/all/libraries/jwplayer6.8';
	
	this.skin = '';
	
	function setupRandomRepeat() {
		
		// try {
		
			console.log('playlistUrl - '+this.playlistUrl);
			
			this.clearWindowText();
			this.showLoadingImg(true);

			jwplayer(this.playerId).setup({
				width: '100%', 
				height: '100%',
				primary: this.primary,
				playlist: this.playlistUrl,
				controls : true,
				skin: this.skin,
				displaytitle : false,
				autostart : false,
				events : {
					
					onComplete : function () {
						var playlist = this.getPlaylist();
						var randomnumber=Math.floor(Math.random()* playlist.length);
						this.playlistItem(randomnumber);
						
						console.log('onComplete');
					}
					
					
					, onError : function (){
						window[this.id].errMsg();
						// console.log('onError - '+ this.message);
						window[this.id].showLoadingImg(true);
						window[this.id].remove();
					}
					
					
					
					, onPlaylist : function(){
						var pl = this.getPlaylist();
			
						for(var i=0; i<pl.length; i++) {
							pl[i].image = window[this.id].audioBackground;
						}
						
						console.log(pl);
			
					}
					
					
					
					, onPlaylistItem : function (){
						
						// var playlist = jwplayer(this.playerId).getPlaylist();
						// var i = jwplayer(this.playerId).getPlaylistIndex();
						window[this.id].updateWindowText();
						
						// console.log('onPlaylistItem');
						
					}
					
					
					, onReady : function (){
						
						var playlist = this.getPlaylist();
						var randomnumber=Math.floor(Math.random()* playlist.length);
						this.playlistItem(randomnumber);
						console.log('ready');
					}
					
					
					, onPlay : function (){
						window[this.id].showLoadingImg(false);
						window[this.id].hideClickMsg();
					}
					
					
					, onBuffer : function (){
						window[this.id].showLoadingImg(false);
					}
					
					
					, onPause : function (){
						window[this.id].showLoadingImg(false);
					}
					
					
					, onIdle : function (){
						window[this.id].showLoadingImg(true);
					}
					
					
					
				}
			});
			
			
			this.domHeader.innerHTML = this.category;
			
		
		// } catch (err){
			// console.log("ItvJWPlayer.setup()  ERROR: " + err.message);
		// }


	}
	this.setupRandomRepeat = setupRandomRepeat;
	

	function updateWindowText() {
		
		var playlist = jwplayer(this.playerId).getPlaylist() || '';
		var i = jwplayer(this.playerId).getPlaylistIndex() || '';
		if( (playlist !='') && (i !='')) {
			var item = playlist[i];
			window[this.playerId].domTitle.innerHTML = item.title;
			window[this.playerId].domDescription.innerHTML = '<span class="artist">' + item.description.split('|')[0] + '</span>';	//<br/><span class="album">' + item.description.split('|')[1] + '</span>';
		}
		
	}
	this.updateWindowText = updateWindowText;
	
	
	function clearWindowText() {
		
		window[this.playerId].domTitle.innerHTML = '';
		window[this.playerId].domDescription.innerHTML = '';
		
	}
	this.clearWindowText = clearWindowText;


	
	function errMsg(){
		this.domContainer.innerHTML = "Music stream presently unavailable. We apologize for any inconvenience. Service will resume shortly.";
	}
	this.errMsg = errMsg;



	
	function showLoadingImg(isVisible){
		this.domImgLoading.style.opacity = isVisible ? 1 : 0;
	}
	this.showLoadingImg = showLoadingImg;
	

	function hideClickMsg(){
		try {
			this.domClickMsg.style.display = 'none';
		} catch (err){}
	}
	this.hideClickMsg = hideClickMsg;
	
	
	
	function playRandomItem(id){
		var playlist = jwplayer(id).getPlaylist();
		var randomnumber=Math.floor(Math.random()* playlist.length);
		jwplayer(id).playlistItem(randomnumber);
		
	}
	this.playRandomItem = playRandomItem;
	
	
	
	
	function remove() {
		try {
			this.clearWindowText();
			jwplayer(this.playerId).remove();
		} catch (err) {
			
		}
	}
	this.remove = remove;
	
	
	
	function forward() {
		var p = jwplayer(this.playerId).getPosition();
		var d = jwplayer(this.playerId).getDuration();
		
		jwplayer(this.playerId).seek(p+10);
		
	}
	this.forward = forward;
	
	
	
	
	function play() {
		jwplayer(this.playerId).play(true);
	}
	this.play = play;


	
	
	function togglePlay() {
		jwplayer(this.playerId).play();
	}
	this.togglePlay = togglePlay;
	
	
	
	
	function nextTrack() {
		this.playRandomItem(this.playerId);
		this.updateWindowText(this.playerId);
	}
	this.nextTrack = nextTrack;


	
	
	function pause() {
		jwplayer(this.playerId).pause(true);
	}
	this.pause = pause;

	
	

	function stop() {
		jwplayer(this.playerId).stop();
	}
	this.stop = stop;
	
	
	

}	


