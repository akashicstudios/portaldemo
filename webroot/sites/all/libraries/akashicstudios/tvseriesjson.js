
function TvSeriesJson() {
	
	this._json = {};
	
	this._vodTvSeries = '';
	
	function init() {
		
		var myself = this;
		
		jQuery.getJSON( '/json/vod_tvseries' )
		.done(function(data){
			
			myself._json = data;			
			
			myself._vodTvSeries = new Array();
			
			myself._json.forEach(function(elem){
				var ts = new VodTvSeries();
				ts._json = elem;
				myself._vodTvSeries.push(ts);
				
				ts._episodes = new Array();
				elem.episodes.forEach(function(ep){
					var item = new VodTvSeriesEpisode();
					item._json = ep; 
					ts._episodes.push(item);	
				});
				
			});
			
			window.AkashicStudios._pageController._controller.onTvSeriesJsonLoaded();
			
//			window.AkashicStudios._pageController._controller._dom_tvseries_carousel.loadCarouselImgSrc();
			
//			jQuery.getJSON( '/json/vod-tvseries-episodes' )
//			.done(function(data){
//				
//				var tse = data.vod_tvseries_episodes;
//				
//				myself._vodTvSeries.forEach(function(elem){
//					elem._episodes = new Array();
//					
//					tse.forEach(function(ep){
//						if(ep.vod_tvseries_episode.nid == elem.nid()){
//							var item = new VodTvSeriesEpisode();
//							item._json = ep.vod_tvseries_episode;
//							elem._episodes.push(item);	
//						}
//					});
//				});

				


//			})
//			.fail(function( jqXHR, textStatus, errorThrown){
//				console.log(textStatus + '\n' + errorThrown);
//			});
			
		})
		.fail(function( jqXHR, textStatus, errorThrown){
			console.log(textStatus + '\n' + errorThrown);
		});
	}
	this.init = init;
	
	
	
	
	
	function getTvSeries(nid) {
		
		var retval = null;
		
		this._vodTvSeries.forEach(function(elem){
			if(nid == elem.nid()) {
				retval = elem;
			}
		});
		
		return retval;
	}
	this.getTvSeries = getTvSeries;
	
	
	

}
