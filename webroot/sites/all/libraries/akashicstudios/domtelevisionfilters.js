function DomTelevisionFilters(){

	this._contentSelect = {};


	function selectedContent(){
		var s = this._contentSelect.firstElementChild;
		return s.options[s.selectedIndex].value;
	}
	this.selectedContent = selectedContent;
	

	
	function init() {

		var options = [{
			'value' : 'tvseries',
			'innerHTML' : 'TV OnDemand'
		},{
			'value' : 'satellitetv',
			'innerHTML' : 'Satellite TV'			
		}];

		var params = {
				'id' : 'entertainment-television-select-content',
				'className' : 'entertainment-television-select-content',
				'selectText' : '',
				'options' : options,
				'onchange' : function(){
					window.AkashicStudios._pageController._controller.showContent();
				}					
			};
		
		this._contentSelect = window.AkashicStudios.getSelect(params);
		
		var params = {
				'type' : 'select',
				'selects' : [this._contentSelect]
		};
		
		window.AkashicStudios.getHeaderBannerFilter(params);
		
		
	}
	this.init = init;
	
	
	
	
}


	
	
