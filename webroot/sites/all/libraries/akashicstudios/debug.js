function Debug(){
	
	
	this.bIsDebug = false;
//	this.bIsDebug = true;


	this.domDebugWindow = document.getElementById('debug-messages');
	(this.bIsDebug) ? this.domDebugWindow.style.display = 'block' : this.domDebugWindow.style.display = 'none';
	
	
	
	window.onerror = function (msg, url, line, column) {
		window.AkashicStudios._debug.global_onError(msg, url, line);
		this.bIsDebug = true;
	};

	
	function isDebug(){
		return this.bIsDebug;		
	}
	this.isDebug = isDebug;
	
	
	
	function onScreenOutput(str){
		if(this.isDebug()) {
			if(this.domDebugWindow != null){
				this.domDebugWindow.innerHTML = '<p>'+str.replace('\n','')+'</p>';
			}
		}
	}
	this.onScreenOutput = onScreenOutput;
	
	
	function global_onError(msg, url, line){
		var str = "msg " + msg + '\n'
		+ "url : " + url + '\n'
		+ "line : " + line + '\n';
		
		console.log(str);
		
		this.onScreenOutput(str);
		
		
	}
	this.global_onError = global_onError;

	
	
	function getViewportStats(){
		
		var retval = '';

		if(this.isDebug()) {

			var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
			var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)

			retval = 'viewport ' + w + ' x ' + h;
			var d = document.createElement('DIV');
			d.innerHTML = retval;
			d.className = 'debug';
			document.getElementsByTagName('BODY').item(0).appendChild(d);
		}
		return retval;
		
	}
	this.getViewportStats = getViewportStats;

	
	
	
	
}