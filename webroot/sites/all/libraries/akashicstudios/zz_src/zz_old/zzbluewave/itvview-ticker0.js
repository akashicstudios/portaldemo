

var itvViewTicker0Controller = new ItvView('itvViewTicker0');

itvViewTicker0Controller.viewItemDefaults.tagName = 'LI';
itvViewTicker0Controller.viewItemDefaults.navIdPrefix = itvViewTicker0Controller.cssClass + 'Item';
itvViewTicker0Controller.viewItemDefaults.inFocusCss = itvViewTicker0Controller.cssClass + '-list-item-focus';
itvViewTicker0Controller.html.scroll.direction = 'vertical';

itvViewTicker0Controller.html.position.maxWidth = 1000;
itvViewTicker0Controller.html.region = 'ticker';

itvViewTicker0Controller.init();
//itvViewTicker0Controller.hide();

window.gNav.views['itvViewTicker0'] = itvViewTicker0Controller;
window.gNav.viewsZeroBased.push(itvViewTicker0Controller);


for(var i=1; i< itvViewTicker0Controller.viewItemsZeroBased.length; i++) {
	itvViewTicker0Controller.viewItemsZeroBased[i].domNode.style.display = 'none';
}

jQuery('#tickerBackgroundHeader').css('backgroundColor','#'+ itvViewTicker0Controller.viewItemsZeroBased[0].nav.misc);
jQuery('#tickerBackgroundHeader').css('borderColor','#'+ itvViewTicker0Controller.viewItemsZeroBased[0].nav.misc);

itvViewTicker0Controller.advanceTicker = function() {
	
	jQuery('li.'+this.viewItemsZeroBased[this.html.scroll.index].cssClass).hide('slow');
	
	this.html.scroll.index++;
	
	if(this.html.scroll.index ==  this.viewItemsZeroBased.length) {
		this.html.scroll.index=0;
	}
	
	if(this.viewItemsZeroBased[this.html.scroll.index].nav.misc != '') {
		jQuery('#tickerBackgroundHeader').hide();
		jQuery('#tickerBackgroundHeader').css('backgroundColor','#'+ this.viewItemsZeroBased[this.html.scroll.index].nav.misc);
		jQuery('#tickerBackgroundHeader').css('borderColor','#'+ this.viewItemsZeroBased[this.html.scroll.index].nav.misc);
		jQuery('#tickerBackgroundHeader').show();
	}
	jQuery('li.'+this.viewItemsZeroBased[this.html.scroll.index].cssClass).show('slow');
	
};

window.setInterval(function(){window.gNav.views['itvViewTicker0'].advanceTicker();}, 15000);
