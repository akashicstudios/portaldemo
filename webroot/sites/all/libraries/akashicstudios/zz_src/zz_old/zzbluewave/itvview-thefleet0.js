

var itvViewTheFleet0Controller = new ItvView('itvViewTheFleet0');

itvViewTheFleet0Controller.viewItemDefaults.tagName = 'LI';
itvViewTheFleet0Controller.viewItemDefaults.navIdPrefix = itvViewTheFleet0Controller.cssClass + 'Item';
itvViewTheFleet0Controller.viewItemDefaults.inFocusCss = itvViewTheFleet0Controller.cssClass + '-list-item-focus';
itvViewTheFleet0Controller.html.scroll.direction = 'omni';
itvViewTheFleet0Controller.html.scroll.viewableItems = 1;

itvViewTheFleet0Controller.nav.dpad.back = 'itvMainMenu';
itvViewTheFleet0Controller.nav.dpad.left = 'itvViewMainMenuItem4';


itvViewTheFleet0Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewTheFleet0-nav-breadcrumb-thefleet', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewTheFleet0Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbTheFleet
});

itvViewTheFleet0Controller.init();
itvViewTheFleet0Controller.hide();

window.gNav.views['itvViewTheFleet0'] = itvViewTheFleet0Controller;
window.gNav.viewsZeroBased.push(itvViewTheFleet0Controller);


// window.fleetMap = new FleetMap();
// window.fleetMap.init(true);

itvViewTheFleet0Controller.onLoad = function() {
	
	jQuery('div.itvView.itvViewContent.itvViewTheFleet0').show();
	
	if(typeof window.fleetMap =='undefined') {
		window.fleetMap = new FleetMap();
		window.fleetMap.init(true);
	}
	
	window.fleetMap.intervalID = window.setInterval(function(){window.fleetMap.mapRotate();},window.fleetMap.rotationIntervalMS);	
	// console.log('map rotation started, window.fleetMap.intervalID = ' + window.fleetMap.intervalID);
	
	
};

itvViewTheFleet0Controller.onExit = function() {
	
	if(typeof window.fleetMap !='undefined'){
	
		window.clearInterval(window.fleetMap.intervalID);
		// console.log('map rotation window.fleetMap.intervalID ' + window.fleetMap.intervalID + ' now stopped');
	}
	
	jQuery('div.itvView.itvViewContent.itvViewTheFleet0').hide();
};