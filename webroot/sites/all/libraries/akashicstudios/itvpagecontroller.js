
function ItvPageController() {	
	
	
		this.MainMenuView = null;
		this.ShipServicesView = null;
		this.MovieCategoryView = null;
		this.MoviesView = null;
		this.MoviesDetailView = null;
		this.MovieAudioView = null;
		this.SatelliteTvView = null;
		this.TvShowsView = null;
		this.TvShowsDetailView = null;
		this.MusicView = null;
		this.MusicPlayerView = null;
		this.FleetTrackerView = null;
		this.SettingsView = null;
		this.TickerView = null;
		
		window.itvNavState = new ItvNavigationState();
		this.itvNavState = window.itvNavState;
		this.itvNavState.init();
		
		
		this.navArrows = {
				top : new ItvArrow(),
				right : new ItvArrow(),
				bottom : new ItvArrow(),
				left : new ItvArrow(),
				movieaudioviewtop : new ItvArrow(),
				movieaudioviewbottom : new ItvArrow(),
				tvepisodesviewtop : new ItvArrow(),
				tvepisodesviewbottom : new ItvArrow()
		};

	
	
		this.init = function(){
			
			this.headerFirst();
			this.headerSecond();
			this.headerThird();
			
			// menu region (itv-menubar)
			this.mainMenuViewInit();
			
			// content-main (itv-content-main)
			this.shipServicesViewInit();
			this.movieCategoryViewInit();
			this.moviesViewInit();
			this.satelliteTvViewInit();
			this.tvShowsViewInit();
			this.fleetTrackerViewInit();
			this.musicViewInit();
			this.moviesDetailViewInit();
			this.tvShowsDetailViewInit();
			this.musicPlayerViewInit();
			this.movieAudioViewInit();
			
			
			this.navArrowsInit();
			
			this.tickerViewInit();
			
			this.MainMenuView.focus();
			
		}

		

		this.mainMenuViewInit = function(){
			
			this.MainMenuView = new ItvMenuView();
			this.MainMenuView.id = 'MainMenuView';
			this.MainMenuView.jsonUrl = '/sql/json/itv_views()';
			this.MainMenuView.controller = this;
			this.MainMenuView.init();
			
			
		};

		this.shipServicesViewInit = function(){
			
			this.ShipServicesView = new ItvShipServicesView();
			this.ShipServicesView.id = 'ShipServicesView';
//			this.ShipServicesView.jsonUrl = '/sql/json/ship_services()';
			this.ShipServicesView.controller = this;
			this.ShipServicesView.init();			
			this.ShipServicesView.onJsonLoaded(window.AkashicStudios._ship_services._json);
		};
		
		this.movieCategoryViewInit = function(){
			
			this.MovieCategoryView = new ItvMovieCategoryView();
			this.MovieCategoryView.id = 'MovieCategoryView';
			this.MovieCategoryView.jsonUrl = '/sql/json/moviecategoryview()';
			this.MovieCategoryView.controller = this;
			this.MovieCategoryView.init();
			
		};
		
		
		this.moviesViewInit = function(){
			
			this.MoviesView = new ItvMoviesView();
			this.MoviesView.id = 'MoviesView';
			this.MoviesView.jsonUrl = '/json/vod_films';
			this.MoviesView.controller = this;
			this.MoviesView.init();			
			
		};
		
		this.satelliteTvViewInit = function(){

			this.SatelliteTvView = new ItvSatelliteTvView();
			this.SatelliteTvView.id = 'SatelliteTvView';
			this.SatelliteTvView.jsonUrl = '/sql/json/vod_satellitetv()';
			this.SatelliteTvView.controller = this;
			this.SatelliteTvView.init();			
			
		};
		

		this.tvShowsViewInit = function(){

			this.TvShowsView = new ItvTvShowsView();
			this.TvShowsView.id = 'TvShowsView';
			this.TvShowsView.jsonUrl = '/json/vod_tvseries';
			this.TvShowsView.controller = this;
			this.TvShowsView.init();			
			
		};

		this.musicViewInit = function(){

			this.MusicView = new ItvMusicView();
			this.MusicView.id = 'MusicView';
			this.MusicView.jsonUrl = '/sql/json/music_categories()';
			this.MusicView.controller = this;
			this.MusicView.init();			
			
		};

		
		
		this.moviesDetailViewInit = function(){
			
			this.MoviesDetailView = new ItvMoviesDetailView();
			this.MoviesDetailView.id = 'MoviesDetailView';
			this.MoviesDetailView.jsonUrl = '/json/vod_films';
			this.MoviesDetailView.controller = this;
			this.MoviesDetailView.init();			
			
		};

		this.movieAudioViewInit = function(){
			
			this.MovieAudioView = new ItvMovieAudioView();
			this.MovieAudioView.id = 'MovieAudioView';
			this.MovieAudioView.controller = this;
			this.MovieAudioView.init();			
			
		};

		this.tvShowsDetailViewInit = function(){
			
			this.TvShowsDetailView = new ItvTvShowsDetailView();
			this.TvShowsDetailView.id = 'TvShowsDetailView';
			this.TvShowsDetailView.jsonUrl = '/json/vod_tvseries';
			this.TvShowsDetailView.controller = this;
			this.TvShowsDetailView.init();			
			
		};

		
		this.musicPlayerViewInit = function(){
			
			this.MusicPlayerView = new ItvMusicPlayerView();
			this.MusicPlayerView.id = 'MusicPlayerView';
			this.MusicPlayerView.jsonUrl = '/sql/json/music_categories()';
			this.MusicPlayerView.controller = this;
			this.MusicPlayerView.init();			
			
		};
		
		this.fleetTrackerViewInit = function(){
			
			this.FleetTrackerView = new ItvFleetTrackerView();
			this.FleetTrackerView.id = 'FleetTrackerView';
			this.FleetTrackerView.controller = this;
			this.FleetTrackerView.init();			
			
		};
		
		
		this.headerFirst = function(){
			
			window.AkashicStudios.currentDateAndTimeDivUpdate();
			
		};
		
		
		this.headerSecond = function(){
			
			var data = window.AkashicStudios._system_defaults._defaults.ship;
			var img = document.getElementById('itv-header-second-img');
			img.src = data.ship_logo_itv.ship_logo_itv_image.image;
			img.className = data.ship_logo_itv.ship_logo_itv_css.value;	
			
		};
		
		
		this.headerThird = function(){
			
			var data = window.AkashicStudios._system_defaults.mtngps();
			
			var d = document.getElementById('itv-header-third-shipname-gps');
			d.children[0].innerHTML = window.AkashicStudios._system_defaults._defaults.ship.ship_display_name.value;
			d.children[1].innerHTML = window.AkashicStudios.convertDMS(data.latitude,data.longitude);
			
		};

		
		this.tickerViewInit = function(){

			this.TickerView = new ItvTickerView();
			this.TickerView.id = 'TickerView';
			this.TickerView.jsonUrl = '/sql/json/ticker_items()';
			this.TickerView.controller = this;
			this.TickerView.init();			
			
		};

		
		this.navArrowsInit = function(){
			
			this.navArrows.top.init({
				name : 'top',
				visible : false,
				id : 'itv-navarrow-top',
				opacity : 1
			});

			this.navArrows.right.init({
				name : 'right',
				visible : false,
				id : 'itv-navarrow-right',
				opacity : 1
			});

			this.navArrows.bottom.init({
				name : 'bottom',
				visible : false,
				id : 'itv-navarrow-bottom',
				opacity : 1
			});

			this.navArrows.left.init({
				name : 'left',
				visible : false,
				id : 'itv-navarrow-left',
				opacity : 1
			});

			this.navArrows.movieaudioviewtop.init({
				name : 'movieaudioviewtop',
				visible : false,
				id : 'itv-navarrow-movieaudioviewtop',
				opacity : 0
			});

			this.navArrows.movieaudioviewbottom.init({
				name : 'movieaudioviewbottom',
				visible : false,
				id : 'itv-navarrow-movieaudioviewbottom',
				opacity : 1
			});
			
			this.navArrows.tvepisodesviewtop.init({
				name : 'tvepisodesviewtop',
				visible : false,
				id : 'itv-navarrow-tvepisodesviewtop',
				opacity : 0
			});

			this.navArrows.tvepisodesviewbottom.init({
				name : 'tvepisodesviewbottom',
				visible : false,
				id : 'itv-navarrow-tvepisodesviewbottom',
				opacity : 1
			});
			
			
			
			window.AkashicStudios._debug.onScreenOutput(navigator.userAgent);
		};

}


