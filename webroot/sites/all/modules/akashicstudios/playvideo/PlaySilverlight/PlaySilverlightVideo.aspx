﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PlaySilverlightVideo.aspx.cs" Inherits="WiFiVideo.PlaySilverlightVideo" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    html, body {
        height: 100%;
        overflow: auto;
    }
    body {
        padding: 0;
        margin: 0;
    }
    #silverlightControlHost {
        height: 100%;
        text-align:center;
    }
    </style>
</head>
<body style="width:100%; height:100%;">
    <form id="form1" runat="server" style="width:100%; height:100%;">  
        <div id ="infoDiv" runat = "server" ></div>  
		<div id="silverlightControlHost" style="width:100%; height:100%;">
            <object width="100%" height="100%"
                type="application/x-silverlight-2" 
                data="data:application/x-silverlight-2," >
			    <param name="source" value="LiveSmoothStreaming.xap"/> 
                <param name ="initParams" value="<%=GetSrc%>" />
			    <param name="background" value="black" />
			    <param name="minRuntimeVersion" value="4.0.50401.0" />
			    <param name="autoUpgrade" value="true" />
			    <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=4.0.50401.0" style="text-decoration: none;">
				    <img src="http://go.microsoft.com/fwlink/?LinkID=161376" alt="Get Microsoft Silverlight" style="border-style: none"/>
			    </a>
            </object>
        </div>                      
        
    </form>
</body>
</html>
