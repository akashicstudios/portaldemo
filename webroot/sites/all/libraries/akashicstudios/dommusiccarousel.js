function DomMusicCarousel(){
	
	this._cssClass = '';
	this._cssClassLink = '';
	
	this._prevButton = {};
	this._nextButton = {};
	
	this._domNode = {};
	this._links = new Array();
	this._ul = {};
	this._liWidth = {};
	
	
	function init() {
		this.loadDomNode();
		this.loadItems();
		
		this.jcarousel_init();
		this.jcarousel_setwidth();
		
		
	}
	this.init = init;
	
	
	function loadDomNode(){
		
		var dn = document.getElementsByClassName(this._cssClass);
		if(dn != null) {
			this._domNode = dn.item(0);
		}		
		
		this._ul = this._domNode.firstElementChild;
		
		var p = document.createElement('A');
		p.className='jcarousel-prev';
		p.href='#';
		var n = document.createElement('A');
		n.className='jcarousel-next';
		n.href='#';
		this._domNode.appendChild(p);
		this._domNode.appendChild(n);
		
		this._domNode.addEventListener("touchmove", function(event){
			event.preventDefault();
			
		}, false);
		
	}
	this.loadDomNode = loadDomNode;
	
	
	
	function loadItems(){
		var me = this;
		
		var mc = window.AkashicStudios._pageController._controller._music_json._musicCategory;
		var dn = document.getElementById('jcarousel-entertainment-music-item');
		
		mc.forEach(function(s, i){
			
			var li = dn.cloneNode(true);
			var d = li.children[0];
			
			li.removeAttribute('id');
			li.style.removeProperty('display');
			
			d.dataset.tid = s.tid();
			
			d.children[0].src = s.poster();
			li.children[1].innerHTML = s.category();
			
			d.onclick = function(){
				window.AkashicStudios._pageController._controller._dom_music.lookup(this.dataset.tid);				
			};
			
			dn.parentElement.appendChild(li);
			me._links.push(li.firstElementChild);
			
		});
		
		this._ul = dn.parentElement;
		dn.parentElement.removeChild(dn);
		
	}
	this.loadItems = loadItems;
	
	
//	function loadCarouselImgSrc(){
//		var myself = this;
//		
//		this._links.forEach(function(a, i){
//			var src = window.AkashicStudios._pageController._controller._music_json.getMusicCategory(a.rel).poster();
//			a.firstElementChild.src = src;
//		});
//	}
//	this.loadCarouselImgSrc = loadCarouselImgSrc;
		
	
	function jcarousel_setwidth(){
		this._liWidth = 0;
		var ch = this._ul.children;
		
		for(var i=0; i<ch.length; i++){
			if(ch[i].scrollWidth>0) {
				this._liWidth += ch[i].scrollWidth;
			}
		}
		
		this._ul.style.width = this._liWidth + 'px';
	}
	this.jcarousel_setwidth = jcarousel_setwidth;
	
	
	function jcarousel_init(){
		
		var jc = jQuery('.jcarousel-entertainment-music').jcarousel({
			wrap: 'circular'
		});
		jQuery('.jcarousel-entertainment-music .jcarousel-prev').jcarouselControl({
	        target: '-=6',
	        carousel: jc
	    });

		jQuery('.jcarousel-entertainment-music .jcarousel-next').jcarouselControl({
	        target: '+=6',
	        carousel: jc
	    });

	}
	this.jcarousel_init = jcarousel_init;


	function jcarousel_destroy(){
		jQuery('.jcarousel-entertainment-music').jcarousel('destroy');
	}
	this.jcarousel_destroy = jcarousel_destroy;
	
	
	function jcarousel_moveFirst(){
		jQuery('.jcarousel-entertainment-music').jcarousel('scroll',0);
		this._ul.children[0].children[0].onclick();
	}
	this.jcarousel_moveFirst = jcarousel_moveFirst;
	

	
//	function setClickHandler() {
//		
//		var myself = this;
//		
//		var l = this._domNode.getElementsByClassName(this._cssClassLink);
//		if(l!=null) {
//			
//			for(var i=0; i<l.length; i++){
//				l[i].onclick = function() {
//					window.AkashicStudios._pageController._controller._dom_music.lookup(this.rel);
//				};
//			}
//		}
//	} 
//	this.setClickHandler = setClickHandler;
	
	
}