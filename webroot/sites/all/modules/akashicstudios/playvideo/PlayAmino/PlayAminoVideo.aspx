﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PlayAminoVideo.aspx.cs" Inherits="WiFiVideo.PlayAminoVideo" EnableEventValidation="false"%>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <script type="text/javascript" src="../../Styles/DeviceDetect.js"></script>
    <script type="text/javascript">
        var starting = true;
        var videostatus = 0;
        var vodMovie = '<%=GetSrc %>';
        var streamName = getVar("stream");
        var categoryName = getVar("cat");
        var streamPos = getVar("pos");
        var mediaDuration = 0;
        var canSeek = true;
        var mediaPos = 0;
        var channelKey = 0;
        var channelTimer = null;
        var timerBack = null;
        var videoError = false;
        var setPosAtStart = false;

        videoWindow = VideoDisplay.GetVideoWindow(0);
        videoWindow.state = "fullscreen";
        AudioControl.SetVolume(80); 
        AVMedia.onEvent = "handle_event();";

        function GetMediaPos() {
            try {
                mediaPos = AVMedia.GetPos();
            }
            catch (err) {
            }
        }

        function BackToMenu() {
            GetMediaPos();
            __doPostBack("PostBackBtn", "back_to_menu," + categoryName + "," + streamName + "," + mediaPos);
        }
        
        function handle_event() {
            videostatus = AVMedia.Event;

            if (timerBack != null) {
                clearTimeout(timerBack);
                timerBack = null;
                videoError = false;
                //infoDiv.innerHTML = infoDiv.innerHTML + " : " + videostatus;
            }

            if (videostatus == 99) {
                if (!starting && canSeek && !setPosAtStart) {
                    TextOverlay("The video server is not responding.... please wait");
                    videoError = true;
                    timerBack = setTimeout("BackToMenu();", 5000);
                }
            }
            else if (videostatus == 92) // Play
            {
                if (starting) {
                    starting = false;
                    mediaDuration = AVMedia.GetDuration();
                    TextOverlay("");
                    if (streamPos != null && streamPos != "") {
                        setPosAtStart = true;
                        AVMedia.SetPos(parseInt(streamPos));
                        streamPos = "";
                    }
                }
                else
                    setPosAtStart = false;
                if (canSeek == false) {
                    TextOverlay('...');
                    setTimeout("TextOverlay(''); canSeek = true; ", 500);
                }
                else
                    TextOverlay("");
            }
            else if (videostatus == 90) // END OF STREAM
                BackToMenu();
            
           // infoDiv.innerHTML = infoDiv.innerHTML + " : " + videostatus;
        }

        function MultiKey() {
            channelTimer = null;
            TextOverlay("");
            __doPostBack("PostBackBtn", "live_channel," + categoryName + "," + channelKey);
            channelKey = 0;
        }

        function keyAction(e) {
            var newPos = 0;

            if (e.which >= 48 && e.which <= 57) {
                if (channelTimer != null)
                    clearTimeout(channelTimer);
                GetMediaPos();
                if (mediaPos == 0 || starting) {
                    channelKey = (channelKey * 10) + (e.which - 48);
                    TextOverlay(channelKey);
                    channelTimer = setTimeout("MultiKey()", 3000);
                }
                else
                    FlashOverlay("Invalid Key");
            }
            else {
                switch (e.which) {
                    case 8568: // Back
                    case 8537: // Guide
                        document.location.href = "../../Portal.aspx";
                        break;
                    case 8516: // Menu
                        BackToMenu();
                        break;
                    case 8536: // Exit
                        if (infoDiv.innerHTML != "")
                            infoDiv.innerHTML = "";
                        break;
                    case 8501: // Stop
                        TextOverlay("Stopped. Press 'play' to restart");
                        break;
                    case 8500: // Forward
                        if (canSeek) {
                            canSeek = false;
                            TextOverlay(">>>");
                            GetMediaPos();
                            newPos = mediaPos + 120;
                            if (newPos > mediaDuration)
                                newPos = mediaDuration - 1;
                            AVMedia.SetPos(newPos);
                        }
                        break;
                    case 8567: // Slow Forward
                        if (canSeek) {
                            canSeek = false;
                            TextOverlay("|>");
                            GetMediaPos();
                            newPos = mediaPos + 20;
                            if (newPos > mediaDuration)
                                newPos = mediaDuration - 1;
                            AVMedia.SetPos(newPos);
                        }
                        break;
                    case 8502: // Reverse
                        if (canSeek) {
                            canSeek = false;
                            TextOverlay("<<<");
                            GetMediaPos();
                            newPos = mediaPos - 120;
                            if (newPos < 0)
                                newPos = 0;
                            AVMedia.SetPos(newPos);
                        }
                        break;
                    case 8566: // Slow Reverse
                        if (canSeek) {
                            canSeek = false;
                            TextOverlay("<|");
                            GetMediaPos();
                            newPos = mediaPos - 20;
                            if (newPos < 0)
                                newPos = 0;
                            AVMedia.SetPos(newPos);
                        }
                        break;
                    case 8504: // Pause
                        TextOverlay("Paused. Press 'play' to resume");
                        break;
                    case 8534: // Info
                        if (infoDiv.innerHTML == "")
                            TextOverlay(hiddenInfoDiv.innerHTML);
                        else
                            infoDiv.innerHTML = "";
                        break;
                    case 8: //P<P
                        GetMediaPos();
                        __doPostBack("PostBackBtn", "swap_stream," + categoryName + "," + streamName + "," + mediaPos);
                        break;
                    case 8492: //P+
                        GetMediaPos();
                        if (mediaPos == 0 || starting)
                            __doPostBack("PostBackBtn", "P+," + categoryName + "," + streamName);
                        else
                            FlashOverlay("Not a Live channel");
                        break;
                    case 8494: //P-
                        GetMediaPos();
                        if (mediaPos == 0 || starting)
                            __doPostBack("PostBackBtn", "P-," + categoryName + "," + streamName);
                        else
                            FlashOverlay("Not a Live channel");
                        break;
                    case 8499: //Play
                        TextOverlay("");
                        break;
                    case 8572: //Titles
                        FlashOverlay(hiddenTitleDiv.innerHTML);
                        break;
                    default:
                        FlashOverlay("Invalid Key (" + e.which + ")");
                }
            }
            return false;
        } 

        function TextOverlay(MessageString) {
            if (MessageString != "")
                infoDiv.innerHTML = "<font size ='+2' color='yellow'><b>" + MessageString + "</b></font>";
            else
                infoDiv.innerHTML = "";
        }

        function FlashOverlay(MessageString) {
            TextOverlay(MessageString);
            setTimeout("TextOverlay('');", 1000);
        }

        function PageInit() {
            VideoDisplay.SetChromaKey(0x00ff00);
            Browser.SetToolbarState(0);
            setTimeout(" AVMedia.Play(vodMovie);", 100);
        }

</script>

</head>

<body style="color:#ffffff; background-color:#999999; overflow:hidden;" onload="PageInit()" onkeypress="keyAction(event)">
    <form id="form1" runat="server" >
    <asp:ScriptManager runat="server" ID="ScriptManager1" EnablePartialRendering="true"></asp:ScriptManager>
    <div id="infoDiv" runat="server" style="margin-left:25px; margin-top:25px; margin-right:25px; margin-bottom:25px;"></div>
    <div id="hiddenInfoDiv" runat="server"   style="visibility:hidden" ></div>   
    <div id="hiddenTitleDiv" runat="server"   style="visibility:hidden" ></div>   
    <asp:Button ID="PostBackBtn" runat="server" CausesValidation="false" UseSubmitBehavior="False" style="display:none"  onclick="PostBackBtn_Click" />  
    </form>
</body>

</html>
