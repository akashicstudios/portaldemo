
function DropMenu(){
	
	this._cssClass = '';
	this._containerId = '';
	this._buttonId = '';

	this._dom_container = {};
	this._dom_button = {};
	this._domnode = {};

	this._menuItemsDom = '';
	this._menuItemsZeroBased = new Array();
	
	this._isVisible = false;

	this._json_uri = '';
	this._menuType = '';
	
	
	function init(){
		this._dom_container = document.getElementById(this._containerId);
		this._dom_button = document.getElementById(this._buttonId);

		this.getDefaultDomnode();
		this.loadMenuItems();
		
		this._dom_container.appendChild(this._domnode);
		
		this.hideMenu();
	}
	this.init = init;
	
	
	
	function getDefaultDomnode(){
		
		var dn = document.createElement('DIV');
		dn.id = this._containerId+'-menu';
		dn.className = dn.id;
		
		this._domnode = dn;
	}
	this.getDefaultDomnode = getDefaultDomnode;
	
	
	
	function hideMenu() {
		this._isVisible = true;
		this.toggleMenu();
	}
	this.hideMenu = hideMenu;
	
	
	
	function showMenu() {
		this._isVisible = false;
		this.toggleMenu();
	}
	this.showMenu = showMenu;
	
	
	
	function toggleMenu() {
		this._isVisible = !this._isVisible;
		
		if(this._isVisible) {
			this._dom_container.classList.add('hover');
			this._dom_button.classList.add('hover');
			jQuery('.'+this._cssClass).show();
		}
		else {
			jQuery('.'+this._cssClass).hide();
			this._dom_container.classList.remove('hover');
			this._dom_button.classList.remove('hover');
		}

	}
	this.toggleMenu = toggleMenu;
	
	
	
	function loadMenuItems() {
		
		// loop through document and find items that have 'AkMenuitem' class marker, call init on each one
		var me = this;

		jQuery.getJSON(this._json_uri)
		.done(function(data){
			
			var items = data;
			
			items.forEach(function(item,i){
				
				var mi = new DropMenuItem();

				mi._json = item;
				mi._cssClass = me._cssClass + '-item';
				i % 2 == 1 ? mi._cssClass += ' odd' : mi._cssClass += ' even'; 
				
				mi._parentMenu = me;
				mi.init();
				
				if(me._menuType == 'telephone_list'){
					mi.telephone_list_getContent();
				}
				
				me._menuItemsZeroBased.push(mi);
				
				me._domnode.appendChild(mi._domnode);
			});

		})
		.fail(function( jqXHR, textStatus, errorThrown){
			console.log(textStatus + '\n' + errorThrown);
		});
		
	
	}
	this.loadMenuItems = loadMenuItems;

	
	
	
	function showHideMenuItems(isVisible) {
		this._menuItemsZeroBased.forEach(function(mi) {
			mi.showHide(isVisible);
		});
	}
	this.showHideMenuItems = showHideMenuItems;
	
	
	

	
	
}











/*
 */
function DropMenuItem(){
	
	this._cssClass = '';
	this._parentMenu = {};
	
	this._domnode = {};
	this._menuItemType = 'txt';		// 'img'
	
	this._imageDom = {};
	this._textDom = {};
	
	this._target = '';
	
	this._isVisible = true;
	
	this._json = {};


	function init(){
		
		this._domnode = this.getDefaultDomnode();
		this.setClickHandler();

	}
	this.init = init;	
	
	
	
	function getDefaultDomnode(){
		var dn = document.createElement('DIV');
		dn.className = this._cssClass;
		
		var a = document.createElement('A');
		
		if(this._menuItemType == 'img'){
			var i = document.createElement('IMG');
			a.appendChild(i);
			this._imageDom = i;
		}
		else{
			var t = document.createElement('SPAN');
			t.className = 'linkText';
			a.appendChild(t);
			this._textDom = t;
		}
		
		dn.appendChild(a);
		
		return dn;
	}
	this.getDefaultDomnode = getDefaultDomnode;
	
	
	
	
	function showHide(isVisible) {
		
		if(isVisible) {
			this._domnode.style.display = 'none';
		}
		else {
			this._domnode.style.removeProperty('display');
		}
		this._isVisible = isVisible;
		
	}
	this.showHide = showHide;
	
	
	
	
	function setClickHandler() {
		
		var me = this;
		
		this._domnode.onclick = function(){
		};
		
	}
	this.setClickHandler = setClickHandler;
	

	
	function telephone_list_getContent(){
		
		var dn = document.createElement('DIV');
		var s1 = document.createElement('DIV');
		s1.className = 'content-left';
		s1.innerHTML = this._json.display_title;
		
		var s2 = document.createElement('DIV');
		s2.className = 'content-right';
		s2.innerHTML = this._json.telephone;
		
		dn.appendChild(s1);
		dn.appendChild(s2);
		
		this._domnode.innerHTML = '';
		this._domnode.appendChild(dn);
	}
	this.telephone_list_getContent = telephone_list_getContent;

}
















