
function SatelliteTvJson() {
	
	this._json = {};
	
	this._satellitetv = '';
	
	function init() {
		
		var myself = this;
		
		jQuery.getJSON( '/json/vod_satellitetv' )
		.done(function(data){
			
			myself._json = data;			
			
			myself._satellitetv = new Array();
			
			myself._json.forEach(function(elem){
				var s = new VodSatelliteTv();
				s._json = elem;
				myself._satellitetv.push(s);
			});
			
			window.AkashicStudios._pageController._controller.onSatelliteTvJsonLoaded();

		})
		.fail(function( jqXHR, textStatus, errorThrown){
			console.log(textStatus + '\n' + errorThrown);
		});
	}
	this.init = init;
	
	
	
	
	
	function getSatelliteTv(nid) {
		
		var retval = null;
		
		this._satellitetv.forEach(function(elem){
			if(nid == elem.nid()) {
				retval = elem;
			}
		});
		
		return retval;
	}
	this.getSatelliteTv = getSatelliteTv;
	
	
	

}
