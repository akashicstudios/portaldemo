

var tabletViewMusic0Controller = new ItvView('tabletViewMusic0');

tabletViewMusic0Controller.viewItemDefaults.tagName = 'LI';
tabletViewMusic0Controller.viewItemDefaults.navIdPrefix = tabletViewMusic0Controller.cssClass + 'Item';
tabletViewMusic0Controller.viewItemDefaults.inFocusCss = tabletViewMusic0Controller.cssClass + '-list-item-focus';

tabletViewMusic0Controller.init();
//tabletViewMusic0Controller.hide();

window.gNav.views['tabletViewMusic0'] = tabletViewMusic0Controller;
window.gNav.viewsZeroBased.push(tabletViewMusic0Controller);

for (var i=0; i< tabletViewMusic0Controller.viewItemsZeroBased.length; i++) {
	
	var cf = tabletViewMusic0Controller.viewItemsZeroBased[i].nav.dpad.enter.viewMusicFilter;
	tabletViewMusic0Controller.viewItemsZeroBased[i].domNode.dataset.viewmusicfilter = cf;
	tabletViewMusic0Controller.viewItemsZeroBased[i].domNode.dataset.cssclass = tabletViewMusic0Controller.viewItemsZeroBased[i].cssClass;
	
	tabletViewMusic0Controller.viewItemsZeroBased[i].domNode.onclick = function(){

		// if(typeof window.tabletViewMusic1JWPlayer0 == 'undefined') {
			window.gNav.views['tabletViewMusic1'].jwPlayer(this.dataset.viewmusicfilter);
		// }		
		
		// window.gNav.views['tabletViewMusic1'].init(this.dataset.viewmusicfilter);
		// window.tabletViewMusic1JWPlayer0.setupRandomRepeat();
		
		jQuery('h3.tabletViewMusicHeader').slideUp('slow');
		jQuery('.tabletViewMusic1').show();
		
	
	};
}
jQuery('.tabletViewMusic1').hide('fast');

// 
// jQuery(".tabletViewMusic0FancyBox").fancybox({
	// maxWidth	: 820,
	// maxHeight	: 500,
	// width		: 820,
	// height		: 500,
	// fitToView	: false,
	// autoSize	: true,
	// autoCenter	: true,
	// closeClick	: false,
	// openEffect	: 'none',
	// closeEffect	: 'none',
	// iframe		: {scrolling : 'no'},
	// padding		: 10,
	// margin		: 0
// });
// 
