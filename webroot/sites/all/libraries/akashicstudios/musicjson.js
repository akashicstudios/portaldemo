
function MusicJson() {
	
	this._json = {};
	
	this._musicCategory = '';
	
	function init() {
		
		var myself = this;
		
		jQuery.getJSON( '/sql/json/music_categories()' )
		.done(function(data){
			
			myself._json = data;			
			
			myself._musicCategory = new Array();
			
			myself._json.forEach(function(elem){
				var o = new MusicCategory();
				o._json = elem;
				myself._musicCategory.push(o);
			});
			
//			window.AkashicStudios._pageController._controller._dom_music_carousel.loadCarouselImgSrc();
			
			window.AkashicStudios._pageController._controller.onMusicJsonLoaded();
						
		})
		.fail(function( jqXHR, textStatus, errorThrown){
			console.log(textStatus + '\n' + errorThrown);
		});
	}
	this.init = init;
	
	
	
	
	
	function getMusicCategory(tid) {
		
		var retval = null;
		
		this._musicCategory.forEach(function(elem){
			if(tid == elem.id()) {
				retval = elem;
			}
		});
		
		return retval;
	}
	this.getMusicCategory = getMusicCategory;
	
	
	

}
