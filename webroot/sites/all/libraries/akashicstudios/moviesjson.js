
function MoviesJson() {
	
	this._json = {};
	
	this._vodFilms = '';
	
	function init() {
		
		var me = this;
		
		jQuery.getJSON( '/json/vod_films' )
		.done(function(data){
			
			me._json = data;			
			
			me._vodFilms = new Array();
			
			me._json.forEach(function(elem){
				var vf = new VodFilm();
				vf._json = elem;
				me._vodFilms.push(vf);
			});
			
			
			me._vodFilms.forEach(function(vf){
				
				vf._json.meta_data.forEach(function(elem){
					
					var item = new VodFilmMetaData();
					item._json = elem;
					vf._metaData.push(item);

				});

				vf._json.media_items.forEach(function(elem){
					
					var item = new VodFilmMediaItem();
					item._json = elem;
					vf._mediaItems.push(item);

				});

			});
			
			
			window.AkashicStudios._pageController._controller.onJsonLoaded();

			
		})
		.fail(function( jqXHR, textStatus, errorThrown){
			console.log(textStatus + '\n' + errorThrown);
		});
	}
	this.init = init;
	
	
	
	
	
	function getVodFilm(nid) {
		
		var vf = null;
		
		this._vodFilms.forEach(function(elem){
			if(nid == elem.nid()) {
				vf = elem;
			}
		});
		
		return vf;
	}
	this.getVodFilm = getVodFilm;
	
	
	

}
