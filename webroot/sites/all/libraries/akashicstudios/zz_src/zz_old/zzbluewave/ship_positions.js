var map, layer;


	

    function init(){
        // OpenLayers.ProxyHost="/proxy/?url=";

		map = new ol.Map({
		  view: new ol.View({
		    center: [0, 0],
		    zoom: 1
		  }),
		  layers: [
		    new ol.layer.Tile({
		      source: new OpenLayers.source.MapQuest({layer: 'osm'})
		    })
		  ],
		  target: 'shipPositionsMap'
		});

        // map = new OpenLayers.Map('shipPositionsMap');
        // layer = new OpenLayers.Layer.WMS( "OpenLayers WMS", 
            // "http://vmap0.tiles.osgeo.org/wms/vmap0", {layers: 'basic'} );            
        // map.addLayer(layer);
        
        map.setCenter(new OpenLayers.LonLat(0, 0), 0);

        // var newl = new OpenLayers.Layer.Text( "text", { location:"./textfile.txt"} );
        // map.addLayer(newl);

        var markers = new OpenLayers.Layer.Markers( "Markers" );
        map.addLayer(markers);

        var size = new OpenLayers.Size(21,25);
        var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
        var icon = new OpenLayers.Icon('/sites/all/libraries/openlayers/img/marker.png',size,offset);
        
        // markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(0,0),icon));

        // var halfIcon = icon.clone();
        // markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(0,45),halfIcon));

        // marker = new OpenLayers.Marker(new OpenLayers.LonLat(90,10),icon.clone());
        
        for(var i=0; i<Drupal.settings.redCell.mtngps.length; i++) {
	        
	        var ship = Drupal.settings.redCell.mtngps[i];
	        
	        var marker = new OpenLayers.Marker(new OpenLayers.LonLat(ship.longitude,ship.latitude),icon.clone());	
	        markers.addMarker(marker);
	        	
        }
        
        // marker.setOpacity(0.2);

        // marker.events.register('mousedown', marker, function(evt) { alert(this.icon.url); OpenLayers.Event.stop(evt); });
         
        map.addControl(new OpenLayers.Control.LayerSwitcher());
        map.zoomToMaxExtent();

        // halfIcon.setOpacity(0.5);
    }
    
init();
