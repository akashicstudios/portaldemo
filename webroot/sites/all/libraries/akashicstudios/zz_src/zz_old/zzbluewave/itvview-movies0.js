

var itvViewMovies0Controller = new ItvView('itvViewMovies0');

itvViewMovies0Controller.viewItemDefaults.tagName = 'LI';
itvViewMovies0Controller.viewItemDefaults.navIdPrefix = itvViewMovies0Controller.cssClass + 'Item';
itvViewMovies0Controller.viewItemDefaults.inFocusCss = itvViewMovies0Controller.cssClass + '-list-item-focus';
itvViewMovies0Controller.html.scroll.direction = 'omni';

itvViewMovies0Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewMovies0-nav-breadcrumb-movies', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewMovies0Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbMovies
});


itvViewMovies0Controller.nav.dpad.back = 'itvMainMenu';
itvViewMovies0Controller.nav.dpad.left = 'itvViewMainMenuItem0';

itvViewMovies0Controller.init();
itvViewMovies0Controller.hide();

window.gNav.views['itvViewMovies0'] = itvViewMovies0Controller;
window.gNav.viewsZeroBased.push(itvViewMovies0Controller);


