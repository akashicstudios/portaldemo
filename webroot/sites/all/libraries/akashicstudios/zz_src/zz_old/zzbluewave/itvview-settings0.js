

var itvViewSettings0Controller = new ItvView('itvViewSettings0');

itvViewSettings0Controller.viewItemDefaults.tagName = 'LI';
itvViewSettings0Controller.viewItemDefaults.navIdPrefix = itvViewSettings0Controller.cssClass + 'Item';
itvViewSettings0Controller.viewItemDefaults.inFocusCss = itvViewSettings0Controller.cssClass + '-list-item-focus';
itvViewSettings0Controller.html.scroll.direction = 'omni';
itvViewSettings0Controller.html.scroll.viewableItems = 3;

itvViewSettings0Controller.nav.dpad.back = 'itvMainMenu';
itvViewSettings0Controller.nav.dpad.left = 'itvViewMainMenuItem6';


itvViewSettings0Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewSettings0-nav-breadcrumb-settings', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewSettings0Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbSettings
});

itvViewSettings0Controller.init();
itvViewSettings0Controller.hide();

window.gNav.views['itvViewSettings0'] = itvViewSettings0Controller;
window.gNav.viewsZeroBased.push(itvViewSettings0Controller);



