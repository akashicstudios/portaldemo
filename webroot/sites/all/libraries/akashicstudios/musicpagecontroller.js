function MusicPageController(){

	
	this._music_json = new MusicJson();
	this._music_json.init();

	this._dom_music = new DomMusic();

	
	this._dom_music_carousel = new DomMusicCarousel();
	this._dom_music_carousel._cssClass = 'jcarousel-entertainment-music';
	this._dom_music_carousel._cssClassLink = 'link-jcarousel-music';
	

		
	function init(){
//		this._dom_music_carousel.setClickHandler();
	}
	this.init = init;

	
	this.onMusicJsonLoaded = function(){
		
		this._dom_music_carousel.init();
		this._dom_music_carousel.jcarousel_moveFirst();

	}

	
}