

var itvViewMusic0Controller = new ItvView('itvViewMusic0');

itvViewMusic0Controller.viewItemDefaults.tagName = 'LI';
itvViewMusic0Controller.viewItemDefaults.navIdPrefix = itvViewMusic0Controller.cssClass + 'Item';
itvViewMusic0Controller.viewItemDefaults.inFocusCss = itvViewMusic0Controller.cssClass + '-list-item-focus';
itvViewMusic0Controller.html.scroll.direction = 'omni';

itvViewMusic0Controller.nav.dpad.back = 'itvMainMenu';
itvViewMusic0Controller.nav.dpad.left = 'itvViewMainMenuItem3';


itvViewMusic0Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewMusic0-nav-breadcrumb-music', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewMusic0Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbMusic
});


itvViewMusic0Controller.init();
itvViewMusic0Controller.hide();

window.gNav.views['itvViewMusic0'] = itvViewMusic0Controller;
window.gNav.viewsZeroBased.push(itvViewMusic0Controller);


