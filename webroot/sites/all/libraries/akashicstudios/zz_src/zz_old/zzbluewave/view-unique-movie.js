

jQuery(function($) {


	try {

		//collect play button canvas elements into array
		var buttonDivs = document.getElementsByClassName("canvas_button_container");

		//loop through each div element
		//	create the canvas div 			<canvas id="canvas-button_play_[nid]_canvas" width="180" height="30" class="canvas-button-play"></canvas>

		//	call the setCanvasButtonBlack
		for(var i=0; i<buttonDivs.length; i++) {

			var canvas = document.createElement("CANVAS");
			canvas.width = buttonDivs[i].scrollWidth;
			canvas.height = buttonDivs[i].scrollHeight;
			canvas.id = buttonDivs[i].id + '_canvas';
			canvas.style.cursor = 'pointer';

			buttonDivs[i].appendChild(canvas);

			var strLanguage = document.getElementById(buttonDivs[i].id +'_language').innerHTML;

			setCanvasButtonBlack(canvas, strLanguage, 0, '', 1);
		}


		$('.canvas_button_container').click(function(){
			setCanvasButtonBlack(    $(this).children("canvas").get(0) , $(this).children(".language").html(), 1, '', 1);
			window.location.assign( $(this).find(".url").text() );
			//alert($(this).find(".url").html() );
		});

		$('.canvas_button_container').mouseenter(function(){
			setCanvasButtonBlack(    $(this).children("canvas").get(0) , $(this).children(".language").html(), 1, '', 1);
		});

		$('.canvas_button_container').mouseleave(function(){
			setCanvasButtonBlack(    $(this).children("canvas").get(0) , $(this).children(".language").html(), 0, '', 1);
		});

		var movieTitle= $('.view-unique-movie .views-field-title span.field-content').get(0).innerHTML;
		$('.view-unique-movie .views-field-title span.field-content').get(0).innerHTML = movieTitle.toUpperCase();

		$('.view-unique-movie .ctools-jump-menu-select option').each(function(){
			$(this).text($(this).get(0).innerHTML.toUpperCase());
		});


	} catch (exc) {

	}

});
			
