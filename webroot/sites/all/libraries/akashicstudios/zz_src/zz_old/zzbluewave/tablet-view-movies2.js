

var tabletViewMovies2Controller = new ItvView('tabletViewMovies2');

tabletViewMovies2Controller.viewItemDefaults.tagName = 'LI';
tabletViewMovies2Controller.viewItemDefaults.navIdPrefix = tabletViewMovies2Controller.cssClass + 'Item';
tabletViewMovies2Controller.viewItemDefaults.inFocusCss = tabletViewMovies2Controller.cssClass + '-list-item-focus';

// tabletViewMovies2Controller.icons.arrows.push(new ItvNavIcon('back', 'itv-nav-arrow-back', 317, 295, 0.5));
// tabletViewMovies2Controller.icons.arrows.push(new ItvNavIcon('forward', 'itv-nav-arrow-forward', 317, 1119, 0.5));


tabletViewMovies2Controller.init();
// tabletViewMovies2Controller.domNode.style.display='none';
tabletViewMovies2Controller.html.region = '';
tabletViewMovies2Controller.html.scroll.direction = 'vertical-scroll';	
tabletViewMovies2Controller.html.position.maxWidth = 846;
//tabletViewMovies2Controller.hide();

window.gNav.views['tabletViewMovies2'] = tabletViewMovies2Controller;
window.gNav.viewsZeroBased.push(tabletViewMovies2Controller);





for (var i=0; i< tabletViewMovies2Controller.viewItemsZeroBased.length; i++) {

	var nid = tabletViewMovies2Controller.viewItemsZeroBased[i].nav.viewItemNid;
	tabletViewMovies2Controller.viewItemsZeroBased[i].domNode.dataset.viewitemnid = nid;
	tabletViewMovies2Controller.viewItemsZeroBased[i].domNode.dataset.cssclass = tabletViewMovies2Controller.viewItemsZeroBased[i].cssClass;
	
	var elemLink = tabletViewMovies2Controller.viewItemsZeroBased[i].domNode.getElementsByClassName('subTabletViewMovies2-fancybox').item(0);
	
	var elems = tabletViewMovies2Controller.viewItemsZeroBased[i].domNode.getElementsByClassName('subViewMovieUrls');
	if(elems.item(0).firstElementChild.children.length==1){
		
		elemLink.setAttribute('href',elems.item(0).firstElementChild.firstElementChild.firstElementChild.dataset.movieurl);
		elemLink.removeAttribute('class');
		elemLink.setAttribute('target', '_blank');
		
	}
	else {
		
		var fbClass = tabletViewMovies2Controller.viewItemsZeroBased[i].cssClass+'-fancybox';
		elemLink.setAttribute('class',fbClass+' fancybox.iframe');
		
		jQuery('.'+fbClass).fancybox({
			type	: 'iframe',
			maxWidth	: 560,
			iframe		: {scrolling : 'no'},
			padding		: 10,
			margin		: 0,
			closeBtn	: false
			
		});
	}

}
