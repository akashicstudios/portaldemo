
function DomSatelliteTvCarousel() {
	
	this._cssClass = '';
	this._cssClassLink = '';
	
	this._prevButton = {};
	this._nextButton = {};
	
	this._domNode = {};
	this._links = new Array();
	this._ul = {};
	this._liWidth = {};
	
	
	function init() {
		this.loadDomNode();
		this.loadItems();
		
		this.jcarousel_init();
		this.jcarousel_setwidth();


	}
	this.init = init;
	
	
	function loadDomNode(){
		
		var dn = document.getElementsByClassName(this._cssClass);
		if(dn != null) {
			this._domNode = dn.item(0);
		}		
		
		this._ul = this._domNode.firstElementChild;
		
		var p = document.createElement('A');
		p.className='jcarousel-prev';
		p.href='#';
		var n = document.createElement('A');
		n.className='jcarousel-next';
		n.href='#';
		this._domNode.appendChild(p);
		this._domNode.appendChild(n);
		
		this._domNode.addEventListener("touchmove", function(event){
			event.preventDefault();
			
		}, false);
		
	}
	this.loadDomNode = loadDomNode;
	
	
	
	function loadItems(){
		var me = this;
		
		var ts = window.AkashicStudios._pageController._controller._satellitetv_json._satellitetv;
		var dn = document.getElementById('jcarousel-entertainment-satellitetv-item');
		
		ts.forEach(function(s, i){
			
			var li = dn.cloneNode(true);
			var d = li.firstElementChild;
			
			li.removeAttribute('id');
			li.style.removeProperty('display');
			
			d.dataset.nid = s.nid();
			
			d.children[0].src = s.poster();
			
			d.onclick = function(){
				window.AkashicStudios._pageController._controller._dom_satellitetv.lookup(this.dataset.nid);				
			};
			
			dn.parentElement.appendChild(li);
			me._links.push(li.firstElementChild);
			
		});
		
		this._ul = dn.parentElement;
		
		dn.parentElement.removeChild(dn);
		
	}
	this.loadItems = loadItems;
	
	
//	function loadCarouselImgSrc(){
//		var myself = this;
//		
//		this._links.forEach(function(a, i){
//			var src = window.AkashicStudios._pageController._controller._satellitetv_json.getSatelliteTv(a.rel).poster();
//			a.firstElementChild.src = src;
//		});
//	}
//	this.loadCarouselImgSrc = loadCarouselImgSrc;
	
	
	
	function jcarousel_setwidth(){
		this._liWidth = 0;
		var ch = this._ul.children;
		
		for(var i=0; i<ch.length; i++){
			if(ch[i].scrollWidth>0) {
				this._liWidth += ch[i].scrollWidth;
			}
		}
		
		this._ul.style.width = this._liWidth + 'px';
	}
	this.jcarousel_setwidth = jcarousel_setwidth;
	
	
	function jcarousel_init(){
		
		var jc = jQuery('.jcarousel-entertainment-satellitetv').jcarousel({
			wrap: 'circular'
		});
		jQuery('.jcarousel-entertainment-satellitetv .jcarousel-prev').jcarouselControl({
	        target: '-=6',
	        carousel: jc
	    });

		jQuery('.jcarousel-entertainment-satellitetv .jcarousel-next').jcarouselControl({
	        target: '+=6',
	        carousel: jc
	    });

	}
	this.jcarousel_init = jcarousel_init;


	function jcarousel_destroy(){
		jQuery('.jcarousel-entertainment-satellitetv').jcarousel('destroy');
	}
	this.jcarousel_destroy = jcarousel_destroy;
	
	
	function jcarousel_moveFirst(){
		jQuery('.jcarousel-entertainment-satellitetv').jcarousel('scroll',0);
		this._ul.firstElementChild.firstElementChild.onclick();
	}
	this.jcarousel_moveFirst = jcarousel_moveFirst;
	

	
//	function setClickHandler() {
//		
//		var myself = this;
//		
//		var l = this._domNode.getElementsByClassName(this._cssClassLink);
//		if(l!=null) {
//			
//			for(var i=0; i<l.length; i++){
//				l[i].onclick = function() {
//					window.AkashicStudios._pageController._controller._dom_satellitetv.lookup(this.rel);
//				};
//			}
//		}
//	} 
//	this.setClickHandler = setClickHandler;
	
	
	


}
