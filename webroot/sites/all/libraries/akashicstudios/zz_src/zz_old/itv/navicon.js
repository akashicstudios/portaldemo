
/*
 * 
 * 
 * <div class="itv-nav-arrow tablet-nav-arrow-forward" style="position: fixed; top: 317px; left: 1119px; opacity: 0;">
 * <img typeof="foaf:Image" src="http://localhost:9001/sites/default/files/nodes/media/arrow-forward.png" width="50" height="50" alt="">
 * </div>
 * 
 * name, cssClass, top, left, opacity, position, parentElement, imgPath
 * */
function AKNavIcon(options) {
    
    this.options = options;
    
    this.name = options.name;
    this.cssClass = options.cssClass;
    this.top = options.top;
    this.left = options.left;
    this.opacity = options.opacity;
    this.position = options.position || 'fixed';
    this.parentElement = options.parentElement;
    
    
    this.imgNode = document.createElement('IMG');
    this.imgNode.src = options.imgPath || '';

    this.domNode = document.createElement('DIV');
    this.domNode.className = this.cssClass;
    this.domNode.style.position = this.position;
    this.domNode.style.top = this.top + 'px';       
    this.domNode.style.left = this.left + 'px';
    
    this.domNode.appendChild(this.imgNode);
    this.parentElement.appendChild(this.domNode);
    

    
    function init() {
    }
    this.init = init;
    
    
    function hide() {
        this.domNode.style.opacity = 0;     
    }
    this.hide = hide;

    function dim() {
        this.domNode.style.opacity = this.opacity * 0.5;
    }
    this.dim = dim;

    function show() {
        this.domNode.style.opacity = this.opacity;      
    }
    this.show = show;

}
