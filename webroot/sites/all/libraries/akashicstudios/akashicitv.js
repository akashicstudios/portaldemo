


function ItvNavigationState(){
	
	this.menuView = null;

	this.views = new Array();
	this.viewItemStack = new Array();
	
	this.keyBuffer = new Array();
	
	this.domBreadcrumbs = {
			view : document.getElementById('itv-breadcrumb-view')
	};
	
	this.itvRegionMenu = new ItvRegion();
	this.itvRegionMenu.id = 'itv-menubar';
	this.itvRegionMenu.init();
	
	this.itvRegionContent = new ItvRegion();
	this.itvRegionContent.id = 'itv-content-main';
	this.itvRegionContent.init();
	
	this.scrollDirection = [
	                        {'vertical' : 0},
	                        {'horizontal':1},
	                        {'omni':2}
	                        ];
	
	this.state = {
		active :  {
			'region' : '',
			'view' : '',
			'viewItem' : '',
			'subViewItem' : '',
			'viewItemMenu' : ''
			},
		previous : {
			'region' : '',
			'view' : '',
			'viewItem' : '',
			'subViewItem' : '',
			'viewItemMenu' : ''
			}
	};

	this.chainedKeystroke = '';
	
	this.init = function(){
		
		jQuery(document).bind('keydown', function (event) {									
			window.itvNavState.onKeydown(event);
		});
		
	};
	
	
	this.setBreadcrumb = function(id){
		
		this.domBreadcrumbs.view.src = this.domBreadcrumbs.view.parentElement.dataset.imgsrc 
			+ id.toLowerCase()
			+ '.png';
		
	};

	
	this.chainKeystroke = function(direction){
//		window.setTimeout(function(){window.itvNavState.moveDpad(direction);},1000);
		this.chainedKeystroke = direction;
	};
	
	
	this.onKeydown = function(event) {
		
		//handle any easter eggs:
		if(event.keyCode==66){
			this.keyBuffer.length=0;
		}
		else {
			this.keyBuffer.push(event.keyCode);
		}
		
		if(this.keyBuffer.length==8) {
			// 38 40 38 40 37 39 37 39
			if ((this.keyBuffer[0]==38) &&
					(this.keyBuffer[1]==40) &&
					(this.keyBuffer[2]==38) &&
					(this.keyBuffer[3]==40) &&
					(this.keyBuffer[4]==37) &&
					(this.keyBuffer[5]==39) &&
					(this.keyBuffer[6]==37) &&
					(this.keyBuffer[7]==39) ){
				
				window.AkashicStudios._pageController._controller.MainMenuView.unhideSettings();
			}
			
		}

		//process normal input
		else {
			this.processNav(event.keyCode);
		}
		
		if(this.chainedKeystroke != '') {
			var dir = this.chainedKeystroke;
			this.chainedKeystroke = '';
			this.moveDpad(dir);
		}
		
	}
	
	
	this.reloadDocument = function(){
		
		this.hideAllViews();
		event.preventDefault();
		window.location.reload();
		
	};
	
	
	this.popToHome = function(){
		if((this.viewItemStack.length==0) && (this.menuView.viewMode.viewItemActiveIndex==0)){
			this.reloadDocument();
		}			
		else {
			while(this.viewItemStack.length>0) {
				this.state.active.view.viewMode.onDpadBack();					
			}
			this.menuView.viewMode.moveFirst();
		}
		
			
		
	};
	
	this.processNav = function(keyCode) {
		
//		console.log(event.keyCode);

		window.AkashicStudios._debug.onScreenOutput('keycode:' + keyCode);
		
		switch (keyCode) {
		
			case 13:		// ENTER
				this.state.active.view.viewMode.onDpadEnter();
				break;
				
				
			case 85: 	// 'U' for unbind
				event.preventDefault();
				unBindFromDocument();
				break;
	
				
			case 82:	// 'R' for reload
				this.reloadDocument();
				break;
	
				
			case 70: 		// F	
			case 102: 		// f
	//			window[window.gNav.navState.active.view.cssClass+'JWPlayer0'].forward();
				break;
	
			case 78: 		// N
			case 110: 		// n				
	//			window.itvJWPlayer0.nextTrack();				
				break;
	
	
			case 72: 		// H (home)		V1 stick Home Button
			case 104: 		// h (home)
				this.popToHome();
				break; 
	
				
				
				
				// D-Pad movement / everything else
			case 87:		// V1 stick
			case 38:		// up arrow - V2 stick
				this.state.active.view.viewMode.onDpadUp();
				break;
	
			case 68:		// V1 stick
			case 39: 		// right arrow - V2 stick
				this.state.active.view.viewMode.onDpadRight();
				break;
				
			case 83:		// V1 stick
			case 40: 		// down arrow - V2 stick
				this.state.active.view.viewMode.onDpadDown();
				break;
				
			case 65:		// V1 stick
			case 37: 		// left arrow - V2 stick
				this.state.active.view.viewMode.onDpadLeft();
				break;
				
	
			case 66: 		// B (back)				
			case 98: 		// b (back)			
				this.state.active.view.viewMode.onDpadBack();
				break;
				
				
			case 177:
				this.state.active.view.viewMode.onScrubBack();
				break;
				
			case 179:
				this.state.active.view.viewMode.onScrubPlayPause();
				break;

			case 176:
				this.state.active.view.viewMode.onScrubForward();
				break;
				
			default:    	
	//			alert('new keycode: ' + event.keyCode)
	
		}	
		
		
		
		
	};
	
	this.contentMainViewFocus = function(){};
	this.contentMainViewBlur = function(){};
	this.contentMainViewDim = function(){};
	this.contentMainViewUnDim = function(){};
	
	this.hideAllViews = function(){};
	
	this.setActiveRegion = function(reg){
		
		this.state.previous.region = this.state.active.region;
		this.state.active.region = reg;
		
		this.setRegionFocus();
	};

	this.setRegionFocus = function(){
		this.itvRegionContent.focus();
		
		if(this.state.active.region == 'menu') {
//			this.itvRegionContent.blur();
			this.itvRegionMenu.focus();
		}
		else {
			this.itvRegionMenu.blur();
//			this.itvRegionContent.focus();
		}
		
	};
	
	
	
	this.moveDpad = function(direction) {

		try {
			var nCode = 0;
			
			switch(direction) {
			
				case 'left':		//left
					nCode = 65;
					this.state.active.view.viewMode.onDpadLeft();
					break; 
	
				case 'up':		//up
					nCode = 87;
					this.state.active.view.viewMode.onDpadUp();
					break; 
	
				case 'right':		//right
					nCode = 68;
					this.state.active.view.viewMode.onDpadRight();
					break; 
	
				case 'down':		//down
					nCode = 83;
					this.state.active.view.viewMode.onDpadDown();
					break; 
					
				case 'back':
					nCode = 66;
					this.state.active.view.viewMode.onDpadBack();
					break;

				case 'enter':
					nCode = 13;
					this.state.active.view.viewMode.onDpadEnter();
					break;
			}
			
//			var e = jQuery.Event( "keydown", { keyCode: nCode } );
//			jQuery(document).trigger(e);
			
		}
		catch(err) {
			console.log("moveDPad()  ERROR: " + err.message);
		}

	}
	
};




function ItvRegion(){
	
	this.id = '';
	this.domnode = null;
	
	this.position = {x : '', y : ''};
	this.size = {w : '', h : ''};
	
	
	this.init = function(){
		this.domnode = document.getElementById(this.id);
	};
	
	this.focus = function(){
		this.domnode.classList.add('focus');
	};
	
	this.blur = function(){
		if(this.domnode.classList.contains('focus')) {
			this.domnode.classList.remove('focus');
		}
	};
	
};


	
	
	

function ItvView(){
	
	this.region = 'content';
	this.id = '';
	this.viewItems = new Array();
	this.viewItemsVisible = new Array();
	
	this.controller = null;
	this.parentView = null;
	this.menuViewItem = null;
	
	this.childView = null;
	
	this.domnode = null;
	this.ul = null;
	this.li_default = null;
	this.lis_default = new Array();
	
	this.viewMode = null;
	this.bReadOnly = false;
	this.bModalView = false;
	
	this.jsonUrl = '';
	this.jsonData = null;
	
	this.filters = new Array();
	
	
	
	
	/************************************************************************************************************************************************************/

	this.init = function() {
		
		if(this.jsonUrl!=''){
			window.AkashicStudios.getJson(this.jsonUrl,window.AkashicStudios._pageController._controller[this.id]);
		}
		
		this.domnode = document.getElementById(this.id);
		this.ul = this.domnode.getElementsByClassName('akashicitv-view-list').item(0);
		this.li_def = this.ul.removeChild(this.ul.children[0]);
		
		if(this.subclassInit != undefined){ 	this.subclassInit();}
		
	};
	
	
	
	this.onJsonLoaded = function(data){
		
		this.jsonData = data;
		
		this.loadItems();
		
		this.outputItems();
		
		if(typeof this.subclassOnJsonLoaded != 'undefined'){this.subclassOnJsonLoaded();}
		
		window.AkashicStudios._pageController._controller.MainMenuView.refreshContentViews();
		
	};
	
	
//	this.getNewViewItem = function(){};
//	this.loadItems = function() {};
	
	this.loadItems = function() {
		
		var me = this;
		
		this.jsonData.forEach(function(elem,i){
			
			var vi = me.getNewViewItem();
			var li = me.li_def.cloneNode(true);
			if(li.style.display != undefined) li.style.removeProperty('display');
			
			vi.domnode = li;
			vi.jsonData = elem;
			vi.parentView = me;
			vi.init();
			
			me.lis_default.push(li);
			me.viewItems.push(vi);
			
		});		
		
		if(this.subclassLoadItems != undefined){ 	this.subclassLoadItems();	}
		
	};


	/**
	 * clear out this.ul
	 * loop through the view items
	 * check each against the existing filters for this view, appendit it to this.ul
	 */
	this.outputItems = function(){
		
		var me = this;
		me.ul.innerHTML = '';
		me.ul.style.marginTop = '0px';
		//if(typeof me.ul.style.marginTop != 'undefined') {}
		
		me.viewItemsVisible = new Array();
		
		me.viewItems.forEach(function(elem){
			
			// check filters, if ok then append
			me.filters.forEach(function(f,i){
				
				if(f.property!=''){
					var viVal = new String(elem.jsonData[f.property]);
					if(viVal.length>0) {
						viVal = viVal.split(',');
						var idx = viVal.indexOf(f.value); 
						elem.isFilteredOut = idx==-1;						
					}
				}
				
			});
			
			
			if((!elem.isHidden) && (!elem.isFilteredOut)) {
				me.viewItemsVisible.push(elem);
				me.ul.appendChild(elem.domnode);
			}
			
			
			
		});
		
//		if(this.viewMode.setScrollWidth != undefined){ 	this.viewMode.setScrollWidth();	}
		
		
	};
	
	
	this.show = function(){

		if(this.domnode.style.display != undefined) {
			this.domnode.style.removeProperty('display');
		}
		
		if(this.subclassShow != undefined){ 
			this.subclassShow();	
		}	
		
		window.itvNavState.setBreadcrumb(this.id);
 		
	};
	

	this.hide = function() {
		this.domnode.style.display = 'none';
	};


	
	this.dim = function() {
		if(!this.domnode.classList.contains('dim')){
			this.domnode.classList.add('dim');
		}		
	};
	
	
	
	this.removeDim = function() {
//		this.domnode.style.removeProperty('opacity');
//		window.gNav.contentViewAlight();
	};

	
	this.focus = function() {
		
		window.itvNavState.state.previous.view = window.itvNavState.state.active.view;
		window.itvNavState.state.active.view = this;
		
		if(window.itvNavState.state.previous.view.region == 'content'){
			window.itvNavState.state.previous.view.blur();	
		}
		
		if(this.region == 'content'){
			
			if(window.itvNavState.state.previous.view.region == 'content'){
				if(!this.bModalView){
					window.itvNavState.state.previous.view.hide();
				}
				else {
					window.itvNavState.state.previous.view.dim();
				}
			}
		}
		
		window.itvNavState.state.active.region = this.region;
		window.itvNavState.setRegionFocus();
		
		this.inFocus = true;
		this.setFocusCss();
		
		this.show();
		
	};

	
	this.setFocusCss = function(){
		if(!this.domnode.classList.contains('focus')){
			this.domnode.classList.add('focus');
		}		
		if(this.domnode.classList.contains('dim')){
			this.domnode.classList.remove('dim');
		}		

	}
	
	
//	this.onActive = function(){
//		if(this.subclassOnActive != undefined){ 	this.subclassOnActive();	}
//	};
	
	

	this.blur = function() {
		this.inFocus = false;
		if(this.domnode.classList.contains('focus'))	this.domnode.classList.remove('focus');
		if(this.subclassBlur != undefined){ 	this.subclassBlur();	}		

		this.viewMode.hideNavArrows();
		this.blurViewItemsVisible();
	};


	this.blurViewItemsVisible = function() {
		
		this.viewItemsVisible.forEach(function(elem){
			elem.blur();
		});
		
	};
	
	
	/**
	 * attribute = dataset attribute that we are filtering on
	 * value = the value to match
	 */
	this.filter = function(attribute,value){};
	
	
	
	this.setViewItemChildViewFilters = function(filters){
		this.viewItems.forEach(function(elem){
			elem.childViewFilters = filters;
		});
	};
	
	this.getDomField = function(field) {
		if(this.domnode != null) {
			return this.domnode.getElementsByClassName(field).item(0);
		}
	};

};





function ItvViewItem() {
	
	this.id = '';
	this.jsonData = null;
	this.parentView = null;

	this.inFocus = false;
	this.isHidden = false;
	this.isFilteredOut = false;
	this.domnode = null;
	this.domDiv = null;
	
	this.childViewFilters = [{property : '',value : ''}];
	

	this.init = function(){
		this.id = this.jsonData.id;
		this.domDiv = this.domnode.children[0];
		
		//	/sites/all/themes/silversurfer/img/itv/breadcrumbs/

		if(this.subclassInit != undefined){ 	this.subclassInit();	}
	};
	
	this.getDomField = function(field) {
		if(this.domDiv != null) {
			return this.domDiv.getElementsByClassName(field).item(0);
		}
	};
	
	this.focus = function(){
		
		if(this.parentView.region == 'content'){
			window.itvNavState.state.previous.viewItem = window.itvNavState.state.active.viewItem;
			if(typeof window.itvNavState.state.previous.viewItem == 'object') {
				if(window.itvNavState.state.previous.viewItem.parentView.region=='content'){
					window.itvNavState.state.previous.viewItem.blur();
				}
			}
			
			window.itvNavState.state.active.viewItem = this;
		}
		
		if(this.parentView.region == 'menu'){
			window.itvNavState.state.previous.viewItemMenu = window.itvNavState.state.active.viewItemMenu;
			if(typeof window.itvNavState.state.previous.viewItemMenu == 'object') {
				window.itvNavState.state.previous.viewItemMenu.blur();
			}
			
			window.itvNavState.state.active.viewItem = this;
			window.itvNavState.state.active.viewItemMenu = this;
		}
		
		if(window.itvNavState.state.active.view != this.parentView){
			this.parentView.focus();
		}

		this.inFocus = true;
		if(!this.domnode.classList.contains('focus')){
			this.domnode.classList.add('focus');
		}
		
		this.domnode.focus();
		if(this.parentView.viewMode.scrollItemIntoFocus != undefined){  this.parentView.viewMode.scrollItemIntoFocus(this);}

		if(this.parentView.region == 'content'){
			this.parentView.viewMode.refreshNavArrows();
		}

		if(this.subclassFocus != undefined){ 	this.subclassFocus();	}

	};

	
	this.blur = function(){
		this.inFocus = false;
		if(this.domnode.classList.contains('focus'))	this.domnode.classList.remove('focus');
		if(this.subclassBlur != undefined){ 	this.subclassBlur();	}		
	};
	
	
	this.show = function(){

		if(this.domnode.style.display != undefined) {
			this.domnode.style.removeProperty('display');
		}
 		
	};
	

	this.hide = function() {
		this.domnode.style.display = 'none';
	};

	
	this.getDomnode = function(filters){
		
		var retval = null;
		
		//do something with filters, check against json, etc
		retval = this.domnode;
		
		return retval;
		
	};
	

	




};

	
	
	


function ItvMenuView(){
	
	this.region = 'menu';
	
	this.subclassInit = function(){
		
		this.viewMode = new ItvViewModeMenu();
		this.viewMode.view = this;
		window.itvNavState.menuView = this;
		
	};
	
	this.getNewViewItem = function(){
		return new ItvMenuViewItem();
	};

	this.subclassLoadItems = function(){
		
		
//		this.viewItems[this.viewItems.length-1].isHidden = true;
		
	}
	
	this.subclassOnJsonLoaded = function(){
		this.viewMode.moveFirst();
	};
	
	this.unhideSettings = function(){
		
		var idx = 0;
		this.viewItems.forEach(function(vi,i){
			if(vi.jsonData.view == 'SettingsView'){
				vi.isHidden = false;
				idx = i;
			}
		});
		
		this.outputItems();
		this.viewMode.moveToItem(idx);
	};
	
	
	this.getChildViewItemIndex = function(parentView){
		
		var retval = '';
		this.viewItems.forEach(function(elem,i){
			if(elem.jsonData.view == parentView) {
				retval = elem.jsonData.id;
			}
		});

		this.viewItems.forEach(function(elem,i){
			if(elem.jsonData.parentId == retval) {
				retval = i;
			}
		});
		return retval;

	};
	
	this.getParentViewItemIndex = function(id){};
	

	
	this.refreshContentViews = function(){
		
		this.viewItems.forEach(function(vi){
			vi.setContentView();
		});
		
	};
	
	
	this.hideContentViews = function(){
		
		this.viewItems.forEach(function(vi){
			if(vi.contentView!=null){
				vi.contentView.hide();
			}
		});
		
	};
	
}
ItvMenuView.prototype = new ItvView();





function ItvMenuViewItem(){
		
	this.contentView = null;
	
	this.domIcon = null;
	this.iconSrc = {blur : '', focus : ''};
	this.domMenuText = null;
	
	
	
	this.subclassInit = function(){
		
		this.domIcon = this.domDiv.children[0].children[0];
		this.domMenuText = this.domDiv.children[1];
		
		this.iconSrc.blur = AkashicStudios.themePath 
			+ '/img/itv/mainMenuView/white/' 
			+ this.jsonData.view.toLowerCase()
			+ '.png';
		
		this.iconSrc.focus = this.iconSrc.blur.replace('white',AkashicStudios._system_defaults.themeColor);
		
		this.domIcon.src = this.iconSrc.blur;
		this.domMenuText.innerHTML = this.jsonData.display_name;
		
		this.setContentView();
		
		if(this.jsonData.parentId != '0'){
			this.isHidden = true;
		}
		
		if(this.jsonData.view == 'SettingsView'){
			this.isHidden = true;
		}
		
	};
	
	
	this.setContentView = function(){
		this.contentView = AkashicStudios._pageController._controller[this.jsonData.view];
		if(this.contentView != null) {
			this.contentView.menuViewItem = this;
		}
	};
	
	
	this.subclassFocus = function(){
		
		this.domIcon.src = this.iconSrc.focus;
		
		this.parentView.hideContentViews();
		
		if(this.contentView!=null){
			this.contentView.show();
			
			if(this.contentView.viewMode.autoView==true){
				
//				window.itvNavState.itvRegionMenu.blur();
				window.itvNavState.itvRegionContent.focus();
				
					
			}
			
			if(this.contentView.onActive != undefined){ 	this.contentView.onActive();	}
		}
		
	};

	this.subclassBlur = function(){
		this.domIcon.src = this.iconSrc.blur;
		
		if(this.contentView!=null){
			if(this.contentView.onBlur != undefined){ 	this.contentView.onBlur();	}
		}

	}
	
}
ItvMenuViewItem.prototype = new ItvViewItem();



function ItvShipServicesView(){

	this.bReadOnly = true;
	
	this.jcarousel = new ItvJCarousel();
	this.jcarousel.cssClass = 'jcarousel-itvshipservicesview';
	this.jcarousel.scrollDirection = 1;
	this.jcarousel.autoScroll.autostart = true;
	this.jcarousel.autoScroll.target = '+=1';
	this.jcarousel.autoScroll.interval = 10000;
	this.jcarousel.parentView = this;
	this.jcarousel.animation.duration = 800;
	this.jcarousel.bFadeTranstion = false;

	
	this.getNewViewItem = function(){
		return new ItvShipServicesViewItem();
	};

	
	this.subclassInit = function(){
		this.viewMode = new ItvViewModeCarouselDetailShipServices();
		this.viewMode.view = this;
		this.domnode.classList.add(this.viewMode.cssClass);
		
	};


	this.subclassOnJsonLoaded = function(){
		this.show();
//		this.jcarousel.autoScroll.interval = new Number(window.AkashicStudios._system_defaults._defaults.modules.ship_services.interval.value);
		this.jcarousel.init();	

	};
	
	
	this.onActive = function(){
		this.jcarousel.jcarousel_autoscroll_start();
	};

	this.onBlur = function(){
		this.jcarousel.jcarousel_autoscroll_stop();
	};

	
	
}
ItvShipServicesView.prototype = new ItvView();

function ItvShipServicesViewItem(){
	
	this.subclassInit = function(){
		
		this.domServiceImg = this.getDomField('service-img');
		this.domTitle = this.getDomField('title');
		this.domDescription = this.getDomField('description');
		this.domDeck = this.getDomField('deck');
		this.domDeckplanImg = this.getDomField('deckplan-img');
		
		this.domServiceImg.src = this.jsonData.content_image_src;	
		this.domTitle.innerHTML = this.jsonData.display_title;
		this.domDescription.innerHTML = this.jsonData.content_text;
		this.domDeck.innerHTML = this.jsonData.ship_location;
		this.domDeckplanImg.src = this.jsonData.footer_image_src;	
		
		
		
//		this.childViewFilters = [{property : 'categories',value : this.jsonData.id}];

		
	};
	
}
ItvShipServicesViewItem.prototype = new ItvViewItem();




function ItvMovieCategoryView(){

	this.getNewViewItem = function(){
		return new ItvMovieCategoryViewItem();
	};
	
	
	this.subclassInit = function(){
		this.viewMode = new ItvViewModeOmniMovieCategory();
		this.viewMode.view = this;
		this.viewMode.omniRows = new Number(this.domnode.dataset.omnirows);
		this.viewMode.omniColumns = new Number(this.domnode.dataset.omnicolumns);
		this.domnode.classList.add(this.viewMode.cssClass);
		
	};
	
	
}
ItvMovieCategoryView.prototype = new ItvView();






function ItvMovieCategoryViewItem(){
	
	this.subclassInit = function(){
		
		this.domPoster = this.domDiv.children[0].children[0];
		this.domTextOverlay = this.domDiv.children[1];
		
		this.domPoster.src = AkashicStudios.themePath 
		+ '/img/itv/moviecategoryview/' 
		+ this.jsonData.name
		+ '.jpg';	
		
		this.domTextOverlay.innerHTML = this.jsonData.display_name;
		
		this.childViewFilters = [{property : 'categories',value : this.jsonData.id}];

		
	};
	
}
ItvMovieCategoryViewItem.prototype = new ItvViewItem();




function ItvMoviesView(){

	this.getNewViewItem = function(){
		return new ItvMoviesViewItem();
	};
	
	
	this.subclassInit = function(){
		this.viewMode = new ItvViewModeOmniMovies();
		this.viewMode.view = this;
		this.viewMode.omniRows = new Number(this.domnode.dataset.omnirows);
		this.viewMode.omniColumns = new Number(this.domnode.dataset.omnicolumns);
		this.viewMode.navArrows = {
				previous : window.AkashicStudios._pageController._controller.navArrows.top,
				next : window.AkashicStudios._pageController._controller.navArrows.bottom
		};


		this.domnode.classList.add(this.viewMode.cssClass);
		
	};
	
	
	

	
	

}
ItvMoviesView.prototype = new ItvView();





function ItvMoviesViewItem(){
	
	this.subclassInit = function(){
		
		var vf = new VodFilm();
		vf._json = this.jsonData;
		
		
		vf._json.meta_data.forEach(function(elem){
			
			var item = new VodFilmMetaData();
			item._json = elem;
			vf._metaData.push(item);

		});

		vf._json.media_items.forEach(function(elem){
			
			var item = new VodFilmMediaItem();
			item._json = elem;
			vf._mediaItems.push(item);

		});

		this.vodFilm = vf;
		
		this.populateFields();
		
		
	};
	
	
	this.populateFields = function(){
		this.domPoster = this.domDiv.children[0].children[0];
		this.domPoster.src = this.vodFilm.getMetaData('en').poster();
	};

}
ItvMoviesViewItem.prototype = new ItvViewItem();




function ItvMoviesDetailView(){
	
	this.getNewViewItem = function(){
		return new ItvMoviesDetailViewItem();
	};
	
	this.subclassInit = function(){
		
		this.viewMode = new ItvViewModeCarouselDetailMovies();
		this.viewMode.view = this;
		this.viewMode.navArrows = {
				previous : window.AkashicStudios._pageController._controller.navArrows.left,
				next : window.AkashicStudios._pageController._controller.navArrows.right
		};

		this.domnode.classList.add(this.viewMode.cssClass);

	};
	

}
ItvMoviesDetailView.prototype = new ItvView();



function ItvMoviesDetailViewItem(){
	
	
	this.populateFields = function(){
		
		this.domFields = {
				
				'poster' : this.getDomField('poster-img'),
				'title' : this.getDomField('movie-field-title'),
				'rating' : this.getDomField('movie-field-rating-img'),
				'year' : this.getDomField('movie-field-year'),
				'runtime' : this.getDomField('movie-field-runtime'),
				'directors' : this.getDomField('movie-field-directors'),
				'actors' : this.getDomField('movie-field-actors'),
				'story' : this.getDomField('movie-field-story-value')				
		};
		
		this.domFields.poster.src = this.vodFilm.getMetaData('en').poster();
		this.domFields.title.innerHTML = this.vodFilm.getMetaData('en').display_title();
		window.AkashicStudios.truncateField(this.domFields.title,'1');
		
		this.domFields.rating.src = window.AkashicStudios.themePath + '/img/itv/movie_ratings/' + this.vodFilm.rating_code() + '.png';

		window.AkashicStudios.setField(this.domFields.year,this.vodFilm.year(),'');
		window.AkashicStudios.setField(this.domFields.runtime,this.vodFilm.runtime(),'');

		window.AkashicStudios.setField(this.domFields.directors,this.vodFilm.directors(),'Directed By: ');
		window.AkashicStudios.truncateField(this.domFields.directors,'1');

		window.AkashicStudios.setField(this.domFields.actors,this.vodFilm.actors(),'Actors: ');
		window.AkashicStudios.truncateField(this.domFields.actors,'1');

		this.domFields.story.innerHTML = '';
		this.domFields.story.innerHTML = this.vodFilm.getMetaData('en').story();
		window.AkashicStudios.truncateField(this.domFields.story,'4');

		
		this._languageFlags = window.AkashicStudios.languages;
		
		var ulLang = this.getDomField('language-flags-list');
		var lf = ulLang.removeChild(ulLang.children[0]);
		
		var vf = this.vodFilm;
		this._languageFlags.forEach(function(l){
			if(vf.getMediaItem(l.code) != null) {
				var d = lf.cloneNode(true);
				d.children[0].src = l.flag_img_src;
				ulLang.appendChild(d);
			}
		});

		
	};
	
}
ItvMoviesDetailViewItem.prototype = new ItvMoviesViewItem();




function ItvMovieAudioView(){

	this.bModalView = true;
	
	this.viewItems = new Array();
	this.viewItemsVisible = new Array();
	
	this.getNewViewItem = function(){
		return new ItvMovieAudioViewItem();
	};
	
	this.subclassInit = function(){

		this.viewMode = new ItvViewModeCarouselListMovieAudio();
		this.viewMode.view = this;
		this.viewMode.navArrows = {
//				previous : window.AkashicStudios._pageController._controller.navArrows.movieaudioviewtop,
//				next : window.AkashicStudios._pageController._controller.navArrows.movieaudioviewbottom
		};
		this.domnode.classList.add(this.viewMode.cssClass);
		
	};
		
	this.loadMovieAudioItems = function(data){

		this.viewItems = new Array();
		this.viewItemsVisible = new Array();

		this.jsonData = data;
		
		this.loadItems();
		
		this.outputItems();
		
	
	};
	
	
	this.subclassShow = function(){
		
//		this.domPlayBtn = window.itvNavState.state.previous.viewItem.getDomField('movie-card-row-play-button');
//		this.domPlayBtn.style.opacity = 0.25;
		
	};

}
ItvMovieAudioView.prototype = new ItvView();


function ItvMovieAudioViewItem(){
	
	this.subclassInit = function(){
		
		this.populateFields();
		
	};
	
	this.populateFields = function(){
		this.domtext = this.getDomField('text'),
		this.domtext.innerHTML = window.AkashicStudios.local_languages[this.jsonData.language];
	};
	
}
ItvMovieAudioViewItem.prototype = new ItvViewItem();




function ItvSatelliteTvView(){

	this.getNewViewItem = function(){
		return new ItvSatelliteTvViewItem();
	};
	
	
	this.subclassInit = function(){
		this.viewMode = new ItvViewModeOmniSatelliteTv();
		this.viewMode.view = this;
		this.viewMode.omniRows = new Number(this.domnode.dataset.omnirows);
		this.viewMode.omniColumns = new Number(this.domnode.dataset.omnicolumns);
		this.viewMode.navArrows = {
				previous : window.AkashicStudios._pageController._controller.navArrows.top,
				next : window.AkashicStudios._pageController._controller.navArrows.bottom
		};
		this.domnode.classList.add(this.viewMode.cssClass);
		
	};

}
ItvSatelliteTvView.prototype = new ItvView();

function ItvSatelliteTvViewItem(){
	
	this.subclassInit = function(){
		
		this.populateFields();
		
	};
	
	this.populateFields = function(){
		this.domPoster = this.getDomField('poster-img'),
		this.domPoster.src = this.jsonData.poster_image_src;
	};
	
	
}
ItvSatelliteTvViewItem.prototype = new ItvViewItem();




function ItvTvShowsView(){

	this.getNewViewItem = function(){
		return new ItvTvShowsViewItem();
	};

	this.subclassInit = function(){
		this.viewMode = new ItvViewModeOmniTvShows();
		this.viewMode.view = this;
		this.viewMode.omniRows = new Number(this.domnode.dataset.omnirows);
		this.viewMode.omniColumns = new Number(this.domnode.dataset.omnicolumns);
		this.viewMode.navArrows = {
				previous : window.AkashicStudios._pageController._controller.navArrows.top,
				next : window.AkashicStudios._pageController._controller.navArrows.bottom
		};

		this.domnode.classList.add(this.viewMode.cssClass);
		
	};

}
ItvTvShowsView.prototype = new ItvView();

function ItvTvShowsViewItem(){
	
	this.subclassInit = function(){
		
		this.populateFields();
		
	};
	
	this.populateFields = function(){
		this.domPoster = this.getDomField('poster-img');
		this.domPoster.src = this.jsonData.poster_image_src;
	};

}
ItvTvShowsViewItem.prototype = new ItvViewItem();






function ItvTvShowsDetailView(){

	this.getNewViewItem = function(){
		return new ItvTvShowsDetailViewItem();
	};

	this.subclassInit = function(){
		this.viewMode = new ItvViewModeCarouselDetailTvShows();
		this.viewMode.view = this;
		this.viewMode.navArrows = {
				previous : window.AkashicStudios._pageController._controller.navArrows.left,
				next : window.AkashicStudios._pageController._controller.navArrows.right
		};

		this.domnode.classList.add(this.viewMode.cssClass);
		
	};

}
ItvTvShowsDetailView.prototype = new ItvView();




function ItvTvShowsDetailViewItem(){
	
	this.episodesView = null;
	
	this.subclassInit = function(){
		
		this.populateFields();
		
		this.episodesView = new ItvTvShowEpisodesView();
		this.episodesView.domnode = this.getDomField('TvShowEpisodesView');
		this.episodesView.parentView = this;
		this.episodesView.jsonData = this.jsonData.episodes;
		this.episodesView.init();	
		
	};

	
	this.populateFields = function(){
		this.domTitle = this.getDomField('tvshow-title');
		this.domTitle.innerHTML = this.jsonData.title;
		window.AkashicStudios.truncateField(this.domTitle,'2');

		this.domPoster = this.getDomField('poster-img');
		this.domPoster.src = this.jsonData.poster_image_src;
	};

	
	this.subclassFocus = function(){
		
		this.episodesView.viewItemsVisible.forEach(function(elem){
			elem.blur();
		});

		this.episodesView.viewMode.moveFirst();
		
	};
	
}
ItvTvShowsDetailViewItem.prototype = new ItvViewItem();



function ItvTvShowEpisodesView(){
	
	
	this.viewItems = new Array();
	this.viewItemsVisible = new Array();
	
	this.getNewViewItem = function(){
		return new ItvTvShowEpisodesViewItem();
	};
	
	

	this.init = function(){
		
		this.ul_episode = this.domnode.children[0];
		
		this.li_episode_def = this.ul_episode.removeChild(this.ul_episode.children[0]);


//		this.loadItems();
		
		var me_episode = this;
		this.ul_episode.innerHTML = '';
		
		this.jsonData.forEach(function(elem,i){
			
			var vi = me_episode.getNewViewItem();
			var li = me_episode.li_episode_def.cloneNode(true);
			if(li.style.display != undefined) li.style.removeProperty('display');
			
			vi.domnode = li;
			vi.jsonData = elem;
			vi.parentView = me_episode;
			vi.init();
			
			me_episode.lis_default.push(li);
			me_episode.viewItems.push(vi);
			me_episode.viewItemsVisible.push(vi);
			me_episode.ul_episode.appendChild(vi.domnode);
			
		});	
		
		
		this.viewMode = new ItvViewModeCarouselListTvEpisodes();
		this.viewMode.view = this;
		this.viewMode.navArrows = {
//				previous : window.AkashicStudios._pageController._controller.navArrows.tvepisodesviewtop,
//				next : window.AkashicStudios._pageController._controller.navArrows.tvepisodesviewbottom
		};

		this.domnode.classList.add(this.viewMode.cssClass);
		
	};
	
	
}
ItvTvShowEpisodesView.prototype = new ItvView();


function ItvTvShowEpisodesViewItem(){
	
	this.subclassInit = function(){
		
		this.populateFields();
				
	};
	
	this.populateFields = function(){
		
		var s = this.jsonData.season;
		var ep = this.jsonData.episode;
		var sep = '';
		if(s!='0' && ep!='0') {
			sep = 's:' + s + ' ep:' + ep;
		}
		
		this.domSEp = this.getDomField('sep');
		(sep=='') ? this.domSEp.parentElement.removeChild(this.domSEp) : this.domSEp.innerHTML = sep; 
//		this.domSEp.innerHTML = sep;

		this.domEpTitle = this.getDomField('ept');
		this.domEpTitle.innerHTML = this.jsonData.title;
		window.AkashicStudios.truncateField(this.domEpTitle,'1');
		
		this.domRuntime = this.getDomField('runtime');
		this.domRuntime.innerHTML =  window.AkashicStudios.getRuntime(this.jsonData.runtime);
		
		
	};

	
	this.focus = function(){
		
		this.inFocus = true;
		if(!this.domnode.classList.contains('focus')){
			this.domnode.classList.add('focus');
		}
		
		window.itvNavState.state.previous.subViewItem = window.itvNavState.state.active.subViewItem;
		if(window.itvNavState.state.previous.subViewItem!=''){
			window.itvNavState.state.previous.subViewItem.blur();
		}
		window.itvNavState.state.active.subViewItem = this;
		
		this.domnode.focus();
		if(this.parentView.viewMode.scrollItemIntoFocus != undefined){  this.parentView.viewMode.scrollItemIntoFocus(this);}

		this.parentView.viewMode.refreshNavArrows();
		
	};

}
ItvTvShowEpisodesViewItem.prototype = new ItvViewItem();


function ItvMusicView(){

	this.getNewViewItem = function(){
		return new ItvMusicViewItem();
	};
	
	this.subclassInit = function(){
		this.viewMode = new ItvViewModeOmniMusic();
		this.viewMode.view = this;
		this.viewMode.omniRows = new Number(this.domnode.dataset.omnirows);
		this.viewMode.omniColumns = new Number(this.domnode.dataset.omnicolumns);
		this.domnode.classList.add(this.viewMode.cssClass);
		
	};

	this.onBlur = function(){
		
	};

}
ItvMusicView.prototype = new ItvView();

function ItvMusicViewItem(){
	
	this.subclassInit = function(){
		
		this.populateFields();
		
		this.childViewFilters = [{property:'categoryId',value:this.jsonData.id}];
	};
	
	this.populateFields = function(){
		this.domPoster = this.getDomField('poster-img');
		this.domPoster.src = this.jsonData.poster_image_src;
		
		this.domTextOverlay = this.getDomField('text-overlay');
		this.domTextOverlay.innerHTML = this.jsonData.category;
	};
	
}
ItvMusicViewItem.prototype = new ItvViewItem();




function ItvMusicPlayerView(){
	
	
	this.musicPlayer = new MusicPlayer();
	this.musicPlayer.category = '';
	this.musicPlayer.playlistUrl = '';
	
	this.musicPlayer.loadImgSrc = {
			'loading' : '',
			'start' : '',
		};

	this.autostart = true;
	
	window.AkashicStudios.musicPlayer = this.musicPlayer;
	
	
	this.init = function(){

		if(this.jsonUrl!=''){
			window.AkashicStudios.getJson(this.jsonUrl,window.AkashicStudios._pageController._controller[this.id]);
		}
		
		this.domnode = document.getElementById(this.id);
		
		this.domHeader = this.getDomField('header');
		this.musicPlayer.domHeader = this.domHeader;
		
		this.domTitle = this.getDomField('title');
		this.musicPlayer.domTitle = this.domTitle;
		
		this.domDescription = this.getDomField('description');
		this.musicPlayer.domDescription = this.domDescription;
		
		this.domLoadImg = this.getDomField('loadImg');
		this.musicPlayer.domLoadImg = this.domLoadImg;
		
		
		this.viewMode = new ItvViewModeStaticMusicPlayer();
		this.viewMode.view = this;
		this.domnode.classList.add(this.viewMode.cssClass);

		
		
	};


	this.onJsonLoaded = function(data){
		
		this.jsonData = data;
		
		
	};

	
	this.loadMusicCategory = function() {
		
		var mc = null;
		
		if(this.filters.length>0){
		
			var tid = this.filters[0].value;
			
			this.jsonData.forEach(function(elem){
				if(tid == elem.id) {
					mc  = elem;
				}
			});

		}
		
		if(mc==null){
			mc = jsonData[0];
		}
		
		this.musicPlayer.category = mc.category;
		this.musicPlayer.playlistUrl = mc.playlist;
		
		
		this.domHeader.innerHTML = mc.category;
		this.domnode.style.backgroundImage = 'url("'+mc.background_image_src+'")';
		
	}

	
	
	this.playChannel = function(){
		
		this.loadMusicCategory();
		this.musicPlayer.setupRandomRepeat();

		
	};
	
	
	
	
}
ItvMusicPlayerView.prototype = new ItvView();


function ItvFleetTrackerView(){

	this.bReadOnly = true;
	
	this.shipTracker = null;
	this.bMapInit = false;
	
	this.getNewViewItem = function(){
		return new ItvFleetTrackerViewItem();
	};
	
	this.init = function(){
		
		this.domnode = document.getElementById(this.id);
		this.ul = this.domnode.getElementsByClassName('akashicitv-view-list').item(0);
		this.li_def = this.ul.removeChild(this.ul.children[0]);
		
		this.jsonData = [{id:'0',title:'FleetTracker'}];
		
		this.loadItems();		
		this.outputItems();

		this.shipTracker = new FleetTracker();
		this.shipTracker.mapDiv = 'fleet-tracker-mapview';
		this.shipTracker.bIsItv = true;
		this.shipTracker.rotationIntervalMS = 5000;
		
		this.shipTracker.init(true);
		
		this.viewMode = new ItvViewModeStaticFleetTracker();
		this.viewMode.view = this;
		this.domnode.classList.add(this.viewMode.cssClass);
		
	};
	
	this.startRotation = function(){
		
		this.mapReset();
		
		this.mapInterval = 
			window.setInterval(
				function(){
					window.AkashicStudios._pageController._controller.FleetTrackerView.shipTracker.mapRotate();
				}, this.shipTracker.rotationIntervalMS);

	};
	
	this.stopRotation = function(){
		window.clearInterval(this.mapInterval);
	};
	
	this.mapReset = function(){
		
		this.shipTracker.mapResetToGlobalView();	
	};
	
	
	this.onActive = function(){
		this.startRotation();
	};

	this.onBlur = function(){
		this.stopRotation();
	}
	
	
}
ItvFleetTrackerView.prototype = new ItvView();

function ItvFleetTrackerViewItem(){
	
	this.subclassOnFocus = function(){
		this.startRotation();		
	};
}
ItvFleetTrackerViewItem.prototype = new ItvViewItem();


function ItvSettingsView(){

	this.getNewViewItem = function(){
		return new ItvSettingsViewItem();
	};

	
	this.subclassInit = function(){
		this.viewMode = new ItvViewModeOmni();
		this.viewMode.view = this;
		this.viewMode.omniRows = new Number(this.domnode.dataset.omnirows);
		this.viewMode.omniColumns = new Number(this.domnode.dataset.omnicolumns);
		this.domnode.classList.add(this.viewMode.cssClass);
		
	};

	
}
ItvSettingsView.prototype = new ItvView();

function ItvSettingsViewItem(){}
ItvSettingsViewItem.prototype = new ItvViewItem();



function ItvTickerView(){

	this.jcarousel = new ItvJCarousel();
	this.jcarousel.cssClass = 'jcarousel-itvtickerview';
	this.jcarousel.scrollDirection = 0;
	this.jcarousel.autoScroll.autostart = true;
	this.jcarousel.autoScroll.target = '+=1';
	this.jcarousel.autoScroll.interval = 10000;
	this.jcarousel.parentView = this;
	
	this.getNewViewItem = function(){
		return new ItvTickerViewItem();
	};

	
	this.subclassInit = function(){
		this.viewMode = new ItvViewModeCarouselListTicker();
		this.viewMode.view = this;
		this.domnode.classList.add(this.viewMode.cssClass);
	
		this.domHeader = this.getDomField('itvtickerview-header');
		this.domHeaderText = this.getDomField('itvtickerview-header-text');
		
	};
	
	this.subclassOnJsonLoaded = function(){
		this.jcarousel.autoScroll.interval = new Number(window.AkashicStudios._system_defaults._defaults.modules.ticker.interval.value);
		this.jcarousel.init();	
		
		var d = this.ul.children[0].children[0].dataset;
		this.setHeader(d.bgcolor,d.headertext);
		
	};
	
	this.setHeader = function(color,text){
		
		if(this.domHeaderText.innerHTML != text) {
			
			var me = this;
			jQuery('.'+this.domHeaderText.className).fadeOut(function(){jQuery(this).html(text);});
			jQuery('.'+this.domHeader.className).animate({'background-color' : color});
			jQuery('.'+this.domHeaderText.className).fadeIn();
		}
		
	};
	
	this.onJCarouselFullyVisibleIn = function(li){
		
		var d = li.children[0].dataset;
		this.setHeader(d.bgcolor,d.headertext);
		
	};

	this.onJCarouselCreate = function(ul){};

}
ItvTickerView.prototype = new ItvView();


function ItvTickerViewItem(){

	this.subclassInit = function(){
		
		this.populateFields();
		
	};
	
	this.populateFields = function(){
		
		this.domDiv.dataset.bgcolor = '#' + window.AkashicStudios._system_defaults._defaults.modules.ticker.category[this.jsonData.category].color.value;
		this.domDiv.dataset.headertext = this.jsonData.category_display_name;
		
		this.domDiv.innerHTML = this.jsonData.body;
		window.AkashicStudios.truncateField(this.domDiv,'2');
		
	};
	
	
}
ItvTickerViewItem.prototype = new ItvViewItem();






function ItvViewMode(){
	
	this.scrollDirection = 0;
	this.view = null;
	this.cssClass = '';
	
	this.viewItemActiveIndex = 0;
	this.viewItemPreviousIndex = 0;
	
	this.autoView = false;
	this.bScreenTop = false;
	this.bScreenBottom = false;
	
	this.navArrows = {
			previous : null,
			next : null
	};

	
	
	/**
	 * show all the child arrows
	 *
	 */
	this.showNavArrows = function () {		
		if((this.navArrows.previous!=null) && (this.navArrows.next!=null)) {
			this.navArrows.previous.show();
			this.navArrows.next.show();
		}
	};
	
	this.hideNavArrows = function () {
		if((this.navArrows.previous!=null) && (this.navArrows.next!=null)) {
			this.navArrows.previous.hide();
			this.navArrows.next.hide();
		}
	};
	
	this.dimNavArrows = function () {
		if((this.navArrows.previous!=null) && (this.navArrows.next!=null)) {
			this.navArrows.previous.dim();
			this.navArrows.next.dim();
		}
	};
	

	
	/**
	 * iff we are at the firstChild or lastElementChild, hide the appropriate arrows
	 */
	this.refreshNavArrows = function () {

		var p = false;
		var n = false;
		
		if(this.viewItemActiveIndex==0){
			p = false;
		}
		
		if(this.viewItemActiveIndex > 0){
			p = true;
		}

		if(this.viewItemActiveIndex < this.view.viewItemsVisible.length-1){
			n = true;
		}
		
		if(this.viewItemActiveIndex == this.view.viewItemsVisible.length-1){
			n = false;
		}
		
		var sub = null;
		if(this.subclassRefreshNavArrows != undefined){ 	
			sub = this.subclassRefreshNavArrows(p,n);	
		}
		
		if(sub!=null){
			p = sub.prev;
			n = sub.next;
		}
	
		if(this.navArrows.previous!=null) {
			this.navArrows.previous.visible = p;
			this.navArrows.previous.refreshVisible();
		}
		if(this.navArrows.next!=null) {
			this.navArrows.next.visible = n;
			this.navArrows.next.refreshVisible();
		}
		
	};
	
	
	
	
	this.onDpadUp = function(){
		this.movePrevious();
	};
	
	this.onDpadDown = function(){
		this.moveNext();
	};

	this.onDpadLeft = function(){
		this.movePrevious();
	};
	
	this.onDpadRight = function(){
		this.moveNext();
	};

	this.onScrubBack = function(){};
	this.onScrubPlayPause = function(){};
	this.onScrubForward = function(){};
	
	this.onDpadEnter = function(){};

	this.onDpadBack = function(){
		if(window.itvNavState.viewItemStack.length>0){
			var vi = window.itvNavState.viewItemStack.pop();
			vi.focus();
		}
		else {
			this.moveFirst();
			window.itvNavState.state.active.viewItemMenu.focus();
		}
		
//		this.hideNavArrows();
		
		if(this.subclassOnDpadBack != undefined){ 	this.subclassOnDpadBack();	}
	};


	this.movePrevious = function(){
		
		this.viewItemPreviousIndex = this.viewItemActiveIndex;
		this.viewItemActiveIndex--;
		var bMove = false;
		
		if(this.viewItemActiveIndex<0)	{ 
		
			this.viewItemActiveIndex=0;
			
			if(window.itvNavState.viewItemStack.length==0){
				if(this.view.menuViewItem != null){
					this.view.menuViewItem.focus();
				}
			}
		}
		else {
			bMove = true;
		}
				
		if(bMove) {
			var newItem = this.view.viewItemsVisible[this.viewItemActiveIndex]; 
			newItem.focus();
		}
		
		if(this.subclassMovePrevious != undefined){ 	this.subclassMovePrevious();	}
		
		
		
	};
	
	this.moveNext = function(){
		
		this.viewItemPreviousIndex = this.viewItemActiveIndex;
		this.viewItemActiveIndex++;
		var bMove = true;
		
		if(this.viewItemActiveIndex==this.view.viewItemsVisible.length)	{ 
			this.viewItemActiveIndex--;
			bMove = false;		
		}
		
		if(bMove){
			var newItem = this.view.viewItemsVisible[this.viewItemActiveIndex]; 
			newItem.focus();
		}
		
		if(this.subclassMoveNext != undefined){ 	this.subclassMoveNext();	}
		
	};
	
	this.moveFirst = function(){
		this.viewItemPreviousIndex = this.viewItemActiveIndex;
		this.viewItemActiveIndex=0;
		
		this.view.viewItemsVisible[this.viewItemActiveIndex].focus();

		if(this.subclassMoveFirst != undefined){ 	this.subclassMoveFirst();	}
	};
	
	this.moveLast = function(){

		this.viewItemPreviousIndex = this.viewItemActiveIndex;
		this.viewItemActiveIndex = this.view.viewItemsVisible.length-1;
		
		this.view.viewItemsVisible[this.viewItemActiveIndex].focus();
		
		if(this.subclassMoveLast != undefined){ 	this.subclassMoveLast();	}
	};
	
	
	this.moveToItem = function(index){
		
		var me = this;
		
		this.viewItemPreviousIndex = this.viewItemActiveIndex;
		this.viewItemActiveIndex = index;
				
		this.view.viewItemsVisible[this.viewItemActiveIndex].focus();
	
		if(this.subclassMoveToItem != undefined){ 	this.subclassMoveToItem(index);	}
	};

	
	
	
	
	
	
	
	
	
	
	
	
		
};


function ItvViewModeMenu(){
	
	this.scrollDirection = window.itvNavState.scrollDirection.vertical;
	this.cssClass = 'itvviewmode-menu';
	
	
	this.onDpadRight = function(){
		
		var vi = this.view.viewItemsVisible[this.viewItemActiveIndex];
		
		var bMoveRight = true;
		
		if(vi.contentView != null) {
			
			bMoveRight = !vi.contentView.bReadOnly;
			
			if(bMoveRight){
				bMoveRight = (vi.contentView.viewItemsVisible.length > 0);
			}
			
		}
		
		if(bMoveRight) {
			vi.contentView.viewMode.moveFirst();
		}
		
	};
	
	this.onDpadEnter = function(){
		this.onDpadRight();	
	};
	
	this.onDpadLeft = function(){};
	
	this.onDpadBack = function(){
		this.moveFirst();
	};
	
}
ItvViewModeMenu.prototype = new ItvViewMode();



function ItvViewModeOmni(){

	this.scrollDirection = 2;
	this.cssClass = 'itvviewmode-omni';
	this.omniRows = 0;
	this.omniColumns = 0;

	this.onDpadUp = function(){
		this.moveOmniUp();
	};
	
	this.onDpadDown = function(){
		this.moveOmniDown();
	};
	
	this.onDpadEnter = function(){
		var vi = window.itvNavState.state.active.viewItem;
		
		if(this.subclassOnDpadEnter != undefined){ 	this.subclassOnDpadEnter(vi);	}
		
	};

	
	this.moveOmniUp = function(){
		
		this.viewItemPreviousIndex = this.viewItemActiveIndex;
		this.viewItemActiveIndex = this.viewItemActiveIndex - this.omniColumns;
		
		if(this.viewItemActiveIndex<0)	{ this.viewItemActiveIndex=0;}
		
		var newItem = this.view.viewItemsVisible[this.viewItemActiveIndex]; 
		newItem.focus();
//		this.scrollItemIntoFocus(newItem);
				
	};
	
	this.moveOmniDown = function(){
		
		this.viewItemPreviousIndex = this.viewItemActiveIndex;
		this.viewItemActiveIndex = this.viewItemActiveIndex + this.omniColumns;
		
		if(this.viewItemActiveIndex >= this.view.viewItemsVisible.length)	{ this.viewItemActiveIndex = this.view.viewItemsVisible.length-1;}
		
		var newItem = this.view.viewItemsVisible[this.viewItemActiveIndex]; 
		newItem.focus();
//		this.scrollItemIntoFocus(newItem);
		
	};	
	
	
	
	
	this.scrollItemIntoFocus = function(vi){
		
		var domView = this.view.domnode;
		
		var ulMarginLeft = 0 + Math.floor(this.view.ul.style.marginLeft.replace('px',''));
		var ulMarginTop = 0 + Math.floor(this.view.ul.style.marginTop.replace('px',''));
		
		var scrollerDivLeft = domView.offsetLeft;
		var leftEdge = scrollerDivLeft + vi.domnode.offsetLeft;
		var rightEdge = leftEdge + vi.domnode.offsetWidth;
		var scrollerDivRight = scrollerDivLeft + domView.clientWidth;		//
		
/*			console.log('scrollerDivLeft: '+scrollerDivLeft);
		console.log('leftEdge: '+leftEdge);
		console.log('scrollerDivRight: '+scrollerDivRight);
		console.log('rightEdge: '+rightEdge);		*/
		
		if(domView.clientWidth != 1) {
		
			if(vi.domnode.offsetTop >= domView.clientHeight) {
				vi.domnode.parentElement.style.marginTop = ulMarginTop - (vi.domnode.offsetTop - domView.clientHeight) - vi.domnode.clientHeight + 'px';
			}

			if(vi.domnode.offsetTop < 0) {

				vi.domnode.parentElement.style.marginTop = (ulMarginTop - vi.domnode.offsetTop) + 'px';
			}
			
		}
		
	};
	
	
	this.subclassRefreshNavArrows = function(p,n){
		
		
		var pos = this.viewItemActiveIndex;
		var len = this.view.viewItemsVisible.length;
		
		var grid = this.omniRows * this.omniColumns;

		var rowFirst = 0;
		var rowSecond = rowFirst + this.omniColumns;
		
		if(pos < rowSecond){
			p = false;
			this.bScreenTop = true;
		}

		if(pos >= (rowSecond+this.omniColumns)){
			this.bScreenTop = false;
		}

		if(pos >= rowSecond){
			p = !this.bScreenTop;
		}
		

		var rowLast = len - this.omniColumns;
		if(len % this.omniColumns >0) {
			rowLast = len - (len % this.omniColumns);
		}
		var row2ndToLast = rowLast - this.omniColumns;

		
		if(pos < row2ndToLast) {
			this.bScreenBottom = false;
		}

		if(pos >= row2ndToLast) {
			n = !this.bScreenBottom;
		}
		
		if(pos >= rowLast) {
			n = false;
			this.bScreenBottom = true;
		}

		if(len <= grid){
			p = false;
			n = false;
		}
		
		return {'prev':p,'next':n};
	};
	
	
}
ItvViewModeOmni.prototype = new ItvViewMode();



function ItvViewModeOmniMovieCategory(){
	
	this.subclassOnDpadEnter = function(vi){
		
		var viewName = 'MoviesView';	//window.itvNavState.menuView.viewItems[window.itvNavState.menuView.getChildViewItemIndex(vi.parentView.id)].jsonData.view;
		
		var newView = this.view.controller[viewName];
		this.view.controller.MoviesView.filters = vi.childViewFilters;
		this.view.controller.MoviesView.setViewItemChildViewFilters(vi.childViewFilters);
		this.view.controller.MoviesView.outputItems();
		
		this.view.controller.MoviesView.viewMode.moveFirst();
		window.itvNavState.viewItemStack.push(vi);

		this.view.controller.MoviesDetailView.filters = vi.childViewFilters;
		this.view.controller.MoviesDetailView.setViewItemChildViewFilters(vi.childViewFilters);
		this.view.controller.MoviesDetailView.outputItems();

	};
	
}
ItvViewModeOmniMovieCategory.prototype = new ItvViewModeOmni();



function ItvViewModeOmniMovies(){
	
	
	this.subclassOnDpadEnter = function(vi){
		
		var idx = 0;
		this.view.controller.MoviesDetailView.viewItemsVisible.forEach(function(v,i){
			if(v.jsonData.id == vi.jsonData.id) {
				idx = i;
			}
		});
		
		this.view.controller.MoviesDetailView.viewMode.moveToItem(idx);
		window.itvNavState.viewItemStack.push(vi);
		
//		alert('ItvViewModeOmniMovies::onDpadEnter() ' + viewName);

	};
	
	
	
}
ItvViewModeOmniMovies.prototype = new ItvViewModeOmni();



function ItvViewModeOmniSatelliteTv(){
	
	this.subclassOnDpadEnter = function(vi){
		
		window.location.href = window.AkashicStudios._system_defaults._defaults.modules.vod.vod_play_url.value + vi.jsonData.media_id;
		
	};
	
	
}
ItvViewModeOmniSatelliteTv.prototype = new ItvViewModeOmni();



function ItvViewModeOmniTvShows(){
	

	
	this.subclassOnDpadEnter = function(vi){
		
//		var viewName = 'TvShowsDetailView';

		var idx = 0;
		this.view.controller.TvShowsDetailView.viewItemsVisible.forEach(function(v,i){
			if(v.jsonData.id == vi.jsonData.id) {
				idx = i;
			}
		});
		
		this.view.controller.TvShowsDetailView.viewMode.moveToItem(idx);
		window.itvNavState.viewItemStack.push(vi);

//		alert('ItvViewModeOmniTvShows::onDpadEnter() ' + viewName);
	};
	
	
}
ItvViewModeOmniTvShows.prototype = new ItvViewModeOmni();


function ItvViewModeOmniMusic(){
	
	this.subclassOnDpadEnter = function(vi){
		
		window.itvNavState.viewItemStack.push(vi);
		
		this.view.controller.MusicPlayerView.filters = vi.childViewFilters;
		this.view.controller.MusicPlayerView.focus();
		this.view.controller.MusicPlayerView.playChannel();
				
//		alert('ItvViewModeOmniMusic::onDpadEnter() ' + viewName);
	};
	
	
}
ItvViewModeOmniMusic.prototype = new ItvViewModeOmni();



function ItvViewModeCarousel(){
	this.scrollDirection = 1;
	this.cssClass = 'itvviewmode-carousel';
	
	
	this.onDpadLeft = function(){
		this.movePrevious();
	};
	this.onDpadRight = function(){
		this.moveNext();
	};

	
	this.onDpadEnter = function(){
		var vi = window.itvNavState.state.active.viewItem;
		
		if(this.subclassOnDpadEnter != undefined){ 	this.subclassOnDpadEnter(vi);	}
		
	};

	this.scrollItemIntoFocus = function(vi){
		
		this.setScrollWidth();
		
		var domView = this.view.domnode;
		
		var ulMarginLeft = 0 + Math.floor(this.view.ul.style.marginLeft.replace('px',''));
		var ulMarginTop = 0 + Math.floor(this.view.ul.style.marginTop.replace('px',''));
		
		var scrollerDivLeft = domView.offsetLeft;
		var leftEdge = scrollerDivLeft + vi.domnode.offsetLeft;
		var rightEdge = leftEdge + vi.domnode.offsetWidth;
		var scrollerDivRight = scrollerDivLeft + domView.clientWidth;		//
		
/*			console.log('scrollerDivLeft: '+scrollerDivLeft);
		console.log('leftEdge: '+leftEdge);
		console.log('scrollerDivRight: '+scrollerDivRight);
		console.log('rightEdge: '+rightEdge);		*/
		
		if(domView.clientWidth != 1) {

			if(rightEdge > scrollerDivRight) {
				this.view.ul.style.marginLeft = (ulMarginLeft - (rightEdge - scrollerDivRight)) + 'px';

			}

			if(leftEdge < scrollerDivLeft) {

				this.view.ul.style.marginLeft = (ulMarginLeft + (scrollerDivLeft - leftEdge)) + 'px';
			}
			
		}
		
	};
	
	
	this.setScrollWidth = function(){
		
		this.view.ul.style.width = (this.view.viewItemsVisible[0].domnode.clientWidth * this.view.viewItemsVisible.length) + 'px';
		
	};
	
	
	
	
	

}
ItvViewModeCarousel.prototype = new ItvViewMode();



function ItvViewModeCarouselDetail(){
	this.cssClass = 'itvviewmode-carouseldetail';
	
}
ItvViewModeCarouselDetail.prototype = new ItvViewModeCarousel();


function ItvViewModeStatic(){
	this.cssClass = 'itvviewmode-static';
}
ItvViewModeStatic.prototype = new ItvViewMode();



function ItvViewModeStaticFleetTracker(){
	this.cssClass = 'itvviewmode-static-fleettracker';
	this.autoView = true;
	
	
	this.subclassOnDpadBack = function(){
		this.view.stopRotation();
		this.view.mapReset();
		this.view.menuViewItem.focus();		
	};
	
//	this.onDpadBack = function(){
//	};
	
	this.onDpadLeft = function(){
		this.onDpadBack();
	};
	
	this.onDpadUp = function(){};
	this.onDpadRight = function(){};
	this.onDpadDown = function(){};
	this.onDpadEnter = function(){};
	
}
ItvViewModeStaticFleetTracker.prototype = new ItvViewModeStatic();





function ItvViewModeStaticMusicPlayer(){
	
	this.onDpadEnter = function(vi){
		this.view.musicPlayer.togglePlay();		// toggle music play
	};
	
	this.onDoubleMovePrevious = function(){};
	
	this.onDpadLeft = function(){
		this.view.musicPlayer.restartTrack();
		//previous track ?
	};
	
	this.onDpadRight = function(){
		this.view.musicPlayer.nextTrack();
		//next track
	};
	
	this.subclassOnDpadBack = function(){
		this.view.musicPlayer.stop();
	};
	
	this.onScrubBack = function(){
		this.onDpadLeft();
	};
	
	this.onScrubPlayPause = function(){
		this.onDpadEnter();
	};
	
	this.onScrubForward = function(){
		this.onDpadRight();
	};

	
}
ItvViewModeStaticMusicPlayer.prototype = new ItvViewModeStatic();




function ItvViewModeCarouselDetailMovies(){
	
	this.subclassOnDpadEnter = function(vi){
		
		var viewName = 'MovieAudioView';
		
		window.itvNavState.viewItemStack.push(vi);
		
		this.view.controller.MovieAudioView.loadMovieAudioItems(vi.jsonData.media_items);
		
//		this.view.controller.MovieAudioView.focus();
		this.view.controller.MovieAudioView.viewMode.moveFirst();

	};
	
	
	this.replaceViewStackParent = function(){
		
		var replaceItem = this.view.controller.MoviesView.viewItemsVisible[this.viewItemActiveIndex];
		replaceItem.parentView.viewMode.viewItemActiveIndex = this.viewItemActiveIndex;
		window.itvNavState.viewItemStack.pop();
		window.itvNavState.viewItemStack.push(replaceItem);

	};
	
	this.subclassMovePrevious = function(){
		this.replaceViewStackParent();
	};
	
	this.subclassMoveNext = function(){
		this.replaceViewStackParent();		
	};
	
//	this.onDpadUp = function(){};
//	this.onDpadDown = function(){};

}
ItvViewModeCarouselDetailMovies.prototype = new ItvViewModeCarouselDetail();






function ItvViewModeCarouselDetailTvShows(){

	
	this.subclassOnDpadEnter = function(){
		
		window.itvNavState.state.active.viewItem.episodesView.viewMode.onDpadEnter();
//		alert('ItvViewModeCarouselDetailTvShows::subclassOnDpadEnter()');
	};
	
	
	
	this.subclassOnDpadBack = function(){
		
		window.itvNavState.state.active.subViewItem.parentView.viewMode.hideNavArrows();
		
		window.itvNavState.state.previous.subViewItem = '';
		window.itvNavState.state.active.subViewItem = '';
		
		
		
	};

	
	this.replaceViewStackParent = function(){
		
		var replaceItem = this.view.controller.TvShowsView.viewItemsVisible[this.viewItemActiveIndex];
		replaceItem.parentView.viewMode.viewItemActiveIndex = this.viewItemActiveIndex;
		window.itvNavState.viewItemStack.pop();
		window.itvNavState.viewItemStack.push(replaceItem);
		
		
	};
	
	this.subclassMovePrevious = function(){
		this.replaceViewStackParent();
	};
	
	this.subclassMoveNext = function(){
		this.replaceViewStackParent();	
		
	};
	

	this.onDpadUp = function(){
		window.itvNavState.state.active.viewItem.episodesView.viewMode.onDpadUp();
	};
	
	this.onDpadDown = function(){
		window.itvNavState.state.active.viewItem.episodesView.viewMode.onDpadDown();
	};

}
ItvViewModeCarouselDetailTvShows.prototype = new ItvViewModeCarouselDetail();



function ItvViewModeCarouselDetailShipServices(){
	
	this.scrollDirection = 1;
	
	this.onDpadUp = function(){};
	this.onDpadDown = function(){};
	this.onDpadEnter = function(){};
//	this.onDpadLeft = function(){};
	this.onDpadRight = function(){};
	
}
ItvViewModeCarouselDetailShipServices.prototype = new ItvViewModeCarouselDetail();



function ItvViewModeCarouselList(){
	
	this.scrollDirection = 0;
	this.cssClass = 'itvviewmode-carousellist';

	
	this.scrollItemIntoFocus = function(vi){
		
		var domView = vi.parentView.domnode;
		var ul = vi.domnode.parentElement;
		
		var ulMarginTop = 0 + Math.floor(ul.style.marginTop.replace('px',''));
//		var ulMarginBottom = ulMarginTop + ul.clientHeight;
		var liOffsetTop = vi.domnode.offsetTop-domView.offsetTop;
//		var liBottom = liOffsetTop + vi.domnode.clientHeight;
		
		if(domView.clientWidth != 1) {
		
			if((liOffsetTop) >= domView.clientHeight) {
				ul.style.marginTop = ulMarginTop - (liOffsetTop - domView.clientHeight) - vi.domnode.clientHeight + 'px';
			}
	
			if(liOffsetTop < 0) {
				ul.style.marginTop = (ulMarginTop - liOffsetTop) + 'px';
			}
			
		}
		
	};

}
ItvViewModeCarouselList.prototype = new ItvViewModeCarousel();



function ItvViewModeCarouselListMovieAudio(){
	

	this.cssClass = 'itvviewmode-carousellist-movieaudio';

	this.onDpadEnter = function(){

		var vi = window.itvNavState.state.active.viewItem;
		window.location.href = window.AkashicStudios._system_defaults._defaults.modules.vod.vod_play_url.value + vi.jsonData.media_id;
		
	};
	

	
	
	
	this.scrollItemIntoFocus = function(vi){
		
		var domView = vi.parentView.domnode;
		var ctr = vi.parentView.getDomField('media-items-list');
		var ul = vi.domnode.parentElement;
		
		var ctrTop = 0 + ctr.offsetTop; 
		var ctrBottom = ctrTop + ctr.clientHeight;
			
		var ulTop = 0 + Math.floor(ul.style.marginTop.replace('px',''));
		
		var liTop = vi.domnode.offsetTop;
		var liBottom = liTop + vi.domnode.clientHeight;
		
		
		if(domView.clientWidth != 1) {
		
			if(liBottom > ctrBottom) {
				var val = ulTop - (liBottom-ctrBottom);
				if(vi.parentView.viewItemsVisible.length-1 == this.viewItemActiveIndex){
					val -= 5;
				}
				ul.style.marginTop = val + 'px';
			}
	
			if(liTop < ctrTop) {
				var val = ulTop + (ctrTop - liTop);
				if(this.viewItemActiveIndex==0){
					val += 5;
				}
				ul.style.marginTop = val + 'px';	
			}
			
		}
		
		
		
	};

	
	
}
ItvViewModeCarouselListMovieAudio.prototype = new ItvViewModeCarouselList();




function ItvViewModeCarouselListTvEpisodes(){

	
	this.onDpadEnter = function(){

		var vi = window.itvNavState.state.active.subViewItem;
		window.location.href = window.AkashicStudios._system_defaults._defaults.modules.vod.vod_play_url.value + vi.jsonData.media_id;
		
	};
	
	
	this.scrollItemIntoFocus = function(vi){
		
		var domView = vi.parentView.domnode;
		var ctr = domView;
		var ul = vi.domnode.parentElement;
		
		var ctrTop = 0 + ctr.offsetTop; 
		var ctrBottom = ctrTop + ctr.clientHeight;
			
		var ulTop = 0 + Math.floor(ul.style.marginTop.replace('px',''));
		
		var liTop = vi.domnode.offsetTop;
		var liBottom = liTop + vi.domnode.clientHeight;
		
		
		if(domView.clientWidth != 1) {
		
			if(liBottom > ctrBottom) {
				var val = ulTop - (liBottom-ctrBottom);
				if(vi.parentView.viewItemsVisible.length-1 == this.viewItemActiveIndex){
					val -= 5;
				}
				ul.style.marginTop = val + 'px';
			}
	
			if(liTop < ctrTop) {
				var val = ulTop + (ctrTop - liTop);
				if(this.viewItemActiveIndex==0){
					val += 5;
				}
				ul.style.marginTop = val + 'px';
			}
			
		}
		
		
		
	};

	

	
}
ItvViewModeCarouselListTvEpisodes.prototype = new ItvViewModeCarouselList();



function ItvViewModeCarouselListTicker(){
	this.scrollItemIntoFocus = function(vi){};
}
ItvViewModeCarouselListTicker.prototype = new ItvViewModeCarouselList();



function ItvArrow() {
	
	this.domnode = null;
	this.name = '';
	this.id = '';
	this.opacity = '1';
	this.visible = false;
	
	
	this.init = function(params){
		
		this.name = params.name;
		this.visible = params.visible;
		this.id = params.id;
		this.opacity = params.opacity;
		this.domnode = document.getElementById(this.id);
		
//		this.hide();
	};
	
	this.hide = function(){
		if(this.domnode!=null){
			this.domnode.style.opacity = 0;
		}
		this.visible = false;
	};
	
	this.show = function(){
		if(this.domnode!=null){
			this.domnode.style.opacity = this.opacity; 
		}
		this.visible = true;

	};
	
	this.dim = function(){
		if(this.domnode!=null){
			this.domnode.style.opacity = this.opacity * 0.5;
		}
		this.visible = true;
	};
	
	this.refreshVisible = function(){
		(this.visible) ? this.show() : this.hide();
	};

}





function ItvJCarousel() {
	
	this.cssClass = '';
	this.parentView = null;
	this.scrollDirection = 0;
	
	this.domnode = {};
	this.ul = {};
	
	this.bFadeTranstion = false;
	

	this.autoScroll = {
			'autostart' : false,
			'target' : '+=1',
			'interval' : 5000
	};
	
	this.animation = { duration : 700};

	
	
	this.init = function() {
		
		this.loadDomNode();
		
		this.jcarousel_init();
		
		if(this.scrollDirection==0){
			this.jcarousel_setheight();
		}

		if(this.scrollDirection==1){
			this.jcarousel_setwidth();
		}

	}
	
	
	
	this.jcarousel_setwidth = function(){
		var w = 0;
		var lis = this.ul.children;
		
		for(var i=0; i<lis.length; i++) {
			w += lis[i].scrollWidth;
		}
				
		this.ul.style.width = w + 'px';
	}
	

	this.jcarousel_setheight = function(){
		var h = 0;
		var lis = this.ul.children;
		
		for(var i=0; i<lis.length; i++) {
			h += lis[i].scrollHeight;
		}
				
		this.ul.style.height = h + 'px';
	}

	
	
	
	this.loadDomNode = function(){
			
		var dn = document.getElementsByClassName(this.cssClass);
		if(dn != null) {
			this.domnode = dn.item(0);
		}		
		
		this.ul = this.domnode.firstElementChild;
		
	}
	
	
	
	this.jcarousel_init = function(){	
		var me = this;
		
//		if(this.bFadeTranstion) {
//			
//			var jc = jQuery('.'+this.cssClass).jcarousel({
//				wrap: 'circular',
//				animation: {
//			        duration: me.animation.duration,
//			        easing:   'linear'},
//		        itemLoadCallback: {
//		            onBeforeAnimation: function(carousel){
//		            	var JCcontainerID = carousel.clip.context.id;
//		            	jQuery('#' + JCcontainerID).fadeOut();
//		            },
//		            onAfterAnimation: function(carousel){
//		            	var JCcontainerID = carousel.clip.context.id;
//		            	jQuery('#' + JCcontainerID).fadeIn();
//		            }
//		        }
//			        
//			});
//			
//		
//		}
//		else {
//		
			var jc = jQuery('.'+this.cssClass).jcarousel({
				wrap: 'circular',
				animation: {
			        duration: me.animation.duration,
			        easing:   'linear'}
			        
			});
//			
//		}
		
		if(me.bFadeTranstion){
			
			jQuery('.'+this.cssClass).on('jcarousel:scroll', function(event, carousel) {
			    // "this" refers to the root element
			    // "carousel" is the jCarousel instance
//				if(me.bFadeTranstion){
	            	jQuery('.'+me.cssClass).fadeOut(100);
//				}
			});
			
			jQuery('.'+this.cssClass).on('jcarousel:scrollend', function(event, carousel) {
			    // "this" refers to the root element
			    // "carousel" is the jCarousel instance
				
//				if(me.bFadeTranstion){
	            	jQuery('.'+me.cssClass).fadeIn();
//				}
			});
			
		}
		
//		jQuery('.'+this.cssClass+' .jcarousel-prev').jcarouselControl({
//	        target: '-=1',
//			carousel: jc
//	    });
//	
//		jQuery('.'+this.cssClass+' .jcarousel-next').jcarouselControl({
//	        target: '+=1',
//	        carousel: jc
//	    });
		
//		    autostart: this.autoScroll.autostart,
		
		jQuery('.'+this.cssClass).jcarouselAutoscroll({
		    target: this.autoScroll.target,
		    interval: this.autoScroll.interval
		});
		
		jQuery('.'+this.cssClass).on('jcarousel:fullyvisiblein', 'li', function(event, carousel) {
			
			if(me.parentView.onJCarouselFullyVisibleIn != undefined) {
				me.parentView.onJCarouselFullyVisibleIn(this);
			}
			
		});
		
		
		jQuery('.'+this.cssClass).on('jcarousel:create', function(event, carousel) {
		    // "this" refers to the root element
		    // "carousel" is the jCarousel instance
			if(me.parentView.onJCarouselCreate != undefined) {
				me.parentView.onJCarouselCreate(this);
			}
			
		});
				
	}
	
	
	
	
	this.jcarousel_destroy = function(){
		jQuery('.'+this.cssClass).jcarousel('destroy');
	}
	

	this.jcarousel_autoscroll_start = function(){
		jQuery('.'+this.cssClass).jcarouselAutoscroll('start');
	}

	
	this.jcarousel_autoscroll_stop = function(){
		jQuery('.'+this.cssClass).jcarouselAutoscroll('stop');
	}
	
	
	this.jcarousel_moveTo = function(tid){
		var index = null;
		
		for(var i=0; i<this.ul.children.length; i++){
			if(typeof this.ul.children[i].firstElementChild.dataset.id != undefined) {
				if(this.ul.children[i].firstElementChild.dataset.id == tid) {
					index = i;
				}			
			}
	
		}
		if(index !=null){
			jQuery('.'+this.cssClass).jcarousel('scroll',index);
		}
	}
	
	
	
	
	this.jcarousel_moveFirst = function(){
		jQuery('.'+this.cssClass).jcarousel('scroll',0);
	}
	


}







