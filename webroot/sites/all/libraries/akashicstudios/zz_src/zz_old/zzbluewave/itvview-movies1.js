

var itvViewMovies1Controller = new ItvView('itvViewMovies1');

itvViewMovies1Controller.viewItemDefaults.tagName = 'LI';
itvViewMovies1Controller.viewItemDefaults.navIdPrefix = itvViewMovies1Controller.cssClass + 'Item';
itvViewMovies1Controller.viewItemDefaults.inFocusCss = itvViewMovies1Controller.cssClass + '-list-item-focus';
itvViewMovies1Controller.html.scroll.direction = 'omni';

itvViewMovies1Controller.nav.dpad.back = 'itvViewMovies0';

itvViewMovies1Controller.icons.arrows.push(new ItvNavIcon({
	name: 'up', 
	cssClass : 'itvViewMovies1-nav-arrow-up', 
	top: 65, 
	left: 715, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewMovies1Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.up
}));

itvViewMovies1Controller.icons.arrows.push(new ItvNavIcon({
	name: 'down', 
	cssClass : 'itvViewMovies1-nav-arrow-down', 
	top: 565, 
	left: 715, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewMovies1Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.down
}));


itvViewMovies1Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewMovies1-nav-breadcrumb-movies', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewMovies1Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbMovies
});


itvViewMovies1Controller.init();
itvViewMovies1Controller.hide();

window.gNav.views['itvViewMovies1'] = itvViewMovies1Controller;
window.gNav.viewsZeroBased.push(itvViewMovies1Controller);


