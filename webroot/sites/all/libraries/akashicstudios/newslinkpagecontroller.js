function NewslinkPageController(){

	
	this._newslink_json = new NewslinkJson();
	this._newslink_json.init();

	this._domTitleSelect = {};

	
	this._domContentTop = document.getElementById('content-top');
	this._domTitle = this._domContentTop.getElementsByClassName('title').item(0);;
	this._domDescription = this._domContentTop.getElementsByClassName('description').item(0);

	this._domContentBottom = document.getElementById('content-bottom');

	this._carousel = new NewslinkCarousel();
	this._carousel._cssClass = 'jcarousel-news-newslink';
	

	this._lis_default = new Array();
	this._ul = document.getElementById('jcarousel-news-newslink').firstElementChild;
	
	
	function init(){
		this.loadPapersImageSrc();
		
	}
	this.init = init;
	
	
	
	function newslinkjson_onload(){		
		this.getCarouselItems();
		this._carousel.init();
		this._ul.children[0].onclick();
		this.getTitleSelect();
		
		var params = {
				'type' : 'select',
				'selects' : [this._domTitleSelect]
		};
		
		window.AkashicStudios.getHeaderBannerFilter(params);

	}
	this.newslinkjson_onload = newslinkjson_onload;

	
	this.selectedTitle = function(){
		var s = this._domTitleSelect.firstElementChild;
		return s.options[s.selectedIndex].value;
	}


	
	this.getTitleSelect = function(){
		
		var items = this._newslink_json._newslinkItems;
		var options = new Array();
		
		items.forEach(function(elem){
			options.push({
				'value' : elem.nid(),
				'innerHTML' : elem.title() 
			});
		});		

		var params = {
				'id' : 'news-newslink-select-item',
				'className' : 'news-newslink-select-item',
				'selectText' : '',
				'options' : options,
				'onchange' : function(){
					window.AkashicStudios._pageController._controller._carousel.jcarousel_moveTo(this.options[this.selectedIndex].value);
				}					
			};

		
		this._domTitleSelect = window.AkashicStudios.getSelect(params);

		
	}
	
	this.setTitleSelect = function(tid){
		
		if(typeof this._domTitleSelect.firstElementChild != 'undefined') {
			var s = this._domTitleSelect.firstElementChild;
			
			for(var i=0; i<s.options.length; i++){
				if(s.options[i].value == tid){
					s.selectedIndex = i;
	//				return 0;
				}
			}
		}
	}
	
	
	this.loadPapersImageSrc = function(){
		
		var weekdays = window.AkashicStudios.getWeekdaysToCurrent();

		var cb = this._domContentBottom;
		var src = '/sites/all/themes/silversurfer/img/tablet/news/';
		
		for(var i=0; i<weekdays.length; i++){
			cb.children[i].firstElementChild.src = src + weekdays[i].toLowerCase() + '.jpg';
		}
		
		
	}
	
	
	
	
	this.getCarouselItems = function(){
		
		var nts = this._newslink_json._newslinkItems;
		var li_def = document.getElementById('jcarousel-news-newslink-item');
		
		this._ul.innerHTML = '';
		var me = this;
		
		nts.forEach(function(nt){
			
			var li = li_def.cloneNode(true);
			
			li.style.removeProperty('display');
			li.children[0].dataset.nid = nt.nid();
			li.children[0].innerHTML = nt.title();
			li.children[1].innerHTML = nt.body();
			
			li.onclick = function(){
				event.preventDefault();
				event.stopPropagation();
					
				window.AkashicStudios._pageController._controller.loadImgUrls(this.children[0].dataset.nid);
				window.AkashicStudios._pageController._controller.setTitleSelect(this.children[0].dataset.nid);				
			};
			
			me._lis_default.push(li);
			me._ul.appendChild(li);

		});		
		
	}
	
	
	
	this.loadImgUrls = function(nid){
		
		var nt = this._newslink_json.getNewslinkItem(nid);
		//var opacity = new Array('0.15','0.25','0.40','0.55','0.7','0.85','1.0');
		
		var cb = document.getElementById('content-bottom');
		var urls = nt.getTitleUrls();
		
		for(var i=urls.length-1; i>=0; i--) {
			
			cb.children[i].style.removeProperty('display');
			//cb.children[i].style.opacity = opacity[i];
			cb.children[i].dataset.url = urls[i];
			cb.children[i].onclick = function(){
				window.open(this.dataset.url);
			}
			
		}
		
		if(urls.length<7){
			for(var i = 0; i<(6-urls.length); i++){
				cb.children[i].style.display = 'none';
			}
		}
		
		
		
	}
	
	

	
	
	
}


	
function NewslinkCarousel(){
	
	
}
NewslinkCarousel.prototype = Object.create(AkashicJCarousel.prototype);

