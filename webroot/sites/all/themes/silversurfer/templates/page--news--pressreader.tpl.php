	<div id="header">
	
  		<?php print render($page['header_first']); ?>
  		<?php print render($page['header_second']); ?>
  		<?php print render($page['header_third']); ?>
		<div id="header-banner">
  			<?php print render($page['header_banner']); ?>
			<div id="header-banner-page-title">
				<?php print render($page['header_page_title']); ?>
			</div>
		</div>
  		  			  
	</div>	
  
  
  	<div id="content" class="news-pressreader-content">

	    <div class="separator"></div>
	  
	  	<div id="content-top" class="news-pressreader-content-top">
  			<div class="logo">
  				<img src="/sites/all/themes/silversurfer/img/tablet/news/pressreader-logo.png">
  			</div>
  			<div class="description"></div>
  		</div>

  		
  		<div class="separator"></div>
				
		<div id="content-bottom" class="news-pressreader-content-bottom">
		
  			<div class="link-section download">
  				<div class="text">Download the PressReader App</div>
  				<div class="icons">
	  				<div class="pressreader-link pressreader-link-download">
	  					<a href="" target="_blank" class="link-android">
	  						<img src="/sites/all/themes/silversurfer/img/tablet/news/logo-android.png">
						</a>
	  				</div>
		  			<div class="pressreader-link pressreader-link-download">
		  				<a href="" target="_blank" class="link-apple">
							<img src="/sites/all/themes/silversurfer/img/tablet/news/logo-apple.png">
		  				</a>
		  			</div>
	  			</div>
  			</div>
  			
  			<div class="link-section spacer">
  				<div class="text">Then ...</div>
  			</div>
  			
  			<div class="link-section activate">
  				<div class="text">Click to connect</div>
  				<div class="icons">
  					<div class="pressreader-link pressreader-link-activate">
		  				<a href="" target="_blank" class="link-activate">
		  					<img src="/sites/all/themes/silversurfer/img/tablet/news/logo-silversea.png">
		  				</a>
		  			</div>
	  			</div>
  			</div>

  					
		</div>
  		
	    <div class="separator"></div>
				
	</div>

  
  
	<div id="footer">
  	
  		<?php print render($page['footer']); ?>
  	
	</div>
  
	<div id="content-hidden" style="background-color: #000; display: none;">
		<?php print render($page['content']); ?>
		<?php print render($page['content_top']); ?>
		<?php print render($page['content_bottom']); ?>
     	<?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      	<?php endif; ?>
    	<?php print render($page['content_messages']); ?>
	</div>

	
