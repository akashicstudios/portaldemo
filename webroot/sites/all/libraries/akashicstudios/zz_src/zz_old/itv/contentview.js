
/*
*		AKContentView  		
*/
function AKContentView(cssClass) {
	
	this.cssClass = cssClass;
	this.domNode = jQuery('.'+this.cssClass).get(0);
	this.viewItemTagName = '';

	this.html = { 
		'visible' :		false,
		'region' : 		'content',
		'isOverlay' :	false,
		'isJWPlayer' :	false,
		'position' : 	{ 'top' : 0, 'left' : 0, 'maxWidth' : 740},
		'css' : 		{ 'inFocus' : '' },
		'scroll' : 		{ 'container' : '', 'direction' : 'horizontal', 'index' : 0, 'viewableItems' : 4, 'viewableRows' : 2 },
		'scrollWidth' : 0
	};
	
	this.icons = {
		'breadcrumb' : 	'',
		'arrows' : 		new Array()
	};
	
	this.dataModel = '';
	
	this.viewItems = new Array();
	this.viewItemsZeroBased = new Array();
	
	this.viewItemsVisible = new Array();

	this.viewItemDefaults = {
		'tagName' : '',
		'navIdPrefix' : '',
		'inFocusCss' : '',
		'dpad' : 	{ 'back' : '', 'up' : '', 'left' : '', 'right' : '', 'down' : '' }
	};
	
	this.showAnimationType = '';
	this.hideAnimationType = '';
	
	this.nav = {
		'categoryFilter' : '',
		'viewItemParentNidFilter' : '',
		'viewItemNidFilter' : '',
		'id' : '',	
		dpad : {
			'up' : '', 
			'left' : '', 
			'right' : '', 
			'down' : '',
			'back' : ''
		}
	};

 	

	/************************************************************************************************************************************************************/

	var cssClass = function() {
		
//		return 
	};




	/************************************************************************************************************************************************************/

	var init = function() {
		
// 		this.domNode = document.getElementsByClassName(this.cssClass).item(0);

		this.html.css.inFocus = this.cssClass + '-focus';
		
		if(typeof this.domNode != 'undefined') {

			this.html.scroll.container = this.domNode.getElementsByTagName('UL').item(0).parentElement;
			
			this.loadChildren();

			if(this.html.region == 'content') {	
			//	this.setScrollerWidth();
 				this.centerInParent();	
			}

			this.refreshViewItemsVisible();
			
			this.refreshNavArrowLimits();
			
			return true;
			
		}
		else {
			return false;
		}

	};
	this.init = init;




	/************************************************************************************************************************************************************/

	var setScrollerWidth = function () {
		var w = 0;
		for(var i = 0; i < this.viewItems.length; i++) {
			w += this.viewItems[i].domNode.clientWidth + 10;
		}
		this.html.scroll.container.style.width = w + 'px';
	};
	this.setScrollerWidth = setScrollerWidth;



	
	/************************************************************************************************************************************************************/
	
	var acquireNavArrows = function() {
		for(var i=0; i< this.icons.arrows.length; i++) {
//			this.icons.arrows[i].show();
		}
	};
	this.acquireNavArrows = acquireNavArrows;





	/************************************************************************************************************************************************************/
	
	var showNavArrows = function () {
		for(var i=0; i< this.icons.arrows.length; i++) {
			this.icons.arrows[i].show();
		}
		
//		this.refreshNavArrowLimits();
		
	};
	this.showNavArrows = showNavArrows;



	/************************************************************************************************************************************************************/
	
	var refreshNavArrowLimits = function () {

		if(this.html.scroll.index == 0) {
			this.hideNavArrow('back');				
		} else {
			this.showNavArrow('back');	
		}

		if(this.html.scroll.index == this.viewItemsVisible.length-1) {
			this.hideNavArrow('forward');				
		} else {
			this.showNavArrow('forward');	
		}
		
		if(this.html.scroll.direction=='omni') {
			
			if(this.html.scroll.container.firstElementChild.offsetTop < 0) {
				this.showNavArrow('up');
			}
			else {
				this.hideNavArrow('up');
			}
			
			if ( (this.html.scroll.container.scrollHeight > this.html.scroll.container.clientHeight) ) { 	//&& 
				this.showNavArrow('down');
			}
			else {
				this.hideNavArrow('down');
			}
			
		}
	
	};
	this.refreshNavArrowLimits = refreshNavArrowLimits;
	
	
	

	/************************************************************************************************************************************************************/
	
	var showNavArrow = function (name) {
		if( (this.html.scroll.direction == 'horizontal') || (this.html.scroll.direction == 'omni') ) {
			for(var i=0; i< this.icons.arrows.length; i++) {
				if(this.icons.arrows[i].name == name) {
					this.icons.arrows[i].show();
				}			
			}
		}
	};
	this.showNavArrow = showNavArrow;



	/************************************************************************************************************************************************************/
	
	var hideNavArrows = function () {
		for(var i=0; i< this.icons.arrows.length; i++) {
			this.icons.arrows[i].hide();
		}
	};
	this.hideNavArrows = hideNavArrows;


	/************************************************************************************************************************************************************/
	
	var hideNavArrow = function (name) {
		if( (this.html.scroll.direction == 'horizontal') || (this.html.scroll.direction == 'omni') ) {
			for(var i=0; i< this.icons.arrows.length; i++) {
				if(this.icons.arrows[i].name == name) {
					this.icons.arrows[i].hide();
				}	
			}
		}
	};
	this.hideNavArrow = hideNavArrow;



	/************************************************************************************************************************************************************/
	
	var dimNavArrows = function () {
		for(var i=0; i< this.icons.arrows.length; i++) {
			this.icons.arrows[i].dim();
		}
	};
	this.dimNavArrows = dimNavArrows;





	/************************************************************************************************************************************************************
	 * will clear one dpad direction of all elements so that they don't move in that direction
	 * */
	var setNavEdgeOnElements = function(direction) {

		this.setNavIdOnElements(direction,'');

	};
	this.setNavEdgeOnElements = setNavEdgeOnElements;
	



	/************************************************************************************************************************************************************
	 * will set one 'side' of all elements in the block to the specified element
	 * */
	var setNavIdOnElements = function(direction,dpadnavidNew) {

		try {

			for(var i=0; i < this.viewItems.length; i++) {
				this.viewItems[i].setNavOnElement(direction, dpadnavidNew);
			
			}
		}
		catch(err) {
			console.log("ItvView.setNavEdgeOnElements()  ERROR: " + err.message);
		}

	};
	this.setNavIdOnElements = setNavIdOnElements;
	
	

	/************************************************************************************************************************************************************/


	function loadChildren() {

		// try {
			
			var navItemElements = this.domNode.getElementsByTagName(this.viewItemDefaults.tagName);
			
			for (var i=0; i< navItemElements.length; i++) {

				var ni = new ItvViewItem();
				
				// Identity (dom & css)
				ni.domNode = navItemElements.item(i);
				
				ni.itvViewParent = this;
				//ni.navState = window.gNav;
				
				// set NavID's
				ni.nav.id = this.viewItemDefaults.navIdPrefix +i;
				
				// set a 'unique' class on the element
				ni.cssClass = ni.nav.id;
				if(	 ! ni.domNode.classList.contains(ni.cssClass) ) {
					ni.domNode.classList.add(ni.cssClass);
				}

				var navDomNode = ni.domNode.getElementsByClassName('itvNavBlockItem-navData').item(0);
				ni.nav.viewItemCategory = navDomNode.dataset.viewitemcategory || ''; 
				ni.nav.viewItemNid = navDomNode.dataset.viewitemnid || '';
				ni.nav.viewItemParentNid = navDomNode.dataset.viewitemparentnid || '';
				ni.nav.misc = navDomNode.dataset.misc || '';
				ni.nav.dpad.up = navDomNode.dataset.dpadup || '';
				ni.nav.dpad.left = navDomNode.dataset.dpadleft || '';
				ni.nav.dpad.right = navDomNode.dataset.dpadright || '';
				ni.nav.dpad.down = navDomNode.dataset.dpaddown || '';
				ni.nav.dpad.enter.dpadNavId = navDomNode.dataset.dpadenterdpadnavid || '';
				ni.nav.dpad.enter.mediaId = navDomNode.dataset.dpadentermediaid || '';
				ni.nav.dpad.enter.url = navDomNode.dataset.dpadenterurl || '';
				ni.nav.dpad.enter.urlTarget = navDomNode.dataset.dpadenterurltarget || '';
				ni.nav.dpad.enter.view = navDomNode.dataset.dpadenterview || '';
				ni.nav.dpad.enter.viewCategoryFilter = navDomNode.dataset.dpadenterviewcategoryfilter || '';
				ni.nav.dpad.enter.viewMusicFilter = navDomNode.dataset.dpadenterviewmusicfilter || '';
				ni.nav.dpad.enter.viewMusicBkgdImg = navDomNode.dataset.dpadenterviewmusicbgkdimg || '';
				ni.nav.dpad.enter.viewItemFocus = navDomNode.dataset.dpadenterviewitemfocus || '';
				
				
				this.viewItemsZeroBased[i] = ni;
				this.viewItems[ni.cssClass] = ni;

				window.gNav.viewItemsZeroBased.push(ni);
				window.gNav.viewItems[ni.cssClass] = ni;
				
				if(this.html.region == 'menu') {
					window.gNav.menuViewItemsZeroBased.push(ni);
					window.gNav.menuViewItems[ni.cssClass] = ni;					
				}

			}


		// } catch(err){
			// console.log("loadChildren()  ERROR: " + err.message);
		// }	
	}

	this.loadChildren = loadChildren;







	/************************************************************************************************************************************************************/


	var resetDomNavIdOnNavItems = function() {

		try {

			for (var i=0; i<this.viewItems.length; i++) {
//				this.viewItems[i].domNode.dataset.dpadnavid = this.navIdGroup + i;
				this.viewItems[i].domNode.removeAttribute('title');
			}
		}
		catch(err){
			console.log("resetNavIdOnNavItems()  ERROR: " + err.message);
		}	

	};
	this.resetDomNavIdOnNavItems = resetDomNavIdOnNavItems;





	/************************************************************************************************************************************************************/
	//	Drupal.settings.redCell.navArrowDefaults.back  |  backMenu#  |  Up | left | right | down

	function centerInParent () {

//		this.domNode.style.display = 'inline-block';

// 		var bufferHorizontal = Math.floor(Drupal.settings.redCell.regionContentBoundaries.maxWidth) - this.domNode.clientWidth;
// 		var bufferVertical = Math.floor(Drupal.settings.redCell.regionContentBoundaries.maxHeight) - this.domNode.clientHeight;


//		this.domNode.style.position = 'fixed';
// 		this.domNode.style.left = Math.floor(Math.floor(Drupal.settings.redCell.regionContentBoundaries.left) + (bufferHorizontal * 0.5)) + 'px';
// 		this.domNode.style.top = Math.floor(Math.floor(Drupal.settings.redCell.regionContentBoundaries.top) + (bufferVertical * 0.5)) + 'px'

	}
	this.centerInParent = centerInParent;

	


	/************************************************************************************************************************************************************/
	
	function show() {

 		try {

//			if(!this.html.isJWPlayer) {
//				if(typeof window.itvViewMusic1JWPlayer0 != 'undefined') {
//					window.itvViewMusic1JWPlayer0.remove();
//				}
//			}
		
			// determine if we need to hide an old conventView
			// if this is a contentView, determine if it replaces the existing contentView
			if(!this.html.isOverlay) {

				if(this.html.region == 'content') {

					//if there is an active contentView already
					if(window.gNav.navState.active.contentView instanceof ItvView) {

						//if this view is different from the active contentView
						if( this.cssClass != window.gNav.navState.active.contentView.cssClass )	{

							window.gNav.navState.active.contentView.hide();		

						}

					}

				}	
				
			}


			// refresh the array of visible items
			this.refreshViewItemsVisible();

			// set the new view position top/left
// 			this.centerInParent();

			// if this view has items in it, proceed to display it
//			if(this.countViewItemsVisible > 0) {
				// show this new view
				this.showAnimated();

				// set the active contentView if nec.
				if(this.html.region == 'content') {
					window.gNav.navState.active.contentView = this;
					
					if(typeof this.onLoad != 'undefined') { 
						this.onLoad();
					}
				
				}
				
//			}

// 			if(this.html.region == 'menu') {
// 				if(window.gNav.navState.active.contentView !='') {
// 					window.gNav.navState.active.contentView.dimNavArrows();
// 				}
// 			}
			
			

 		} catch (err) {
 			console.log(this.cssClass + ': ItvView.show():  ' + err.message);
 		}

	}
	this.show = show;



	/************************************************************************************************************************************************************/
	
	function hide() {
		
		this.hideNavArrows();						// hide the nav arrows. 
		
		if(this.html.isJWPlayer) {
			if(window[this.cssClass + 'JWPlayer0'] instanceof ItvJWPlayer) {
				window[this.cssClass + 'JWPlayer0'].remove();			
			}
		}
		
		if(this.icons.breadcrumb instanceof ItvNavIcon) {
			this.icons.breadcrumb.hide();
		}
		
//		jQuery('.'+this.cssClass).hide('fast');		// hide this div. all divs hide by fast fade out
		this.domNode.style.opacity = 0;
		
		this.html.visible = false;
		
		if(typeof this.onExit != 'undefined') { 
			this.onExit();
		}


	}
	this.hide = hide;


	
	function showAnimated() {
	
		try {

			this.aLight();
//			jQuery('.'+this.cssClass).show('normal');		// hide this div. all divs hide by fast fade out
			this.domNode.style.opacity = 1;
			this.refreshNavArrowLimits();							// show the nav arrows attached to this view
			if(this.icons.breadcrumb instanceof ItvNavIcon) {
				this.icons.breadcrumb.show();
			}
			
			this.html.visible = true;
				

		} catch (err) {
			console.log(this.cssClass + ': ItvView.showAnimated():  ' + err.message);
		}

	}
	this.showAnimated = showAnimated;


	
	function dim() {
		this.domNode.style.opacity = '0.2';
		this.hideNavArrows();

	}
	this.dim = dim;
	
	
	
	function aLight() {
		this.domNode.style.removeProperty('opacity');
		window.gNav.contentViewAlight();
	}
	this.aLight = aLight;

	
	

	/************************************************************************************************************************************************************/
	function focus() {

		try {
			
			if(	 ! this.domNode.classList.contains(this.html.css.inFocus)  	) {		 
				this.domNode.classList.add(this.html.css.inFocus);		
			}
			
			//	update global navState active view
			window.gNav.navState.active.view = this;	
	
			if(this.cssClass == 'itvViewMainMenu') {
				window.gNav.contentViewBlur();		
			}
			else {
				window.gNav.contentViewInFocus();		
			}

		} catch(err){
			console.log(this.cssClass + ": ItvView.focus()  ERROR: " + err.message);
		}	

		
	}
	this.focus = focus;



	/*****************************/
	function blur() {

		try{
					
			if(	 this.domNode.classList.contains(this.html.css.inFocus)  	) {		 
				this.domNode.classList.remove(this.html.css.inFocus);		
			}

		} catch(err){
			console.log(this.cssClass + ": ItvView.blur()  ERROR: " + err.message);
		}	
		
	}
	this.blur = blur;



	/*****************************/
	function blurViewItems() {
		
		for(var i=0; i< this.viewItemsZeroBased.length; i++) {
			this.viewItemsZeroBased[i].blur();
		}

	}
	this.blurViewItems = blurViewItems;
	
	



	/*****************************/
	function setViewItemsCategoryFilter(cf) {
		
		for(var i=0; i< this.viewItemsZeroBased.length; i++) {
			this.viewItemsZeroBased[i].setViewItemCategoryFilter(cf);
		}
		this.nav.categoryFilter = cf;
		this.refreshViewItemsVisible();

	}
	this.setViewItemsCategoryFilter = setViewItemsCategoryFilter;
	
	

	/*****************************/
	function setViewItemsNidFilter(nid) {
		
		for(var i=0; i< this.viewItemsZeroBased.length; i++) {
			this.viewItemsZeroBased[i].setViewItemNidFilter(nid);
		}
		this.nav.viewItemNidFilter = nid;
		this.refreshViewItemsVisible();

	}
	this.setViewItemsNidFilter = setViewItemsNidFilter;
	
	


	/*****************************/
	function setViewItemsParentNidFilter(nid) {
		
		for(var i=0; i< this.viewItemsZeroBased.length; i++) {
			this.viewItemsZeroBased[i].setViewItemParentNidFilter(nid);
		}
		this.nav.viewItemParentNidFilter = nid;
		this.refreshViewItemsVisible();

	}
	this.setViewItemsParentNidFilter = setViewItemsParentNidFilter;
	
	
		
	
	
	/*****************************/
	function clearViewItemsCategoryFilter() {
		
		for(var i=0; i< this.viewItemsZeroBased.length; i++) {
			this.viewItemsZeroBased[i].setViewItemCategoryFilter('');
		}
		this.refreshViewItemsVisible();

	}
	this.clearViewItemsCategoryFilter = clearViewItemsCategoryFilter;
	
	
	

	/*****************************/
	function setViewItemsLanguageFilter() {
		
		for(var i=0; i< this.viewItemsZeroBased.length; i++) {
			this.viewItemsZeroBased[i].setViewItemLanguageFilter();
		}
		this.refreshViewItemsVisible();

	}
	this.setViewItemsLanguageFilter = setViewItemsLanguageFilter;
	
	
	
	/*****************************/
	function setScrollIndex(css) {
		
		for(var i=0; i< this.viewItemsVisible.length; i++) {
			if(this.viewItemsZeroBased[this.viewItemsVisible[i]].cssClass == css) {
				this.html.scroll.index = i;
				break;
			}
			
		}

	}
	this.setScrollIndex = setScrollIndex;
	

	//setScrollIndex(this.cssClass);
	
	
	/************************************************************************************************************************************************************/
	function movePrevious(){
		try{
		
			// decrement the index & set the focus on the previous element
			this.html.scroll.index--;
			
			// if we get to the beginning, see if we move off the view
			if(this.html.scroll.index < 0) { 
			
				this.html.scroll.index=0;			// keep index at 0 - first position

				//if dpad.left && horizontal , move left off the view
				if(this.nav.dpad.left != '') {		
					if(this.html.scroll.direction == 'horizontal') {
						window.gNav.viewItems[this.nav.dpad.left].focus();
					}
				}

				//if dpad.up && vertical , move up off the view
				if(this.nav.dpad.up != '') {		
					if( (this.html.scroll.direction == 'vertical') || (this.html.scroll.direction == 'vertical-scroll')  ) {
						window.gNav.viewItems[this.nav.dpad.up].focus();
					}
				}
				
			} 

			else {
				
// 				this.focusOnViewItem(this.html.scroll.index,'previous');
				this.viewItemsZeroBased[this.viewItemsVisible[this.html.scroll.index]].focus();
				
			}
			
//			this.refreshNavArrowLimits();			
			
			
		} catch (err) {		console.log(this.cssClass + ": ItvView.movePrevious()  ERROR: " + err.message);		}
		
	}
	this.movePrevious = movePrevious;
	

	
	/*****************************/
	function moveNext(){
		try{

			// increment the index & set the focus on the next element
			this.html.scroll.index++;
			
			// if we get to the end, see if we move off the view
			if(this.html.scroll.index > this.viewItemsVisible.length-1) { 

				this.html.scroll.index = this.viewItemsVisible.length-1;		// keep index at last position
				
				//if dpad.right && horizontal , move left off the view
				if(this.nav.dpad.right != '') {		
					if(this.html.scroll.direction == 'horizontal') {
						window.gNav.viewItems[this.nav.dpad.right].focus();
					}
				}

				//if dpad.downup && vertical , move up off the view
				if(this.nav.dpad.down != '') {		
					if( (this.html.scroll.direction == 'vertical') || (this.html.scroll.direction == 'vertical-scroll')  ) {
						window.gNav.viewItems[this.nav.dpad.down].focus();
					}
				}
								
			} 

			else {
			
				this.viewItemsZeroBased[this.viewItemsVisible[this.html.scroll.index]].focus();

			}
			
//			this.refreshNavArrowLimits();			
			
		
		} catch (err) {		console.log(this.cssClass + ": ItvView.moveNext()  ERROR: " + err.message);		}
		
	}
	this.moveNext = moveNext;

	
	
	/*****************************/
	function moveOmniUp(){
		try{
		
			// bounce back x number of positions
			if(this.html.scroll.index > (this.html.scroll.viewableItems-1)) {
				this.html.scroll.index = this.html.scroll.index - this.html.scroll.viewableItems;

				//set the focus on the new item, thus scrolling it into view, etc.
				this.viewItemsZeroBased[this.viewItemsVisible[this.html.scroll.index]].focus();

			}
			
//			this.refreshNavArrowLimits();
			
		} catch (err) {		console.log(this.cssClass + ": ItvView.moveFirst()  ERROR: " + err.message);		}
		
	}
	this.moveOmniUp = moveOmniUp;



	/*****************************/
	function moveOmniDown(){
		try{
		
			// bounce back x number of positions
			if( (this.html.scroll.index + (this.html.scroll.viewableItems)) < (this.viewItemsVisible.length) ) {
				this.html.scroll.index = this.html.scroll.index + this.html.scroll.viewableItems;
				
			}
			
			else {
				this.html.scroll.index = this.viewItemsVisible.length-1;
			}

			//set the focus on the new item, thus scrolling it into view, etc.
			this.viewItemsZeroBased[this.viewItemsVisible[this.html.scroll.index]].focus();

//			this.refreshNavArrowLimits();			
			
		} catch (err) {		console.log(this.cssClass + ": ItvView.moveFirst()  ERROR: " + err.message);		}
		
	}
	this.moveOmniDown = moveOmniDown;

	
	
	/*****************************/
	function moveFirst(){
		try{
		
			// set index to 0 - first position
			this.html.scroll.index=0;

			//set the focus on the new item, thus scrolling it into view, etc.
// 			this.focusOnViewItem(this.html.scroll.index,'next');
			this.viewItemsZeroBased[this.viewItemsVisible[this.html.scroll.index]].focus();

//			this.refreshNavArrowLimits();
			
		} catch (err) {		console.log(this.cssClass + ": ItvView.moveFirst()  ERROR: " + err.message);		}
		
	}
	this.moveFirst = moveFirst;





	/*****************************/
	function moveLast(){
		try{
		
			// set index to last position
			this.html.scroll.index = this.viewItemsZeroBased.length-1;

			//set the focus on the new item, thus scrolling it into view, etc.
// 			this.focusOnViewItem(this.html.scroll.index,'previous');
			this.viewItemsZeroBased[this.viewItemsVisible[this.html.scroll.index]].focus();

//			this.refreshNavArrowLimits();
			
			
		} catch (err) {		console.log(this.cssClass + ": ItvView.moveLast()  ERROR: " + err.message);		}
		
	}
	this.moveLast = moveLast;



	/*****************************/
	function refreshViewItemsVisible() {
		
		this.viewItemsVisible.length = 0;
		for(var i=0; i<this.viewItemsZeroBased.length; i++) {
			
			if(this.viewItemsZeroBased[i].nav.visible) {
				this.viewItemsVisible.push(i);
			}
		}
		
	}
	this.refreshViewItemsVisible = refreshViewItemsVisible;
			
			


	/*****************************/
// 	function focusOnViewItem(index, direction) {
		
// 		if(this.viewItemsZeroBased[this.html.scroll.index].nav.visible) {
// 			this.viewItemsZeroBased[this.html.scroll.index].focus();
// 		}
// 		else {
// 			if(direction == 'previous'){
// 				this.movePrevious();
// 			}
// 			else{
// 				this.moveNext();
// 			}
// 			this.focusOnViewItem(index, direction);
// 		}
// 	}
// 	this.focusOnViewItem = focusOnViewItem;


	
	
	/*****************************/
	function focusOnViewItemNodeId(nodeid) {
		
		var vi = '';
		for(var i=0; i<this.viewItemsZeroBased.length; i++) {
			
			if(this.viewItemsZeroBased[i].nav.viewItemNid == nodeid) {
				
				vi = this.viewItemsZeroBased[i];
				this.html.scroll.index = i;
				
			}
			
		}
		
		if(vi != ''){
			vi.focus();
			this.refreshNavArrowLimits();
		}
		else {
			this.moveFirst();
		}
		
		
	}
	
	this.focusOnViewItemNodeId = focusOnViewItemNodeId;
	
	
	
	
	/*****************************/
	function countViewItemsVisible() {
		
		var vi = 0;
		for(var i=0; i<this.viewItemsZeroBased.length; i++) {
			
			if(this.viewItemsZeroBased[i].nav.visible == true) {
				
				vi++;
				
			}
			
		}
		
		return vi;		
		
	}
	
	this.countViewItemsVisible = countViewItemsVisible;
	
	
	
	/************************************************************************************************************************************************************/
	
	/*
	// DPAD keycode processing - 
	*/
	function processDPAD(event) {

		try {
			var newItvNavElement;
			var newItvView;
			
			//	- switch case: determine the U|D|L|R  navid that the current object is pointing to 
			switch (event.keyCode) {

				case 38:		// up arrow
				case 87:		// W
					
					if(  (this.html.scroll.direction=='vertical') || (this.html.scroll.direction=='vertical-scroll') ) {
						this.movePrevious();
					}


					else if (this.html.scroll.direction=='omni') {
						
						this.moveOmniUp();
					}
					
					else {
						if(window.gNav.navState.active.viewItem.nav.dpad.up != '') {
							window.gNav.viewItems[window.gNav.navState.active.viewItem.nav.dpad.up].focus();
						}
					}
					
					break; 

				case 37: 		// left arrow
				case 65:		// A

					if (this.html.scroll.direction=='horizontal') {
						this.movePrevious();
					}

					else if ( (this.html.scroll.index > 0) && (this.html.scroll.direction=='omni') ) {
						this.movePrevious();
					}
					
					else {
						if(this.nav.dpad.left != '') {
							window.gNav.viewItems[this.nav.dpad.left].focus();
						}
					}
					
					break; 
				
				case 39: 		// right arrow
				case 68:		// D
					
					
					
					if( (this.html.scroll.direction=='horizontal') || (this.html.scroll.direction=='omni') ) {
						
						if(window.gNav.navState.active.view.html.isJWPlayer == true) {
							window[window.gNav.navState.active.view.cssClass+'JWPlayer0'].nextTrack();
						}
						
						else {
							this.moveNext();
						}
						
					}
					
					else {
						if(window.gNav.navState.active.viewItem.nav.dpad.right != '') {
							window.gNav.viewItems[window.gNav.navState.active.viewItem.nav.dpad.right].focus();
						}
					}
					
					break; 

				case 40: 		// down arrow
				case 83:		// S

					if( (this.html.scroll.direction=='vertical') || (this.html.scroll.direction=='vertical-scroll') ){
						this.moveNext();
					}

					else if (this.html.scroll.direction=='omni') {
						
						this.moveOmniDown();
					}

					else {
						if(window.gNav.navState.active.viewItem.nav.dpad.down != '') {
							window.gNav.viewItems[window.gNav.navState.active.viewItem.nav.dpad.down].focus();
						}
					}

					break; 
					
					
				case 66: 		// B (back)
				case 98: 		// b (back)			
						
						if(this.html.isJWPlayer == true) {
							window[this.cssClass + 'JWPlayer0'].remove();			
						}

						if(this.nav.dpad.back=='itvMainMenu') {
							window.gNav.viewItems[this.nav.dpad.left].focus();
						}
						
						else {
							
// 							if(this.cssClass == 'itvViewMovies3') {
// 								this.hide();
// 							}
							
							newItvView = window.gNav.views[this.nav.dpad.back];
							newItvView.viewItemsZeroBased[newItvView.viewItemsVisible[newItvView.html.scroll.index]].focus();
						}

					break;					


				default:
				
			}


		} catch (err) {
			console.log(err);
		}

	}
	this.processDPAD = processDPAD;




}
