
function tabletPageShipGuideDeckSelect_OnChange(strLIDeckId) {
	
	var newOpt = document.getElementById(strLIDeckId);

	var opts = document.getElementsByClassName('tabletPageShipGuide-list-item');
	for(i=0; i< opts.length; i++) {
		opts[i].classList.add('tabletPageShipGuide-list-item-hidden');
	}
	
	newOpt.classList.remove('tabletPageShipGuide-list-item-hidden');
	newOpt.children[1].firstElementChild.firstElementChild.scrollIntoView(true);		//img deck plan
}


var divDeckSelect = document.getElementById('divShipGuideDeckSelect');
var selOptions = document.getElementsByClassName('tabletPageShipGuide-list-item');


var deckSelect = document.createElement('SELECT');
deckSelect.id = 'tabletPageShipGuideDeckSelect';
deckSelect.className = 'tabletPageShipGuideDeckSelect';

for(i=0; i<selOptions.length; i++) {
	var catOpt = document.createElement('OPTION');
	catOpt.className = 'tabletPageShipGuideDeckSelect-option';
	var optId = 'tabletPageShipGuide-list-item.' + selOptions[i].firstElementChild.firstElementChild.innerHTML.replace(' ','.');
	selOptions[i].id = optId;
	catOpt.setAttribute('value',optId);
	
	catOpt.innerHTML = selOptions[i].firstElementChild.firstElementChild.innerHTML;
	deckSelect.appendChild(catOpt);
}

divDeckSelect.innerHTML = '<div class="select-container"></div>';
divDeckSelect.firstElementChild.appendChild(deckSelect);

deckSelect.onchange = function(){
	tabletPageShipGuideDeckSelect_OnChange(this.options[this.selectedIndex].value);
};


deckSelect.onchange();
document.getElementsByTagName('BODY').item(0).scrollIntoView(true);
