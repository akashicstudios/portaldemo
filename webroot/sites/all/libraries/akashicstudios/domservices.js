
function DomServices() {
	
	this._parent = {};
	
	function init(){}
	this.init = init;
	
	
	function lookup(tid){
		
	}
	this.lookup = lookup;
	
	
	
}
 




function DomServicesFilters(){

	this._deckSelect = {};
	this._venueSelect = {};
	
	this._shipServices = window.AkashicStudios._ship_services;
	
	this.selectedDeck = function(){
		var s = this._deckSelect.firstElementChild;
		return s.options[s.selectedIndex].value;
	}
	
	this.selectedVenue = function(){
		var s = this._venueSelect.firstElementChild;
		return s.options[s.selectedIndex].value;
	}
	
	
	function init() {
		
		this.getDeckSelect();
		this.getVenueSelect();
		
		var params = {
				'type' : 'filter',
				'selects' : [this._deckSelect,this._venueSelect]
		};
		
		window.AkashicStudios.getHeaderBannerFilter(params);
	}
	this.init = init;
	
	
	
	
	
	this.getDeckSelect = function(){
		
		var decks = window.AkashicStudios._ship_locations.getLocationsByType('d');
		var options = new Array();
		
		decks.forEach(function(elem){
			options.push({
				'value' : elem.id(),
				'innerHTML' : elem.description() 
			});
		});		

		var params = {
				'id' : 'onboard-services-select-deck',
				'className' : 'onboard-services-select-deck',
				'selectText' : 'All Decks',
				'options' : options,
				'onchange' : function(){
					window.AkashicStudios._pageController._controller._dom_services_carousel.setFilters();
				}					
			};

		
		this._deckSelect = window.AkashicStudios.getSelect(params);

	}
	
	
	this.redrawVenueSelect = function(){
		
		var b = document.getElementById('header-banner-filters');
		b.children[2].innerHTML = '';
		this.getVenueSelect();
		b.children[2].appendChild(this._venueSelect.firstElementChild);
		
		
	}
	
	
	this.getVenueSelect = function(){

		var deck = this.selectedDeck();
		var venues = deck == '0' ? this._shipServices._shipServices : this._shipServices.getServicesByDeckId(deck);
		var options = new Array();
		
		venues.forEach(function(elem){
			options.push({
				'value' : elem.id(),
				'innerHTML' : elem.display_title() 
			});
		});		

		var params = {
				'id' : 'onboard-services-select-venue',
				'className' : 'onboard-services-select-venue',
				'selectText' : 'Select Venues',
				'options' : options,
				'onchange' : function(){
					if(this.options[this.selectedIndex].value != '0') {
						window.AkashicStudios._pageController._controller._dom_services_carousel.jcarousel_moveTo(this.options[this.selectedIndex].value);
					}
					else {
						window.AkashicStudios._pageController._controller._dom_services_carousel.jcarousel_moveFirst();
					}
				}					
			};
		
		this._venueSelect = window.AkashicStudios.getSelect(params);
	}	
	
	
	
	this.setDeckSelect = function(tid){
		var s = this._deckSelect.firstElementChild;
		
		for(var i=0; i<s.options.length; i++){
			if(s.options[i].value == tid){
				s.selectedIndex = i;
//				return 0;
			}
		}
	}
	
	
	
	this.setVenueSelect = function(tid){
		var s = this._venueSelect.firstElementChild;
		
		for(var i=0; i<s.options.length; i++){
			if(s.options[i].value == tid){
				s.selectedIndex = i;
//				return 0;
			}
		}
	}
	
}


	
	
