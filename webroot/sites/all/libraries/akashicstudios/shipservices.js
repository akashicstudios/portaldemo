
function ShipServices() {
	
	this._json = {};
	
	this._shipServices = '';
	
	function init() {
		
		var me = this;
		
		jQuery.getJSON( '/sql/json/ship_services()' )
		.done(function(data){
			
			me._json = data;			
			
			me._shipServices = new Array();
			
			me._json.forEach(function(elem){
				var sv = new ShipService();
				sv._json = elem;
				me._shipServices.push(sv);
				
//				var i = new Image();
//				i.src = sv.content_image_src();
//				i.src = sv.footer_image_src();
				
			});
			
			window.AkashicStudios.onShipServicesLoaded();
			
		})
		.fail(function( jqXHR, textStatus, errorThrown){
			console.log(textStatus + '\n' + errorThrown);
		});
	
		

	}
	this.init = init;
	
	
	
	this.getShipService = function(tid) {
		
		var retval = null;
		
		this._shipServices.forEach(function(elem){
			if(tid == elem.id()) {
				retval = elem;
			}
		});
		
		return retval;
	}

	
	function getServicesByDeckId(tid_deck){
		
		var me = this;
		
		var retval = new Array();
		
		this._shipServices.forEach(function(elem){
			if(elem.ship_deck_id() == tid_deck){
				retval.push(elem);
			}
		});
		
		return retval;
		
	}
	this.getServicesByDeckId = getServicesByDeckId;
	

}




function ShipService(){
	
	this._json = {};

	
	this.ship_location_id = function(){
		return this._json.ship_location_id;
	}
	
	this.ship_location = function(){
		return this._json.ship_location;
	}
	
	this.ship_deck_id = function(){
		return this._json.ship_deck_id;
	}
	
	this.ship_deck = function(){
		return this._json.ship_deck;
	}
	
	this.id = function(){
		return this._json.id;
	}
	
	this.display_title = function(){
		return this._json.display_title;
	}
	
	this.content_image_src = function(){
		return this._json.content_image_src;
	}

	this.footer_image_src = function(){
		return this._json.footer_image_src;
	}
	
	this.content_text = function(){
		return this._json.content_text;
	}
	
	this.telephone = function(){
		return this._json.telephone;
	}
	
	this.hours_from = function(){
		return this._json.hours_from;
	}

	this.hours_to = function(){
		return this._json.hours_to;
	}

	this.weight = function(){
		return this._json.weight;
	}
	
	
	
	
	
	
}
