function ItvNavigationState(){
	
	this.views = new Array();
	this.viewsZeroBased = new Array();
	
	this.viewItems = new Array();
	this.viewItemsZeroBased = new Array();
	
	this.menuView = '';
	this.menuViewItems = new Array();
	this.menuViewItemsZeroBased = new Array();
	
	this.navStack = new Array();
	
	this.navState = {
			'active' : {
				'view' : '',
				'contentView' : '',
				'viewItem' : '',
				'menuItem' : ''
			},
			'previous' : {
				'view' : '',
				'viewItem' : '',
				'menuItem' : ''
			}
	};
	
	this.domRegions = {
			'menu' : (document.getElementById('region-sidebar-first') || ''),
			'content' : (document.getElementById('region-content-background') || '')
	};
	
	this.icons = {
			'arrows' : {
				'home' : '',
				'slash' : '',
				'back' : ''
			}
	};
	
	function init(){
		
		/***************************************
		FINAL STAGE, VOD Client Register IP
		****************************************/
		
		jQuery(function($){
			
			$(document).bind('keydown', function (event) {									
				window.gNav.processNavEvent(event);
			});
			
//			$.ajax({
//			    type: "POST",
//			    url: '/sites/all/modules/redcell/vodregisterclient.php',
//			    data: "{''}",
//			    contentType: "application/json; charset=utf-8",
//			    dataType: "json",
//			    success: function (msg) {},
//			    error: function (e) {}
//			});
		
		});
		
		
		
	}
	this.init = init;
	
	
	/************************************************************************************************************************************************************/
	function contentViewInFocus() {

		//	set infocus of region-content-background region
		if(window.gNav.domRegions.content != '') {
			if(	 ! window.gNav.domRegions.content.classList.contains('inFocus')  	) {		 
				window.gNav.domRegions.content.classList.add('inFocus');
			}					
		}
		
	}
	this.contentViewInFocus = contentViewInFocus;

	
	
	function contentViewBlur() {

		//	remove infocus of region-content-background region
		if(window.gNav.domRegions.content != '') {
			if(	window.gNav.domRegions.content.classList.contains('inFocus')  	) {		 
				window.gNav.domRegions.content.classList.remove('inFocus');		
			}					
		}
		
	}
	this.contentViewBlur = contentViewBlur;

	
	function contentViewDim() {
		if(window.gNav.domRegions.content != '') {
			if( ! window.gNav.domRegions.content.classList.contains('dim')  	) {		 
				window.gNav.domRegions.content.classList.add('dim');		
			}					
		}
	}
	this.contentViewDim = contentViewDim;
	
	
	function contentViewAlight() {
		if(window.gNav.domRegions.content != '') {
			if(	window.gNav.domRegions.content.classList.contains('dim')  	) {		 
				window.gNav.domRegions.content.classList.remove('dim');		
			}					
		}
	}
	this.contentViewAlight = contentViewAlight;

	
	
	/************************************************************************************************************************************************************/
	function hideViews() {
		
		for(var i=0; i< window.gNav.viewsZeroBased.length; i++) {
			
			if(window.gNav.viewsZeroBased[i].html.region == 'content') {
				window.gNav.viewsZeroBased[i].hide();	
			}
			
		}

	}
	this.hideViews = hideViews;
	
		
	
	
	/************************************************************************************************************************************************************/
	function processNavEvent(event) {
		

		//alert(event.keyCode);
		var newItvNavElement;
		var newItvView;
		
		switch (event.keyCode) {
		
			case 13:
				
				if(window.gNav.navState.active.view.html.isJWPlayer == true) {
					window[window.gNav.navState.active.view.cssClass+'JWPlayer0'].togglePlay();
				}
				else {
					window.gNav.navState.active.viewItem.processENTER();
				}
				break;
				
			case 85: 	// 'U' for unbind
				event.preventDefault();
				unBindFromDocument();
				break;
	
			// case 72: 		// H (home)
			// case 104: 		// h (home)

			case 82:	// 'R' for reload
				this.hideViews();
				event.preventDefault();
				window.location.reload();
				break;
				
// 			case 66: 		// B (back)
// 			case 98: 		// b (back)
// 				if(this.navStack.length>0) {
// 					this.navState.active.view.html.scroll.index = 0;
// 					this.navStack.pop().focus();
					
// 				} else {
// 					if(this.navState.active.view.nav.dpad.back != '') {
// 						this.navState.active.view.moveFirst();
// 						this.viewItems[this.navState.active.view.nav.dpad.back].focus();
// 					}
// 				}
// 				break; 
			
			
			case 70: 		// F
			case 102: 		// f
				window[window.gNav.navState.active.view.cssClass+'JWPlayer0'].forward();
				break;


			case 72: 		// H (home)
			case 104: 		// h (home)
				this.navStack.length = 0;				// clear the navStack & set focus back to top menu item
				this.hideViews();							// hide all views
				this.menuView.blurViewItems();
				this.menuView.html.scroll.index = 0;
				this.menuViewItemsZeroBased[0].focus();
				this.navState.active.contentView.moveFirst();
				this.menuViewItemsZeroBased[0].focus();	// this is on purpose! keep this second call in here
				break; 

				
			case 78: 		// N
			case 110: 		// n				
				window.itvJWPlayer0.nextTrack();				
				break;
				
				
				// D-Pad movement / everything else
			case 39: 		// right arrow
			case 68:		// D
			case 40: 		// down arrow
			case 37: 		// left arrow
			case 38:		// up arrow
			case 65:		// A
			case 83:		// S
			case 87:		// W
			case 66: 		// B (back)
			case 98: 		// b (back)			
				window.gNav.navState.active.view.processDPAD(event);
				break;
				
			default:    	
//				alert('new keycode: ' + event.keyCode)

		}	
		
		window.gNav.navState.active.view.refreshNavArrowLimits();
		
	}
	this.processNavEvent = processNavEvent;
		

	
	
	/************************************************************************************************************************************************************/	
	function unBindFromDocument() {
		jQuery(function($){
	
			$(document).unbind('keydown');
	
		});
	}
	
	
	/************************************************************************************************************************************************************/
	function keyPress(code) {
		
		jQuery(function() {
		    var e = jQuery.Event('keypress');
		    e.which = code; // Character 'A'
		    jQuery('body').trigger(e);
		});
	}
	this.keyPress = keyPress;
	
	
	
	
	/************************************************************************************************************************************************************/	
	/*
	 * simulates a keydown event causing focus to move in a direction
	 * */
	function moveDPad(direction) {

		try {
			var nCode = 0;
			
			switch(direction) {
			
				case 'left':		//left
					nCode = 65;
					break; 
	
				case 'up':		//up
					nCode = 87;
					break; 
	
				case 'right':		//right
					nCode = 68;
					break; 
	
				case 'down':		//down
					nCode = 83;
					break; 
					
				case 'back':
					nCode = 66;
					break;

				case 'enter':
					nCode = 13;
					break;
			}
			
			var e = jQuery.Event( "keydown", { keyCode: nCode } );
			jQuery("body").trigger(e);
			
		}
		catch(err) {
			console.log("moveDPad()  ERROR: " + err.message);
		}

	}
	this.moveDPad = moveDPad;
	
	
	
}



















