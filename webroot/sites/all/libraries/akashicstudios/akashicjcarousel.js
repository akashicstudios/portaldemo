function AkashicJCarousel() {
	
	this._cssClass = '';
	
	this._prevButton = {};
	this._nextButton = {};
	
	this._domnode = {};
	this._ul = {};
}



AkashicJCarousel.prototype.init = function() {
	
	this.loadDomNode();
	
	this.jcarousel_init();
	this.jcarousel_setwidth();

}



AkashicJCarousel.prototype.jcarousel_setwidth = function(){
	var w = 0;
	var lis = this._ul.children;
	
	for(var i=0; i<lis.length; i++) {
		w += lis[i].scrollWidth;
	}
			
	this._ul.style.width = w + 'px';
}





AkashicJCarousel.prototype.loadDomNode = function(){
		
	var dn = document.getElementsByClassName(this._cssClass);
	if(dn != null) {
		this._domnode = dn.item(0);
	}		
	
	this._ul = this._domnode.firstElementChild;
	
	
	var p = document.createElement('A');
	p.className='jcarousel-prev';
	p.href='#';
	var n = document.createElement('A');
	n.className='jcarousel-next';
	n.href='#';
	this._domnode.appendChild(p);
	this._domnode.appendChild(n);
	
	this._domnode.addEventListener("touchmove", function(event){
		event.preventDefault();
		
	}, false);
	
}



AkashicJCarousel.prototype.jcarousel_init = function(){	
	var me = this;
	
	var jc = jQuery('.'+this._cssClass).jcarousel({
		wrap: 'circular'
	});
	
	jQuery('.'+this._cssClass+' .jcarousel-prev').jcarouselControl({
        target: '-=1',
		carousel: jc
    });

	jQuery('.'+this._cssClass+' .jcarousel-next').jcarouselControl({
        target: '+=1',
        carousel: jc
    });
	
	jQuery('.'+this._cssClass).on('jcarousel:fullyvisiblein', 'li', function(event, carousel) {
		this.onclick();
	});
			
}




AkashicJCarousel.prototype.jcarousel_destroy = function(){
	jQuery('.'+this._cssClass).jcarousel('destroy');
}





AkashicJCarousel.prototype.jcarousel_moveTo = function(tid){
	var index = null;
	
	for(var i=0; i<this._ul.children.length; i++){
		if(typeof this._ul.children[i].firstElementChild.dataset.tid != undefined) {
			if(this._ul.children[i].firstElementChild.dataset.tid == tid) {
				index = i;
			}			
		}
		
		if(typeof this._ul.children[i].firstElementChild.dataset.nid != undefined) {
			if(this._ul.children[i].firstElementChild.dataset.nid == tid) {
				index = i;
			}			
		}

	}
	if(index !=null){
		jQuery('.'+this._cssClass).jcarousel('scroll',index);
	}
}




AkashicJCarousel.prototype.jcarousel_moveFirst = function(){
	jQuery('.'+this._cssClass).jcarousel('scroll',0);
}
	








