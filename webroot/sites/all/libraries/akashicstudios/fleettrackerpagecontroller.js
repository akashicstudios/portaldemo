function FleettrackerPageController(){
	
	this._fleettracker = new FleetTracker();
	this._fleettracker.mapDiv = 'onboard-fleettracker-content-leaflet';
	
	this._dom_fleettracker_filters = new DomFleettrackerFilters();
	this._dom_fleettracker_filters.init();
	
	
	function init(){
		this._fleettracker.init();
	}
	this.init = init;
	

	
}