function Newslink(){
	
	this._json = {};
	
	this._newslinkFiles = new Array();
	

	this.nid = function(){
		return this._json.nid;
	}
	
	this.title = function(){
		return this._json.title;
	}
	
	this.body = function(){
		return this._json.body;
	}
	
	this.filename_mask = function(){
		return this._json.filename_mask;
	}
	

	this.getFileByWeekday = function(weekday){
		
		var retval = null;
		this._newslinkFiles.forEach(function(nf){
			var fn = nf.filename();
			if(fn.indexOf(weekday)!=-1){
				retval = nf.url();
			}
		});
		return retval;
		
	}
	
	
	
	this.getTitleUrls = function(){
		
		var me = this;
		var weekdays = window.AkashicStudios.getWeekdaysToCurrent();
		
		var retval = new Array();
			
		weekdays.forEach(function(w){
			retval.push(me.getFileByWeekday(w));			
		});

		return retval;
		
	}
	

	

}


function NewslinkFile(){
	
	this._json = {};
	
	this.filename = function(){
		return this._json.filename;
	}
	
	this.url = function(){
		return this._json.url;
	}
	
	

}

