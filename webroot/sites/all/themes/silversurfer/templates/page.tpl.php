	<div id="header">
	
		<div id="header-first">
	  		<?php print render($page['header_first']); ?>
	  	</div>
	  	<div id="header-second">
	  		<?php print render($page['header_second']); ?>
	  		<span>&nbsp;</span>
	  	</div>
	  	<div id="header-third">
	  		<?php print render($page['header_third']); ?>
	  		<div id="header-third-menu-button" class="header-third-menu-button">
				<div class="text">MENU</div>
				<div class="menu-lines">
					<div class="menu-line"></div>
					<div class="menu-line"></div>
					<div class="menu-line"></div>
				</div>
			</div>
  		</div>
	
	  	<div id="header-banner">
			<?php print render($page['header_banner']); ?>
			<?php 
			if(isset($node)) {
				$body = field_get_items('node',$node, 'body');
				print $body[0]['value']; 
			} ?>
			<?php print render($page['header_filters']); ?>  	
	  	</div>
  
	</div>	
  
	<div id="content">

	    <div class="separator"></div>
	  
	  	<div id="content-top" class="content-row">
			<?php print render($page['content_top']); ?>
		</div>
		
	    <div class="separator"></div>
				
		<div id="content-bottom" class="content-row">
			<?php print render($page['content_bottom']); ?>
		</div>
		
	    <div class="separator"></div>
				
	</div>

  
  
	<div id="footer">
  	
  		<?php print render($page['footer']); ?>
  	
	</div>
  
	<div id="content" style="background-color: #888888;">
		<?php print render($page['content']); ?>
	</div>

  	<div id="content-messages">
	      
      <?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      <?php endif; ?>

      <?php print render($page['content_messages']); ?>
      
    </div>
    
	
		
	