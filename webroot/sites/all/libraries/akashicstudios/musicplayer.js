
function MusicPlayer() {

	
	this.domMusic = {};
	this.category = '';
	this.playlistUrl = '';
	this.playList = '';

	//defaults:
	this.playerId = 'entertainment_music_jwplayer';
	this.skin = '/sites/all/themes/silversurfer/jwskin/skin-tablet.xml';
	this.scrubLeft = 0;
	this.scrubRight = 0;
	this.primary = 'html5';
	this.autostart = false;
	this.audioBackground = '';
	this.volume = 100;
	this.mobileStart = true;
	
	this.loadImgSrc = {
		'loading' : '/sites/all/themes/silversurfer/img/loading-logo-dots.gif',
		'start' : '/sites/all/themes/silversurfer/jwskin/controlbar/click-to-launch-rosso.png',
	};

	
	
	
	
	
	function setupRandomRepeat() {
		
			// try {
		
			console.log('playlistUrl - '+this.playlistUrl);
			
			this.clearWindowText();
			this.showLoadingImg(true);
			
			this.mobileStart = true;
			
			if(navigator.userAgent == 'ss-4gb') {
				this.primary = 'flash';
			}
			

			jwplayer(this.playerId).setup({
				width: '100%', 
				height: '100%',
				playlist: this.playlistUrl,
				controls : true,
				skin: this.skin,
				displaytitle : false,
				autostart : false,
				primary : this.primary,
				events : {
					
					onComplete : function () {
						var playlist = this.getPlaylist();
						var randomnumber=Math.floor(Math.random()* playlist.length);
						this.playlistItem(randomnumber);
						
						console.log('onComplete');
					}
					
					
					, onError : function (){
						window.AkashicStudios.musicPlayer.errMsg();
						console.log('onError - '+ this.message);
						window.AkashicStudios.musicPlayer.showLoadingImg(true,'loading');
						window.AkashicStudios.musicPlayer.remove();
					}
					
					
					
					, onPlaylist : function(){
						var pl = this.getPlaylist();
			
						for(var i=0; i<pl.length; i++) {
							pl[i].image = null;
						}
						
						console.log(pl);
			
					}
					
					
					
					, onPlaylistItem : function (){
						
						window.AkashicStudios.musicPlayer.updateWindowText();
						
					}
					
					
					, onReady : function (){
						
						window.AkashicStudios.musicPlayer.showLoadingImg(false,'loading');
						var playlist = this.getPlaylist();
						var randomnumber=Math.floor(Math.random()* playlist.length);
						this.playlistItem(randomnumber);
						console.log('ready');
//						alert('ready');
					}
					
					
					, onPlay : function (){
						window.AkashicStudios.musicPlayer.showLoadingImg(false,'loading');
//						alert('onPlay');
					}
					
					
					, onBuffer : function (){
						
						//on mobile devices this gets called prior to loading the player
						if(window.AkashicStudios.isMobile()){
							if(window.AkashicStudios.musicPlayer.mobileStart) {
								window.AkashicStudios.musicPlayer.showLoadingImg(true,'start');
								window.AkashicStudios.musicPlayer.mobileStart = false;
							}
						}
						else {
							window.AkashicStudios.musicPlayer.showLoadingImg(false,'loading');
						}
//						alert('onBuffer');
					}
					
					
					, onPause : function (){
						//on mobile, arrive here after loading the player, then the user must play
						window.AkashicStudios.musicPlayer.showLoadingImg(false,'loading');
//						alert('onPause');
					}
					
					
					, onIdle : function (){
						window.AkashicStudios.musicPlayer.showLoadingImg(false,'loading');
//						alert('onIdle');
					}
					
					
					
				}
			});
			
			
			window.AkashicStudios.musicPlayer.domHeader.innerHTML = this.category;
			
		
		// } catch (err){
			// console.log("ItvJWPlayer.setup()  ERROR: " + err.message);
		// }


	}
	this.setupRandomRepeat = setupRandomRepeat;
	

	function updateWindowText() {
		
		var playlist = jwplayer(this.playerId).getPlaylist() || '';
		var i = jwplayer(this.playerId).getPlaylistIndex() || '';
		if( (playlist !='') && (i !='')) {
			var item = playlist[i];
			window.AkashicStudios.musicPlayer.domTitle.innerHTML = item.title;
			window.AkashicStudios.musicPlayer.domDescription.innerHTML = '<span class="artist">' + item.description.split('|')[0] + '</span>';	//<br/><span class="album">' + item.description.split('|')[1] + '</span>';
		}
		
	}
	this.updateWindowText = updateWindowText;
	
	
	function clearWindowText() {
		
		window.AkashicStudios.musicPlayer.domTitle.innerHTML = '';
		window.AkashicStudios.musicPlayer.domDescription.innerHTML = '';
		
	}
	this.clearWindowText = clearWindowText;


	
	function errMsg(){
		window.AkashicStudios.musicPlayer.domDescription.innerHTML = "Music stream presently unavailable. We apologize for any inconvenience. Service will resume shortly.";
	}
	this.errMsg = errMsg;



	
	function showLoadingImg(isVisible,src){
		window.AkashicStudios.musicPlayer.domLoadImg.src = this.loadImgSrc[src];
		window.AkashicStudios.musicPlayer.domLoadImg.parentElement.style.display = isVisible ? 'block' : 'none';
	}
	this.showLoadingImg = showLoadingImg;
	
	
	
	function playRandomItem(id){
		var playlist = jwplayer(id).getPlaylist();
		var randomnumber=Math.floor(Math.random()* playlist.length);
		jwplayer(id).playlistItem(randomnumber);
		
	}
	this.playRandomItem = playRandomItem;
	
	
	
	
	function remove() {
		try {
			this.clearWindowText();
			jwplayer(this.playerId).remove();
		} catch (err) {
			
		}
	}
	this.remove = remove;
	

	function restartTrack() {
		jwplayer(this.playerId).seek(0);		
	}
	this.restartTrack = restartTrack;

	
	function forward() {
		var p = jwplayer(this.playerId).getPosition();
		var d = jwplayer(this.playerId).getDuration();
		
		jwplayer(this.playerId).seek(p+10);
		
	}
	this.forward = forward;
	
	
	
	
	function play() {
		jwplayer(this.playerId).play(true);
	}
	this.play = play;


	
	
	function togglePlay() {
		jwplayer(this.playerId).play();
	}
	this.togglePlay = togglePlay;
	
	
	
	
	function nextTrack() {
		this.playRandomItem(this.playerId);
		this.updateWindowText(this.playerId);
	}
	this.nextTrack = nextTrack;


	
	
	function pause() {
		jwplayer(this.playerId).pause(true);
	}
	this.pause = pause;

	
	

	function stop() {
		jwplayer(this.playerId).stop();
	}
	this.stop = stop;
	
	
	

}	


