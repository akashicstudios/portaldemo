	<div id="header">
	
  		<?php print render($page['header_first']); ?>
  		<?php print render($page['header_second']); ?>
  		<?php print render($page['header_third']); ?>
		<div id="header-banner">
  			<?php print render($page['header_banner']); ?>
			<div id="header-banner-page-title">
				<?php print render($page['header_page_title']); ?>
			</div>
		</div>
  		  			  
	</div>	
  
	<div id="content">

		<div class="separator"></div>
		  
	  	<div id="content-top" class="content-row entertainment-movies-content-top entertainment-movies">
			
			<div id="movie-card">
			
				<div id="movie-poster-fields" class="movie-card-section">
					<div id="movie-poster" class="field poster">
						<img id="movie-poster-img" src="" width="220" height="330">
					</div>
					<div id="movie-play-button"></div>	
				</div>
				
				<div id="movie-text-fields" class="movie-card-section">
				
					<div id="row-title" class="movie-text-fields-row">
						
						<div id="row-title-column0">
							<div id="movie-field-title"></div>
						</div>
						<div id="row-title-column1">
							<div id="movie-field-rating">
								<img id="movie-field-rating-img" src="" width="80" height="60">
							</div>
							<div id="row-title-column1-text">
								<div id="movie-field-year"></div>
								<div id="movie-field-runtime"></div>
							</div>
						</div>
					</div>
					
					<div id="row-text" class="movie-text-fields-row">
						<div id="movie-field-directors" class="field directors"></div>  
						<div id="movie-field-actors" class="field actors"></div>  
						<div id="movie-field-story" class="field story">
							<div class="label">Story:</div>
							<div class="value"></div>
						</div>  
							
					</div>
					
					<div id="row-flags" class="movie-text-fields-row">
						<div id="row-flags-inner" class="">
							<div id="language-flag-def" class="language-flag-item">
								<img id="language-flag-img-def" src="" width="35" height="35">
							</div>
						</div>
					</div>
				
				</div>
				
			</div>
	  	
		</div>
		
	    <div class="separator"></div>
		
		<div id="content-bottom" class="content-row entertainment-movies-content-bottom">
			<div class="jcarousel-entertainment-movies jcarousel-entertainment" data-jcarousel="true">    
				<ul class="jcarousel-list">          
					<li id="jcarousel-entertainment-movies-item" style="display: none;">  
          				<div>
							<img src="" width="120" height="180">
						</div>  
					</li>
      			</ul>
      		
      		</div>
		</div>
		
		<div class="separator"></div>
		
	</div>

  
  
	<div id="footer">
  	
  		<?php print render($page['footer']); ?>
  	
	</div>
  
	<div id="content" style="background-color: #000; display: none;">
		<?php print render($page['content']); ?>
	</div>

  	<div id="content-messages">
      
      <?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      <?php endif; ?>

      <?php print render($page['content_messages']); ?>
      
    </div>

		
	