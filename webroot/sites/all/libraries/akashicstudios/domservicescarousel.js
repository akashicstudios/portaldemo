
function DomServicesCarousel() {
	
	this._cssClass = '';
	this._cssClassLink = '';
	
	this._prevButton = {};
	this._nextButton = {};
	
	this._domNode = {};
	this._links = new Array();
	this._ul = {};
	this._ul_default = {};
	this._liWidth = {};
	
	function init() {
		this.loadDomNode();
		this.loadItems();
		
		this.jcarousel_init();
		this.jcarousel_setwidth();

	}
	this.init = init;
	
	
	function loadDomNode(){
		
		var dn = document.getElementsByClassName(this._cssClass);
		if(dn != null) {
			this._domNode = dn.item(0);
		}		
		
		this._ul = this._domNode.firstElementChild;
		
		
		var p = document.createElement('A');
		p.className='jcarousel-prev';
		p.href='#';
		var n = document.createElement('A');
		n.className='jcarousel-next';
		n.href='#';
		this._domNode.appendChild(p);
		this._domNode.appendChild(n);
		
		this._domNode.addEventListener("touchmove", function(event){
			event.preventDefault();
			
		}, false);
		
	}
	this.loadDomNode = loadDomNode;
	
	
	function loadItems(){
		var me = this;
		
		var sg = window.AkashicStudios._ship_services._shipServices;
		var dn = document.getElementById('jcarousel-onboard-services-item');
		
		sg.forEach(function(s, i){
			
			var li = dn.cloneNode(true);
			li.removeAttribute('id');
			li.style.removeProperty('display');
			
			li.firstElementChild.dataset.tid = s.id();
			li.firstElementChild.dataset.image_hover = s.footer_image_src();
			li.firstElementChild.dataset.tid_deck = s.ship_deck_id();
			li.firstElementChild.children[0].src = s.content_image_src();
			li.firstElementChild.children[1].innerHTML = s.ship_location();
			li.firstElementChild.children[2].children[0].innerHTML = s.display_title();
			li.firstElementChild.children[2].children[1].innerHTML = s.content_text();
			
			li.firstElementChild.onclick = function(){
				var deckplanImg = document.getElementById('onboard-services-content-bottom-deckplan-img');
				deckplanImg.src = this.dataset.image_hover;
//				window.AkashicStudios._pageController._controller._dom_services_filters.setVenueSelect(this.dataset.tid);
//				window.AkashicStudios._pageController._controller._dom_services_filters.setDeckSelect(this.dataset.tid_deck);				
			};
			
			dn.parentElement.appendChild(li);
			me._links.push(li.firstElementChild);
			
		});
		
		this._ul = dn.parentElement;
		this._ul_default = this._ul.cloneNode(true);
		this._ul_default.removeChild(this._ul_default.children[0]);
		
		dn.parentElement.removeChild(dn);
		
	}
	this.loadItems = loadItems;
	
	
	
	function jcarousel_setwidth(){
		this._liWidth = 0;
		var ch = this._ul.children;
		
		for(var i=0; i<ch.length; i++){
			if(ch[i].scrollWidth>0) {
				this._liWidth += ch[i].scrollWidth;
			}
		}
		
		this._ul.style.width = this._liWidth + 'px';
	}
	this.jcarousel_setwidth = jcarousel_setwidth;
	
	
	function jcarousel_init(){
		
		var me = this;
		
		var jc = jQuery('.'+this._cssClass).jcarousel({
			wrap: 'circular'
		});
		
		jQuery('.'+this._cssClass+' .jcarousel-prev').jcarouselControl({
	        target: '-=1',
			carousel: jc
	    });

		jQuery('.'+this._cssClass+' .jcarousel-next').jcarouselControl({
	        target: '+=1',
	        carousel: jc
	    });
		
		jQuery('.'+this._cssClass).on('jcarousel:fullyvisiblein', 'li', function(event, carousel) {
			this.firstElementChild.onclick();
		});
				
	}
	this.jcarousel_init = jcarousel_init;


	function jcarousel_destroy(){
		jQuery('.'+this._cssClass).jcarousel('destroy');
	}
	this.jcarousel_destroy = jcarousel_destroy;
	
	
	function jcarousel_moveTo(tid){
		var index = null;
		
		for(var i=0; i<this._ul.children.length; i++){
			if(this._ul.children[i].firstElementChild.dataset.tid == tid) {
				index = i;
			}			
		}
		if(index !=null){
			jQuery('.'+this._cssClass).jcarousel('scroll',index);
		}
	}
	this.jcarousel_moveTo = jcarousel_moveTo;
	
	
	function jcarousel_moveFirst(){
		jQuery('.'+this._cssClass).jcarousel('scroll',0);
		this._ul.children[0].children[0].onclick();
		
	}
	this.jcarousel_moveFirst = jcarousel_moveFirst;
		

	
	
	
	function setFilters() {
		
		var vf = null;
		var c=0;
		
		var deck = window.AkashicStudios._pageController._controller._dom_services_filters.selectedDeck();
		
		window.AkashicStudios._pageController._controller._dom_services_filters.redrawVenueSelect();
		
		
		var myself = this;

		this.jcarousel_destroy();
		this._ul.innerHTML = '';
		
		for(var i = 0; i<this._ul_default.children.length; i++) {
			
			var li = this._ul_default.children[i];
			
			var bAppend = true;
			
			if(deck != 0) {
				bAppend = (li.firstElementChild.dataset.tid_deck == deck);
			}
			
			if(bAppend) {

				var newli = myself._ul.appendChild(li.cloneNode(true));
				newli.firstElementChild.onclick = function(){
					var deckplanImg = document.getElementById('onboard-services-content-bottom-deckplan-img');
					deckplanImg.src = this.dataset.image_hover;
				};
			}
			
		}
	
		this.jcarousel_init();
		this.jcarousel_setwidth();
		this.jcarousel_moveFirst();
		
	}
	this.setFilters = setFilters;

}
