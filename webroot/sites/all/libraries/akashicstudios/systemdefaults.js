function SystemDefaults() {
	
	this._defaults = {};
	this._loaded = false;
	this._mtngps = null;
	
	this.themeColor = 'aqua';	//'rosso';
	
	
	this.init = function(){

		var me = this;
		jQuery.getJSON( '/json/system_defaults' )
		.done(function(data){
			me._defaults = data.system_defaults;
			me._loaded = true;

			window.AkashicStudios.onSystemDefaultsLoaded();
		})
		.fail(function( jqXHR, textStatus, errorThrown){
			console.log(textStatus + '\n' + errorThrown);
		});

	};
	

	this.ship = function(){
		return this._defaults.ship;
	}

	
	
	this.mtngps = function(){
		
		var retval = this._mtngps;
		var me = this;
		
		if(retval==null){
			Drupal.settings.silversurfer.mtngps.forEach(function(m){
				if(m.shipcode==me._defaults.ship.ship_code.value) {
					retval = m;
				}
			});
		}
		
		if(retval!=null){
			this._mtngps = retval;
		}
		
		return retval;
		
	};
	
}