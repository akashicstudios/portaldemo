
	<div id="itv-body">
	
		<div id="itv-navarrow-top" class="itv-navarrow" style="opacity: 0;"></div>
		<div id="itv-navarrow-right" class="itv-navarrow" style="opacity: 0;"></div>
		<div id="itv-navarrow-bottom" class="itv-navarrow" style="opacity: 0;"></div>
		<div id="itv-navarrow-left" class="itv-navarrow" style="opacity: 0;"></div>
		<div id="itv-navarrow-movieaudioviewtop" class="itv-navarrow" style="opacity: 0;"></div>
		<div id="itv-navarrow-movieaudioviewbottom" class="itv-navarrow" style="opacity: 0;"></div>
		<div id="itv-navarrow-tvepisodesviewtop" class="itv-navarrow" style="opacity: 0;"></div>
		<div id="itv-navarrow-tvepisodesviewbottom" class="itv-navarrow" style="opacity: 0;"></div>
		
		<div id="itv-body-inner">
		
			<div id="itv-header">
			
				<div id="itv-header-first">
					<div id="currentDateAndTime" class="currentDateAndTime"></div>
			  	</div>
			  	
			  	<div id="itv-header-second">
			  		<div class="itv-header-second-img">
			  			<img id="itv-header-second-img" src="">
			  		</div>
			  		
			  	</div>
	
			  	<div id="itv-header-third">
			  		<div id="itv-header-third-shipname-gps" class="">
			  			<div class="shipname"></div>
			  			<div class="gps"></div>
			  		</div>
			  	</div>
			
			</div>	
		  
			<div id="itv-content" class="focus">
		
				<div id="itv-menubar">
				
					<div id="MainMenuView">
							
						<div class="jcarousel-mainmenu" data-jcarousel="true">    
							<ul class="jcarousel-list akashicitv-view-list jcarousel-mainmenu-list">          
								<li class="jcarousel-mainmenu-list-item akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div">
										<div class="icon"><img src=""></div>
										<div class="menu-text"></div>
									</div>  
								</li>
							</ul>
						</div>
						
					</div>
					
				</div>	
			
				<div id="itv-content-main">
				  	
				  	<div id="itv-content-main-header">
				  		<div class="breadcrumb-icons">
				  			<div class="breadcrumb-icons-left">
					  			<div class="breadcrumb home"><img src="/sites/all/themes/silversurfer/img/itv/breadcrumbs/home.png" width="25" height="25"></div>
					  			<div class="breadcrumb slash"><img src="/sites/all/themes/silversurfer/img/itv/breadcrumbs/slash.png" width="25" height="25"></div>
					  			<div class="breadcrumb view" data-imgsrc="/sites/all/themes/silversurfer/img/itv/breadcrumbs/"><img id="itv-breadcrumb-view" src="/sites/all/themes/silversurfer/img/itv/breadcrumbs/shipservicesview.png" width="25" height="25"></div>
					  		</div>
					  		<div class="breadcrumb-icons-right">
				  				<div class="breadcrumb back"><img src="/sites/all/themes/silversurfer/img/itv/breadcrumbs/back.png" width="25" height="25"></div>
			  				</div>
				  		</div>
					</div>
					
					<div id="itv-content-main-view">
					
						<div id="ShipServicesView" class="jcarousel-itvshipservicesview jcarousel-autoscroll jcarousel-horizontal" style="display: none;">
							<ul class="akashicitv-view-list">          
								<li class="akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div">
										<div class="serviceimg"><img class="service-img" src="" width="800" height="300"></div>
										<div class="text-overlay">
											<div class="text title"></div>
											<div class="text description"></div>
											<div class="text deck"></div>
										</div>
										<div class="deckplanimg"><img class="deckplan-img" src="" width="764" height="160"></div>
									</div>  
								</li>
							</ul>
						</div>
						
						<div id="MovieCategoryView" style="display: none;" data-omnirows="2" data-omnicolumns="4">
							<ul class="akashicitv-view-list">          
								<li class="akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div">
										<div class="poster"><img src=""></div>
										<div class="text-overlay"></div>
									</div>  
								</li>
							</ul>
						</div>
						
						<div id="MoviesView" style="display: none;" data-omnirows="2" data-omnicolumns="4">
							<ul class="akashicitv-view-list">          
								<li class="akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div">
										<div class="poster"><img src="" width="120" height="180"></div>
									</div>  
								</li>
							</ul>
						</div>
						
						<div id="MoviesDetailView" style="display: none;">
							<ul class="akashicitv-view-list">          
								<li class="akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div movie-detail-card">

										<div class="movie-card-row movie-card-row-title">
											<div class="movie-field-title"></div>
										</div>
										
										
										<div class="movie-card-row movie-card-row-fields">
										
											<div class="column column-poster">
											
												<div class="poster">
													<img class="poster-img" src="" width="160" height="240">
												</div>
											
											</div>
											
											
											<div class="column column-fields">
											
												<div class="movie-field-directors"></div>  
												<div class="movie-field-actors"></div>  
												<div class="movie-field-story">
													<div class="movie-field-story-label">Story:</div>
													<div class="movie-field-story-value"></div>
												</div>  
											
											</div>
											
											
											<div class="column column-rating">
												
												<div class="rating">
													<img class="movie-field-rating-img" src="" width="40" height="30">
												</div>
												<div class="text">
													<div class="movie-field-year"></div>
													<div class="movie-field-runtime"></div>
												</div>
											
											</div>
											
										</div>
										
										
										<div class="movie-card-row movie-card-row-play-button">
											<div class="play-button">PLAY</div>
										</div>
										
										<div class="movie-card-row movie-card-row-language-icons">
											<div class="language-flags">
												<ul class="language-flags-list">
													<li class="language-flags-list-item">
														<img src="" width="30" height="30">
													</li>
												</ul>
											</div>
										
										</div>

									
									</div>  
								</li>
							</ul>
						</div>
						
						<div id="MovieAudioView" style="display: none;">
							<div class="media-items">
								<div class="list-header">Select Audio:</div>
								<div class="media-items-list">
									<ul class="akashicitv-view-list">          
										<li class="akashicitv-view-list-item" style="display: none;">  
											<div class="list-item-div">
												<div class="text"></div>
											</div>  
										</li>
									</ul>
								</div>
							</div>
						</div>
						
						<div id="SatelliteTvView" style="display: none;" data-omnirows="2" data-omnicolumns="4">
							<ul class="akashicitv-view-list">          
								<li class="akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div">
										<div class="poster"><img class="poster-img" src="" width="120" height="180"></div>
									</div>  
								</li>
							</ul>
						</div>
						
						<div id="TvShowsView" style="display: none;" data-omnirows="2" data-omnicolumns="4">
							<ul class="akashicitv-view-list">          
								<li class="akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div">
										<div class="poster"><img class="poster-img" src="" width="120" height="180"></div>
									</div>  
								</li>
							</ul>
						</div>
						
						<div id="TvShowsDetailView" style="display: none;">
							<ul class="akashicitv-view-list">          
								<li class="akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div">
										<div class="section-top">
											<div class="tvshow-title"></div>
										</div>
										<div class="section-bottom">
											<div class="poster">
												<img class="poster-img" src="" width="160" height="240">
											</div>
											<div class="episodes">
												<div class="list-header">Select Episode:</div>
												<div class="TvShowEpisodesView">
													<ul class="episodes-list">
														<li class="episodes-list-item"> 
															<div class="episodes-list-item-div">
																<div class="episode-title">
																	<div class="sep"></div>
																	<div class="ept"></div>
																</div>
																<div class="runtime"></div>
															</div>  
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>	
						</div>
						
						<div id="MusicView" style="display: none;" data-omnirows="2" data-omnicolumns="4">
							<ul class="akashicitv-view-list">          
								<li class="akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div">
										<div class="poster"><img class="poster-img" src="" width="120" height="180"></div>
										<div class="text-overlay"></div>
									</div>  
								</li>
							</ul>
						</div>
						

						
						<div id="MusicPlayerView" style="display: none;">
							
							<div class="header"></div>
							<div class="title">Title</div>
							<div class="description">Description</div>
							
							 <div id="jwplayer-container" class="jwplayer-container">
							     <div style="display: none;"><img class="loadImg" src=""/></div>
							     <div id="entertainment_music_jwplayer"></div>
							 </div>
								
						</div>
						
						
						
						
						<div id="FleetTrackerView" style="display: none;">
						
							<ul class="akashicitv-view-list" style="display: none;">          
								<li class="akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div"></div>  
								</li>
							</ul>
										
							<div id="fleet-tracker-mapview" class=""></div>
						</div>
						
						<div id="SettingsView" style="display: none;" data-omnirows="2" data-omnicolumns="4">
							<ul class="akashicitv-view-list">          
								<li class="akashicitv-view-list-item" style="display: none;">  
									<div class="list-item-div">
										<div class="poster"><img class="poster-img" src="" width="120" height="180"></div>
										<div class="text-overlay"></div>
									</div>  
								</li>
							</ul>
						</div>
					
					
					</div>
				
				</div>
				
						
			</div>
		
		  
		  
			<div id="itv-footer">
			
				<div id="TickerView">
					<div class="itvtickerview-header">
						<div class="itvtickerview-header-text"></div>
					</div>
					<div class="jcarousel-itvtickerview jcarousel-autoscroll">
						<ul class="akashicitv-view-list">          
							<li class="akashicitv-view-list-item" style="display: none;">  
								<div class="list-item-div">
								</div>  
							</li>
						</ul>
					</div>
				</div>
		
		  		<div id="debug-messages" style="display: none; position: fixed; top: 320px; left: 620px; z-index: 999; width: 500px; height: 100px; padding: 10px; background-color: rgba(0,0,0,0.3); color: #fff; font-family: 'Courier'; overflow: scroll;"></div>

		  	</div>
			
			
		</div>
	
	</div>
	
	
    <div id="hidden" style="margin-top: 60px; display: none;">
      <?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      <?php endif; ?>

      <?php print render($page['content_messages']); ?>
    
    	<?php print render($page['header_first']); ?>
	  	<?php print render($page['header_second']); ?>
	  	<?php print render($page['header_third']); ?>
  		<?php print render($page['header_banner']); ?>
		<?php print render($page['content_top']); ?>
		<?php print render($page['content_bottom']); ?>
		<?php print render($page['footer']); ?>
    
    </div>
