<?php

module_load_include('inc', 'akashicstudios', 'akashicstudios_include');




function silversurfer_preprocess_page(&$vars) {

	if($_SERVER['HTTP_USER_AGENT'] == 'ss-4gb') {

		$uri = $_SERVER['REQUEST_URI'];

		if(request_path() != 'itv') {
			drupal_goto('itv');
		}
	}

	drupal_add_library('system','effects.core');

	if ($node = menu_get_object()) {
		$alias = drupal_get_path_alias("node/$node->nid");
	
		// Remove slashes from the potential filename
		$clean_alias = str_replace('/', '__', $alias);
	
		$vars['theme_hook_suggestions'][] = 'page__' . $clean_alias;
	}
	
	// get the basic_page content (body field) to appear in the header_banner region
	if (array_key_exists('node', $vars)) {
	
		$node = node_view($vars['node'], 'full');
		if (array_key_exists('body', $node)) {
			$obj = $node['body']['#object'];
			$vars['page']['header_page_title'] = $obj->body['und'][0]['value'];
		}
	}
	
}




function silversurfer_preprocess_region(&$variables, $hook) {
	
	if($variables['region']=='header_banner') {
	
	}

}


function silversurfer_css_alter(&$css){

// 	array_push($css,path_to_theme(). '/css/global.css');

	//echo '<!--'. print_r($css). '-->';
	
// 	if(request_path() != 'itv') {
// 		array_push($css, path_to_theme(). '/css/front.css');
// 	}
	
// 	if(request_path() == 'itv') {
		
// 		$css[] = path_to_theme(). '/css/itv.css';
// 		$css[] = path_to_theme(). '/css/itv-layout.css';
		
// 	}
	
	
}

function silversurfer_preprocess_html(&$vars) {
	
	//MODULES and LIBRARIES
	drupal_add_js(libraries_get_path('clamp') . '/clamp.min.js');
	
	drupal_add_js(libraries_get_path('jcarousel') . '/lib/jquery.jcarousel.js');
	
	drupal_add_js(libraries_get_path('jwplayer6.8') . '/jwplayer.js');
	drupal_add_js(libraries_get_path('jwplayer6.8') . '/jwplayer.html5.js');
	
	drupal_add_js(libraries_get_path('jquery.color') . '/jquery.color.js');
	drupal_add_css(libraries_get_path('leaflet') . '/leaflet.css');
	
	
	// GLOBAL INCLUDES
	//////	akashic-globals.min.js.gz :::
	    drupal_add_js(libraries_get_path('akashicstudios') . '/akashicjcarousel.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/akashicstudios.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/debug.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/dropmenu.js');
		drupal_add_js(libraries_get_path('akashicstudios') . '/pagecontroller.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/shiplocations.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/shipservices.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/systemdefaults.js');
			
	    drupal_add_css(path_to_theme(). '/css/global.css');
	
	
	// ITV INCLUDES
	if(request_path() == 'itv') {
	    drupal_add_css(path_to_theme(). '/css/itv-layout.css');
	    drupal_add_css(path_to_theme(). '/css/itv.css');
	}
	
    	drupal_add_js(libraries_get_path('akashicstudios') . '/akashicitv.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/fleettracker.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/itvpagecontroller.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/moviesjson.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/musiccategory.js');

    	drupal_add_js(libraries_get_path('akashicstudios') . '/musicjson.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/musicplayer.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/satellitetvjson.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/tvseriesjson.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/vodfilm.js');

    	drupal_add_js(libraries_get_path('akashicstudios') . '/vodfilmmediaitem.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/vodfilmmetadata.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/vodsatellitetv.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/vodtvseries.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/vodtvseriesepisode.js');
				
    		
		drupal_add_js(libraries_get_path('leaflet') . '/leaflet.js');
			
	
	// TABLET INCLUDES
	if(request_path() != 'itv') {
	    drupal_add_js(libraries_get_path('akashicstudios') . '/tabletmenu.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/tabletmenuitem.js');
			
		drupal_add_css(path_to_theme(). '/css/tablet-layout.css');
			
	    drupal_add_js(libraries_get_path('akashicstudios') . '/front.js');
	   			
		drupal_add_css(path_to_theme(). '/css/front.css');
// 	}
	
// 	if(request_path() == 'onboard/fleettracker') {
    	drupal_add_js(libraries_get_path('akashicstudios') . '/domfleettracker.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/fleettracker.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/fleettrackerpagecontroller.js');
    				
		drupal_add_css(path_to_theme(). '/css/fleettracker.css');
// 	}
	
// 	if(request_path() == 'onboard/services') {
    	drupal_add_js(libraries_get_path('akashicstudios') . '/domservices.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/domservicescarousel.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/servicespagecontroller.js');

    	drupal_add_css(path_to_theme(). '/css/services.css');
    	drupal_add_css(path_to_theme(). '/css/jcarousel-services.css');
// 	}
	
	
	
// 	if(request_path() == 'entertainment/movies') {
    	drupal_add_js(libraries_get_path('akashicstudios') . '/domfilmcarousel.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/dommoviefilters.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/domuniquemovie.js');
		drupal_add_js(libraries_get_path('akashicstudios') . '/moviesjson.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/moviespagecontroller.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/vodfilm.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/vodfilmmediaitem.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/vodfilmmetadata.js');
    	
		drupal_add_css(path_to_theme(). '/css/jcarousel-entertainment.css');
		drupal_add_css(path_to_theme(). '/css/movies.css');
// 		drupal_add_css(path_to_theme(). '/css/movies-layout.css');
		
// 	}
	
	
// 	if(request_path() == 'entertainment/television') {
			
	    drupal_add_js(libraries_get_path('akashicstudios') . '/domsatellitetv.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/domsatellitetvcarousel.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/domtelevisionfilters.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/domtvseries.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/domtvseriescarousel.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/satellitetvjson.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/televisionpagecontroller.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/tvseriesjson.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/vodsatellitetv.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/vodtvseries.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/vodtvseriesepisode.js');
			
		drupal_add_css(path_to_theme(). '/css/jcarousel-entertainment.css');
		drupal_add_css(path_to_theme(). '/css/television.css');
			
// 	}
	
	
// 	if(request_path() == 'entertainment/music') {
	    drupal_add_js(libraries_get_path('akashicstudios') . '/dommusic.js');
		drupal_add_js(libraries_get_path('akashicstudios') . '/dommusiccarousel.js');
    	drupal_add_js(libraries_get_path('akashicstudios') . '/musiccategory.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/musicjson.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/musicpagecontroller.js');
	    drupal_add_js(libraries_get_path('akashicstudios') . '/musicplayer.js');
						
		drupal_add_css(path_to_theme(). '/css/jcarousel-entertainment.css');
		drupal_add_css(path_to_theme(). '/css/music.css');
// 	}
	
	
// 	if(request_path() == 'news/pressreader') {
    	drupal_add_js(libraries_get_path('akashicstudios') . '/pressreaderpagecontroller.js');
			
		drupal_add_css(path_to_theme(). '/css/pressreader.css');
// 	}
	
	
// 	if(request_path() == 'news/newslink') {
	    drupal_add_js(libraries_get_path('akashicstudios') . '/newslink.js');
		drupal_add_js(libraries_get_path('akashicstudios') . '/newslinkjson.js');
		drupal_add_js(libraries_get_path('akashicstudios') . '/newslinkpagecontroller.js');

		drupal_add_css(path_to_theme(). '/css/newslink.css');
		drupal_add_css(path_to_theme(). '/css/jcarousel-newslink.css');
	}
	
	
	
	
    // add global parameters to JS for script use
    // Drupal.settings.redCell.itv_home_page = /itv
    // region-content 
    //  rectangle = 730 x 515
    //  position top: 90; 
    //  offsetLeft: 368px |  clientWidth 830  | padding-left 50  | contentLeft 418  |   margin-left: contentWidth  730   | contentRight  1198   
    // left arrow: 443 --> 493  |<--      647 width      -->|   right arrow: 1140 --> 1190
    // workspace 


    
    
    
    $mtnParams = array(
            'UI' => isset($_GET['UI']) ? $_GET['UI'] : '',
            'NI' => isset($_GET['NI']) ? $_GET['NI'] : '',
            'UIP' => isset($_GET['UIP'])  ? $_GET['UIP'] : '',
            'MA' => isset($_GET['MA'])  ? $_GET['MA'] : '',
            'OS' => isset($_GET['OS'])  ? $_GET['OS'] : '','',
            'SIP' => isset($_GET['SIP'])  ? $_GET['SIP'] : ''
        );


    $jsonVOD = array(
    		array('objectName' => 'json_vod_films', 'url' => '/json/vod-films'),
    		array('objectName' => 'json_vod_meta_data', 'url' => '/json/vod-film-meta-data'),
       		array('objectName' => 'json_vod_media_items', 'url' => '/json/vod-film-media-items'),    		
    );

    
    $silversurferJavaScriptSettings = array(
        'silversurfer' => array(
                'theme' => path_to_theme(),
                'itvHomePage' => '/itv',                
                'musicFeedPath' => '/sites/default/files/feeds/music',
                'serverJsonUrl' => 'http://'.$_SERVER['SERVER_NAME']. ':'.$_SERVER['SERVER_PORT'],
                'MTN' => $mtnParams,
                'defaultServers' => akashicstudios_default_servers(),
                'mtngps' => akashicstudios_sql_rows('mtngps()'),  
        		'jsonVOD' => $jsonVOD,
        	),
        );

    drupal_add_js($silversurferJavaScriptSettings, 'setting');



    // disable apple's 'telephone-number' handling in url's
    $appleTags = array(
      '#tag' => 'meta', 
      '#attributes' => array(
        'name' => 'format-detection', 
        'content' => 'telephone=no',
      ),
    ); 
    drupal_add_html_head($appleTags, 'format-detection');



    // prevent the site from caching any data on the client side
//  drupal_add_html_head( array('#tag' => 'meta', '#attributes' => array('name' => 'cache-control', 'content' => 'max-age-0')), 'cache-control');
//    drupal_add_html_head( array('#tag' => 'meta', '#attributes' => array('http-equiv' => 'CACHE-CONTROL', 'content' => 'NO-CACHE')), 'CACHE-CONTROL');
//  drupal_add_html_head( array('#tag' => 'meta', '#attributes' => array('name' => 'expires', 'content' => '0')), 'expires');
//    drupal_add_html_head( array('#tag' => 'meta', '#attributes' => array('http-equiv' => 'EXPIRES', 'content' => 'Tue, 01 Jan 1980 1:00:00 GMT')), 'EXPIRES');
    
   $ua = $_SERVER['HTTP_USER_AGENT']; 
    if($ua == 'ssv2-8gb'){
//        drupal_add_html_head( array('#tag' => 'meta', '#attributes' => array('name' => 'viewport', 'content' => 'width=1920,initial-scale=1.0,user-scalable=no')), 'viewport');
    }

    if($ua == 'ss-4gb'){
    }
    
    
    if(($ua != 'ss-4gb') && ($ua != 'ssv2-8gb')) {
    
	    if(!strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
	//       drupal_add_html_head( array('#tag' => 'meta', '#attributes' => array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0,user-scalable=yes')), 'viewport');
	      	drupal_add_html_head( array('#tag' => 'meta', '#attributes' => array('name' => 'viewport', 'content' => 'width=980,user-scalable=no')), 'viewport');
	 	}
	 	else {
 	 		drupal_add_html_head( array('#tag' => 'meta', '#attributes' => array('name' => 'viewport', 'content' => 'width=980,user-scalable=yes')), 'viewport');
	 	}
	    //error_log($_SERVER['HTTP_USER_AGENT']);
    }
    
    
}

function silversurfer_process_html(&$variables) {

	
	
	
    // Remove Query Strings from CSS filenames (CacheBuster)
    //	$variables['styles'] = preg_replace('/\.css\?.*"/','.css"', $variables['styles']);
    $variables['styles'] = preg_replace('/\.css\?[^"]+/','.css', $variables['styles']);
    $variables['styles'] = preg_replace('/\.css.gz\?[^"]+/','.css.gz', $variables['styles']);
    
//     $variables['styles'] = preg_replace('/\.js\?[^"]+/','.js', $variables['styles']);
    
    
 
}



