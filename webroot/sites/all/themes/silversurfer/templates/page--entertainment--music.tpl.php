	<div id="header">
	
  		<?php print render($page['header_first']); ?>
  		<?php print render($page['header_second']); ?>
  		<?php print render($page['header_third']); ?>
		<div id="header-banner">
  			<?php print render($page['header_banner']); ?>
			<div id="header-banner-page-title">
				<?php print render($page['header_page_title']); ?>
			</div>
		</div>
  		  			
	</div>	
  
	<div id="content">

		<div class="separator"></div>
		
	  	<div id="content-top" class="content-row entertainment-music-content-top">
	  	
		  	 <div class="region region-content-top entertainment-music" style="">
			  	<div id="entertainment-music-content" class="entertainment-music vod-music">
					
					<div class="header"></div>
					<div class="title"></div>
					<div class="description"></div>
					
					 <div id="jwplayer-container" class="jwplayer-container">
					     <div class="loadimg"><img src=""/></div>
					     <div id="entertainment_music_jwplayer"></div>
					 </div>
					
			  	</div>
			</div>  	
		</div>
		
		<div class="separator"></div>
		
		<div id="content-bottom" class="content-row entertainment-music-content-bottom">
			
			<div class="jcarousel-entertainment jcarousel-entertainment-music" data-jcarousel="true">    
				<ul class="jcarousel-list">          
					<li id="jcarousel-entertainment-music-item" style="display: none;">  
		  				<div>        
		  					<img src="" width="120" height="180">
		  				</div>
						<div class="name">Classical</div>
					</li>
				</ul>
			</div>
			
		</div>
		
		<div class="separator"></div>
		
	</div>

  
  
	<div id="footer">
  	
  		<?php print render($page['footer']); ?>
  	
	</div>
  
	<div id="content" style="background-color: #000; display: none;">
		<?php print render($page['content']); ?>
	</div>
		  
  	<div id="content-messages">
      
      <?php if ($messages): ?>
        <div id="console" class="clearfix"><?php print $messages; ?></div>
      <?php endif; ?>

      <?php print render($page['content_messages']); ?>
      
    </div>

	