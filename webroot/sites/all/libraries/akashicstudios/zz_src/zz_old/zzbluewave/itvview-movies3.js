

var itvViewMovies3Controller = new ItvView('itvViewMovies3');

itvViewMovies3Controller.viewItemDefaults.tagName = 'LI';
itvViewMovies3Controller.viewItemDefaults.navIdPrefix = itvViewMovies3Controller.cssClass + 'Item';
itvViewMovies3Controller.viewItemDefaults.inFocusCss = itvViewMovies3Controller.cssClass + '-list-item-focus';
itvViewMovies3Controller.html.scroll.direction = 'vertical';

itvViewMovies3Controller.nav.dpad.back = 'itvViewMovies2';





itvViewMovies3Controller.html.position.maxWidth = 540;
itvViewMovies3Controller.html.isOverlay = true;

itvViewMovies3Controller.init();
itvViewMovies3Controller.hide();

window.gNav.views['itvViewMovies3'] = itvViewMovies3Controller;
window.gNav.viewsZeroBased.push(itvViewMovies3Controller);


