
function VodTvSeriesEpisode() {
	
	this._json = null;

	function media_id(){
		return this._json.media_id;
	}
	this.media_id = media_id;

	
	function season(){
		return this._json.season;
	}
	this.season = season;

	
	function episode(){
		return this._json.episode;
	}
	this.episode = episode;

	
	function title(){
		return this._json.title;
	}
	this.title = title;

	
	function runtime(){
		var retval = '';
		var rt = new Number(this._json.runtime);
		if(rt>0){
			rt = rt/60;
			retval = Math.floor(rt/60) + ':';
			var s = rt%60;
			if(s<10){retval+='0';}
			retval += s; 
		}
		return retval;		
	}
	this.runtime = runtime;



	
	
}

