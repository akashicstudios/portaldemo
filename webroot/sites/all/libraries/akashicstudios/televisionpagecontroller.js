function TelevisionPageController(){

	this._tvseries_json = new TvSeriesJson();

	this._dom_tvseries = new DomTvSeries();
	this._dom_tvseries.init();

	this._dom_tvseries_carousel = new DomTvSeriesCarousel();
	this._dom_tvseries_carousel._cssClass = 'jcarousel-entertainment-tvseries';
	this._dom_tvseries_carousel._cssClassLink = 'link-jcarousel-tvseries';
	
	
	this._dom_television_filters = new DomTelevisionFilters();
	this._dom_television_filters.init();
		

	this._css = {
		'content_blocks' : {
			'tvseries' : 'entertainment-television-content-tvseries',
			'satellitetv' : 'entertainment-television-content-satellitetv'
		},
		'carousel_blocks' : {
			'tvseries' : 'jcarousel-entertainment-tvseries',
			'satellitetv' : 'jcarousel-entertainment-satellitetv'			
		}
	};

	this._dom = {
		'content_blocks' : {
			'tvseries' : document.getElementsByClassName(this._css.content_blocks.tvseries).item(0),
			'satellitetv' : document.getElementsByClassName(this._css.content_blocks.satellitetv).item(0)
		},
		'carousel_blocks' : {
			'tvseries' : document.getElementsByClassName(this._css.carousel_blocks.tvseries).item(0),
			'satellitetv' : document.getElementsByClassName(this._css.carousel_blocks.satellitetv).item(0)			
		}
	};

	
	this._satellitetv_json = new SatelliteTvJson();
	
	this._dom_satellitetv = new DomSatelliteTv();
	this._dom_satellitetv.init();

	this._dom_satellitetv_carousel = new DomSatelliteTvCarousel();
	this._dom_satellitetv_carousel._cssClass = 'jcarousel-entertainment-satellitetv';
	this._dom_satellitetv_carousel._cssClassLink = 'link-jcarousel-satellitetv';
	
	
	function init(){
		this._tvseries_json.init();
		this._satellitetv_json.init();

//		this.hideContent();		
	}
	this.init = init;
	
	
	this.onTvSeriesJsonLoaded = function(){
		this._dom_tvseries_carousel.init();
		this._dom_tvseries_carousel.jcarousel_moveFirst();
		
	}
	
	this.onSatelliteTvJsonLoaded = function(){
		this._dom_satellitetv_carousel.init();		
		this._dom_satellitetv_carousel.jcarousel_moveFirst();
		
		this.showContent();
	}
	
	
	function hideContent(){
		this._dom.content_blocks.tvseries.style.display = 'none';
		this._dom.content_blocks.satellitetv.style.display = 'none';
		this._dom.carousel_blocks.tvseries.style.display = 'none';
		this._dom.carousel_blocks.satellitetv.style.display = 'none';
	}
	this.hideContent = hideContent;
	
	
	function showContent() {
		
		var contentBlock = this._dom_television_filters.selectedContent();
		
		this.hideContent();
		
		this._dom.content_blocks[contentBlock].style.removeProperty('display');		
		this._dom.carousel_blocks[contentBlock].style.removeProperty('display');		
		
	}
	this.showContent = showContent;

	
	
	
}


	


