

var itvViewMovies2Controller = new ItvView('itvViewMovies2');

itvViewMovies2Controller.viewItemDefaults.tagName = 'LI';
itvViewMovies2Controller.viewItemDefaults.navIdPrefix = itvViewMovies2Controller.cssClass + 'Item';
itvViewMovies2Controller.viewItemDefaults.inFocusCss = itvViewMovies2Controller.cssClass + '-list-item-focus';
itvViewMovies2Controller.html.scroll.direction = 'horizontal';

itvViewMovies2Controller.nav.dpad.back = 'itvViewMovies1';

itvViewMovies2Controller.icons.arrows.push(new ItvNavIcon({
	name: 'back', 
	cssClass : 'itvViewMovies2-nav-arrow-back', 
	top: 317, 
	left: 295, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewMovies2Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.back
}));

itvViewMovies2Controller.icons.arrows.push(new ItvNavIcon({
	name: 'forward', 
	cssClass : 'itvViewMovies2-nav-arrow-forward', 
	top: 317, 
	left: 1119, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewMovies2Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.forward
}));


itvViewMovies2Controller.icons.breadcrumb = new ItvNavIcon({
	name: 'breadcrumb', 
	cssClass : 'itvViewMovies2-nav-breadcrumb-movies', 
	top: 100, 
	left: 370, 
	opacity: 1, 
	position: 'fixed', 
	parentElement: itvViewMovies2Controller.domNode, 
	imgPath : Drupal.settings.redCell.navArrowPaths.breadcrumbMovies
});


itvViewMovies2Controller.html.position.maxWidth = 800;

itvViewMovies2Controller.init();
itvViewMovies2Controller.hide();

window.gNav.views['itvViewMovies2'] = itvViewMovies2Controller;
window.gNav.viewsZeroBased.push(itvViewMovies2Controller);

