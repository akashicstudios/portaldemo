
function setFilters(cf){

	window.gNav.views['tabletViewMovies1'].clearViewItemsCategoryFilter();
	window.gNav.views['tabletViewMovies1'].setViewItemsCategoryFilter(cf);
	
	// window.gNav.views['tabletViewMovies1'].domNode.style.opacity = 0.1;
	window.gNav.views['tabletViewMovies1'].clearViewItemsCategoryFilter();	
	window.gNav.views['tabletViewMovies1'].setViewItemsCategoryFilter(cf);
	
	// window.gNav.views['tabletViewMovies1'].setVisibleScrollWidth();
	
	var visNode = window.gNav.views['tabletViewMovies1'].viewItemsVisible[0];
	var nodeWidth = window.gNav.views['tabletViewMovies1'].viewItemsZeroBased[visNode].domNode.scrollWidth;
	var nodeCount = window.gNav.views['tabletViewMovies1'].viewItemsVisible.length;
	
	window.gNav.views['tabletViewMovies1'].domNode.scrollLeft = 0;
	
	var list = window.gNav.views['tabletViewMovies1'].domNode.getElementsByTagName('UL');
	if(list.length>0) {
		list[0].style.width = (nodeWidth * nodeCount) + 'px';	
	}

	window.gNav.views['tabletViewMovies2'].clearViewItemsCategoryFilter();	
	window.gNav.views['tabletViewMovies2'].setViewItemsCategoryFilter(cf);
		

}




var tabletViewMovies0Controller = new ItvView('tabletViewMovies0');

tabletViewMovies0Controller.viewItemDefaults.tagName = 'LI';
tabletViewMovies0Controller.viewItemDefaults.navIdPrefix = tabletViewMovies0Controller.cssClass + 'Item';
tabletViewMovies0Controller.viewItemDefaults.inFocusCss = tabletViewMovies0Controller.cssClass + '-list-item-focus';

tabletViewMovies0Controller.init();
//tabletViewMovies0Controller.hide();

window.gNav.views['tabletViewMovies0'] = tabletViewMovies0Controller;
window.gNav.viewsZeroBased.push(tabletViewMovies0Controller);


//redraw select boxes

var divCatSelect = document.getElementsByClassName('tabletMovies0Select').item(0);
var selOptions = divCatSelect.getElementsByClassName('select-option');

var catSelect = document.createElement('SELECT');
catSelect.id = 'tabletMovies0Select';
catSelect.className = 'tabletMovies0Select';

for(i=0; i<selOptions.length; i++) {
	var catOpt = document.createElement('OPTION');
	catOpt.className = 'select-option';
	catOpt.innerHTML = selOptions[i].innerHTML;
	catSelect.appendChild(catOpt);
}

divCatSelect.firstElementChild.innerHTML = '<div class="select-container"></div>';
divCatSelect.firstElementChild.firstElementChild.appendChild(catSelect);

catSelect.onchange = function(){
	setFilters(this.options[this.selectedIndex].value);
};




var visNode = window.gNav.views['tabletViewMovies1'].viewItemsVisible[0];
var item1 = window.gNav.views['tabletViewMovies1'].viewItemsZeroBased[visNode].domNode;
item1.onclick();






/*
var divLangSelect = document.getElementsByClassName('tabletLanguageSelect').item(0);
var selOptions = divLangSelect.getElementsByClassName('select-option');

var langSelect = document.createElement('SELECT');
langSelect.id = 'tabletLanguageSelect';
langSelect.className = 'tabletLanguageSelect';

for(i=0; i<selOptions.length; i++) {
	var catOpt = document.createElement('OPTION');
	catOpt.className = 'select-option';
	catOpt.innerHTML = selOptions[i].innerHTML;
	langSelect.appendChild(catOpt);
}

divLangSelect.innerHTML = '';
divLangSelect.appendChild(langSelect);
*/



/*
for (var i=0; i< tabletViewMovies0Controller.viewItemsZeroBased.length; i++) {
	
	var cf = tabletViewMovies0Controller.viewItemsZeroBased[i].nav.dpad.enter.viewCategoryFilter;
	tabletViewMovies0Controller.viewItemsZeroBased[i].domNode.dataset.viewcategoryfilter = cf;
	tabletViewMovies0Controller.viewItemsZeroBased[i].domNode.dataset.cssclass = tabletViewMovies0Controller.viewItemsZeroBased[i].cssClass;
	tabletViewMovies0Controller.viewItemsZeroBased[i].domNode.onclick = function(){
		window.gNav.views['tabletViewMovies0'].blurViewItems();
		
		if(	 ! this.classList.contains(window.gNav.views['tabletViewMovies0'].viewItemDefaults.inFocusCss)  	) {		 
			this.classList.add(window.gNav.views['tabletViewMovies0'].viewItemDefaults.inFocusCss);		
		}
		
//		window.gNav.views['tabletViewMovies0'].viewItems[this.dataset.cssclass].focus();

		window.gNav.views['tabletViewMovies1'].domNode.style.opacity = 0.1;
		window.gNav.views['tabletViewMovies1'].clearViewItemsCategoryFilter();	
		window.gNav.views['tabletViewMovies1'].setViewItemsCategoryFilter(this.dataset.viewcategoryfilter);
		
		// window.gNav.views['tabletViewMovies1'].setVisibleScrollWidth();
		
		var visNode = window.gNav.views['tabletViewMovies1'].viewItemsVisible[0];
		var nodeWidth = window.gNav.views['tabletViewMovies1'].viewItemsZeroBased[visNode].domNode.scrollWidth;
		var nodeCount = window.gNav.views['tabletViewMovies1'].viewItemsVisible.length;
		
		window.gNav.views['tabletViewMovies1'].domNode.scrollLeft = 0;
		
		var list = window.gNav.views['tabletViewMovies1'].domNode.getElementsByTagName('UL');
		if(list.length>0) {
			list[0].style.width = (nodeWidth * nodeCount) + 'px';	
		}
		
//		window.gNav.views['tabletViewMovies1'].centerInParent();
		window.gNav.views['tabletViewMovies1'].domNode.style.opacity = 1;
		

		// jQuery('div.tabletViewMovies2').hide();
		window.gNav.views['tabletViewMovies2'].clearViewItemsCategoryFilter();	
		window.gNav.views['tabletViewMovies2'].setViewItemsCategoryFilter(this.dataset.viewcategoryfilter);
		
	};
	
}

*/


























