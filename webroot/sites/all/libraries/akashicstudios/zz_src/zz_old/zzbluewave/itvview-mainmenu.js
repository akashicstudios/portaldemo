

var itvMainMenuController = new ItvView('itvViewMainMenu');

itvMainMenuController.viewItemDefaults.tagName = 'LI';
itvMainMenuController.viewItemDefaults.navIdPrefix = itvMainMenuController.cssClass + 'Item';
itvMainMenuController.viewItemDefaults.inFocusCss = itvMainMenuController.cssClass + '-list-item-focus';

itvMainMenuController.html.region = 'menu';
itvMainMenuController.html.position.maxWidth = 280;
itvMainMenuController.html.scroll.direction = 'vertical';

itvMainMenuController.init();

window.gNav.views['itvViewMainMenu'] = itvMainMenuController;
window.gNav.viewsZeroBased.push(itvMainMenuController);

window.gNav.menuView = itvMainMenuController;



function debarkInfoVisible() {
	
	showHideDebarkItem(false);
	
	var urlDtm = '/json/datetime';
	var urlTimePeriod = '/json/timeperiods/ldoc';
	
	jQuery.getJSON(urlDtm,function(obj) {
		if(typeof obj != 'undefined') {
			getTP(obj);
		}
	});		
	
	
	function getTP(dtm){
		jQuery.getJSON(urlTimePeriod,function(obj) {
			if(typeof obj != 'undefined') {
				isValidDtm(dtm,obj);
			}
		});	
	}
	
	
	function isValidDtm(dtm,timeperiod) {
		var retval = false;
		
		// console.log(dtm);
		// console.log(timeperiod);
		
		dow = dtm.w;
		tp = timeperiod[0];
		
		if((parseInt(tp.startDow) <= parseInt(dow)) && (parseInt(dow) <= parseInt(tp.endDow))) {
			// console.log('dow: ' + dow);
			
			retval = true;
			
			// if the dow matches startdow
			if(parseInt(tp.startDow) == parseInt(dow)) {
				
				// console.log('tp.startDow '+tp.startDow);
				// console.log('dow '+dow);
				// console.log('tp.startHh '+tp.startHh);
				// console.log('dtm.G '+ dtm.G);
				
				// check the hour vs start hour
				if(parseInt(tp.startHh) > parseInt(dtm.G)) {
					retval = false;
					// console.log('dtm.G: '+ dtm.G);	
				}
				
			}
			
			// if the dow matches end dow
			if(parseInt(tp.endDow) == parseInt(dow)) {
				
				// console.log('tp.endDow');
				
				// check the hour vs end hour
				if(parseInt(dtm.G) > parseInt(tp.endHh)) {
					retval = false;
					// console.log('dtm.G: '+ dtm.G);	
				}
				
			}
			
		}

		// console.log('1. retval '+retval);
				
		showHideDebarkItem(retval);
		
	}
	

	function showHideDebarkItem(isVisible) {
		
		if(!isVisible) {
			jQuery('li.itvViewMainMenuDebarkInfo').hide('fast');
			window.gNav.views['itvViewMainMenu'].viewItems['itvViewMainMenuItem5'].nav.visible = false;
		}
		else {
			jQuery('li.itvViewMainMenuDebarkInfo').show('fast');
			window.gNav.views['itvViewMainMenu'].viewItems['itvViewMainMenuItem5'].nav.visible = true;			
		}
		
	}
	this.showHideDebarkItem = showHideDebarkItem;
	
}

var dbk = new debarkInfoVisible();
dbk.showHideDebarkItem(false);

delete(dbk);

window.setInterval( function(){ debarkInfoVisible(); }, 1200000);	//1200000 = 20 minutes

























